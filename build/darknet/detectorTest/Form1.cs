﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using detectorbridge;
using System.Runtime.InteropServices;
namespace detectorTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        detectorbridge.Detectorbridge loTest = null;

        void init()
        {
            lock(this)
            {
                if (null == loTest)
                {
                    loTest = new detectorbridge.Detectorbridge(null, null);
                }
            }
        }

        void testInifinit()
        {
            while(true)
            {
                this.testc();
            }
        }

        void testc()
        {
            
            this.init();

          

            Dictionary<  int, bbox_t_csharp> loCheck = new Dictionary<int, bbox_t_csharp>();

           List<bbox_t_csharp> loRet = loTest.detect("./image4.jpg", 0.2f);

            for (int i = 0; i < loRet.Count; i++)
            {
                bbox_t_csharp data = loRet[i];

                System.Diagnostics.Debug.WriteLine(data.toString());

                if(!loCheck.ContainsKey((int)data.x))
                {
                    loCheck.Add((int)data.x, data);
                }

               
            //    this.richTextBox1.Text += data.toString() + "\r\n";
            }


            Byte[] loData = System.IO.File.ReadAllBytes("./image4.jpg");

            List<bbox_t_csharp> loRet2 = new List<bbox_t_csharp>();

            int lnPerson = 0;

            unsafe
            {
                fixed(byte * charPtr = &loData[0])
                {
                     loRet2 = loTest.detect(charPtr,loData.Length,0);
// 
                      lnPerson = loTest.detectPerson(charPtr, loData.Length,0.2f);
                }
            }

            for (int i = 0; i < loRet2.Count; i++)
            {
                bbox_t_csharp data = loRet2[i];

                System.Diagnostics.Debug.WriteLine(data.toString());

               if(!loCheck.ContainsKey((int)data.x))
                 {
                    Console.WriteLine(data.toString());
                }

             //   this.richTextBox2.Text += data.toString() + "\r\n";
            }
            
        }

        

        private void button1_Click(object sender, EventArgs e)
        {
            this.richTextBox1.Text = "";
            this.richTextBox2.Text = "";
            this.init();

            int lnThreadCount = 7;

            for(int i=0;i<lnThreadCount;i++)
            {
                System.Threading.Thread lpThread = new System.Threading.Thread(testInifinit);
                lpThread.IsBackground = true;
                lpThread.IsBackground = true;
                lpThread.Start();
            }
        }
    }
}
