﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace video_process_display_client
{
    public partial class ConfirmWindow :MetroForm
    {
        public ConfirmWindow()
        {
            InitializeComponent();
        }

        private Boolean m_bConfirmed = false;
        public System.Boolean Confirmed
        {
            get { return m_bConfirmed; }
            set { m_bConfirmed = value; }
        }
        private void ConfirmWindow_Load(object sender, EventArgs e)
        {

        }

        private void metroButtonConfirm_Click(object sender, EventArgs e)
        {
            this.Confirmed = true;
            this.Close();
        }

        private void metroButtonCancel_Click(object sender, EventArgs e)
        {
            this.Confirmed = false;
            this.Close();
        }
    }
}
