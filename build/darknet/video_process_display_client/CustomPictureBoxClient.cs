﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using EricZhao.UiThread;
using System.Linq;
using EricZhao;

namespace video_process_display_client
{
    public partial class CustomPictureBoxClient : PictureBox
    {
        public CustomPictureBoxClient()
        {
            InitializeComponent();
            this.SizeMode = PictureBoxSizeMode.StretchImage;
            this.m_oInvoker = new Method_invoker(this.cloneBmp2);
            this.DoubleBuffered = true;
        }

        private void InitializeComponent()
        {
           // throw new NotImplementedException();

        }

        private bool shifted = false;

        private int m_lUserID = -1;
        public int UserID
        {
            get { lock(this)return m_lUserID; }
            set { lock (this) m_lUserID = value; }
        }

        string DVRIPAddress = "192.168.1.64"; //设备IP地址或者域名
        public string IP
        {
            get { lock (this) return DVRIPAddress; }
            set { lock (this) DVRIPAddress = value; }
        }
        Int16 DVRPortNumber = 8000;//设备服务端口号
        public System.Int16 Port
        {
            get { lock (this) return DVRPortNumber; }
            set { lock (this) DVRPortNumber = value; }
        }
        private uint iLastErr = 0;
        private string str = "";

        string DVRUserName = "admin";//设备登录用户名
        public string UserName
        {
            get { lock (this) return DVRUserName; }
            set { lock (this) DVRUserName = value; }
        }
        string DVRPassword = "admin6666";//设备登录密码
        public string Password
        {
            get { lock (this) return DVRPassword; }
            set { lock (this) DVRPassword = value; }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
          /*  if (e.Button == MouseButtons.Left && this.Image != null)
            {
                this.shifted = true;
                this.Invalidate();
            }*/

            base.OnMouseDown(e);
        }

        Bitmap m_oBitMap = null;
        public System.Drawing.Bitmap BitMap
        {
            get { lock (this) return m_oBitMap; }
            set { lock (this) m_oBitMap = value; }
        }
        protected override void OnMouseUp(MouseEventArgs e)
        {
          
            base.OnMouseUp(e);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
           try
            {
                base.OnPaint(e);
            }
            catch(Exception e1)
            {
                System.Diagnostics.Debug.Assert(false);
            }
           

            if (this.m_nDisplayType == DisplayType.all_display_leak)
            {
                using (Font myFont = new Font("Tahoma", 40))
                {
                    e.Graphics.DrawString("疑似泄漏", myFont, Brushes.Yellow, new Point(5, 15));
                    if (!this.Maxed)
                    {
                        Color funnyColor = Color.FromArgb(80, 255, 0, 0);
                        SolidBrush br = new SolidBrush(funnyColor);
                        e.Graphics.FillRectangle(br, this.DisplayRectangle);
                    }
                }
            }
            else if (this.m_nDisplayType == DisplayType.all_display_fire)
            {
                using (Font myFont = new Font("Tahoma", 40))
                {
                    e.Graphics.DrawString("疑似着火", myFont, Brushes.DarkRed, new Point(5, 15));
                    if(!this.Maxed)
                    {
                        Color funnyColor = Color.FromArgb(80, 255, 0, 0);
                        SolidBrush br = new SolidBrush(funnyColor);
                        e.Graphics.FillRectangle(br, this.DisplayRectangle);
                    }

                }
            }
        }


        private Boolean m_bMaxed = false;
        public System.Boolean Maxed
        {
            get { return m_bMaxed; }
            set { m_bMaxed = value; }
        }

        public event EventHandler ImageChanged = null;

        public void OnImageChanged(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Image changed");
        }

        private Bitmap m_oCurrentBitMap = null;
        public System.Drawing.Bitmap CurrentBitMap
        {
            get
            {
                lock (this) return m_oCurrentBitMap;
            }
            set
            {
                if (null == value)
                {
                    return;
                }

                lock (this)
                {
                    if (this.m_oCurrentBitMap != null)
                    {
                        this.CurrentBitMap.Dispose();
                    }

                    m_oCurrentBitMap = value;
                }
            }
        }

        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);
        private void mainthreadUpdatePicture()
        {
           
            if (this.Image != null)
            {
                this.Image.Dispose();
            }
            
            this.Image =this.CurrentBitMap;         
           
        }
        public void MethodCallCaller(ThreadUiController.CallerParameter args)
        {
            if(args!=null && args.in_value!=null)
            {
               try
                {
                    object[] loObjectArray = (object[])args.in_value;
                    if(loObjectArray.Length>0)
                    {
                        Bitmap loTemp = (Bitmap)loObjectArray[0];
                        if(this.Image!=null)
                        {
                            this.Image.Dispose();
                        }

                        this.Image = loTemp;
                    }
                }
                catch(Exception e)
                {

                }
            }
        }
        public void SetPicture2(Bitmap img)
        {
          
           
            if (null == img)
            {
                return;
            }      
           
            if (this.InvokeRequired)
            {
                Bitmap loTemp = (Bitmap)img;
                object[] args = new object[1];
                args[0] = loTemp;
                ThreadUiController.InvokeControlCustomMethod3(this, this.MethodCallCaller, args);
            }
            else
            {
                this.mainthreadUpdatePicture();
            }
        }
      
        public void SetPicture(Bitmap img)
        {
            if (this.InvokeRequired)
            {
                this.Image = img;
                /* this.Image = img;
                                this.Invoke(new MethodInvoker(
                                  delegate ()
                                  {
                                      this.Image = img;
                                  }));*/
                /*Graphics gr = Graphics.FromImage(img);
                IntPtr hdc = gr.GetHdc();
               
                gr.ReleaseHdc(hdc);*/
            }
            else
            {
                this.Image = img;
            }
        }

        int m_nChannelIndex = 0;
        public int ChannelIndex
        {
            get { lock(this)return m_nChannelIndex; }
            set { lock (this) m_nChannelIndex = value; }
        }
        public delegate void Method_invoker(IntPtr hdc);

        public Method_invoker m_oInvoker = null;

        public void cloneBmp2(IntPtr hdc)
        {
            try
            {
                Bitmap bitMap = Bitmap.FromHbitmap(hdc);
                String lstrImagePath = "./xxxxx_" + DateTime.Now.ToFileTime() + "_test.bmp";
                bitMap.Save(lstrImagePath, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            catch(Exception e)
            {
                ThreadUiController.outputDebugString(e.Message);
            }

        }

        public void cloneBmp(IntPtr hdc)
        {
            try
            {
                var ctr = this;
                Image bmp = new Bitmap(ctr.Width, ctr.Height);
                var gg = Graphics.FromImage(bmp);
                var rect = ctr.RectangleToScreen(ctr.ClientRectangle);
                gg.CopyFromScreen(rect.Location, Point.Empty, ctr.Size);
                String lstrImagePath = "./xxxxx_" + DateTime.Now.ToFileTime() + "_test.bmp";
                bmp.Save(lstrImagePath);
            }
            catch (Exception e)
            {
                ThreadUiController.outputDebugString(e.Message);
            }

        }

        public enum DisplayType
        {
            only_alarm = 0,
            only_normal,
            both,
            all_display_leak,
            all_display_fire
        }

        DisplayType m_nDisplayType = 0; // 0:only alarm 1:only normal 2:all
        public DisplayType PicDisplayType
        {
            get { return m_nDisplayType; }
            set { m_nDisplayType = value; }
        }

        System.EventArgs m_e = new EventArgs();
       
    }

    static class Gdi32
    {
        [DllImport("gdi32.dll")]
        public static extern bool BitBlt(IntPtr hdcDest, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc, int nXSrc, int nYSrc, int dwRop);

        [DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
        public static extern IntPtr CreateCompatibleDC(IntPtr hDC);

        [DllImport("gdi32.dll", ExactSpelling = true)]
        public static extern IntPtr SelectObject(IntPtr hDC, IntPtr hObject);

        [DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);
    }
}
