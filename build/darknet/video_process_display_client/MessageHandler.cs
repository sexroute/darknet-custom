﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EricZhao;

namespace video_process_display_client
{
    public class MessageHandler
    {
        Thread m_pThread = null;
        public System.Threading.Thread Thread
        {
            get { return m_pThread; }
            set { m_pThread = value; }
        }

        Thread m_pThread2 = null;
        public System.Threading.Thread Thread2
        {
            get { return m_pThread2; }
            set { m_pThread2 = value; }
        }
        FixedSizedQueue<AlarmImagePacket> m_oMessageQueue = new FixedSizedQueue<AlarmImagePacket>(1400);
        public EricZhao.FixedSizedQueue<EricZhao.AlarmImagePacket> MessageQueue
        {
            get { return m_oMessageQueue; }
            set { m_oMessageQueue = value; }
        }
        public MessageHandler()
        {

        }

        AutoResetEvent m_oAutoEvent = new AutoResetEvent(false);
        public System.Threading.AutoResetEvent AutoEvent
        {
            get { return m_oAutoEvent; }
            set { m_oAutoEvent = value; }
        }
    }
}
