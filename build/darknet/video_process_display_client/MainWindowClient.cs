﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using EricZhao;
using EricZhao.UiThread;
using MetroFramework.Forms;
using RabbitMQ.Client.Events;
/***
 * 流程：1.读取配置，实时更新，也算是心跳
 *       2.读取视频帧，显示报警帧
 *       3.双击放大后，显示原帧，并持续更新
 */
namespace video_process_display_client
{
    public partial class MainWindowClient : MetroForm
    {
        public MainWindowClient()
        {
            InitializeComponent();
        }
        EricZhao.AlarmImageRabbitExchangHandler m_oAlarmMQ = new EricZhao.AlarmImageRabbitExchangHandler(2);
        EricZhao.AlarmImageRabbitExchangHandler m_oNormalMQ = new EricZhao.AlarmImageRabbitExchangHandler(1);
        protected String m_strSettingFile = "./settings.ini";


        String m_strSystemUserName = "admin";
        public System.String SystemUserName
        {
            get { return m_strSystemUserName; }
            set { m_strSystemUserName = value; }
        }
        String m_strSystemPassword = "admin6666";
        public System.String SystemPassword
        {
            get { return m_strSystemPassword; }
            set { m_strSystemPassword = value; }
        }

        String m_strMSQURL = "amqp://guest:guest@127.0.0.1:5672";
        public System.String MSQURL
        {
            get { return m_strMSQURL; }
            set { m_strMSQURL = value; }
        }

        public bool ShouldFullScreen = false;
        public bool PasswordFormShowed = false;

        Boolean m_bShowMaxWindow = false;
        public System.Boolean ShowMaxWindow
        {
            get { return m_bShowMaxWindow; }
            set { m_bShowMaxWindow = value; }
        }
        List<CustomPictureBoxClient> m_oPics = new List<CustomPictureBoxClient>();
        private void initUI()
        {
            this.StyleManager = this.metroStyleManager1;
            this.StyleManager.Style = MetroFramework.MetroColorStyle.Blue;
            this.StyleManager.Theme = MetroFramework.MetroThemeStyle.Light;
          
            foreach(var pb in this.m_oPics)
            {
                MainWindowClient.UnRegistWindow(pb.ChannelIndex, pb);
            }
            this.m_oPics.Clear();
            MainWindowClient.m_oCameras.Clear();
            this.initTableLayout(); 
                      
            foreach (var pb in this.tableLayoutPanel1.Controls.OfType<CustomPictureBoxClient>())
            {
                this.m_oPics.Add(pb);
                pb.PicDisplayType = CustomPictureBoxClient.DisplayType.only_normal;
                MainWindowClient.RegisWindow(pb.ChannelIndex,pb);
            }

        }

        private void QuitApp()
        {

            MiniDump.TerminateProcess();

            try
            {
                if (System.Windows.Forms.Application.MessageLoop)
                {
                    // WinForms app
                    System.Windows.Forms.Application.Exit();
                    System.Environment.Exit(1);
                }
                else
                {
                    // Console app
                    System.Environment.Exit(1);
                }
            }
            catch (Exception e)
            {

            }


        }

        private int calcPicBoxCount(int anInput)
        {
            if (anInput <= 1)
            {
                return 1;
            }

            return anInput;

            if (anInput <= 2)
            {
                return 2;
            }

            if (anInput <= 4)
            {
                return 4;
            }

            if (anInput <= 9)
            {
                return 9;
            }

            if (anInput <= 10)
            {
                return 10;
            }

            if (anInput <= 16)
            {
                return 16;
            }

            if (anInput <= 25)
            {
                return 25;
            }

            return 16;
        }

        private void EnableAlarm(Boolean abEnable, int anConfirmedStatus, int anIndex)
        {
            lock(m_oCameras)
            {
                if( m_oCameras.ContainsKey(anIndex))
                {
                    Dictionary<IntPtr, CustomPictureBoxClient> loPics = m_oCameras[anIndex];
                    if(loPics!=null)
                    {
                        foreach(var pair in loPics)
                        {
                            if(!abEnable)
                            {
                                pair.Value.PicDisplayType = CustomPictureBoxClient.DisplayType.only_normal;
                            }
                            else
                            {
                                if(anConfirmedStatus== 1)
                                {
                                    pair.Value.PicDisplayType = CustomPictureBoxClient.DisplayType.all_display_leak;
                                }else if(anConfirmedStatus == 2)
                                {
                                    pair.Value.PicDisplayType = CustomPictureBoxClient.DisplayType.all_display_fire;
                                }else
                                {
                                    
                                        pair.Value.PicDisplayType = CustomPictureBoxClient.DisplayType.both;
                                    
                                }
                                
                            }
                        }
                    }
                }
            }            
        }


        private void MainWindow_Load(object sender, EventArgs e)
        {
            this.loadSettings();
            this.initUI();
            this.start();
          //  this.startMessageHandleThread();
        }

        private void initReciver()
        {
            m_oConfirmLeakReciver = new AlarmImageRabbitExchangHandler(PacketTypeValues.CONFIRM_LEAK);
            m_oConfirmLeakReciver.URL = this.MSQURL;
            m_oConfirmLeakReciver.m_OnMessageReceived += this.OnMessageReicved;
            m_oConfirmLeakReciver.Read();

            m_oConfirmFireReciver = new AlarmImageRabbitExchangHandler(PacketTypeValues.CONFIRM_FIRE);
            m_oConfirmFireReciver.URL = this.MSQURL;
            m_oConfirmFireReciver.m_OnMessageReceived += this.OnMessageReicved;
            m_oConfirmFireReciver.Read();

            m_oConfirmCancelReciver = new AlarmImageRabbitExchangHandler(PacketTypeValues.CANCEL_ALL_ALARM);
            m_oConfirmCancelReciver.URL = this.MSQURL;
            m_oConfirmCancelReciver.m_OnMessageReceived += this.OnMessageReicved;
            m_oConfirmCancelReciver.Read();
        }
      

        AlarmImageRabbitExchangHandler m_oConfirmLeakReciver = null;
        AlarmImageRabbitExchangHandler m_oConfirmFireReciver = null;
        AlarmImageRabbitExchangHandler m_oConfirmCancelReciver = null;


        private void GoFullscreen(bool fullscreen)
        {
            if (!ShouldFullScreen)
            {
                return;
            }
            if (fullscreen)
            {
                Screen scrn = Screen.FromControl(this);

                if ((this.Bounds != scrn.Bounds))
                {
                    this.WindowState = FormWindowState.Normal;
                    if (this.PasswordFormShowed || this.ShowMaxWindow)
                    {
                        this.TopMost = false;
                    }
                    else
                    {
                        this.TopMost = true;
                    }

                    this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    this.Bounds = Screen.PrimaryScreen.Bounds;
                }

            }
            else
            {
                this.WindowState = FormWindowState.Maximized;
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            }
        }

        private void loadSettings()
        {
           
            IniFile loFile = new IniFile(this.m_strSettingFile);
            int lnTemp = loFile.IniReadIntValue("process_client_global", "window", 10, true);
            String baseDir = loFile.IniReadStringValue("process_client_global", "base_dir", "./pic_data", true);
            String tempDir = loFile.IniReadStringValue("process_client_global", "temp_dir", "./pic_temp", true);
            this.TotalChannels = this.calcPicBoxCount(lnTemp);
            this.SystemUserName = loFile.IniReadStringValue("process_client_global", "system_username", "admin", true);
            this.SystemPassword = loFile.IniReadStringValue("process_client_global", "system_password", "admin6666", true);
            this.ShouldFullScreen = loFile.IniReadBooleanValue("process_client_global", "full_screen", false, true);
            String lstrTitle = loFile.IniReadStringValue("process_client_global", "title", "视频图像识别系统", true);
            String lstrMSQURL = loFile.IniReadStringValue("process_client_global", "message_queue_url", "amqp://guest:guest@127.0.0.1:5672", true);
            this.MSQURL = lstrMSQURL;
            this.Text = lstrTitle;
            if (this.TotalChannels <= 0)
            {
                this.TotalChannels = 1;
            }
          
        }

        static  Dictionary<int, Dictionary<IntPtr, CustomPictureBoxClient>>  m_oCameras
            = new Dictionary<int, Dictionary<IntPtr, CustomPictureBoxClient>>();
        public static System.Collections.Generic.Dictionary<int, 
            System.Collections.Generic.Dictionary<System.IntPtr,
            video_process_display_client.CustomPictureBoxClient>> Cameras
        {
            get { return m_oCameras; }
            set { m_oCameras = value; }
        }

        public static void RegisWindow(int anIndex,CustomPictureBoxClient apBox)
        {
            if( null == apBox)
            {
                return;
            }
            if (m_oCameras.ContainsKey(anIndex))
            {
                Dictionary<IntPtr, CustomPictureBoxClient> loWindows = Cameras[anIndex];
                if (loWindows == null)
                {
                    loWindows = new Dictionary<IntPtr, CustomPictureBoxClient>();
                }
                if (!loWindows.ContainsKey(apBox.Handle))
                {
                    loWindows.Add(apBox.Handle, apBox);
                }

            }else
            {
                Dictionary<IntPtr, CustomPictureBoxClient> loWindows = new Dictionary<IntPtr, CustomPictureBoxClient>();
                loWindows.Add(apBox.Handle, apBox);
                m_oCameras.Add(anIndex, loWindows);
            }
        }

        public static void UnRegistWindow(int anIndex, CustomPictureBoxClient apBox)
        {
            if ( null == apBox)
            {
                return;
            }

            if (Cameras.ContainsKey(anIndex))
            {
                Dictionary<IntPtr, CustomPictureBoxClient> loWindows = Cameras[anIndex];
                if (loWindows != null)
                {
                    if (loWindows.ContainsKey(apBox.Handle))
                    {
                        loWindows.Remove(apBox.Handle);
                    }
                }
            }
        }
        private void OnPic_DoubleClick(object sender, EventArgs e)
        {
            if (sender.GetType() == typeof(CustomPictureBoxClient))
            {
                int lnIndex = -1;
                Image lpImage = null;
                foreach (var pb in m_oPics)
                {
                    if (pb == sender)
                    {
                        lnIndex = pb.ChannelIndex;
                        lpImage = pb.Image;
                        break;
                    }
                }

                if(lnIndex>=0)
                {
                    this.TopMost = false;
                    ShowMaxWindow = true;
                    MaxCemraClient MaxWindow = new MaxCemraClient();
                    MaxWindow.initPicturebox(lpImage);
                    MaxWindow.ChannIndex = lnIndex;
                    MaxWindow.MSG_URL = this.MSQURL;
                    MaxWindow.WindowState = FormWindowState.Maximized;
                    MaxWindow.Show();
                }
            }
        }

        private int calcColumnCount(int anInput)
        {
            if (anInput <= 1)
            {
                return 1;
            }

            if (anInput <= 2)
            {
                return 2;
            }

            if (anInput <= 4)
            {
                return 2;
            }

            if (anInput <= 9)
            {
                return 3;
            }

            if (anInput <= 10)
            {
                return 5;
            }

            if (anInput <= 16)
            {
                return 4;
            }

            if (anInput <= 25)
            {
                return 5;
            }

            if (anInput <= 48)
            {
                return 8;
            }

            return 4;
        }
        public  void RemoveRow( TableLayoutPanel panel, int rowIndex)
        {
            panel.RowStyles.RemoveAt(rowIndex);

            for (int columnIndex = 0; columnIndex < panel.ColumnCount; columnIndex++)
            {
                var control = panel.GetControlFromPosition(columnIndex, rowIndex);
                if (null != control)
                {
                    panel.Controls.Remove(control);
                }
                    
            }

            for (int i = rowIndex + 1; i < panel.RowCount; i++)
            {
                for (int columnIndex = 0; columnIndex < panel.ColumnCount; columnIndex++)
                {
                    var control = panel.GetControlFromPosition(columnIndex, i);
                    if(null!=control)
                    {
                        panel.SetRow(control, i - 1);
                    }
                   
                }
            }

            panel.RowCount--;
        }
        private void initTableLayout()
        {
            for(int i=0;i<this.tableLayoutPanel1.RowCount;i++)
            {
                this.RemoveRow(this.tableLayoutPanel1, i);
            }
            this.tableLayoutPanel1.Controls.Clear();
           
            this.tableLayoutPanel1.RowStyles.Clear();

            // this.tableLayoutPanel1.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.Dock = DockStyle.Fill;
            // this.tableLayoutPanel1.Padding = GetCorrectionPadding(tableLayoutPanel1, 5);

            int lnPicBox = this.TotalChannels;

            TableLayoutPanel panel = this.tableLayoutPanel1;

            int lnColumn = this.calcColumnCount(lnPicBox);
            int lnRowCount = lnPicBox / lnColumn;
            panel.ColumnCount = lnColumn;

            int lnColPercent = (int)(1 / (lnColumn * 1.0) * 100.0);
            int lnRowPercent = (int)(1 / (lnRowCount * 1.0) * 100.0);

            panel.ColumnStyles.Clear();

            for (int i = 0; i < lnColumn; i++)
            {
                panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, lnColPercent));
            }

            for (int i = 0; i < lnRowCount; i++)
            {
                panel.RowStyles.Add(new RowStyle(SizeType.Percent, lnRowPercent));
            }
            int lnChannIndex = 0;
            for (int i = 0; i < lnRowCount; i++)
            {
                for (int j = 0; j < lnColumn; j++)
                {

                    CustomPictureBoxClient pb = new CustomPictureBoxClient();                    
                    pb.BackColor = Color.Black;
                    pb.Dock = DockStyle.Fill;
                    pb.Name = "index:" + i + "" + j + "1";
                    pb.Margin = new Padding(0);
                    pb.ChannelIndex = lnChannIndex;
                   
                    lnChannIndex++;
                    pb.DoubleClick += new System.EventHandler(this.OnPic_DoubleClick);             

                    panel.Controls.Add(pb, j, i);
                }
            }

            panel.ResumeLayout();
        }

        private void toggleUI(Boolean abStart)
        {
            this.btnStart.Enabled = !abStart;
            this.btnStop.Enabled = abStart;
        }

        private void start()
        {
            m_oAlarmMQ.m_OnMessageReceived += this.OnMessageReicved;
            m_oAlarmMQ.URL = this.MSQURL;
            m_oAlarmMQ.Read();


            m_oNormalMQ.m_OnMessageReceived += this.OnNormalMessageReicved;
            m_oNormalMQ.URL = this.MSQURL;
            m_oNormalMQ.Read();

            this.toggleUI(true);
        }

        int m_nTotalChannels = 0;
        public int TotalChannels
        {
            get { lock(this) return m_nTotalChannels; }
            set { lock (this) m_nTotalChannels = value; }
        }

        Boolean m_bNeedUpdateUI = false;
        public System.Boolean NeedUpdateUI
        {
            get { lock (this) return m_bNeedUpdateUI; }
            set { lock (this) m_bNeedUpdateUI = value; }
        }
        DateTime m_oLastHeartBeatTime = new DateTime();
        public System.DateTime LastHeartBeatTime
        {
            get { lock (this) return m_oLastHeartBeatTime; }
            set { lock (this) m_oLastHeartBeatTime = value; }
        }

        public void OnNormalMessageReicved_thread(object sender, BasicDeliverEventArgs e)
        {

            if (e != null)
            {
                try
                {
                    var body = e.Body;
                    AlarmImagePacket loPacket = AlarmImagePacket.toObject(body);

                    switch (loPacket.PacketType)
                    {
                        case PacketTypeValues.CONFIG:
                            {
                                int lnTotalChannels = loPacket.Channels;
                                if (lnTotalChannels != this.TotalChannels)
                                {
                                    this.TotalChannels = lnTotalChannels;
                                    this.NeedUpdateUI = true;
                                }
                            }
                            break;
                        case PacketTypeValues.HEART_BEAT:
                            {
                                this.LastHeartBeatTime = loPacket.DateTime;
                            }
                            break;

                        case PacketTypeValues.ALARM_IMAGE_DATA:

                        case PacketTypeValues.NORMAL_DATA:
                            {
                                int lnIndex = loPacket.VideoChannelIndex;
                                MessageHandler lpHandler = this.m_oMessageNormalHandleThread[lnIndex];
                                lpHandler.MessageQueue.Enqueue(loPacket);
                                lpHandler.AutoEvent.Set();
                            }
                            break;

                    }


                }
                catch (Exception ex)
                {

                }

            }
        }
        public void OnNormalMessageReicved(object sender, BasicDeliverEventArgs e)
        {

            if (e != null)
            {
                try
                {
                    var body = e.Body;
                    AlarmImagePacket loPacket = AlarmImagePacket.toObject(body);

                    switch (loPacket.PacketType)
                    {
                        case PacketTypeValues.CONFIG:
                            {
                                int lnTotalChannels = loPacket.Channels;
                                if (lnTotalChannels != this.TotalChannels)
                                {
                                    this.TotalChannels = lnTotalChannels;
                                    this.NeedUpdateUI = true;
                                }
                            }
                            break;
                        case PacketTypeValues.HEART_BEAT:
                            {
                                this.LastHeartBeatTime = loPacket.DateTime;
                            }
                            break;

                        case PacketTypeValues.ALARM_IMAGE_DATA:
                            {
                                int lnTotalChannels = loPacket.Channels;
                                if (lnTotalChannels != this.TotalChannels)
                                {
                                    this.TotalChannels = lnTotalChannels;
                                    this.NeedUpdateUI = true;
                                }

                                int lnChannIndex = loPacket.VideoChannelIndex;

                                List<CustomPictureBoxClient> loAlarms = new List<CustomPictureBoxClient>();
                                List<CustomPictureBoxClient> loNormals = new List<CustomPictureBoxClient>();

                                if (MainWindowClient.Cameras.ContainsKey(lnChannIndex))
                                {
                                    Dictionary<IntPtr, CustomPictureBoxClient> loDict = MainWindowClient.Cameras[lnChannIndex];
                                    if (null != loDict)
                                    {
                                        foreach (KeyValuePair<IntPtr, CustomPictureBoxClient> lpIt in loDict)
                                        {
                                            if (lpIt.Value != null)
                                            {
                                                if (lpIt.Value.PicDisplayType == CustomPictureBoxClient.DisplayType.only_normal)
                                                {
                                                    loNormals.Add(lpIt.Value);
                                                }
                                                else if (lpIt.Value.PicDisplayType == CustomPictureBoxClient.DisplayType.only_alarm)
                                                {
                                                    loAlarms.Add(lpIt.Value);
                                                }
                                                else
                                                {
                                                    loNormals.Add(lpIt.Value);
                                                    loAlarms.Add(lpIt.Value);
                                                }
                                            }
                                        }
                                    }
                                }


                                if (loPacket.DetectImage != null && loPacket.DetectImage.Length > 0)
                                {
                                    using (Stream stream = new MemoryStream(loPacket.DetectImage))
                                    {
                                        /*   Bitmap loBitMap = new Bitmap(stream);
                                           Bitmap loTemp = (Bitmap)loBitMap.Clone();
                                           foreach (CustomPictureBoxClient pic in loAlarms)
                                           {
                                               pic.SetPicture(loTemp);
                                           }

                                          loBitMap.Dispose();*/
                                    }
                                }

                                if (loPacket.OriginalImage != null && loPacket.OriginalImage.Length > 0)
                                {
                                    using (Stream stream = new MemoryStream(loPacket.OriginalImage))
                                    {
                                        /* Bitmap loBitMap = new Bitmap(stream);
                                         Bitmap loTemp = (Bitmap)loBitMap.Clone();
                                         foreach (CustomPictureBoxClient pic in loNormals)
                                         {
                                             pic.SetPicture(loTemp);
                                         }
                                        loBitMap.Dispose();*/
                                    }
                                }
                            }
                            break;
                        case PacketTypeValues.NORMAL_DATA:
                            {
                                int lnTotalChannels = loPacket.Channels;
                                if (lnTotalChannels != this.TotalChannels || this.m_oPics.Count != this.TotalChannels)
                                {
                                    this.TotalChannels = lnTotalChannels;
                                    this.NeedUpdateUI = true;
                                }

                                if (loPacket.OriginalImage != null && loPacket.OriginalImage.Length > 0)
                                {

                                    List<CustomPictureBoxClient> loAlarms = new List<CustomPictureBoxClient>();
                                    List<CustomPictureBoxClient> loNormals = new List<CustomPictureBoxClient>();
                                    int lnChannIndex = loPacket.VideoChannelIndex;
                                    if (MainWindowClient.Cameras.ContainsKey(lnChannIndex))
                                    {
                                        Dictionary<IntPtr, CustomPictureBoxClient> loDict = MainWindowClient.Cameras[lnChannIndex];
                                        if (null != loDict)
                                        {
                                            foreach (KeyValuePair<IntPtr, CustomPictureBoxClient> lpIt in loDict)
                                            {

                                                if (lpIt.Value != null)
                                                {
                                                    if (lpIt.Value.PicDisplayType == CustomPictureBoxClient.DisplayType.only_normal)
                                                    {
                                                        loNormals.Add(lpIt.Value);
                                                    }
                                                    else if (lpIt.Value.PicDisplayType == CustomPictureBoxClient.DisplayType.only_alarm)
                                                    {
                                                        loAlarms.Add(lpIt.Value);
                                                    }
                                                    else
                                                    {
                                                        loNormals.Add(lpIt.Value);
                                                        loAlarms.Add(lpIt.Value);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    Stream stream = new MemoryStream(loPacket.OriginalImage);
                                    {
                                        Bitmap loBitMap = new Bitmap(stream);

                                        foreach (CustomPictureBoxClient pic in loNormals)
                                        {
                                            pic.SetPicture(loBitMap);
                                        }
                                        //loBitMap.Dispose();
                                    }
                                    stream.Close();

                                    loNormals.Clear();
                                    loAlarms.Clear();
                                }
                            }
                            break;

                    }


                }
                catch (Exception ex)
                {

                }

            }
        }
        List<MessageHandler> m_oMessageHandleThread = new List<MessageHandler>();
        List<MessageHandler> m_oMessageNormalHandleThread = new List<MessageHandler>();

        
        public void startMessageHandleThread()
        {
            if(this.m_oMessageHandleThread.Count==0)
            {                
                for(int i=0;i<12;i++)
                {
                    {
                        MessageHandler loHandler = new MessageHandler();
                        loHandler.Thread = new System.Threading.Thread(this.MessageHandlerThreadEntry);
                        loHandler.Thread.IsBackground = true;
                        loHandler.Thread.Start(loHandler);
                        

//                         loHandler.Thread2 = new System.Threading.Thread(this.MessageHandlerThreadEntry);
//                         loHandler.Thread2.IsBackground = true;
//                         loHandler.Thread2.Start(loHandler);
                        this.m_oMessageHandleThread.Add(loHandler);
                    }             

                }


                for (int i = 0; i < 12; i++)
                {                  

                    {
                        MessageHandler loHandler = new MessageHandler();
                        loHandler.Thread = new System.Threading.Thread(this.NormalMessageHandlerThreadEntry);
                        loHandler.Thread.IsBackground = true;
                        loHandler.Thread.Start(loHandler);

//                         loHandler.Thread2 = new System.Threading.Thread(this.NormalMessageHandlerThreadEntry);
//                         loHandler.Thread2.IsBackground = true;
//                         loHandler.Thread2.Start(loHandler);

                        this.m_oMessageNormalHandleThread.Add(loHandler);
                        
                    }
                }

            }
        }

        public void HandleNormalMessage(MessageHandler loHandler)
        {
            AlarmImagePacket loPacket = null;
            loHandler.MessageQueue.dequeue(out loPacket);
            if (loPacket != null)
            {
                try
                {
                  
                    switch (loPacket.PacketType)
                    {
                        case PacketTypeValues.CONFIG:
                            {
                                int lnTotalChannels = loPacket.Channels;
                                if (lnTotalChannels != this.TotalChannels)
                                {
                                    this.TotalChannels = lnTotalChannels;
                                    this.NeedUpdateUI = true;
                                }
                            }
                            break;
                        case PacketTypeValues.HEART_BEAT:
                            {
                                this.LastHeartBeatTime = loPacket.DateTime;
                            }
                            break;

                        case PacketTypeValues.ALARM_IMAGE_DATA:
                            {
                                int lnTotalChannels = loPacket.Channels;
                                if (lnTotalChannels != this.TotalChannels)
                                {
                                    this.TotalChannels = lnTotalChannels;
                                    this.NeedUpdateUI = true;
                                }

                                int lnChannIndex = loPacket.VideoChannelIndex;

                                List<CustomPictureBoxClient> loAlarms = new List<CustomPictureBoxClient>();
                                List<CustomPictureBoxClient> loNormals = new List<CustomPictureBoxClient>();

                                if (MainWindowClient.Cameras.ContainsKey(lnChannIndex))
                                {
                                    Dictionary<IntPtr, CustomPictureBoxClient> loDict = MainWindowClient.Cameras[lnChannIndex];
                                    if (null != loDict)
                                    {
                                        foreach (KeyValuePair<IntPtr, CustomPictureBoxClient> lpIt in loDict)
                                        {
                                            if (lpIt.Value != null)
                                            {
                                                if (lpIt.Value.PicDisplayType == CustomPictureBoxClient.DisplayType.only_normal)
                                                {
                                                    loNormals.Add(lpIt.Value);
                                                }
                                                else if (lpIt.Value.PicDisplayType == CustomPictureBoxClient.DisplayType.only_alarm)
                                                {
                                                    loAlarms.Add(lpIt.Value);
                                                }
                                                else
                                                {
                                                    loNormals.Add(lpIt.Value);
                                                    loAlarms.Add(lpIt.Value);
                                                }
                                            }
                                        }
                                    }
                                }


                                if (loPacket.DetectImage != null && loPacket.DetectImage.Length > 0)
                                {
                                    using (Stream stream = new MemoryStream(loPacket.DetectImage))
                                    {
                                        /*   Bitmap loBitMap = new Bitmap(stream);
                                           Bitmap loTemp = (Bitmap)loBitMap.Clone();
                                           foreach (CustomPictureBoxClient pic in loAlarms)
                                           {
                                               pic.SetPicture(loTemp);
                                           }

                                          loBitMap.Dispose();*/
                                    }
                                }

                                if (loPacket.OriginalImage != null && loPacket.OriginalImage.Length > 0)
                                {
                                    using (Stream stream = new MemoryStream(loPacket.OriginalImage))
                                    {
                                        /* Bitmap loBitMap = new Bitmap(stream);
                                         Bitmap loTemp = (Bitmap)loBitMap.Clone();
                                         foreach (CustomPictureBoxClient pic in loNormals)
                                         {
                                             pic.SetPicture(loTemp);
                                         }
                                        loBitMap.Dispose();*/
                                    }
                                }
                            }
                            break;
                        case PacketTypeValues.NORMAL_DATA:
                            {
                                int lnTotalChannels = loPacket.Channels;
                                if (lnTotalChannels != this.TotalChannels)
                                {
                                    this.TotalChannels = lnTotalChannels;
                                    this.NeedUpdateUI = true;
                                }

                                if (loPacket.OriginalImage != null && loPacket.OriginalImage.Length > 0)
                                {

                                    List<CustomPictureBoxClient> loAlarms = new List<CustomPictureBoxClient>();
                                    List<CustomPictureBoxClient> loNormals = new List<CustomPictureBoxClient>();
                                    int lnChannIndex = loPacket.VideoChannelIndex;
                                    if (MainWindowClient.Cameras.ContainsKey(lnChannIndex))
                                    {
                                        Dictionary<IntPtr, CustomPictureBoxClient> loDict = MainWindowClient.Cameras[lnChannIndex];
                                        if (null != loDict)
                                        {
                                            foreach (KeyValuePair<IntPtr, CustomPictureBoxClient> lpIt in loDict)
                                            {

                                                if (lpIt.Value != null)
                                                {
                                                    if (lpIt.Value.PicDisplayType == CustomPictureBoxClient.DisplayType.only_normal)
                                                    {
                                                        loNormals.Add(lpIt.Value);
                                                    }
                                                    else if (lpIt.Value.PicDisplayType == CustomPictureBoxClient.DisplayType.only_alarm)
                                                    {
                                                        loAlarms.Add(lpIt.Value);
                                                    }
                                                    else
                                                    {
                                                        loNormals.Add(lpIt.Value);
                                                        loAlarms.Add(lpIt.Value);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    Stream stream = new MemoryStream(loPacket.OriginalImage);
                                    {
                                        Bitmap loBitMap = new Bitmap(stream);

                                        foreach (CustomPictureBoxClient pic in loNormals)
                                        {
                                            pic.SetPicture(loBitMap);
                                        }
                                        //loBitMap.Dispose();
                                    }
                                    stream.Close();

                                    loNormals.Clear();
                                    loAlarms.Clear();
                                }
                            }
                            break;

                    }


                }
                catch (Exception ex)
                {

                }

            }
        }
        public void HandleMeasage(MessageHandler loHandler)
        {
            AlarmImagePacket loPacket = null;
            loHandler.MessageQueue.dequeue(out loPacket);
            if(loPacket!=null)
            {
                switch (loPacket.PacketType)
                {
                    case PacketTypeValues.CONFIRM_FIRE:
                    case PacketTypeValues.CONFIRM_LEAK:
                        {
                            int lnConfirmedStatus = loPacket.LeakMode;
                            int lnIndex = loPacket.VideoChannelIndex;
                            this.EnableAlarm(true, lnConfirmedStatus, lnIndex);
                        }
                        break;
                    case PacketTypeValues.CANCEL_ALL_ALARM:
                        {
                            int lnIndex = loPacket.VideoChannelIndex;
                            this.EnableAlarm(false, 0, lnIndex);
                        }
                        break;

                    case PacketTypeValues.NOTIFY_STATUS:
                        {
                            int lnIndex = loPacket.VideoChannelIndex;
                            int lnStatus = loPacket.Status;
                            int lnConfirmedStatus = loPacket.LeakMode;
                            if (lnStatus == 0)
                            {
                                this.EnableAlarm(true, lnConfirmedStatus, lnIndex);
                            }
                            else
                            {
                                this.EnableAlarm(false, 0, lnIndex);
                            }

                        }
                        break;

                    case PacketTypeValues.CONFIG:
                        {
                            int lnTotalChannels = loPacket.Channels;
                            if (lnTotalChannels != this.TotalChannels)
                            {
                                this.TotalChannels = lnTotalChannels;
                                this.NeedUpdateUI = true;
                            }
                        }
                        break;
                    case PacketTypeValues.HEART_BEAT:
                        {
                            this.LastHeartBeatTime = loPacket.DateTime;
                        }
                        break;

                    case PacketTypeValues.ALARM_IMAGE_DATA:
                        {
                            int lnTotalChannels = loPacket.Channels;
                            if (lnTotalChannels != this.TotalChannels)
                            {
                                this.TotalChannels = lnTotalChannels;
                                this.NeedUpdateUI = true;
                            }

                            int lnChannIndex = loPacket.VideoChannelIndex;

                            List<CustomPictureBoxClient> loAlarms = new List<CustomPictureBoxClient>();
                            List<CustomPictureBoxClient> loNormals = new List<CustomPictureBoxClient>();

                            if (MainWindowClient.Cameras.ContainsKey(lnChannIndex))
                            {
                                Dictionary<IntPtr, CustomPictureBoxClient> loDict = MainWindowClient.Cameras[lnChannIndex];
                                if (null != loDict)
                                {
                                    foreach (KeyValuePair<IntPtr, CustomPictureBoxClient> lpIt in loDict)
                                    {
                                        if (lpIt.Value != null)
                                        {

                                            if (lpIt.Value != null)
                                            {
                                                if (lpIt.Value.PicDisplayType == CustomPictureBoxClient.DisplayType.only_normal)
                                                {
                                                     loNormals.Add(lpIt.Value);
                                                }
                                                else if (lpIt.Value.PicDisplayType == CustomPictureBoxClient.DisplayType.only_alarm)
                                                {
                                                    loAlarms.Add(lpIt.Value);
                                                }
                                                else
                                                {
                                                    loNormals.Add(lpIt.Value);
                                                    loAlarms.Add(lpIt.Value);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if (loPacket.OriginalImage != null && loPacket.OriginalImage.Length > 0)
                            {
                                Stream stream = new MemoryStream(loPacket.OriginalImage);
                                {

                                    Bitmap loBitMap = new Bitmap(stream);
                                    foreach (CustomPictureBoxClient pic in loNormals)
                                    {
                                        pic.SetPicture(loBitMap);
                                    }
                                    // loBitMap.Dispose();
                                }
                                stream.Close();
                            }

                            if (loPacket.DetectImage != null && loPacket.DetectImage.Length > 0)
                            {
                                Stream stream = new MemoryStream(loPacket.DetectImage);
                                {

                                    Bitmap loBitMap = new Bitmap(stream);
                                    foreach (CustomPictureBoxClient pic in loAlarms)
                                    {
                                        pic.SetPicture(loBitMap);
                                    }
                                    //  loBitMap.Dispose();
                                }
                                stream.Close();
                            }


                        }
                        break;
                    case PacketTypeValues.NORMAL_DATA:
                        {
                            if (loPacket.OriginalImage != null && loPacket.OriginalImage.Length > 0)
                            {

                                List<CustomPictureBoxClient> loAlarms = new List<CustomPictureBoxClient>();
                                List<CustomPictureBoxClient> loNormals = new List<CustomPictureBoxClient>();
                                int lnChannIndex = loPacket.VideoChannelIndex;
                                if (MainWindowClient.Cameras.ContainsKey(lnChannIndex))
                                {
                                    Dictionary<IntPtr, CustomPictureBoxClient> loDict = MainWindowClient.Cameras[lnChannIndex];
                                    if (null != loDict)
                                    {
                                        foreach (KeyValuePair<IntPtr, CustomPictureBoxClient> lpIt in loDict)
                                        {
                                            if (lpIt.Value != null)
                                            {

                                                if (lpIt.Value != null)
                                                {
                                                    if (lpIt.Value.PicDisplayType == CustomPictureBoxClient.DisplayType.only_normal)
                                                    {
                                                        loNormals.Add(lpIt.Value);
                                                    }
                                                    else if (lpIt.Value.PicDisplayType == CustomPictureBoxClient.DisplayType.only_alarm)
                                                    {
                                                        loAlarms.Add(lpIt.Value);
                                                    }
                                                    else
                                                    {
                                                        loNormals.Add(lpIt.Value);
                                                        loAlarms.Add(lpIt.Value);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                Stream stream = new MemoryStream(loPacket.OriginalImage);
                                {

                                    Bitmap loBitMap = new Bitmap(stream);
                                    foreach (CustomPictureBoxClient pic in loNormals)
                                    {
                                        pic.SetPicture(loBitMap);
                                    }

                                    //   loBitMap.Dispose();
                                }

                                stream.Close();
                            }
                        }
                        break;

                }

            
            }
        }

        AutoResetEvent autoEvent = new AutoResetEvent(false);
        AutoResetEvent autoNormalEvent = new AutoResetEvent(false);
        EricZhao.FixedSizedQueue<AlarmImagePacket> m_oMessageQueue = new FixedSizedQueue<AlarmImagePacket>(1400);
        EricZhao.FixedSizedQueue<AlarmImagePacket> m_oNormalMessageQueue = new FixedSizedQueue<AlarmImagePacket>(1400);
        public void MessageHandlerThreadEntry(object aoEvent)
        {
            if(aoEvent == null)
            {
                return;
            }

            MessageHandler lpHandler = (MessageHandler)aoEvent;
            while(true)
            {
               try
                {
                    this.HandleMeasage(lpHandler);
                }catch(Exception e)
                {

                }
                lpHandler.AutoEvent.WaitOne();
            }
        }

        public void NormalMessageHandlerThreadEntry(object aoEvent)
        {
            if (aoEvent == null)
            {
                return;
            }

            MessageHandler lpHandler = (MessageHandler)aoEvent;

            while (true)
            {
                try
                {
                    this.HandleNormalMessage(lpHandler);
                }
                catch (Exception e)
                {

                }
                lpHandler.AutoEvent.WaitOne();
            }
        }

        public void OnMessageReicved(object sender, BasicDeliverEventArgs e)
        {
           
            if (e != null)
            {
                try
                {
                    var body = e.Body;
                    AlarmImagePacket loPacket = AlarmImagePacket.toObject(body);
                    switch (loPacket.PacketType)
                    {
                        case PacketTypeValues.CONFIRM_FIRE:
                        case PacketTypeValues.CONFIRM_LEAK:
                            {
                                int lnConfirmedStatus = loPacket.LeakMode;
                                int lnIndex = loPacket.VideoChannelIndex;
                                this.EnableAlarm(true, lnConfirmedStatus, lnIndex);
                            }
                            break;
                        case PacketTypeValues.CANCEL_ALL_ALARM:
                            {
                                int lnIndex = loPacket.VideoChannelIndex;
                                this.EnableAlarm(false, 0, lnIndex);
                            }
                            break;

                        case PacketTypeValues.NOTIFY_STATUS:
                            {
                                int lnIndex = loPacket.VideoChannelIndex;
                                int lnStatus = loPacket.Status;
                                int lnConfirmedStatus = loPacket.LeakMode;
                                if (lnStatus == 0)
                                {
                                    this.EnableAlarm(true, lnConfirmedStatus, lnIndex);
                                }
                                else
                                {
                                    this.EnableAlarm(false, 0, lnIndex);
                                }

                            }
                            break;

                        case PacketTypeValues.CONFIG:
                            {
                                int lnTotalChannels = loPacket.Channels;
                                if (lnTotalChannels != this.TotalChannels)
                                {
                                    this.TotalChannels = lnTotalChannels;
                                    this.NeedUpdateUI = true;
                                }
                            }
                            break;
                        case PacketTypeValues.HEART_BEAT:
                            {
                                this.LastHeartBeatTime = loPacket.DateTime;
                            }
                            break;

                        case PacketTypeValues.ALARM_IMAGE_DATA:
                            {
                                int lnTotalChannels = loPacket.Channels;
                                if (lnTotalChannels != this.TotalChannels || this.m_oPics.Count!= this.TotalChannels)
                                {
                                    this.TotalChannels = lnTotalChannels;
                                    this.NeedUpdateUI = true;
                                }

                                int lnChannIndex = loPacket.VideoChannelIndex;

                                List<CustomPictureBoxClient> loAlarms = new List<CustomPictureBoxClient>();
                                List<CustomPictureBoxClient> loNormals = new List<CustomPictureBoxClient>();

                                if (MainWindowClient.Cameras.ContainsKey(lnChannIndex))
                                {
                                    Dictionary<IntPtr, CustomPictureBoxClient> loDict = MainWindowClient.Cameras[lnChannIndex];
                                    if (null != loDict)
                                    {
                                        foreach (KeyValuePair<IntPtr, CustomPictureBoxClient> lpIt in loDict)
                                        {
                                            if (lpIt.Value != null)
                                            {

                                                if (lpIt.Value != null)
                                                {
                                                    if (lpIt.Value.PicDisplayType == CustomPictureBoxClient.DisplayType.only_normal)
                                                    {
                                                        loNormals.Add(lpIt.Value);
                                                    }
                                                    else if (lpIt.Value.PicDisplayType == CustomPictureBoxClient.DisplayType.only_alarm)
                                                    {
                                                        //loNormals.Add(lpIt.Value);
                                                       // loAlarms.Add(lpIt.Value);
                                                    }
                                                    else
                                                    {
                                                        loNormals.Add(lpIt.Value);
                                                        loAlarms.Add(lpIt.Value);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (loPacket.OriginalImage != null && loPacket.OriginalImage.Length > 0)
                                {
                                    Stream stream = new MemoryStream(loPacket.OriginalImage);
                                    {

                                        Bitmap loBitMap = new Bitmap(stream);
                                        foreach (CustomPictureBoxClient pic in loNormals)
                                        {
                                            pic.SetPicture(loBitMap);
                                        }
                                        // loBitMap.Dispose();
                                    }
                                    stream.Close();
                                }

                                if (loPacket.DetectImage != null && loPacket.DetectImage.Length > 0)
                                {
                                    Stream stream = new MemoryStream(loPacket.DetectImage);
                                    {

                                      /*  Bitmap loBitMap = new Bitmap(stream);
                                        foreach (CustomPictureBoxClient pic in loAlarms)
                                        {
                                            pic.SetPicture(loBitMap);
                                        }*/
                                        //  loBitMap.Dispose();
                                    }
                                    stream.Close();
                                }


                            }
                            break;
                        case PacketTypeValues.NORMAL_DATA:
                            {
                                if (loPacket.OriginalImage != null && loPacket.OriginalImage.Length > 0)
                                {

                                    List<CustomPictureBoxClient> loAlarms = new List<CustomPictureBoxClient>();
                                    List<CustomPictureBoxClient> loNormals = new List<CustomPictureBoxClient>();
                                    int lnChannIndex = loPacket.VideoChannelIndex;
                                    if (MainWindowClient.Cameras.ContainsKey(lnChannIndex))
                                    {
                                        Dictionary<IntPtr, CustomPictureBoxClient> loDict = MainWindowClient.Cameras[lnChannIndex];
                                        if (null != loDict)
                                        {
                                            foreach (KeyValuePair<IntPtr, CustomPictureBoxClient> lpIt in loDict)
                                            {
                                                if (lpIt.Value != null)
                                                {

                                                    if (lpIt.Value != null)
                                                    {
                                                        if (lpIt.Value.PicDisplayType == CustomPictureBoxClient.DisplayType.only_normal)
                                                        {
                                                            loNormals.Add(lpIt.Value);
                                                        }
                                                        else if (lpIt.Value.PicDisplayType == CustomPictureBoxClient.DisplayType.only_alarm)
                                                        {
                                                            loAlarms.Add(lpIt.Value);
                                                        }
                                                        else
                                                        {
                                                            loNormals.Add(lpIt.Value);
                                                            loAlarms.Add(lpIt.Value);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    Stream stream = new MemoryStream(loPacket.OriginalImage);
                                    {

                                        Bitmap loBitMap = new Bitmap(stream);
                                        foreach (CustomPictureBoxClient pic in loNormals)
                                        {
                                            pic.SetPicture(loBitMap);
                                        }

                                        //   loBitMap.Dispose();
                                    }

                                    stream.Close();
                                }
                            }
                            break;
                            /* this.m_oMessageQueue.Enqueue(loPacket);
                             autoEvent.Set();*/
                          /*  {
                               
                                int lnIndex = loPacket.VideoChannelIndex;
                                MessageHandler lpHandler = this.m_oMessageHandleThread[lnIndex];
                                lpHandler.MessageQueue.Enqueue(loPacket);
                                lpHandler.AutoEvent.Set();
                            }
                            break;  */

                    }             
                }
                catch (Exception ex)
                {

                }

               
            }
        }


        public void OnMessageReicved_thread(object sender, BasicDeliverEventArgs e)
        {

            if (e != null)
            {
                try
                {
                    var body = e.Body;
                    AlarmImagePacket loPacket = AlarmImagePacket.toObject(body);
                    switch (loPacket.PacketType)
                    {
                        case PacketTypeValues.CONFIRM_FIRE:
                        case PacketTypeValues.CONFIRM_LEAK:
                            {
                                int lnConfirmedStatus = loPacket.LeakMode;
                                int lnIndex = loPacket.VideoChannelIndex;
                                this.EnableAlarm(true, lnConfirmedStatus, lnIndex);
                            }
                            break;
                        case PacketTypeValues.CANCEL_ALL_ALARM:
                            {
                                int lnIndex = loPacket.VideoChannelIndex;
                                this.EnableAlarm(false, 0, lnIndex);
                            }
                            break;

                        case PacketTypeValues.NOTIFY_STATUS:
                            {
                                int lnIndex = loPacket.VideoChannelIndex;
                                int lnStatus = loPacket.Status;
                                int lnConfirmedStatus = loPacket.LeakMode;
                                if (lnStatus == 0)
                                {
                                    this.EnableAlarm(true, lnConfirmedStatus, lnIndex);
                                }
                                else
                                {
                                    this.EnableAlarm(false, 0, lnIndex);
                                }

                            }
                            break;

                        case PacketTypeValues.CONFIG:
                            {
                                int lnTotalChannels = loPacket.Channels;
                                if (lnTotalChannels != this.TotalChannels)
                                {
                                    this.TotalChannels = lnTotalChannels;
                                    this.NeedUpdateUI = true;
                                }
                            }
                            break;
                        case PacketTypeValues.HEART_BEAT:
                            {
                                this.LastHeartBeatTime = loPacket.DateTime;
                            }
                            break;

                        case PacketTypeValues.ALARM_IMAGE_DATA:
                           
                        case PacketTypeValues.NORMAL_DATA:
                           
                            /* this.m_oMessageQueue.Enqueue(loPacket);
                             autoEvent.Set();*/
                             {

                                  int lnIndex = loPacket.VideoChannelIndex;
                                  MessageHandler lpHandler = this.m_oMessageHandleThread[lnIndex];
                                  lpHandler.MessageQueue.Enqueue(loPacket);
                                  lpHandler.AutoEvent.Set();
                              }
                              break;  

                    }
                }
                catch (Exception ex)
                {

                }


            }
        }
        private void btnStart_Click(object sender, EventArgs e)
        {
            this.start();
        }

       
        private void btnStop_Click(object sender, EventArgs e)
        {
            m_oAlarmMQ.OnStop();
            this.toggleUI(false);
        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if(this.NeedUpdateUI)
            {
              //  this.initUI();
                this.NeedUpdateUI = false;
            }

            this.Update();
        }
        static Boolean m_bShouldQuit = false;
        public static System.Boolean ShouldQuit
        {
            get { return m_bShouldQuit; }
            set { m_bShouldQuit = value; }
        }

        private void MainWindowClient_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!MainWindowClient.ShouldQuit)
            {
                PasswordInput loPasswordForm = new PasswordInput();
                ShowMaxWindow = true;
                this.TopMost = false;
                this.PasswordFormShowed = true;
                loPasswordForm.ShowDialog();
                this.PasswordFormShowed = false;
                if (loPasswordForm.Password != null && loPasswordForm.Password.CompareTo(this.SystemPassword) == 0)
                {
                    return;
                }
                e.Cancel = true;
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            this.GoFullscreen(true);
        }

        private void MainWindowClient_FormClosed(object sender, FormClosedEventArgs e)
        {
            MiniDump.TerminateProcess();
        }
    }
}
