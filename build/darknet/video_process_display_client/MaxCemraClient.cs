﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using EricZhao;
namespace video_process_display_client
{
    public partial class MaxCemraClient : MetroForm
    {
        public MaxCemraClient()
        {
            InitializeComponent();
        }
        String m_strUUID = System.Guid.NewGuid().ToString();
        public System.String UUID
        {
            get { lock (this) return m_strUUID; }
            set { lock (this) m_strUUID = value; }
        }
        public void initPicturebox(Image apImage)
        {
            this.m_oPictureBox.Dock = DockStyle.Fill;
            this.m_oPictureBox.Image = apImage;
            this.m_oPictureBox.BackColor = Color.Black;
            this.m_oPictureBox.MouseDoubleClick += this.pictureBox1_DoubleClick;
            this.m_oPictureBox.SizeMode = PictureBoxSizeMode.StretchImage;           
        }


        private void MaxCemra_Load(object sender, EventArgs e)
        {
            this.splitContainer1.Panel2.Controls.Add(this.m_oPictureBox);
            this.m_oPictureBox.Maxed = true;
            this.play();
        }

        Boolean m_bLive = false;
        Boolean m_blogin = false;

        String m_strMSG_URL = "";
        public System.String MSG_URL
        {
            get { lock (this) return m_strMSG_URL; }
            set { lock (this) m_strMSG_URL = value; }
        }
        int m_nChannIndex = 0;
        public int ChannIndex
        {
            get { return m_nChannIndex; }
            set { m_nChannIndex = value; }
        }
        private void MaxCemra_FormClosing(object sender, FormClosingEventArgs e)
        {
            MainWindowClient.UnRegistWindow(m_oPictureBox.ChannelIndex,this.m_oPictureBox);
        }

        CustomPictureBoxClient m_oPictureBox = new CustomPictureBoxClient();

        private void play()
        {
            m_oPictureBox.PicDisplayType = CustomPictureBoxClient.DisplayType.only_normal;
            m_oPictureBox.ChannelIndex = this.ChannIndex;
            MainWindowClient.RegisWindow(m_oPictureBox.ChannelIndex,this.m_oPictureBox);
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void buttonCancelAlarm_Click(object sender, EventArgs e)
        {
            ConfirmWindow loWindow = new ConfirmWindow();
            loWindow.ShowDialog(this);
            if(loWindow.Confirmed)
            {
                AlarmImagePacket loPacket = new AlarmImagePacket();
                loPacket.PacketType = PacketTypeValues.CANCEL_ALL_ALARM;
                loPacket.DateTime = DateTime.Now;
                loPacket.LeakMode = 0;
                loPacket.PacketSource = Environment.MachineName + ":" + Packet.GetLocalIPAddress();
                loPacket.VideoChannelIndex = this.ChannIndex;
                AlarmImageRabbitExchangHandler.SingleSend(loPacket, this.UUID, this.MSG_URL);
                this.Close();
            }
        }

        private void MaxCemraClient_FormClosed(object sender, FormClosedEventArgs e)
        {
            AlarmImageRabbitExchangHandler.releaseSender(this.UUID);
        }
    }
}
