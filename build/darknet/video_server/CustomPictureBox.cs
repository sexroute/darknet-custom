﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using detectorbridge;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using EricZhao.UiThread;
using System.Linq;


namespace video_process_server
{
    public partial class CustomPictureBox : PictureBox
    {
        public CustomPictureBox()
        {
            InitializeComponent();

            this.m_oInvoker = new Method_invoker(this.cloneBmp2);
        }

        private void InitializeComponent()
        {
           // throw new NotImplementedException();

        }

        int m_nConfirmedStatus = 0;
        public int ConfirmedStatus
        {
            get { lock (this) return m_nConfirmedStatus; }
            set { lock (this) m_nConfirmedStatus = value; }
        }
        private bool shifted = false;

        private int m_lUserID = -1;
        public int UserID
        {
            get { lock(this)return m_lUserID; }
            set { lock (this) m_lUserID = value; }
        }

        string DVRIPAddress = "192.168.1.64"; //设备IP地址或者域名
        public string IP
        {
            get { lock (this) return DVRIPAddress; }
            set { lock (this) DVRIPAddress = value; }
        }
        Int16 DVRPortNumber = 8000;//设备服务端口号
        public System.Int16 Port
        {
            get { lock (this) return DVRPortNumber; }
            set { lock (this) DVRPortNumber = value; }
        }
        private uint iLastErr = 0;
        private string str = "";

        string DVRUserName = "admin";//设备登录用户名
        public string UserName
        {
            get { lock (this) return DVRUserName; }
            set { lock (this) DVRUserName = value; }
        }
        string DVRPassword = "admin6666";//设备登录密码
        public string Password
        {
            get { lock (this) return DVRPassword; }
            set { lock (this) DVRPassword = value; }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && this.Image != null)
            {
                this.shifted = true;
                this.Invalidate();
            }

            base.OnMouseDown(e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && this.Image != null)
            {
                this.shifted = false;
                this.Invalidate();
            }

            base.OnMouseUp(e);
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            if (this.shifted)
            {
                pe.Graphics.TranslateTransform(3, 3, System.Drawing.Drawing2D.MatrixOrder.Append);
            }

            base.OnPaint(pe);
        }

        public event EventHandler ImageChanged = null;

        public void OnImageChanged(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Image changed");
        }


        

        public delegate void Method_invoker(IntPtr hdc);

        public Method_invoker m_oInvoker = null;

        public void cloneBmp2(IntPtr hdc)
        {
            try
            {
                Bitmap bitMap = Bitmap.FromHbitmap(hdc);
                String lstrImagePath = "./xxxxx_" + DateTime.Now.ToFileTime() + "_test.bmp";
                bitMap.Save(lstrImagePath, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            catch(Exception e)
            {
                ThreadUiController.outputDebugString(e.Message);
            }

        }

        int m_nAlarmCanceled = 1;
        public int AlarmCanceled
        {
            get { lock (this) return m_nAlarmCanceled; }
            set { lock (this) m_nAlarmCanceled = value; }
        }

        int m_nLeakMode = 0;
        public int LeakMode
        {
            get { lock (this) return m_nLeakMode; }
            set { lock (this) m_nLeakMode = value; }
        }
        int m_nChannelIndex = 0;
        public int ChannelIndex
        {
            get { lock (this) return m_nChannelIndex; }
            set { lock (this) m_nChannelIndex = value; }
        }

        public void cloneBmp(IntPtr hdc)
        {
            try
            {
                var ctr = this;
                Image bmp = new Bitmap(ctr.Width, ctr.Height);
                var gg = Graphics.FromImage(bmp);
                var rect = ctr.RectangleToScreen(ctr.ClientRectangle);
                gg.CopyFromScreen(rect.Location, Point.Empty, ctr.Size);
                String lstrImagePath = "./xxxxx_" + DateTime.Now.ToFileTime() + "_test.bmp";
                bmp.Save(lstrImagePath);
            }
            catch (Exception e)
            {
                ThreadUiController.outputDebugString(e.Message);
            }

        }
       
    }
}
