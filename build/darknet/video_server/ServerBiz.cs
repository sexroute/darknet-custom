﻿using System;
using System.Collections.Generic;
using System.Drawing;
using detectorbridge;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using EricZhao.UiThread;
using System.IO;
using static video_process_server.video_api;
using System.Drawing.Imaging;
using System.Threading;
using Dse;
using static Dse.FDse_define;
using static Dse.FixDef_Daq;
using static Dse.FixDef_Field;
using System.Diagnostics;
using SenderApplication;
using System.Messaging;
using EricZhao;

namespace video_process_server
{
    public class ServerBiz
    {
        private static int m_nGUID = 0;
        private static String ServerBizLockObject = "ServerBiz.ServerBizLockObject";
        public static int GenUID()
        {
            lock (ServerBizLockObject)
            {
                int lnRet = m_nGUID++;

                return lnRet;
            }
        }

        private Boolean m_bEnableAlarm = false;
        public System.Boolean EnableAlarm
        {
            get { lock (m_strLockSendThreadStop) return m_bEnableAlarm; }
            set { lock (m_strLockSendThreadStop) m_bEnableAlarm = value; }
        }

        public ServerBiz()
        {
            this.UserID = ServerBiz.GenUID();
        }
        int m_nStartIndex = 0;
        public int StartIndex
        {
            get { lock (m_strLockSendThreadStop) return m_nStartIndex; }
            set { lock (m_strLockSendThreadStop) m_nStartIndex = value; }
        }

        String m_strMiddlewareIP = "127.0.0.1";
        public System.String MiddlewareIP
        {
            get { lock (m_strLockSendThreadStop) return m_strMiddlewareIP; }
            set { lock (m_strLockSendThreadStop) m_strMiddlewareIP = value; }
        }
        int m_nMiddlewarePort = 17001;
        public int MiddlewarePort
        {
            get { lock (m_strLockSendThreadStop) return m_nMiddlewarePort; }
            set { lock (m_strLockSendThreadStop) m_nMiddlewarePort = value; }
        }

        int m_nKeepSeconds = 40;
        public int KeepSeconds
        {
            get { lock (m_strLockSendThreadStop) if (m_nKeepSeconds < 0) { m_nKeepSeconds = 0; }; return m_nKeepSeconds; }
            set { lock (m_strLockSendThreadStop) m_nKeepSeconds = value; }
        }
        String m_strCompany = "";
        public System.String Company
        {
            get { lock (m_strLockSendThreadStop) return m_strCompany; }
            set { lock (m_strLockSendThreadStop) m_strCompany = value; }
        }
        String m_strFactory = "";
        public System.String Factory
        {
            get { lock (m_strLockSendThreadStop) return m_strFactory; }
            set { lock (m_strLockSendThreadStop) m_strFactory = value; }
        }
        String m_strPlant = "";
        public System.String Plant
        {
            get { lock (m_strLockSendThreadStop) return m_strPlant; }
            set { lock (m_strLockSendThreadStop) m_strPlant = value; }
        }
        String m_strDetectServerIP = "127.0.0.1";
        public System.String DetectServerIP
        {
            get { lock (this) return m_strDetectServerIP; }
            set { lock (this) m_strDetectServerIP = value; }
        }

        int m_nAlarmStatus = 0;
        String m_strAlarmLock = "";
        public int AlarmStatus
        {
            get { lock (m_strAlarmLock) return m_nAlarmStatus; }
            set { lock (m_strAlarmLock) m_nAlarmStatus = value; }
        }

        int m_nConfirmedStatus = 0;
        public int ConfirmedStatus
        {
            get { lock (m_strLockSendThreadStop) return m_nConfirmedStatus; }
            set { lock (m_strLockSendThreadStop) m_nConfirmedStatus = value; }
        }
        int m_nDetectServerPort = 18081;
        public int DetectServerPort
        {
            get { lock (this) return m_nDetectServerPort; }
            set { lock (this) m_nDetectServerPort = value; }
        }

        int m_nFrameInterval = 20;
        public int FrameInterval
        {
            get { lock (this) return m_nFrameInterval; }
            set { lock (this) { if (m_nFrameInterval >= 0) { m_nFrameInterval = value; } } }
        }

        int m_nIgnorePerson = 0;
        public int IgnorePerson
        {
            get { lock (this) return m_nIgnorePerson; }
            set { lock (this) m_nIgnorePerson = value; }
        }

        double distance_ratio = 2.0;
        public double Distance_ratio
        {
            get { lock (this) return distance_ratio; }
            set { lock (this) distance_ratio = value; }
        }

        int area_min = 100;
        public int Area_min
        {
            get { lock (this) return area_min; }
            set { lock (this) area_min = value; }
        }
        int m_nRTSP_PORT = 0;
        public int RTSP_PORT
        {
            get { lock (this) return m_nRTSP_PORT; }
            set { lock (this) m_nRTSP_PORT = value; }
        }

        bool m_nIsFirst = true;
        public bool IsFirst
        {
            get { lock (this) return m_nIsFirst; }
            set { lock (this) m_nIsFirst = value; }
        }


        int m_nROI_X = 0;
        public int ROI_X
        {
            get { lock (this) return m_nROI_X; }
            set { lock (this) m_nROI_X = value; }
        }
        int m_nROI_Y = 0;
        public int ROI_Y
        {
            get { lock (this) return m_nROI_Y; }
            set { lock (this) m_nROI_Y = value; }
        }
        int m_nROI_W = 0;
        public int ROI_W
        {
            get { lock (this) return m_nROI_W; }
            set { lock (this) m_nROI_W = value; }
        }

        int m_nROI_H = 0;
        public int ROI_H
        {
            get { lock (this) return m_nROI_H; }
            set { lock (this) m_nROI_H = value; }
        }
        int m_nAlarmCanceled = 1;
        public int AlarmCanceled
        {
            get { lock (this) return m_nAlarmCanceled; }
            set { lock (this) m_nAlarmCanceled = value; }
        }
        int m_nConfrimedStatus = 0;
        public int ConfrimedStatus
        {
            get { lock (this) return m_nConfrimedStatus; }
            set { lock (this) m_nConfrimedStatus = value; }
        }
        int m_nIndex = 0;
        public int Index
        {
            get
            {
                lock (this) { return m_nIndex; }
            }
            set
            {
                lock (this) { m_nIndex = value; }
            }
        }
        CustomPictureBox m_oPicPox = null;
        private String m_Str_PicBox_Lock = "m_Str_PicBox_Lock";
        public CustomPictureBox PicPox
        {
            get { lock (this.m_Str_PicBox_Lock) return m_oPicPox; }
            set { lock (this.m_Str_PicBox_Lock) m_oPicPox = value; }
        }
        private int m_lUserID = -1;
        private String m_lUserID_lock = "m_lUserID_lock";
        public int UserID
        {
            get { lock (this.m_lUserID_lock) return m_lUserID; }
            set { lock (this.m_lUserID_lock) m_lUserID = value; }
        }

        String m_strCamSrc = "";
        public System.String CamSrc
        {
            get { lock (this) return m_strCamSrc; }
            set { lock (this) m_strCamSrc = value; }
        }
        int m_nCamMode = 0;
        public int CamMode
        {
            get { lock (this) return m_nCamMode; }
            set { lock (this) m_nCamMode = value; }
        }
        int m_nCamIndex = 0;
        public int CamIndex
        {
            get { lock (this) return m_nCamIndex; }
            set { lock (this) m_nCamIndex = value; }
        }
        private Int32 m_lRealHandle = -1;
        public System.Int32 RealHandle
        {
            get { lock (this) return m_lRealHandle; }
            set { lock (this) m_lRealHandle = value; }
        }
        String msqUrl = "";
        public System.String MSQ_Url
        {
            get { lock (this) return msqUrl; }
            set { lock (this) msqUrl = value; }
        }

        int m_nTotalChannels = 0;
        public int TotalChannels
        {
            get { lock (this) return m_nTotalChannels; }
            set { lock (this) m_nTotalChannels = value; }
        }
        int m_nChannelIndex = 0;
        public int ChannelIndex
        {
            get { lock (this) return m_nChannelIndex; }
            set { lock (this) m_nChannelIndex = value; }
        }
        int m_nIgnore = 0;
        public int Ignore
        {
            get { lock (this) return m_nIgnore; }
            set { lock (this) m_nIgnore = value; }
        }

        String m_strChannID = "泄漏监测";
        public System.String ChannID
        {
            get { lock (this) return m_strChannID; }
            set { lock (this) m_strChannID = value; }
        }
        int m_nDisplayGreenRect = 0;
        public int DisplayGreenRect
        {
            get { lock (this) return m_nDisplayGreenRect; }
            set { lock (this) m_nDisplayGreenRect = value; }
        }
        System.Threading.Thread m_oSendThread = null;
        Boolean m_oSendThreadStop = false;
        String m_strLockSendThreadStop = "m_strLockSendThreadStop";
        public System.Boolean SendThreadStop
        {
            get { lock (m_strLockSendThreadStop) return m_oSendThreadStop; }
            set { lock (m_strLockSendThreadStop) m_oSendThreadStop = value; }
        }

        System.Threading.Thread m_oCaptureThread = null;
        Boolean m_bStop = false;
        public System.Boolean Stop
        {
            get { lock (this) return m_bStop; }
            set { lock (this) m_bStop = value; }
        }
        private static bool m_bInitSDK = false;

        string DVRIPAddress = "192.168.1.64"; //设备IP地址或者域名
        public string IP
        {
            get { lock (this) return DVRIPAddress; }
            set { lock (this) DVRIPAddress = value; }
        }
        Int16 DVRPortNumber = 8000;//设备服务端口号
        public System.Int16 Port
        {
            get { lock (this) return DVRPortNumber; }
            set { lock (this) DVRPortNumber = value; }
        }
        private uint iLastErr = 0;
        private string str = "";

        string DVRUserName = "admin";//设备登录用户名
        public string UserName
        {
            get { lock (this) return DVRUserName; }
            set { lock (this) DVRUserName = value; }
        }
        string DVRPassword = "bohuaxinzhi123";//设备登录密码
        public string Password
        {
            get { lock (this) return DVRPassword; }
            set { lock (this) DVRPassword = value; }
        }
        protected static detectorbridge.Detectorbridge moDetector = null;
        protected static String moDetector_locker = "detectorbridge.Detectorbridge moDetector";

        private String m_strCompareImagePath0 = "";
        public System.String CompareImagePath0
        {
            get { lock (this) return m_strCompareImagePath0; }
            set { lock (this) m_strCompareImagePath0 = value; }
        }
        private String m_strCompareImagePath1 = "";
        public System.String CompareImagePath1
        {
            get { lock (this) return m_strCompareImagePath1; }
            set { lock (this) m_strCompareImagePath1 = value; }
        }

        private String m_strCompareDiffPath = "";
        public System.String CompareDiffPath
        {
            get { lock (this) return m_strCompareDiffPath; }
            set { lock (this) m_strCompareDiffPath = value; }
        }
        String m_strTempDir = "./tmp";
        public System.String TempDir
        {
            get { lock (this) return m_strTempDir; }
            set { lock (this) m_strTempDir = value; }
        }
        String m_strBaseDir = "./data";
        public System.String BaseDir
        {
            get { lock (this) return m_strBaseDir; }
            set { lock (this) m_strBaseDir = value; }
        }

        IntPtr m_hWnd = new IntPtr(0);
        public System.IntPtr hWnd
        {
            get { return m_hWnd; }
            set { m_hWnd = value; }
        }

        private delegate void CaptureBmp_delegate(int index);


        private String m_strCompareImagePath0_bak = "";
        public System.String CompareImagePath0_bak
        {
            get { lock (this) return m_strCompareImagePath0_bak; }
            set { lock (this) m_strCompareImagePath0_bak = value; }
        }
        private String m_strCompareImagePath1_bak = "";
        public System.String CompareImagePath1_bak
        {
            get { lock (this) return m_strCompareImagePath1_bak; }
            set { lock (this) m_strCompareImagePath1_bak = value; }
        }

        private String m_strCompareDiffPath_bak = "";
        public System.String CompareDiffPath_bak
        {
            get { lock (this) return m_strCompareDiffPath_bak; }
            set { lock (this) m_strCompareDiffPath_bak = value; }
        }

        String m_strAviBakPath = "";
        public System.String AviBakPath
        {
            get { lock (this) return m_strAviBakPath; }
            set { lock (this) m_strAviBakPath = value; }
        }
        private String m_strAviPath = "";
        public System.String AviPath
        {
            get { lock (this) return m_strAviPath; }
            set { lock (this) m_strAviPath = value; }
        }

        private int m_nStatus = 0;
        private String m_strStatusLock = "m_strStatusLock";
        public int Status
        {
            get { lock (this.m_strStatusLock) return m_nStatus; }
            set { lock (this.m_strStatusLock) m_nStatus = value; }
        }

        Queue<byte[]> m_oQueue = null;
        String m_StrQueueLock = "m_StrQueueLock";
        public Queue<byte[]> Queue
        {
            get { lock (this.m_StrQueueLock) return m_oQueue; }
            set { lock (this.m_StrQueueLock) m_oQueue = value; }
        }
        private void initDetector()
        {
            lock (ServerBiz.moDetector_locker)
            {
                if (ServerBiz.moDetector == null)
                {
                    ServerBiz.moDetector = new Detectorbridge();
                }
            }
        }


        HKHelper m_oHkHelper = null;
        public detectorbridge.HKHelper HkHelper
        {
            get { lock (this) return m_oHkHelper; }
            set { lock (this) m_oHkHelper = value; }
        }
        private void init()
        {
            lock (ServerBiz.moDetector_locker)
            {
                if (ServerBiz.m_bInitSDK == false)
                {
                    ServerBiz.m_bInitSDK = true;

                }

                if (this.Queue == null)
                {
                    this.Queue = new Queue<byte[]>();
                }

                if (this.OnImageCallback == null)
                {
                    this.OnImageCallback = new HKHelper.DecodeImageBufferCallBack(this.onImageCallBack);
                }

                if (this.HkHelper == null)
                {
                    this.HkHelper = new HKHelper();
                    this.HkHelper.setImageDecodedCallBack(this.OnImageCallback);
                }
            }
        }


        private void startCapture_thread()
        {
            this.stopCapture_thread();

            this.init();

            lock (this)
            {
                this.Stop = false;

            }
        }




        private void startSender_thread()
        {
            return;
            this.stopCapture_thread();

            lock (this.m_strLockSendThreadStop)
            {
                this.SendThreadStop = false;
                this.m_oSendThread = new System.Threading.Thread(send_thread_entry);
                this.m_oSendThread.IsBackground = true;
                this.m_oSendThread.Name = "Sender_thread:" + this.Index;
                this.m_oSendThread.Start();
            }
        }


        private bool detectleak()
        {
            try
            {
                String lstrCString =
                              String.Format("http://127.0.0.1:1808{0}/diff?pic0={1}&pic1={2}&pic_diff={3}",
                              this.Index + 1, this.CompareImagePath0.Replace('\\', '/'), this.CompareImagePath1.Replace('\\', '/'), this.CompareDiffPath.Replace('\\', '/')
                              );

                String lstrUrl = lstrCString;
                System.Net.WebClient loClient = new System.Net.WebClient();
                System.IO.Stream loData = loClient.OpenRead(lstrUrl);
                System.IO.StreamReader loReader = new StreamReader(loData);
                String s = loReader.ReadToEnd();
                int lnRet = 0;
                Int32.TryParse(s, out lnRet);
                if (lnRet > 0)
                {
                    File.Copy(this.CompareImagePath0, this.CompareImagePath0_bak);
                    File.Copy(this.CompareImagePath1, this.CompareImagePath1_bak);
                    File.Copy(this.CompareDiffPath, this.CompareDiffPath_bak);
                    //File.Copy(this.AviPath, this.AviPathb);
                    return true;
                }

                return false;
            }
            catch (Exception e)
            {
                ThreadUiController.log(e.Message, ThreadUiController.LOG_LEVEL.ERROR);
                return false;
            }
        }


        private bool detect(byte[] arefImg, int anLengthReal)
        {
            this.initDetector();

            unsafe
            {
                fixed (byte* charPtr = &arefImg[0])
                {
                    List<bbox_t_csharp> loRet2 = ServerBiz.moDetector.detect(charPtr, (int)anLengthReal, 0);

                    int lnPerson = ServerBiz.moDetector.detectPerson(charPtr, (int)anLengthReal, 0.4f);

                    if (lnPerson > 0)
                    {
                        return true;
                    }

                    String lstrDebug = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + "| userid:" + this.UserID;

                    ThreadUiController.outputDebugString(lstrDebug + "|is Person:" + lnPerson + "\r\n");
                }
            }

            return false;
        }

        AutoResetEvent waitHandle = new AutoResetEvent(false);

        Dse.FixComm m_oFixComm = null;
        public Dse.FixComm FixComm
        {
            get { return m_oFixComm; }
            set { m_oFixComm = value; }
        }
        private void send_thread_entry()
        {
            this.FixComm = new Dse.FixComm();
            bool lbRet = this.FixComm.Link(this.MiddlewareIP, "" + this.MiddlewarePort);
            Dictionary<String, Packet> loLaskPackets = new Dictionary<string, Packet>();
            while (true)
            {


                {
                    if (this.SendThreadStop)
                    {
                        return;
                    }
                }

                Packet lpPacket = null;
                bool lbNeedCheckAndRemovePacket = false;

                if (this.StatusList.Count > 0)
                {
                    lpPacket = this.StatusList[0];
                    lbNeedCheckAndRemovePacket = true;
                }

                if (lpPacket == null)
                {
                    if (loLaskPackets.Count > 0)
                    {
                        foreach (KeyValuePair<String, Packet> lpIt in loLaskPackets)
                        {
                            lpPacket = lpIt.Value;
                        }
                    }
                }


                if (lbRet)
                {
                    lbRet = this.doSend(lpPacket);
                }

                if (lbRet)
                {
                    if (lbNeedCheckAndRemovePacket)
                    {
                        this.StatusList.RemoveAt(0);
                    }
                }


                try
                {
                    Thread.Sleep(100);
                }
                catch (Exception e) { }
            }
        }




        private bool doSend(Packet apPacket)
        {

            try
            {
                if (null == apPacket)
                {
                    return false;
                }

                System.Collections.Generic.List<Packet> loRet = this.StatusList;

                int lnStatus = this.LastAlarmStatus;
                Packet loValue = null;
                DateTime loDateTime = DateTime.Now;


                Dse.FixComm lpFixComm = this.FixComm;
                {
                    bool lbRet = lpFixComm.CreateHead(SERVICE_SERVICE_ON_RECEIVEPROC);
                    if (lbRet)
                    {

                        lpFixComm.Set(FIELD_SYSTEM_COMPANY, apPacket.Company);
                        lpFixComm.Set(FIELD_SYSTEM_FACTORY, apPacket.Factory);
                        lpFixComm.Set(FIELD_SERVICE_PLANTID, apPacket.Plant);
                        lpFixComm.Set(FIELD_SERVICE_ON_SEND_STARTID, apPacket.StartIndex);
                        lpFixComm.Set(FIELD_SERVICE_CHANNTYPE, CHANNTYPE.GE_ALLPROC);
                        lpFixComm.Set(FIELD_SERVICE_ON_SEND_PROCCHANNNUM, 1);
                        using (ValueCache loBuffer = new ValueCache())
                        {

                            loBuffer.Ensure(sizeof(float) * 1);
                            loBuffer.Clear();
                            loBuffer.Add((float)apPacket.Value);
                            var buf = loBuffer.ToArray();
                            Debug.Assert(buf.Length == (sizeof(float) * 1));
                            lpFixComm.Set(FIELD_SERVICE_ON_SEND_ALLPROCVALUE, buf);
                        }

                        lpFixComm.Set(FIELD_SERVICE_TIME, loDateTime);

                        if (lpFixComm.More())
                        {
                            return true;
                        }
                    }
                }


            }
            catch (Exception e)
            {

            }

            return false;
        }





        private delegate void alarm_delegate(bool abAlarm);
        private void alarm(bool abAlarm)
        {
            return;
            lock (this)
            {
                if (abAlarm)
                {
                    this.Status = 1;
                }
                else
                {
                    this.Status = 0;
                }

                // return;

                if (this.PicPox == null)
                {
                    return;
                }

                if (abAlarm)
                {

                    if (this.PicPox.Controls != null && this.PicPox.Controls.Count > 0)
                    {
                        Control c = this.PicPox.Controls[0];
                        if (c.GetType() == typeof(CustomPictureBox))
                        {
                            ((CustomPictureBox)c).UserID = this.UserID;
                        }

                        ThreadUiController.ShowControl(this.PicPox.Controls[0]);
                        //                     this.PicPox.Controls[0].Visible = true;
                        //                     this.PicPox.Controls[0].BackColor = Color.Red;
                    }

                }
                else
                {
                    if (this.PicPox.Controls != null && this.PicPox.Controls.Count > 0)
                    {
                        Control c = this.PicPox.Controls[0];
                        if (c.GetType() == typeof(CustomPictureBox))
                        {
                            ((CustomPictureBox)c).UserID = -1;
                        }

                        ThreadUiController.HideControl(this.PicPox.Controls[0]);
                    }
                }

            }

        }

        private void CaptureBmp2(int index, byte[] loBuffer)
        {
            string sBmpPicFileName;
            //图片保存路径和文件名 the path and file name to save

            string myExeDir = this.BaseDir;

            String myTempDir = this.TempDir;

            myTempDir = Detectorbridge.MakeSureImagePathExist(myTempDir);

            myExeDir = Detectorbridge.MakeSureImagePathExist(myExeDir);

            sBmpPicFileName = myTempDir + "\\" + this.UserID + "_" + index + ".bmp";

            //  string myExeDir = new FileInfo(Assembly.GetEntryAssembly().Location).Directory.ToString();

            if (index == 0)
            {
                this.CompareImagePath0 = sBmpPicFileName;
            }
            else
            {
                this.CompareImagePath1 = sBmpPicFileName;
            }

            this.CompareDiffPath = myTempDir + "\\" + "diff.bmp";
            this.AviPath = myTempDir + "\\" + this.UserID + "_diff.avi";

            long milliseconds = DateTimeOffset.Now.Ticks;

            this.CompareImagePath0_bak = myExeDir + "\\" + this.UserID + "_0_" + milliseconds + "_.bmp";
            this.CompareImagePath1_bak = myExeDir + "\\" + this.UserID + "_1_" + milliseconds + "_.bmp";
            this.m_strCompareDiffPath_bak = myExeDir + "\\" + this.UserID + "_diff_" + milliseconds + "_.bmp";

            this.AviBakPath = myExeDir + "\\" + this.UserID + "_diff.avi";

            File.WriteAllBytes(sBmpPicFileName, loBuffer);

        }


        private void cbDrawFun(int lRealHandle, IntPtr hDc, uint dwUser)
        {
            return;

            Graphics g = null;

            if (null == hDc)
            {
                return;
            }

            try
            {
                g = Graphics.FromHdc(hDc);
            }
            catch (Exception e)
            {
                return;
            }

            try
            {
                if (this.Status > 0)
                {
                    if (this.Status == 2)
                    {


                        SolidBrush opaqueBrush = new SolidBrush(Color.FromArgb(100, 255, 255, 0));
                        using (opaqueBrush)
                        {
                            g.FillRectangle(opaqueBrush, 0, 0, this.PicPox.Width, this.PicPox.Height);
                            g.Dispose();
                        }
                    }
                    else if (this.Status == 1)
                    {


                        SolidBrush opaqueBrush = new SolidBrush(Color.FromArgb(100, 255, 0, 0));
                        using (opaqueBrush)
                        {
                            g.FillRectangle(opaqueBrush, 0, 0, this.PicPox.Width, this.PicPox.Height);
                            g.Dispose();
                        }
                    }
                }

            }
            catch (Exception e)
            {

            }
            finally
            {
                if (null != g)
                {
                    g.Dispose();
                }
            }


        }

        int m_nLastAlarmStatus = 0;
        String m_strLastAlarmStatusLock = "m_strLastAlarmStatusLock";
        public int LastAlarmStatus
        {
            get { lock (m_strLastAlarmStatusLock) return m_nLastAlarmStatus; }
            set { lock (m_strLastAlarmStatusLock) m_nLastAlarmStatus = value; }
        }



        List<Packet> m_oStatusList = new List<Packet>();
        public System.Collections.Generic.List<Packet> StatusList
        {
            get { return m_oStatusList; }
            set { m_oStatusList = value; }
        }

        public void AlarmConfirm(int anValue,String strc)
        {
            Debug.WriteLine(String.Format("alarm confirm {0}:{1}", strc,anValue));
            Packet loPacket = new Packet();
            loPacket.Company = this.Company;
            loPacket.Factory = this.Factory;
            loPacket.Plant = this.Plant;
            loPacket.StartIndex = this.StartIndex;
            loPacket.Value = anValue;
            loPacket.DateTime = DateTime.Now;
            loPacket.Middleware = this.MiddlewareIP;
            loPacket.Port = this.MiddlewarePort;
            loPacket.Type = 1;
            loPacket.Source = strc;
            loPacket.CHANNID = this.ChannID;
            m_oRabbitMq.Send(loPacket);
        }

        DateTime m_oLastSendTime = new DateTime();
        public void onStatusCallback(int anStatus)
        {
            int lnLastStatus = this.AlarmStatus;
            if (lnLastStatus != anStatus)
            {
                this.RegSend(anStatus);
            }

            TimeSpan loSpan = DateTime.Now - m_oLastSendTime;
            if (loSpan.TotalSeconds > 1)
            {
                this.RegSend(anStatus);
                this.m_oLastSendTime = DateTime.Now;
            }
            this.AlarmStatus = anStatus;
        }

        EricZhao.RabbitMQ m_oRabbitMq = new EricZhao.RabbitMQ();
        public void RegSend(int anStatus)
        {
            try
            {
                if(!this.EnableAlarm)
                {
                    anStatus = 0;
                }else
                {
                  if( this.ConfirmedStatus>0)
                    {
                        anStatus = this.ConfirmedStatus;
                    }
                }

                Debug.WriteLine(String.Format("RegSend {0}:", anStatus));
                Packet loPacket = new Packet();
                loPacket.Company = this.Company;
                loPacket.Factory = this.Factory;
                loPacket.Plant = this.Plant;
                loPacket.StartIndex = this.StartIndex;
                loPacket.Value = anStatus;
                loPacket.DateTime = DateTime.Now;
                loPacket.Middleware = this.MiddlewareIP;
                loPacket.Port = this.MiddlewarePort;
                m_oRabbitMq.Send(loPacket);
            }
            catch (Exception e)
            {

            }
        }

    
     
        byte[] data = new byte[1920 * 1080 * 3];
        public unsafe void onFrameCallback(int width,
                int height,
                int stride,
                ref byte buffer,
                int length)
        {
            fixed (byte* p = &buffer)
            {
                Bitmap bitmap = new Bitmap(width, height, PixelFormat.Format24bppRgb);
                BitmapData bmData = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadWrite, bitmap.PixelFormat);
                IntPtr pNative = bmData.Scan0;
                Marshal.Copy(new IntPtr(p), data, 0, length);
                Marshal.Copy(data, 0, pNative, length);
                bitmap.UnlockBits(bmData);
                MemoryStream loBuffer = new MemoryStream();
                bitmap.Save(loBuffer, ImageFormat.Bmp);
                byte[] loBufferTempx = loBuffer.ToArray();
                this.Queue.Enqueue(loBufferTempx);

                if (this.Queue.Count > 2)
                {
                    waitHandle.Set();
                }
            }
        }

        framecallback callback = null;
        OnStatusChange status_callback = null;

        public Boolean login()
        {
            this.status_callback = new OnStatusChange(this.onStatusCallback);

            this.UserID = video_api.process_login(UserID,
                this.UserName,
                this.Password,
                this.IP,
                this.Port,
                this.RTSP_PORT,
                this.DetectServerIP,
                this.DetectServerPort,
                this.FrameInterval,
                this.IgnorePerson,
                this.distance_ratio,
                this.area_min,
                this.status_callback,
                this.ROI_X,
                this.ROI_Y,
                this.ROI_W,
                this.ROI_H,
                this.CamSrc,
                this.CamIndex,
                this.CamMode,
                this.DisplayGreenRect,
                this.Ignore,
                this.MSQ_Url,
                this.ChannelIndex,
                this.TotalChannels);

            if (UserID < 0)
            {
                return false;
            }

            return true;
        }

        private void OnTick(object sender, EventArgs e)
        {
            this.onStatusCallback(this.AlarmStatus);
        }

        private System.Windows.Forms.Timer m_oStatusTimer = new System.Windows.Forms.Timer();
        public Boolean live(CustomPictureBox pictureBox)
        {
            this.callback = new framecallback(onFrameCallback);
            if (null == pictureBox)
            {
                return false;
            }
            int lnErrocCode = 0;
            // HwndSource hwndSource = HwndSource.FromHwnd(pictureBox.Handle);
            // this.live(pictureBox);
            bool lbRet = video_api.process(this.UserID,
                pictureBox.Handle,
                0,
                ref this.m_lRealHandle,
                ref lnErrocCode,
                this.callback);
            this.m_oStatusTimer.Interval = 1;
            this.m_oStatusTimer.Enabled = true;
            this.m_oStatusTimer.Tick += this.OnTick;
            return lbRet;
        }



        public void stopLive(System.Windows.Forms.PictureBox pictureBox)
        {
            if (null == pictureBox)
            {
                return;
            }
        }


        public void OnRealDataCallBackRTSP(int lRealHandle, uint dwDataType, ref byte pBuffer, uint dwBufSize, uint dwUser)
        {
        }

        public void OnRealDataCallBackAll(Int32 lRealHandle, UInt32 dwDataType, ref byte pBuffer, UInt32 dwBufSize, IntPtr pUser)
        {


        }

        HKHelper.DecodeImageBufferCallBack m_oOnImageCallback = null;
        public detectorbridge.HKHelper.DecodeImageBufferCallBack OnImageCallback
        {
            get { lock (this) return m_oOnImageCallback; }
            set { lock (this) m_oOnImageCallback = value; }
        }
        public void onImageCallBack(byte[] apBuffer, int anLength)
        {
            System.Diagnostics.Debug.WriteLine(anLength + "");
        }

        Boolean isStarted = false;
        public System.Boolean IsStarted
        {
            get { lock (this) return isStarted; }
            set { lock (this) isStarted = value; }
        }



        protected bool inner_start()
        {
            {
                this.init();
                bool lbRet = this.login();
                this.isStarted = lbRet;
                if (!lbRet)
                {

                    return false;
                }
                this.live(this.PicPox);
                this.startSender_thread();
                return true;
            }
        }
        public bool start()
        {
            return this.inner_start();
        }

        private void stopSender_thread()
        {
            lock (this.m_strLockSendThreadStop)
            {
                this.SendThreadStop = true;
            }
            if (this.m_oSendThread != null)
            {
                this.SendThreadStop = true;

                Boolean lbSucceedExit = false;

                try
                {
                    lbSucceedExit = this.m_oSendThread.Join(2000);

                }
                catch (Exception e)
                {

                }

                try
                {
                    if (!lbSucceedExit)
                    {
                        this.m_oSendThread.Abort();
                    }

                }
                catch (Exception e)
                {

                }


            }

            lock (this.m_strLockSendThreadStop)
            {
                this.m_oSendThread = null;
            }

        }

        private void stopCapture_thread()
        {
            lock (this)
            {
                this.Stop = true;
            }
            if (this.m_oCaptureThread != null)
            {
                this.Stop = true;

                Boolean lbSucceedExit = false;

                try
                {
                    lbSucceedExit = this.m_oCaptureThread.Join(2000);

                }
                catch (Exception e)
                {

                }

                try
                {
                    if (!lbSucceedExit)
                    {
                        this.m_oCaptureThread.Abort();
                    }

                }
                catch (Exception e)
                {

                }


            }

            lock (this)
            {
                this.m_oCaptureThread = null;
            }

        }
    }
}
