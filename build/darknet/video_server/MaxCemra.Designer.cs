﻿namespace video_process_server
{
    partial class MaxCemra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MaxCemra));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.metroStyleManager1 = new MetroFramework.Components.MetroStyleManager(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.metroButtonConfirmLeak = new MetroFramework.Controls.MetroButton();
            this.metroButtonConfirmFire = new MetroFramework.Controls.MetroButton();
            this.metroButtonCancelAlarm = new MetroFramework.Controls.MetroButton();
            this.metroButtonCloseWindow = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroStyleManager1
            // 
            this.metroStyleManager1.Owner = this;
            this.metroStyleManager1.Style = MetroFramework.MetroColorStyle.Black;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(20, 60);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.metroButtonCloseWindow);
            this.splitContainer1.Panel1.Controls.Add(this.metroButtonCancelAlarm);
            this.splitContainer1.Panel1.Controls.Add(this.metroButtonConfirmFire);
            this.splitContainer1.Panel1.Controls.Add(this.metroButtonConfirmLeak);
            this.splitContainer1.Size = new System.Drawing.Size(981, 491);
            this.splitContainer1.SplitterDistance = 45;
            this.splitContainer1.TabIndex = 0;
            // 
            // metroButtonConfirmLeak
            // 
            this.metroButtonConfirmLeak.Location = new System.Drawing.Point(5, 4);
            this.metroButtonConfirmLeak.Name = "metroButtonConfirmLeak";
            this.metroButtonConfirmLeak.Size = new System.Drawing.Size(113, 39);
            this.metroButtonConfirmLeak.TabIndex = 0;
            this.metroButtonConfirmLeak.Text = "确认泄漏";
            this.metroButtonConfirmLeak.UseSelectable = true;
            this.metroButtonConfirmLeak.Click += new System.EventHandler(this.metroButtonConfirmLeak_Click);
            // 
            // metroButtonConfirmFire
            // 
            this.metroButtonConfirmFire.Location = new System.Drawing.Point(124, 4);
            this.metroButtonConfirmFire.Name = "metroButtonConfirmFire";
            this.metroButtonConfirmFire.Size = new System.Drawing.Size(113, 39);
            this.metroButtonConfirmFire.TabIndex = 1;
            this.metroButtonConfirmFire.Text = "确认着火";
            this.metroButtonConfirmFire.UseSelectable = true;
            this.metroButtonConfirmFire.Click += new System.EventHandler(this.metroButtonConfirmFire_Click);
            // 
            // metroButtonCancelAlarm
            // 
            this.metroButtonCancelAlarm.Location = new System.Drawing.Point(243, 4);
            this.metroButtonCancelAlarm.Name = "metroButtonCancelAlarm";
            this.metroButtonCancelAlarm.Size = new System.Drawing.Size(113, 39);
            this.metroButtonCancelAlarm.TabIndex = 2;
            this.metroButtonCancelAlarm.Text = "取消报警";
            this.metroButtonCancelAlarm.UseSelectable = true;
            this.metroButtonCancelAlarm.Click += new System.EventHandler(this.metroButtonCancelAlarm_Click);
            // 
            // metroButtonCloseWindow
            // 
            this.metroButtonCloseWindow.Location = new System.Drawing.Point(362, 4);
            this.metroButtonCloseWindow.Name = "metroButtonCloseWindow";
            this.metroButtonCloseWindow.Size = new System.Drawing.Size(113, 39);
            this.metroButtonCloseWindow.TabIndex = 3;
            this.metroButtonCloseWindow.Text = "关闭窗口";
            this.metroButtonCloseWindow.UseSelectable = true;
            this.metroButtonCloseWindow.Click += new System.EventHandler(this.metroButtonCloseWindow_Click);
            // 
            // MaxCemra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1021, 571);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MaxCemra";
            this.Text = "放大监控";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MaxCemra_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MaxCemra_FormClosed);
            this.Load += new System.EventHandler(this.MaxCemra_Load);
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private MetroFramework.Components.MetroStyleManager metroStyleManager1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private MetroFramework.Controls.MetroButton metroButtonConfirmLeak;
        private MetroFramework.Controls.MetroButton metroButtonCloseWindow;
        private MetroFramework.Controls.MetroButton metroButtonCancelAlarm;
        private MetroFramework.Controls.MetroButton metroButtonConfirmFire;
    }
}