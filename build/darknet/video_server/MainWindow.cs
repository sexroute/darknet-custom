﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EricZhao;
using EricZhao.UiThread;
using MetroFramework.Components;
using MetroFramework.Forms;
using RabbitMQ.Client.Events;

namespace video_process_server
{
    public partial class MainWindow :  MetroForm
    {

        static Boolean m_bShouldQuit = false;
        public static System.Boolean ShouldQuit
        {
            get { return m_bShouldQuit; }
            set { m_bShouldQuit = value; }
        }
        public MainWindow()
        {
            InitializeComponent();
            this.StyleManager = this.metroStyleManager1;

        }

        Dictionary<Int32, Int32> m_oHandleMap = new Dictionary<int, int>();

        String m_strSystemUserName = "admin";
        public System.String SystemUserName
        {
            get { return m_strSystemUserName; }
            set { m_strSystemUserName = value; }
        }
        String m_strSystemPassword = "admin6666";
        public System.String SystemPassword
        {
            get { return m_strSystemPassword; }
            set { m_strSystemPassword = value; }
        }

        String m_strMSQURL = "amqp://guest:guest@127.0.0.1:5672";
        public System.String MSQURL
        {
            get { return m_strMSQURL; }
            set { m_strMSQURL = value; }
        }
        internal void Exit()
        {

        }

        private void registLive(Int32 handle)
        {
            lock (this)
            {
                if (this.m_oHandleMap.ContainsKey(handle))
                {
                    return;
                }
                this.m_oHandleMap.Add(handle, handle);
            }
        }

        private void unRegist(Int32 handle)
        {
            lock (this)
            {
                if (!this.m_oHandleMap.ContainsKey(handle))
                {
                    return;
                }
                this.m_oHandleMap.Remove(handle);
            }
        }

        private int calcColumnCount(int anInput)
        {
            if (anInput <= 1)
            {
                return 1;
            }

            if (anInput <= 2)
            {
                return 2;
            }

            if (anInput <= 4)
            {
                return 2;
            }

            if (anInput <= 9)
            {
                return 3;
            }

            if (anInput <= 10)
            {
                return 5;
            }

            if (anInput <= 16)
            {
                return 4;
            }

            if (anInput <= 25)
            {
                return 5;
            }

            if (anInput <= 48)
            {
                return 8;
            }

            return 4;
        }
        private void initTableLayout()
        {
            this.tableLayoutPanel1.RowStyles.Clear();

            // this.tableLayoutPanel1.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.Dock = DockStyle.Fill;
            // this.tableLayoutPanel1.Padding = GetCorrectionPadding(tableLayoutPanel1, 5);

            int lnPicBox = this.m_nPicboxCount;

            TableLayoutPanel panel = this.tableLayoutPanel1;

            int lnColumn = this.calcColumnCount(this.m_nPicboxCount);
            int lnRowCount = this.m_nPicboxCount / lnColumn;
            panel.ColumnCount = lnColumn;

            int lnColPercent = (int)(1 / (lnColumn * 1.0) * 100.0);
            int lnRowPercent = (int)(1 / (lnRowCount * 1.0) * 100.0);

            panel.ColumnStyles.Clear();

            for (int i = 0; i < lnColumn; i++)
            {
                panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, lnColPercent));
            }

            for (int i = 0; i < lnRowCount; i++)
            {
                panel.RowStyles.Add(new RowStyle(SizeType.Percent, lnRowPercent));
            }
            int lnChannIndex = 0;
            for (int i = 0; i < lnRowCount; i++)
            {
                for (int j = 0; j < lnColumn; j++)
                {

                    CustomPictureBox pb = new CustomPictureBox();
                    pb.BackColor = Color.Black;
                    pb.Dock = DockStyle.Fill;
                    pb.Name = "index:" + i + "" + j + "1";
                    pb.Margin = new Padding(0);

                    CustomPictureBox pb2 = new CustomPictureBox();
                    pb2.BackColor = Color.Red;
                    pb2.Dock = DockStyle.Bottom;
                    pb2.Name = "index:" + i + "" + j + ":2";
                    pb2.Margin = new Padding(0);

                    pb.DoubleClick += new System.EventHandler(this.OnPic_DoubleClick);
                    pb2.DoubleClick += new System.EventHandler(this.OnPic_DoubleClick);

                    //if (i==0 && j ==0)
                    {
                        pb.Controls.Add(pb2);
                        pb2.Visible = false;
                    }

                    panel.Controls.Add(pb, j, i);
                    pb.ChannelIndex = lnChannIndex;
                    lnChannIndex++;
                }
            }

            panel.ResumeLayout();
        }

        detectorbridge.Detectorbridge moDetector = null;
        List<CustomPictureBox> m_oPics = new List<CustomPictureBox>();
        private void initUI()
        {
            this.StyleManager.Style = MetroFramework.MetroColorStyle.Purple;
            this.StyleManager.Theme = MetroFramework.MetroThemeStyle.Light;
            this.initTableLayout();
            this.m_oPics.Clear();
            foreach (var pb in this.tableLayoutPanel1.Controls.OfType<CustomPictureBox>())
            {
                foreach(var loBiz in this.m_oCameras)
                {
                    if(loBiz.Value.ChannelIndex == pb.ChannelIndex)
                    {
                        pb.AlarmCanceled = loBiz.Value.AlarmCanceled;
                        pb.ConfirmedStatus = loBiz.Value.ConfirmedStatus;
                        break;
                    }
                }
                this.m_oPics.Add(pb);
            }
        }

        private void Start()
        {
            int lnPicIndex = 0;

            foreach (KeyValuePair<string, ServerBiz> entry in m_oCameras)
            {
                if (lnPicIndex < this.m_oPics.Count)
                {
                    entry.Value.PicPox = this.m_oPics[lnPicIndex];
                }
                if (!entry.Value.IsStarted)
                {
                    entry.Value.Index = lnPicIndex;
                    entry.Value.start();
                }

                lnPicIndex++;
            }
        }

        Padding GetCorrectionPadding(TableLayoutPanel TLP, int minimumPadding)
        {
            int minPad = minimumPadding;
            Rectangle netRect = TLP.ClientRectangle;
            netRect.Inflate(-minPad, -minPad);

            int w = netRect.Width / TLP.ColumnCount;
            int h = netRect.Height / TLP.RowCount;

            int deltaX = (netRect.Width - w * TLP.ColumnCount) / 2;
            int deltaY = (netRect.Height - h * TLP.RowCount) / 2;

            int OddX = (netRect.Width - w * TLP.ColumnCount) % 2;
            int OddY = (netRect.Height - h * TLP.RowCount) % 2;

            return new Padding(minPad + deltaX, minPad + deltaY,
                               minPad + deltaX + OddX, minPad + deltaY + OddY);
        }

        private void GoFullscreen(bool fullscreen)
        {
            if (!ShouldFullScreen)
            {
                return;
            }
            if (fullscreen)
            {
                if ((this.Bounds != Screen.PrimaryScreen.Bounds))
                {
                    this.WindowState = FormWindowState.Normal;
                    if (this.PasswordFormShowed || this.ShowMaxWindow)
                    {
                        this.TopMost = false;
                    }
                    else
                    {
                        this.TopMost = true;
                    }

                    this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    this.Bounds = Screen.PrimaryScreen.Bounds;
                }

            }
            else
            {
                this.WindowState = FormWindowState.Maximized;
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            }
        }

      
        private void MainWindow_Load(object sender, EventArgs e)
        {
            this.GoFullscreen(true);
            this.loadSettings();
            this.initUI();
            this.Start();
            this.initReciver();
            this.initSender();
        }

        private void EnableAlarm(Boolean abEnable, int anConfirmedStatus, int anIndex, String astrSrc)
        {
            foreach (KeyValuePair<string, ServerBiz> entry in m_oCameras)
            {
                if (entry.Value.Index == anIndex)
                {
                    entry.Value.EnableAlarm = abEnable;
                    entry.Value.ConfirmedStatus = anConfirmedStatus;
                    if (!abEnable)
                    {
                        entry.Value.AlarmConfirm(0, astrSrc);
                    }
                    else
                    {
                        entry.Value.AlarmConfirm(1, astrSrc);
                    }
                }

            }

            foreach (var pb in m_oPics)
            {
                if (pb.ChannelIndex == anIndex)
                {
                    if(abEnable)
                    {
                        pb.AlarmCanceled = 0;
                    }
                    else
                    {
                        pb.AlarmCanceled = 1;
                    }

                    pb.ConfirmedStatus = anConfirmedStatus;
                    this.saveOneConfirmStatus(pb.ChannelIndex, pb.AlarmCanceled, pb.ConfirmedStatus);
                    break; 
                }
            }
        }

        private void initReciver()
        {
            m_oConfirmLeakReciver = new AlarmImageRabbitExchangHandler(PacketTypeValues.CONFIRM_LEAK);
            m_oConfirmLeakReciver.URL = this.MSQURL;
            m_oConfirmLeakReciver.m_OnMessageReceived += this.OnMessageReicved;
            m_oConfirmLeakReciver.Read();

            m_oConfirmFireReciver = new AlarmImageRabbitExchangHandler(PacketTypeValues.CONFIRM_FIRE);
            m_oConfirmFireReciver.URL = this.MSQURL;
            m_oConfirmFireReciver.m_OnMessageReceived += this.OnMessageReicved;
            m_oConfirmFireReciver.Read();

            m_oConfirmCancelReciver = new AlarmImageRabbitExchangHandler(PacketTypeValues.CANCEL_ALL_ALARM);
            m_oConfirmCancelReciver.URL = this.MSQURL;
            m_oConfirmCancelReciver.m_OnMessageReceived += this.OnMessageReicved;
            m_oConfirmCancelReciver.Read();
        }
        public void OnMessageReicved(object sender, BasicDeliverEventArgs e)
        {
            if (e != null)
            {
                try
                {
                    var body = e.Body;
                    AlarmImagePacket loPacket = AlarmImagePacket.toObject(body);
                    if(loPacket!=null)
                    {
                        switch (loPacket.PacketType)
                        {
                            case PacketTypeValues.CONFIRM_FIRE:
                            case PacketTypeValues.CONFIRM_LEAK:
                                {
                                    int lnConfirmedStatus = loPacket.LeakMode;
                                    int lnIndex = loPacket.VideoChannelIndex;
                                    this.EnableAlarm(true, lnConfirmedStatus, lnIndex,loPacket.PacketSource);
                                }
                                break;
                            case PacketTypeValues.CANCEL_ALL_ALARM:
                                {
                                    int lnIndex = loPacket.VideoChannelIndex;
                                    this.EnableAlarm(false, 0, lnIndex,loPacket.PacketSource);
                                }
                                break;

                        }
                    }

                }
                catch (Exception ex)
                {

                }
            }
        }

        AlarmImageRabbitExchangHandler m_oConfirmLeakReciver = null;
        AlarmImageRabbitExchangHandler m_oConfirmFireReciver = null;
        AlarmImageRabbitExchangHandler m_oConfirmCancelReciver = null;

        AlarmImageRabbitExchangHandler m_oStatusSender = null;

        private void initSender()
        {
            m_oStatusSender = new AlarmImageRabbitExchangHandler(PacketTypeValues.CONFIRM_LEAK);
            m_oConfirmLeakReciver.URL = this.MSQURL;
        }

        Dictionary<String, ServerBiz> m_oCameras = new Dictionary<string, ServerBiz>();
        protected String m_strSettingFile = "./settings.ini";
        protected int m_nPicboxCount = 0;

        private int calcPicBoxCount(int anInput)
        {
            if (anInput <= 1)
            {
                return 1;
            }

            return anInput;

            if (anInput <= 2)
            {
                return 2;
            }

            if (anInput <= 4)
            {
                return 4;
            }

            if (anInput <= 9)
            {
                return 9;
            }

            if (anInput <= 10)
            {
                return 10;
            }

            if (anInput <= 16)
            {
                return 16;
            }

            if (anInput <= 25)
            {
                return 25;
            }

            return 16;
        }
        private void loadSettings()
        {
            this.m_oCameras.Clear();
            IniFile loFile = new IniFile(this.m_strSettingFile);
            int lnTemp = loFile.IniReadIntValue("process_server_global", "window", 10, true);
            String baseDir = loFile.IniReadStringValue("process_server_global", "base_dir", "./pic_data", true);
            String tempDir = loFile.IniReadStringValue("process_server_global", "temp_dir", "./pic_temp", true);
            this.m_nPicboxCount = this.calcPicBoxCount(lnTemp);
            this.SystemUserName = loFile.IniReadStringValue("process_server_global", "system_username", "admin", true);
            this.SystemPassword = loFile.IniReadStringValue("process_server_global", "system_password", "admin6666", true);
            this.ShouldFullScreen = loFile.IniReadBooleanValue("process_server_global", "full_screen", false, true);
            String lstrTitle = loFile.IniReadStringValue("process_server_global", "title", "视频流处理服务端", true);
            String lstrMSQURL = loFile.IniReadStringValue("process_server_global", "message_queue_url", "amqp://guest:guest@127.0.0.1:5672", true);
            this.MSQURL = lstrMSQURL;
            this.Text = lstrTitle;
            if (this.m_nPicboxCount <= 0)
            {
                this.m_nPicboxCount = 1;
            }
            int i = 0;
            while (true)
            {

                String lstrSection = "cam_" + i;
                String lstrIP = loFile.IniReadStringValue(lstrSection, "ip", "", false);
                if (String.IsNullOrEmpty(lstrIP))
                {
                    break;
                }
                int lnPort = loFile.IniReadIntValue(lstrSection, "port", 0, false);
                if (lnPort == 0)
                {
                    break;
                }

                int lnRtspPort = loFile.IniReadIntValue(lstrSection, "rtsp_port", 554, false);
                if (lnRtspPort == 0)
                {
                    break;
                }


                String detect_server_ip = loFile.IniReadStringValue(lstrSection, "detect_server_ip", "127.0.0.1", true);
                int detect_server_port = loFile.IniReadIntValue(lstrSection, "detect_server_port", 18081, true);

                String middleware_ip = loFile.IniReadStringValue(lstrSection, "middleware_ip", "127.0.0.1", true);
                int middleware_port = loFile.IniReadIntValue(lstrSection, "middleware_port", 17001, true);

                String company = loFile.IniReadStringValue(lstrSection, "company", "大连石化", true);
                String factory = loFile.IniReadStringValue(lstrSection, "factory", "三催化", true);
                String plant = loFile.IniReadStringValue(lstrSection, "plant", "原料泵P-2201-1", true);

                int startIndex = loFile.IniReadIntValue(lstrSection, "start_index", 0, true);


                String lstrUser = loFile.IniReadStringValue(lstrSection, "user", "admin", true);
                String lstrPassword = loFile.IniReadStringValue(lstrSection, "password", "admin6666", true);

                int lnFrameInterval = loFile.IniReadIntValue(lstrSection, "frame_interval", 20, true);
                int lnIgnore_Person = loFile.IniReadIntValue(lstrSection, "ignore_person", 1, true);

                int lnMinArea = loFile.IniReadIntValue(lstrSection, "area_min", 100, true);
                double ldblDistanceRatio = loFile.IniReadDoubleValue(lstrSection, "distance_ratio", 4.0, true);
                int lnValueKeepSeconds = loFile.IniReadIntValue(lstrSection, "value_keep_seconds", 40, true);

                int lnROI_X = loFile.IniReadIntValue(lstrSection, "ROI_X", -1, true);
                int lnROI_Y = loFile.IniReadIntValue(lstrSection, "ROI_Y", -1, true);
                int lnROI_W = loFile.IniReadIntValue(lstrSection, "ROI_W", -1, true);
                int lnROI_H = loFile.IniReadIntValue(lstrSection, "ROI_H", -1, true);

                String lstrCamSrc = loFile.IniReadStringValue(lstrSection, "cam_src", "", true);
                int lnCamIndex = loFile.IniReadIntValue(lstrSection, "cam_index", 0, true);
                int lnCamMode = loFile.IniReadIntValue(lstrSection, "cam_mode", 0, true);

                int lnDisplayGreenRect = loFile.IniReadIntValue(lstrSection, "green_rect", 0, true);

                int lnIgnore = loFile.IniReadIntValue(lstrSection, "ignore", 0, true);
                String lstrChannID = loFile.IniReadStringValue(lstrSection, "channid", "泄漏监测", true);

                int lnAlarmCanceled = loFile.IniReadIntValue(lstrSection, "confirm_alarm_canceled", 1, true);
                int lnLeakMode = loFile.IniReadIntValue(lstrSection, "confirm_leak_mode", 0, true);


                String lstrKey = lstrIP + ":" + lnPort + ":" + System.Guid.NewGuid();
                ServerBiz loBiz = new ServerBiz();
                loBiz.IP = lstrIP;
                loBiz.Port = (Int16)lnPort;
                loBiz.UserName = lstrUser;
                loBiz.Password = lstrPassword;
                loBiz.BaseDir = baseDir;
                loBiz.TempDir = tempDir;
                loBiz.RTSP_PORT = lnRtspPort;
                loBiz.DetectServerIP = detect_server_ip;
                loBiz.DetectServerPort = detect_server_port;
                loBiz.FrameInterval = lnFrameInterval;
                loBiz.IgnorePerson = lnIgnore_Person;
                loBiz.Distance_ratio = ldblDistanceRatio;
                loBiz.KeepSeconds = lnValueKeepSeconds;
                loBiz.Area_min = lnMinArea;
                loBiz.Company = company;
                loBiz.Factory = factory;
                loBiz.Plant = plant;
                loBiz.MiddlewareIP = middleware_ip;
                loBiz.MiddlewarePort = middleware_port;
                loBiz.StartIndex = startIndex;
                loBiz.CamMode = lnCamMode;
                loBiz.CamIndex = lnCamIndex;
                loBiz.CamSrc = lstrCamSrc;
                loBiz.DisplayGreenRect = lnDisplayGreenRect;
                loBiz.Ignore = lnIgnore;
                loBiz.ChannID = lstrChannID;
                loBiz.AlarmCanceled = lnAlarmCanceled;
                loBiz.ConfirmedStatus = lnLeakMode;

                loBiz.ROI_H = lnROI_H;
                loBiz.ROI_X = lnROI_X;
                loBiz.ROI_W = lnROI_W;
                loBiz.ROI_Y = lnROI_Y;
                loBiz.MSQ_Url = lstrMSQURL;
                loBiz.ChannelIndex = i;
                if (!this.m_oCameras.ContainsKey(lstrKey))
                {
                    this.m_oCameras.Add(lstrKey, loBiz);
                }
                i++;
            }

            foreach(var pb in this.m_oCameras)
            {
                pb.Value.TotalChannels = this.m_oCameras.Count;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        Boolean m_bShowMaxWindow = false;
        public System.Boolean ShowMaxWindow
        {
            get { return m_bShowMaxWindow; }
            set { m_bShowMaxWindow = value; }
        }
        private void OnPic_DoubleClick(object sender, EventArgs e)
        {
            if (sender.GetType() == typeof(CustomPictureBox))
            {
                ServerBiz loRemoteBiz = null;
                int lnPicIndex = 0;
                int lnIndex = -1;
                foreach (KeyValuePair<string, ServerBiz> entry in m_oCameras)
                {
                    if (entry.Value.PicPox == sender)
                    {
                        loRemoteBiz = entry.Value;
                        lnIndex = entry.Value.ChannelIndex;
                        break;
                    }
                }

                if (loRemoteBiz == null)
                {
                    return;
                }
                ShowMaxWindow = true;
                this.TopMost = false;
                MaxCemra MaxWindow = new MaxCemra();
                MaxWindow.MSG_URL = this.MSQURL;
                MaxWindow.ChannIndex = lnIndex;
                MaxWindow.RemoteBiz = new ServerBizLarge();
                MaxWindow.RemoteBiz.UserName = loRemoteBiz.UserName;
                MaxWindow.RemoteBiz.Password = loRemoteBiz.Password;
                MaxWindow.RemoteBiz.Port = loRemoteBiz.Port;
                MaxWindow.RemoteBiz.IP = loRemoteBiz.IP;
                MaxWindow.RemoteBiz.UserID = loRemoteBiz.UserID;
                MaxWindow.RemoteBiz.RTSP_PORT = loRemoteBiz.RTSP_PORT;
                MaxWindow.RemoteBiz.DetectServerIP = loRemoteBiz.DetectServerIP;
                MaxWindow.RemoteBiz.DetectServerPort = loRemoteBiz.DetectServerPort;
                MaxWindow.RemoteBiz.FrameInterval = loRemoteBiz.FrameInterval;
                MaxWindow.RemoteBiz.IgnorePerson = loRemoteBiz.IgnorePerson;
                MaxWindow.RemoteBiz.Distance_ratio = loRemoteBiz.Distance_ratio;
                MaxWindow.RemoteBiz.Area_min = loRemoteBiz.Area_min;
                MaxWindow.RemoteBiz.CamMode = loRemoteBiz.CamMode;
                MaxWindow.RemoteBiz.CamIndex = loRemoteBiz.CamIndex; ;
                MaxWindow.RemoteBiz.CamSrc = loRemoteBiz.CamSrc; ;
                MaxWindow.RemoteBiz.DisplayGreenRect = loRemoteBiz.DisplayGreenRect; ;
                MaxWindow.RemoteBiz.Ignore = loRemoteBiz.Ignore; ;
                MaxWindow.WindowState = FormWindowState.Maximized;
                MaxWindow.ShowDialog(this);
                ShowMaxWindow = false;
            }
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.QuitApp();
        }

        public bool ShouldFullScreen = false;
        public bool PasswordFormShowed = false;
        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!MainWindow.ShouldQuit)
            {
                PasswordInput loPasswordForm = new PasswordInput();
                this.TopMost = false;
                this.PasswordFormShowed = true;
                loPasswordForm.ShowDialog();
                this.PasswordFormShowed = false;
                if (loPasswordForm.Password != null && loPasswordForm.Password.CompareTo(this.SystemPassword) == 0)
                {
                    return;
                }
                e.Cancel = true;
            }

        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            this.GoFullscreen(true);
        }



        private void QuitApp()
        {

            MiniDump.TerminateProcess();

            try
            {
                if (System.Windows.Forms.Application.MessageLoop)
                {
                    // WinForms app
                    System.Windows.Forms.Application.Exit();
                    System.Environment.Exit(1);
                }
                else
                {
                    // Console app
                    System.Environment.Exit(1);
                }
            }
            catch (Exception e)
            {

            }


        }


        private void MainWindow_KeyPress(object sender, KeyPressEventArgs e)
        {
            char keychar = e.KeyChar;
            if ((keychar == 'q'))
            {
                this.QuitApp();
            }
        }

        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            System.Windows.Forms.Keys lnCode = e.KeyCode;
            if ((lnCode == Keys.Q))
            {
                this.QuitApp();
            }
        }

        private void saveOneConfirmStatus(int anIndex,int anAlarmCanceled,int anLeakMode)
        {
            IniFile loFile = new IniFile(this.m_strSettingFile);
            String lstrSection = "cam_" + anIndex;
            loFile.iniWriteIntValue(lstrSection, "confirm_alarm_canceled", anAlarmCanceled);
            loFile.iniWriteIntValue(lstrSection, "confirm_leak_mode", anLeakMode);
        }

        private void saveAlarmConfirmStatus()
        {
            foreach (var pb in this.tableLayoutPanel1.Controls.OfType<CustomPictureBox>())
            {
                this.saveOneConfirmStatus(pb.ChannelIndex, pb.AlarmCanceled, pb.ConfirmedStatus);
            }
        }

        private void MainWindow_FormClosed_1(object sender, FormClosedEventArgs e)
        {
            MiniDump.TerminateProcess();
        }

        private void MainWindow_FormClosing_1(object sender, FormClosingEventArgs e)
        {
            if (!MainWindow.ShouldQuit)
            {
                PasswordInput loPasswordForm = new PasswordInput();
                this.TopMost = false;
                this.PasswordFormShowed = true;
                loPasswordForm.ShowDialog();
                this.PasswordFormShowed = false;
                if (loPasswordForm.Password != null && loPasswordForm.Password.CompareTo(this.SystemPassword) == 0)
                {
                    return;
                }
                e.Cancel = true;
            }
        }

        private void sendStatus()
        {
            List<AlarmImagePacket> loPackets = new List<AlarmImagePacket>();
            foreach (var pb in m_oPics)
            {
                AlarmImagePacket loPacket = new AlarmImagePacket();
                loPacket.PacketType = PacketTypeValues.NOTIFY_STATUS;
                loPacket.VideoChannelIndex = pb.ChannelIndex;
                loPacket.LeakMode = pb.ConfirmedStatus;
                loPacket.Status = pb.AlarmCanceled;
                this.m_oStatusSender.Send(loPacket);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.sendStatus();
        }
    }
}

