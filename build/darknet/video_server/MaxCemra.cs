﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EricZhao;
using MetroFramework.Forms;

namespace video_process_server
{
    public partial class MaxCemra : MetroForm
    {
        public MaxCemra()
        {
            InitializeComponent();
        }

        ServerBizLarge m_oRemoteBiz = null;
        public video_process_server.ServerBizLarge RemoteBiz
        {
            get { return m_oRemoteBiz; }
            set { m_oRemoteBiz = value; }
        }

        String m_strUUID = System.Guid.NewGuid().ToString();
        public System.String UUID
        {
            get { lock (this) return m_strUUID; }
            set { lock (this) m_strUUID = value; }
        }
        public void initPicturebox()
        {
            this.m_oPictureBox.Dock = DockStyle.Fill;
           
            this.m_oPictureBox.BackColor = Color.Black;
            this.m_oPictureBox.MouseDoubleClick += this.pictureBox1_DoubleClick;
            this.m_oPictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
        }


        CustomPictureBox m_oPictureBox = new CustomPictureBox();

        private void MaxCemra_Load(object sender, EventArgs e)
        {
            this.splitContainer1.Panel2.Controls.Add(this.m_oPictureBox);
            this.initPicturebox();
            this.play();
        }

        Boolean m_bLive = false;
        Boolean m_blogin = false;

        String m_strMSG_URL = "";
        public System.String MSG_URL
        {
            get { lock (this) return m_strMSG_URL; }
            set { lock (this) m_strMSG_URL = value; }
        }
        int m_nChannIndex = 0;
        public int ChannIndex
        {
            get { return m_nChannIndex; }
            set { m_nChannIndex = value; }
        }


        private void MaxCemra_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.m_oRemoteBiz != null)
            {
                this.m_oRemoteBiz.stopLive(this.m_oPictureBox);
                this.m_oRemoteBiz = null;
            }

            if (this.m_oRemoteBiz != null)
            {
                this.m_oRemoteBiz = null;
            }
        }

        private void play()
        {
            this.m_oRemoteBiz.live(this.m_oPictureBox);

        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void metroButtonConfirmLeak_Click(object sender, EventArgs e)
        {
            ConfirmWindow loConfirmWindow = new ConfirmWindow();
            loConfirmWindow.Text = "是否确认推送泄漏?";
            loConfirmWindow.ShowDialog(this);
            if (loConfirmWindow.ConfirmedSend)
            {
                AlarmImagePacket loPacket = new AlarmImagePacket();
                loPacket.PacketType = PacketTypeValues.CONFIRM_LEAK;
                loPacket.DateTime = DateTime.Now;
                loPacket.LeakMode = 1;
                loPacket.VideoChannelIndex = this.ChannIndex;
                loPacket.PacketSource = Environment.MachineName + ":" + Packet.GetLocalIPAddress();
                AlarmImageRabbitExchangHandler.SingleSend(loPacket, this.UUID, this.MSG_URL);
               
                this.Close();
            }
        }

        private void metroButtonConfirmFire_Click(object sender, EventArgs e)
        {
            ConfirmWindow loConfirmWindow = new ConfirmWindow();
            loConfirmWindow.Text = "是否确认推送着火?";
            loConfirmWindow.ShowDialog(this);
            if (loConfirmWindow.ConfirmedSend)
            {
                AlarmImagePacket loPacket = new AlarmImagePacket();
                loPacket.PacketType = PacketTypeValues.CONFIRM_FIRE;
                loPacket.DateTime = DateTime.Now;
                loPacket.LeakMode = 2;
                loPacket.VideoChannelIndex = this.ChannIndex;
                loPacket.PacketSource = Environment.MachineName + ":" + Packet.GetLocalIPAddress();
                AlarmImageRabbitExchangHandler.SingleSend(loPacket, this.UUID, this.MSG_URL);               
                this.Close();
            }
        }

        private void metroButtonCancelAlarm_Click(object sender, EventArgs e)
        {
            ConfirmWindow loConfirmWindow = new ConfirmWindow();
            loConfirmWindow.Text = "是否确认取消推送?";
            loConfirmWindow.ShowDialog(this);
            if (loConfirmWindow.ConfirmedSend)
            {
                AlarmImagePacket loPacket = new AlarmImagePacket();
                loPacket.PacketType = PacketTypeValues.CANCEL_ALL_ALARM;
                loPacket.DateTime = DateTime.Now;
                loPacket.LeakMode = 0;
                loPacket.PacketSource = Environment.MachineName + ":" + Packet.GetLocalIPAddress();
                loPacket.VideoChannelIndex = this.ChannIndex;
                AlarmImageRabbitExchangHandler.SingleSend(loPacket, this.UUID, this.MSG_URL);
                this.Close();
            }
        }

        private void metroButtonCloseWindow_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MaxCemra_FormClosed(object sender, FormClosedEventArgs e)
        {
            AlarmImageRabbitExchangHandler.releaseSender(this.UUID);
        }
    }
}
