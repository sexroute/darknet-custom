﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
namespace video_process_server
{
    class video_api
    {
        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate void framecallback(int width,
    int height,
    int stride, ref byte buffer, int length);

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate void OnStatusChange(int anStatus);

        [DllImport(@"PureCppCallback.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern bool live(int UserID, IntPtr handle, int userData, ref int appRealHandle, ref int errcode);

        [DllImport(@"PureCppCallback.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int login(string strUserName, string strPassword, string strIP, int port);

        [DllImport(@"PureCppCallback.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern bool live_v2(int UserID, IntPtr handle, int userData, ref int appRealHandle, ref int errcode, framecallback callback);

        [DllImport(@"PureCppCallback.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern bool process(int UserID, IntPtr handle, int userData, ref int appRealHandle, ref int errcode, framecallback callback);

        [DllImport(@"PureCppCallback.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int login_v2(string strUserName,
            string strPassword,
            string strIP,
            int port,
            int rtsp_port,
            String strDetectServerIP,
            int detectServerPort,
            int frame_interval,
            int ignore_person,
            double distance_ratio,
            int min_area,
            OnStatusChange callback,
            int ROI_X,
            int ROI_Y,
            int ROI_W,
            int ROI_H,
            String strSrc,
            int nCamIndex,
            int nSrcMode,
            int nDisplayGreenRect,
            int nIgnore);

        [DllImport(@"PureCppCallback.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int process_login(int uid,
                                           string strUserName,
                                           string strPassword,
                                           string strIP,
                                           int port,
                                           int rtsp_port,
                                           String strDetectServerIP,
                                           int detectServerPort,
                                           int frame_interval,
                                           int ignore_person,
                                           double distance_ratio,
                                           int min_area,
                                           OnStatusChange callback,
                                           int ROI_X,
                                           int ROI_Y,
                                           int ROI_W,
                                           int ROI_H,
                                           String strSrc,
                                           int nCamIndex,
                                           int nSrcMode,
                                           int nDisplayGreenRect,
                                           int nIgnore,
                                           String strMSQURL,
                                            int anChannIndex,
                                            int anTotalChannels);

        [DllImport(@"PureCppCallback.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int pure_play(
                                          IntPtr handle,
                                          string strUserName,
                                          string strPassword,
                                          string strIP,
                                          int port,
                                          int rtsp_port,
                                          String strDetectServerIP,
                                          int detectServerPort,
                                          int frame_interval,
                                          int ignore_person,
                                          double distance_ratio,
                                          int min_area,
                                          OnStatusChange callback,
                                          int ROI_X,
                                          int ROI_Y,
                                          int ROI_W,
                                          int ROI_H,
                                          String strSrc,
                                          int nCamIndex,
                                          int nSrcMode,
                                          int nDisplayGreenRect,
                                          int nIgnore);

        [DllImport(@"PureCppCallback.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int stop_pure_play(IntPtr handle);

        [DllImport(@"PureCppCallback.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern bool init(string cfg_filename, string weight_filename, int gpu_id);

        [DllImport(@"PureCppCallback.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern void quit();
    }
}
