﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace video_process_server
{
    public partial class ConfirmWindow : MetroForm
    {
        public ConfirmWindow()
        {
            InitializeComponent();
        }

        Boolean m_bConfirmedSend = false;
        public System.Boolean ConfirmedSend
        {
            get { return m_bConfirmedSend; }
            set { m_bConfirmedSend = value; }
        }

        private void ConfirmWindow_Load(object sender, EventArgs e)
        {

        }

        private void metroButtonOK_Click(object sender, EventArgs e)
        {
            this.ConfirmedSend = true;
            this.Close();
        }

        private void metroButtonCancel_Click(object sender, EventArgs e)
        {
            this.ConfirmedSend = false;
            this.Close();
        }
    }
}
