// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // Exclude rarely-used stuff from Windows headers
#endif



#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions


#include <afxdisp.h>        // MFC Automation classes



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxcontrolbars.h>     // MFC support for ribbons and control bars


#include <iostream>
#include <ostream>
#include <iterator>
#include <fstream>
#include <vector>
#include <algorithm>
#include <vector>
#include <map>
#include "include/HCNetSDK.h"
#include "include/PlayM4.h"
#include <opencv2/opencv.hpp>
using namespace System;
using namespace System::Reflection;

using namespace EricZhao::UiThread;
#define DEFAULT_SETTING_FILE L"./settings.ini"
#define STR
#define BHLOG_TRACE(info)
#define BHLOG_INFO(info)  
#define BHLOG_WARN(info) 
#define BHLOG_ERROR(info) 
#define BHLOG_FATAL(info) 
#define BHLOG_FILE(info)  


#define EX_CATCH_ALL_BEGIN try{
#define EX_CATCH_ALL_END	}catch (CMemoryException* e)\
	{\
		CA2T loX(__FILE__);\
		CString lstrData = loX;\
		CString lstrErrorMsg;\
		e->GetErrorMessage(lstrErrorMsg.GetBuffer(1024),1000);\
		lstrErrorMsg.ReleaseBuffer();\
		lstrData.Append(lstrData);\
		ThreadUiController::log(gcnew String(lstrData.GetBuffer(0)),ThreadUiController::LOG_LEVEL::FATAL,true);\
	}\
	catch (CFileException* e)\
	{\
		CA2T loX(__FILE__);\
		CString lstrData = loX;\
		CString lstrErrorMsg;\
		e->GetErrorMessage(lstrErrorMsg.GetBuffer(1024),1000);\
		lstrErrorMsg.ReleaseBuffer();\
		lstrData.Append(lstrData);\
		ThreadUiController::log(gcnew String(lstrData.GetBuffer(0)),ThreadUiController::LOG_LEVEL::FATAL,true);\
	}\
	catch (CException* e)\
	{\
		CA2T loX(__FILE__);\
		CString lstrData = loX;\
		CString lstrErrorMsg;\
		e->GetErrorMessage(lstrErrorMsg.GetBuffer(1024),1000);\
		lstrErrorMsg.ReleaseBuffer();\
		lstrData.Append(lstrData);\
		ThreadUiController::log(gcnew String(lstrData.GetBuffer(0)),ThreadUiController::LOG_LEVEL::FATAL,true);\
	}\
	catch (...)\
	{\
		CA2T loX(__FILE__);\
		CString lstrData = loX;\
		ThreadUiController::log(gcnew String(lstrData.GetBuffer(0)),ThreadUiController::LOG_LEVEL::FATAL,true);\
	}

class CAutoLock_class
{
	CCriticalSection * m_pLock;
private:


public:

	CAutoLock_class()
	{
		this->m_pLock = NULL;
	}

	CAutoLock_class(CCriticalSection & arefLock, BOOL abAddLock = TRUE)
	{
		this->m_pLock = &arefLock;

		if (abAddLock)
		{
			this->m_pLock->Lock();
		}
	}

	BOOL Attach(CCriticalSection & arefLock, BOOL abAddLock = TRUE)
	{
		ASSERT(this->m_pLock == NULL);

		if (this->m_pLock != NULL)
		{
			this->m_pLock->Unlock();
		}

		this->m_pLock = &arefLock;

		if (abAddLock)
		{
			this->m_pLock->Lock();
		}
	}

	~CAutoLock_class()
	{
		if (NULL != this->m_pLock)
		{
			this->m_pLock->Unlock();
		}

		this->m_pLock = NULL;
	}
};

#define CAutoLock(x) CAutoLock_class lolock(x)