// This is the main DLL file.

#include "stdafx.h"
#include "..\..\..\src\interface.hpp"
#include "detector-bridge.h"
#include <msclr\marshal_cppstd.h>
#include <exception>
#include <map>

#pragma comment(lib,"../x64/detector.lib")
#pragma comment(lib,"../x64/HCNetSDK.lib")
#pragma comment(lib,"../x64/PlayCtrl.lib")
using namespace  detectorbridge;
using namespace msclr::interop;

IDetector * gPDetector = NULL;

CCriticalSection glock;
#define USECOLOR
System::String^ Detectorbridge::MakeSureImagePathExist(String^ strPath)
{
		String^ lstrImagePath = strPath;

	if (!System::IO::Directory::Exists(lstrImagePath))
	{
		System::IO::Directory::CreateDirectory(lstrImagePath);
	}

	lstrImagePath = System::IO::Path::GetFullPath(lstrImagePath);

	return lstrImagePath;
}

detectorbridge::Detectorbridge::Detectorbridge(System::String^ cfg_filename, System::String^ weight_filename)
{	
	this->init(cfg_filename, weight_filename, 0);	
}

detectorbridge::Detectorbridge::Detectorbridge()
{
	this->init(nullptr, nullptr, 0);
}

detectorbridge::Detectorbridge::Detectorbridge(System::String^ cfg_filename, System::String^ weight_filename, int gpu_id)
{
	this->init(cfg_filename, weight_filename, gpu_id);
}

System::Collections::Generic::List<bbox_t_csharp^>^ detectorbridge::Detectorbridge::detect(System::String ^ image_filename, float thresh)
{
	// TODO: insert return statement here
	System::Collections::Generic::List<bbox_t_csharp^>^ loRetCharp = gcnew System::Collections::Generic::List<bbox_t_csharp ^>();

	std::string std_image_filename = marshal_as<std::string>(image_filename);

	std::vector<bbox_t> loRet2 = this->detect(std_image_filename, thresh);

	for (int i = 0; i < loRet2.size();i++)
	{
		bbox_t_csharp^ bbt = gcnew bbox_t_csharp();

		bbt->wrap(&loRet2[i]);

		loRetCharp->Add(bbt);
	}

	return loRetCharp;
}




System::Collections::Generic::List<bbox_t_csharp^>^
detectorbridge::Detectorbridge::detect(unsigned char const *buffer,
	int len, 
	int channels, 
	float thresh, 
	bool use_mean)

{
	System::Collections::Generic::List<bbox_t_csharp^>^ loRetCharp = gcnew System::Collections::Generic::List<bbox_t_csharp ^>();

	if (this->check())
	{
		IDetector * lpDetector = (IDetector *)this->getDetector();
		if (NULL == lpDetector)
		{
			return loRetCharp;
		}

		std::vector<bbox_t> loRet2 = lpDetector->detect(buffer, len, channels, thresh, false);

		for (int i = 0; i < loRet2.size(); i++)
		{
			bbox_t_csharp^ bbt = gcnew bbox_t_csharp();

			bbt->wrap(&loRet2[i]);

			loRetCharp->Add(bbt);
		}
	}

	return loRetCharp;
}

int detectorbridge::Detectorbridge::detectPerson(unsigned char const *buffer, int len, float thresh)
{
	IDetector * lpDetector = (IDetector *)this->getDetector();
	if (NULL == lpDetector)
	{
		return 0;
	}

	std::vector<bbox_t> loRet2 = lpDetector->detect(buffer, len, 0, 0.2, false);

	for (int i = 0; i < loRet2.size(); i++)
	{
		if (loRet2[i].obj_id==14)
		{
			return 1;
		}
	}

	return 0;
}


void detectorbridge::Detectorbridge::PutText(IplImage* image, const char *msg, CvPoint point, CvScalar color,CvScalar aoSize)
{
	::PutText(image, msg, point, color,aoSize);
}

System::Collections::Generic::List<bbox_t_csharp^>^ detectorbridge::Detectorbridge::detect(unsigned char const * buffer, 
	int len, 
	int channels)
{
	return this->detect(buffer, len, channels, 0.2, false);
}

bool detectorbridge::Detectorbridge::init(System::String^ cfg_filename, System::String^ weight_filename, int gpu_id)
{
	try
	{
		
		System::Threading::Monitor::Enter(this);

		if (cfg_filename == nullptr)
		{
			cfg_filename = "./cfg/yolo-voc.cfg"; 
		}

		if (weight_filename == nullptr)
		{
			weight_filename = "./yolo-voc.weights";
		}

		if (gPDetector == NULL)
		{
			int lnId = ::GetCurrentThreadId();
			std::string std_cfg_filename = marshal_as<std::string>(cfg_filename);
			std::string std_weight_filename = marshal_as<std::string>(weight_filename);
			gPDetector = (IDetector *)getInstane(std_cfg_filename, std_weight_filename, gpu_id);
		}
	}
	catch (Exception^ e)
	{

	}
	finally
	{
		System::Threading::Monitor::Exit(this);
	}

	return true;
}

bool detectorbridge::Detectorbridge::check()
{
	return this->init(nullptr, nullptr, 0);
}

void * detectorbridge::Detectorbridge::getDetector()
{
	try
	{
		System::Threading::Monitor::Enter(this);

		return	gPDetector;
		
	}
	catch (Exception^ e)
	{

	}
	finally
	{
		System::Threading::Monitor::Exit(this);
	}
}

System::Collections::Generic::List<bbox_t_csharp^>^ detectorbridge::Detectorbridge::detect(image_t_csharp img, float thresh)
{
	System::Collections::Generic::List<bbox_t_csharp^>^ loRet = gcnew System::Collections::Generic::List<bbox_t_csharp ^>();

	if (this->check())
	{

	}
	return loRet;
}


std::vector<bbox_t> detectorbridge::Detectorbridge::detect(std::string image_filename, float thresh)
{
	std::vector<bbox_t> loRet;

	try
	{
		System::Threading::Monitor::Enter(this);
		if (this->check())
		{
			

		  return	gPDetector->detect(image_filename, thresh);
		}
	}
	catch (Exception^ e)
	{
		
	}
	finally
	{
		System::Threading::Monitor::Exit(this);
	}

	return std::vector<bbox_t>();
}

std::vector<bbox_t> detectorbridge::Detectorbridge::detect(image_t img, float thresh)
{
	return std::vector<bbox_t>();
}

int yv12toYUV(char *outYuv, char *inYv12, int width, int height, int widthStep)
{
	int col, row;
	unsigned int Y, U, V;
	int tmp;
	int idx;
	int lnTotalBufferSize = 0;
	//printf("widthStep=%d.\n",widthStep);  

	for (row = 0; row < height; row++)
	{
		idx = row * widthStep;
		int rowptr = row*width;

		for (col = 0; col < width; col++)
		{
			//int colhalf=col>>1;  
			tmp = (row / 2)*(width / 2) + (col / 2);
			//         if((row==1)&&( col>=1400 &&col<=1600))  
			//         {   
			//          printf("col=%d,row=%d,width=%d,tmp=%d.\n",col,row,width,tmp);  
			//          printf("row*width+col=%d,width*height+width*height/4+tmp=%d,width*height+tmp=%d.\n",row*width+col,width*height+width*height/4+tmp,width*height+tmp);  
			//         }   
			Y = (unsigned int)inYv12[row*width + col];
			U = (unsigned int)inYv12[width*height + width*height / 4 + tmp];
			V = (unsigned int)inYv12[width*height + tmp];
			//         if ((col==200))  
			//         {   
			//         printf("col=%d,row=%d,width=%d,tmp=%d.\n",col,row,width,tmp);  
			//         printf("width*height+width*height/4+tmp=%d.\n",width*height+width*height/4+tmp);  
			//         return ;  
			//         }  
			if ((idx + col * 3 + 2) > (1200 * widthStep))
			{
				//printf("row * widthStep=%d,idx+col*3+2=%d.\n",1200 * widthStep,idx+col*3+2);  
			}
			outYuv[idx + col * 3] = Y;
			outYuv[idx + col * 3 + 1] = U;
			outYuv[idx + col * 3 + 2] = V;
			lnTotalBufferSize += 3;
		}
	}

	return lnTotalBufferSize;
	//printf("col=%d,row=%d.\n",col,row);  
}

void CALLBACK InnerDecCBFun(long nPort, 
	char * pBuf, 
	long nSize, 
	FRAME_INFO * pFrameInfo, 
	long nReserved1, 
	long nReserved2)
{
	return;
	long lFrameType = pFrameInfo->nType;
	if (lFrameType == T_YV12)
	{
#ifdef USECOLOR
		//int start = clock();  
		IplImage* pImgYCrCb = cvCreateImage(cvSize(pFrameInfo->nWidth, pFrameInfo->nHeight), 8, 3);//得到图像的Y分量    
		int lnTotalBufferSize = yv12toYUV(pImgYCrCb->imageData, pBuf, pFrameInfo->nWidth, pFrameInfo->nHeight, pImgYCrCb->widthStep);//得到全部RGB图像  
		IplImage* pImg = cvCreateImage(cvSize(pFrameInfo->nWidth, pFrameInfo->nHeight), 8, 3);
		cvCvtColor(pImgYCrCb, pImg, CV_YCrCb2RGB);
		//int end = clock();  
#else  
		IplImage* pImg = cvCreateImage(cvSize(pFrameInfo->nWidth, pFrameInfo->nHeight), 8, 1);
		memcpy(pImg->imageData, pBuf, pFrameInfo->nWidth*pFrameInfo->nHeight);
#endif  

		
/*
		char buff[100];
		snprintf(buff, sizeof(buff), "%d_test.bmp", nPort);
		cvSaveImage(buff, pImg);*/
		if (pImg != NULL && pImg->imageData!=NULL)
		{
			/*HKHelper^ lpThis = HKHelper::getHKHelper(nPort);
			detectorbridge::HKHelper::DecodeImageBufferCallBack^ lpCallBack = lpThis->getImageDecodedCallBack();
			if (lpThis!=nullptr && lpCallBack !=nullptr)
			{
				try
				{
					IplImage* image = pImg;
					int nl = image->height;
					int nc = image->width * image->nChannels;
					int step = image->widthStep;
					 char* data =(image->imageData);
					//array<byte>^ loBufferBack = gcnew array<byte>(lnTotalBufferSize);
					for (int i = 0; i < lnTotalBufferSize; i++)
					{
						//loBufferBack[i] = image->imageData[i];
					}
					//lpCallBack(loBufferBack, loBufferBack->Length);
				}
				catch (Exception^ e)
				{
					CString lstrMessage = e->Message;
					TRACE(lstrMessage);
				}
			}*/
			
		}
#ifdef USECOLOR  
		cvReleaseImage(&pImgYCrCb);
		cvReleaseImage(&pImg);
#else  
		cvReleaseImage(&pImg);
#endif  
	}
}


detectorbridge::HKHelper::HKHelper()
{
	
}

detectorbridge::HKHelper::~HKHelper()
{
	this->setImageDecodedCallBack(nullptr);
	this->unRegistThis();
}

bool detectorbridge::HKHelper::DecCBFun(Int32 lRealHandle,
	UInt32 dwDataType, 
	byte * pBuffer, 
	UInt32 dwBufSize, 
	IntPtr pUser,
	IntPtr ahWindow,
	DecodeImageBufferCallBack^ callback)
{
	LONG xnPort = this->nPort;
	DWORD dRet = 0;
	switch (dwDataType)
	{
	case NET_DVR_SYSHEAD:
		
		if (!PlayM4_GetPort(&xnPort)) //获取播放库未使用的通道号  
		{
			break;
		}

		this->nPort = xnPort;

		this->registThis();

		if (dwBufSize > 0)
		{
			if (!PlayM4_OpenStream(nPort, pBuffer, dwBufSize, 1024 * 1024))
			{
				dRet = PlayM4_GetLastError(nPort);
				return false;
				break;
			}
			//设置解码回调函数 只解码不显示  
			if (!PlayM4_SetDecCallBack(nPort, InnerDecCBFun))
			{
				dRet=PlayM4_GetLastError(nPort);
				return false;
				break;
			}

			if (!PlayM4_Play(nPort, (HWND)0))
			{
				dRet = PlayM4_GetLastError(nPort);
				return false;
				break;
			}
		}
		break;
	case NET_DVR_STREAMDATA:   //码流数据  
		if (dwBufSize > 0 && nPort != -1)
		{
			BOOL inData = PlayM4_InputData(nPort, pBuffer, dwBufSize);
			while (!inData)
			{
				Sleep(10);
				inData = PlayM4_InputData(nPort, pBuffer, dwBufSize);
				OutputDebugString(L"PlayM4_InputData failed \n");
			}
		}
		break;
	}

	//this->setImageDecodedCallBack(callback);
	return true;
}

void detectorbridge::HKHelper::setImageDecodedCallBack(DecodeImageBufferCallBack^ val)
{
	System::Threading::Monitor::Enter(this);
	m_oImageDecodedCallBack = val;
	System::Threading::Monitor::Exit(this);
}

detectorbridge::HKHelper^ detectorbridge::HKHelper::getHKHelper(LONG allPort)
{
	CAutoLock(glock);
	if (HKHelper::g_callers == nullptr)
	{
		HKHelper::g_callers = gcnew System::Collections::Generic::Dictionary<LONG, HKHelper ^>();
	}

	if (HKHelper::g_callers->ContainsKey(allPort))
	{
		return HKHelper::g_callers[allPort];
	}

	return nullptr;
}

void detectorbridge::HKHelper::registThis()
{
	if (!this->m_bRegisted)
	{
		CAutoLock(glock);
		if (!this->m_bRegisted)
		{
			if (HKHelper::g_callers == nullptr)
			{
				HKHelper::g_callers = gcnew System::Collections::Generic::Dictionary<LONG, HKHelper ^>();
			}

			if (!HKHelper::g_callers->ContainsKey(this->nPort))
			{
				HKHelper::g_callers->Add(this->nPort, this);
			}

			this->m_bRegisted = true;
		}
	}

}

void detectorbridge::HKHelper::unRegistThis()
{
	CAutoLock(glock);
	if (HKHelper::g_callers == nullptr)
	{
		HKHelper::g_callers = gcnew System::Collections::Generic::Dictionary<LONG, HKHelper ^>();
	}

	if (HKHelper::g_callers->ContainsKey(this->nPort))
	{
		HKHelper::g_callers->Remove(this->nPort);
	}
}

detectorbridge::HKHelper::DecodeImageBufferCallBack^ detectorbridge::HKHelper::getImageDecodedCallBack()
{
	System::Threading::Monitor::Enter(this);
	return m_oImageDecodedCallBack;
	System::Threading::Monitor::Exit(this);
}
