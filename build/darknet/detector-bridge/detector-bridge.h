// detector-bridge.h

#pragma once

using namespace System;

namespace detectorbridge {

	public ref class bbox_t_csharp{
	protected:
		bbox_t * m_pData;
	public:
			unsigned int x, y, w, h;		// (x,y) - top-left corner, (w, h) - width & height of bounded box
			float prob;						// confidence - probability that the object was found correctly
			unsigned int obj_id;			// class of object - from range [0, classes-1]
			unsigned int track_id;			// tracking id for video (0 - untracked, 1 - inf - tracked object)
			
			bbox_t_csharp()
			{
				this->x = 0;
				this->y = 0;
				this->w = 0;
				this->h = 0;
				this->prob = 0;
				this->obj_id = -1;
				this->track_id = -1;
				this->m_pData = new bbox_t();
			}

			~bbox_t_csharp()
			{
				if (NULL != this->m_pData)
				{
					delete this->m_pData;
					this->m_pData = NULL;
				}
			}

			void wrap(bbox_t * box)
			{
				this->h = box->h;
				this->w = box->w;
				this->x = box->x;
				this->y = box->y;
				this->obj_id = box->obj_id;
				this->prob = box->prob;
				this->track_id = box->track_id;
			}

			bbox_t * genBBoxT()
			{
				this->m_pData->h = this->h;
				this->m_pData->w = this->w;
				this->m_pData->x = this->x;
				this->m_pData->y = this->y;
				this->m_pData->obj_id = this->obj_id;
				this->m_pData->prob = this->prob;
				this->m_pData->track_id = this->track_id;

				return this->m_pData;
			}

			 String^  toString()
			{
				String^ lstrRet =	String::Format("obj_id:{0} prob:{6} track_id:{1} x:{2} y:{3} w:{4} h:{5} ",this->obj_id,this->track_id,this->x,this->y,this->w,this->h,this->prob);
				return lstrRet;
			}
	};

	public ref class image_t_csharp {
	protected:
		image_t * m_pImageT;
	public:
		int h;								// height
		int w;								// width
		int c;								// number of chanels (3 - for RGB)
		System::Collections::Generic::List<float>^ data;				// pointer to the image data
		
		image_t_csharp()
		{
			this->h = 0;
			this->w = 0;
			this->c = 0;
			this->data = gcnew System::Collections::Generic::List<float>();
			this->m_pImageT = new image_t();
		}

		~image_t_csharp()
		{
			if (NULL!=this->m_pImageT)
			{
				if (NULL!=this->m_pImageT->data)
				{
					delete[] this->m_pImageT->data;
					this->m_pImageT->data = NULL;
				}

				delete this->m_pImageT;
				this->m_pImageT = NULL;
			}
		}

		image_t * genImageT()
		{			
			this->m_pImageT->c = this->c;
			this->m_pImageT->h = this->h;
			this->m_pImageT->w = this->w;
			if (this->data!= nullptr && this->data->Count>0)
			{
				if (this->m_pImageT->data != NULL)
				{
					delete this->m_pImageT->data;
					this->m_pImageT->data = NULL;
				}

				this->m_pImageT->data = new float[this->data->Count];

				for (int i =0;i<this->data->Count;i++)
				{
					this->m_pImageT->data[i] = this->data[i];
				}
			}
			return	this->m_pImageT;
		}
	};

	public ref class HKHelper
	{
	public:
		HKHelper();
		virtual ~HKHelper();
		delegate void DecodeImageBufferCallBack(array<byte>^ apBuffer, int anLength);
		bool CALLBACK DecCBFun(Int32 lRealHandle,
			UInt32 dwDataType,
			byte * pBuffer,
			UInt32 dwBufSize,
			IntPtr pUser,
			IntPtr ahWindow,
			DecodeImageBufferCallBack^ callback);
		static HKHelper^ getHKHelper(LONG allPort);
		detectorbridge::HKHelper::DecodeImageBufferCallBack^ getImageDecodedCallBack();
		void setImageDecodedCallBack(detectorbridge::HKHelper::DecodeImageBufferCallBack^ val);
	protected:
		
		
		DecodeImageBufferCallBack^ m_oImageDecodedCallBack = nullptr;
		static System::Collections::Generic::Dictionary<LONG, HKHelper^>^ g_callers= nullptr ;
		bool m_bRegisted = false;
		LONG nPort = -1;
		void registThis();
		void unRegistThis();
	};

	public ref class Detectorbridge
	{
		// TODO: Add your methods for this class here.
	public:
		Detectorbridge();
		Detectorbridge(System::String^ cfg_filename, System::String^ weight_filename);
		Detectorbridge(System::String^ cfg_filename, System::String^ weight_filename, int gpu_id);
		System::Collections::Generic::List<bbox_t_csharp^>^ detect(System::String^ image_filename, float thresh );
		System::Collections::Generic::List<bbox_t_csharp^>^ detect(image_t_csharp img, float thresh);
		System::Collections::Generic::List<bbox_t_csharp^>^ detect(unsigned char const *buffer, int len, int channels);
		System::Collections::Generic::List<bbox_t_csharp^>^ detect(unsigned char const *buffer, int len, int channels, float thresh , bool use_mean);
		int detectPerson(unsigned char const *buffer, int len, float thresh);
		static System::String^ MakeSureImagePathExist(String^ strPath);
		void PutText(IplImage*  image, const char *msg, CvPoint  point, CvScalar  color, CvScalar aoSize );
	private:
		std::vector<bbox_t> detect(std::string image_filename, float thresh );
		std::vector<bbox_t> detect(image_t img, float thresh );
		
		

	protected:
		bool init(System::String^ cfg_filename, System::String^ weight_filename, int gpu_id);
		bool check();
		void * getDetector();
		
	};
}
