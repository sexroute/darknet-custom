#include "stdafx.h"
#include "RTSP.h"
#include "CvvImage.h"
#include "callback_export_api.h"
#include <gdiplus.h>
#include <gdiplusheaders.h>
#include "..\..\..\src\interface.hpp"
#include "rpcdce.h"
#include "DetectorbridgeV2.hpp"
#pragma comment( lib "Rpcrt4.lib")
using namespace cv;
using namespace Gdiplus;
IDetector * g_Detector = NULL;
CCriticalSection g_lock;

CString MakeUuidString(UUID* pUUID/*=NULL*/)
{
	CString strGUID;
	GUID    m_guid;
	::CoCreateGuid(&m_guid);
	LPOLESTR wszCLSID = NULL;
	StringFromCLSID(m_guid, &wszCLSID);
	int nLen = lstrlenW(wszCLSID);

	TCHAR* szCLSID = new TCHAR[nLen + 1];

#ifndef _UNICODE
	wcstombs(szCLSID, wszCLSID, nLen);
	szCLSID[nLen] = 0;
#else
	lstrcpy(szCLSID, wszCLSID);
#endif

	CoTaskMemFree(wszCLSID);

	strGUID = szCLSID;
	delete szCLSID;

	return strGUID;
}

CRTSP::CRTSP()
{
	this->DisplayGreenRect(FALSE);
	this->IgnorePerson(TRUE);
	this->ExitThread(0);
	this->ShouldStopLive(1);
	this->FunCallback(NULL);
	this->Port(-1);
	this->Leaked(FALSE);
	this->FrameInterval(20);
	this->Distance_ratio(2.0L);
	this->DetectMinArea(100);
	this->StatusCallback(NULL);
	this->Ignore(FALSE);
	CString lstrEventName;
	CString lstrUnique = MakeUuidString(NULL);
	lstrEventName.Format(_T("Local\%s"), lstrUnique);
	this->m_oNotifyDetect = CreateEvent(NULL, FALSE, FALSE, lstrEventName);
	this->m_pThread = AfxBeginThread(CRTSP::liveThread, this, 0, 0, CREATE_SUSPENDED);
	CString lstrThreadName;
	lstrThreadName.Format(_T("RSTP:%s"), "test");
	SetThreadName(this->m_pThread->m_nThreadID, lstrThreadName.GetBuffer(0));
	lstrThreadName.Format(_T("DetectLeakThread:%s"), "test");
	this->m_pThread_DetectLeak = AfxBeginThread(CRTSP::DetectLeakThread, this, 0, 0, CREATE_SUSPENDED);
	SetThreadName(this->m_pThread->m_nThreadID, lstrThreadName.GetBuffer(0));
	this->m_pThread->m_bAutoDelete = TRUE;
	this->m_pThread->ResumeThread();
	this->m_pThread_DetectLeak->ResumeThread();
}

CRTSP::~CRTSP()
{
	try
	{
		this->ExitThread(1);
		SetEvent(this->m_oNotifyDetect);
		DWORD ldwRret = WaitForSingleObject(this->m_pThread->m_hThread, 1000);

		if (ldwRret == WAIT_TIMEOUT)
		{
			try
			{
				TerminateThread(this->m_pThread->m_hThread, -1);
			}
			catch (CMemoryException* e)
			{

			}
			catch (CFileException* e)
			{
			}
			catch (CException* e)
			{
			}

			try
			{
				CloseHandle(this->m_pThread->m_hThread);
			}
			catch (CMemoryException* e)
			{

			}
			catch (CFileException* e)
			{
			}
			catch (CException* e)
			{
			}

			try
			{
				//delete this->m_pThread;
			}
			catch (CMemoryException* e)
			{

			}
			catch (CFileException* e)
			{
			}
			catch (CException* e)
			{
			}
		}
	}
	catch (CMemoryException* e)
	{
		
	}
	catch (CFileException* e)
	{
	}
	catch (CException* e)
	{
	}
	catch (...)
	{
	}
	

	this->m_pThread = NULL;
}

CString CRTSP::UserName()
{
	CAutoLock(this->m_oLock);
	return m_strUserName;
}

void CRTSP::UserName(CString val)
{
	CAutoLock(this->m_oLock);
	m_strUserName = val;
}

CString CRTSP::Password()
{
	CAutoLock(this->m_oLock);
	return m_strPassword;
}

void CRTSP::Password(CString val)
{
	CAutoLock(this->m_oLock);
	m_strPassword = val;
}

CString CRTSP::IP()
{
	CAutoLock(this->m_oLock);
	return m_strIP;
}

void CRTSP::IP(CString val)
{
	CAutoLock(this->m_oLock);
	m_strIP = val;
}

int CRTSP::Port()
{
	CAutoLock(this->m_oLock);
	return m_nPort;
}

void CRTSP::Port(int val)
{
	CAutoLock(this->m_oLock);
	m_nPort = val;
}

void CRTSP::UserID(int val)
{
	CAutoLock(this->m_oLock);
	m_nUserID = val;
}

bool CRTSP::startLive()
{
	this->ShouldStopLive(FALSE);
	return true;
}

void CRTSP::stopLive()
{
	this->ShouldStopLive(TRUE);
}
bool CRTSP::detectLeakV2(DetectorbridgeV2& loBridge, std::vector<bbox_t> & arefRet)
{
	cv::Mat current_image_large;
	cv::Mat previous_image_large;
	
	{
		CAutoLock(this->m_oLock);
		{
			if (this->m_oImagesToDetect.size() < 2)
			{
				return false;
			}
		}

		current_image_large = this->m_oImagesToDetect.front();
		this->m_oImagesToDetect.pop();

		previous_image_large = this->m_oImagesToDetect.front();
		this->m_oImagesToDetect.pop();
	}
	int lnDetectServerPort = this->DetectServerPort();
	CString lstrIP, lstrData, lstrName;
	lstrIP.Format("%s_%d", this->IP(), this->m_nPort);
	lstrData.Format("./pic_temp/%s_pic0.bmp", lstrIP);
	lstrName = lstrData.GetBuffer(0);

	bool lbPerson = loBridge.detectPerson_simple_remote_by_buffer_v2(lstrName, 
		lnDetectServerPort, 
		current_image_large, 
		arefRet,0.2f, false, System::Drawing::Imaging::ImageFormat::Jpeg);

	{
		CString lstrLog;
		lstrLog.Format(_T("Current Person:%d\r\n"), lbPerson);
		OutputDebugString(lstrLog);
	}

	if (!lbPerson)
	{
		std::vector<cv::Rect> loRet2 = this->detectLeak(previous_image_large, current_image_large);
	}

	return false;
}

bool CRTSP::detectLeak(DetectorbridgeV2& loBridge)
{
	cv::Mat current_image_large;
	cv::Mat previous_image_large;
	Scalar loColorGreen = Scalar(0, 255, 0);
	Scalar loColorRed = Scalar(0, 0, 255);
	Scalar loColoryellow = Scalar(0, 255, 255);
	{
		CAutoLock(this->m_oLock);
		{
			if (this->m_oImagesToDetect.size() < 2)
			{
				return false;
			}
		}

		current_image_large = this->m_oImagesToDetect.front();
		this->m_oImagesToDetect.pop();

		previous_image_large = this->m_oImagesToDetect.front();
		this->m_oImagesToDetect.pop();
	}

	int lnDetectServerPort = this->DetectServerPort();
	CString lstrDetectServerIP = this->DetectServerIP();
	CString lstrIP, lstrData, lstrName;
	lstrIP.Format("%s_%d", this->IP(), this->m_nPort);
	lstrData.Format("./pic_temp/%s_pic0.bmp", lstrIP);
	lstrName = lstrData.GetBuffer(0);
	
	cv::Mat loHistCurretn = this->getHistogram(current_image_large);
	double ldblCurrentStd = 0;
	double ldblMean = 0;
	this->calcMeanStdVariant(loHistCurretn, ldblMean, ldblCurrentStd);
	std::vector<cv::Rect> loRet2;
	if (current_image_large.cols<=0 
		|| current_image_large.rows<=0
		|| previous_image_large.cols <= 0 
		|| previous_image_large.rows <= 0)
	{
#ifdef DEBUG
		cv::imwrite("./error_current_image_large.bmp", current_image_large);
		cv::imwrite("./error_previous_image_large.bmp", previous_image_large);
#endif

		this->LeakedRect(loRet2);
		this->Leaked(FALSE);
		return false;
	}
	else
	{
#ifdef DEBUG
		cv::imwrite("./good_current_image_large.bmp", current_image_large);
		cv::imwrite("./good_previous_image_large.bmp", previous_image_large);
#endif
	}
	bool lbPerson = loBridge.detectPerson_simple_remote_by_buffer(lstrName, 
		lstrDetectServerIP,
		lnDetectServerPort, 
		current_image_large, 
		0.2f, 
		false,
		System::Drawing::Imaging::ImageFormat::Jpeg);

	{
		CString lstrLog;
		lstrLog.Format(_T("Current Person:%d\r\n"), lbPerson);
		OutputDebugString(lstrLog);
	}

	BOOL lbIgnorePerson =  this->IgnorePerson();
	
	if (!lbPerson || !lbIgnorePerson)
	{
		loRet2 = this->detectLeak(previous_image_large, current_image_large);
		int lnLeakMode = 0;
		if (loRet2.size()>0)
		{
			this->Leaked(TRUE);
			lnLeakMode = 1;
			this->LeakedRect(loRet2);
			HWND hwnd = this->HWnd();
			RECT loRect;
			::GetClientRect(hwnd, &loRect);
			cv::Size winSize(loRect.right, loRect.bottom);
			cv::Mat cvImgTmp(winSize, CV_8UC3);


			for (int i = 0; i < loRet2.size(); i++)
			{
					cv::Rect * lpRect = &loRet2[i];
					
					cv::Mat loSubMat = current_image_large(*lpRect);

					cv::Mat loHistCurretnSub = this->getHistogram(loSubMat);
					double ldblCurrentStdSub = 0;
					double ldblMeanSub = 0;
					this->calcMeanStdVariant(loHistCurretnSub, ldblMeanSub, ldblCurrentStdSub);
					double ldblDiff = fabs(ldblMeanSub - ldblMean);
					if (ldblDiff> ldblCurrentStd*1.4)
					{
						this->Leaked(2);
						lnLeakMode = 2;
						cv::rectangle(current_image_large, *lpRect, loColorRed, 8);
					}
					else
					{
						cv::rectangle(current_image_large, *lpRect, loColorRed, 8);
					}


				/*	int x = lpRect->x;
					int y = lpRect->y;
					CString lstrLabel;
					lstrLabel.Format("泄漏");
					IplImage * lpImge = &IplImage(current_image_large);

					int font_face = cv::FONT_HERSHEY_COMPLEX;
					double font_scale = 0.4;
					loBridge.PutText(lpImge, lstrLabel.GetBuffer(0), cv::Point(x, y - 4), cv::Scalar(0, 0, 255));
					//PutText(lpImge, lstrLabel.GetBuffer(0), cv::Point(x, y - 4), cv::Scalar(0, 0, 255));
					//lo_pic_diff = cvarrToMat(lpImge);
					//cv::rectangle(current_image_large, loRect, cv::Scalar(0, 0, 255), 1);
					cv::rectangle(current_image_large, *lpRect, loColorRed, 8);*/
			}
#ifdef _DEBUG
			cv::imwrite("xxx.bmp", current_image_large);
#endif
			BOOL lnIgnore = this->Ignore();
			if (!lnIgnore)
			{
				cv::Mat image = current_image_large;
				cv::Mat roi = image;
				cv::Mat colorRed(roi.size(), CV_8UC3, cv::Scalar(0, 0, 125));
				cv::Mat colorYellow(roi.size(), CV_8UC3, cv::Scalar(0, 125, 125));
				double alpha = 0.3;

				if (lnLeakMode > 1)
				{
					//cv::addWeighted(colorRed, alpha, roi, 1.0 - alpha, 0.0, roi);
				}
				else
				{
					//cv::addWeighted(colorYellow, alpha, roi, 1.0 - alpha, 0.0, roi);
				}
				
				cv::resize(current_image_large, cvImgTmp, winSize);
				
				IplImage iplimg = IplImage(cvImgTmp);
				CString lstrData = "可能泄漏";
				cv::Point loPoint;
				loPoint.x = (loRect.right - loRect.left) / 2.0;
				loPoint.y = (loRect.bottom - loRect.top) / 2.0;
			//	loBridge.PutText(&iplimg, lstrData, loPoint, loColorRed,cvScalar(12,0,0,0));
				CvvImage iimg;
				iimg.CopyOf(&iplimg);
				HDC loDc = ::GetDC(hwnd);
				iimg.DrawToHDC(loDc, &loRect);
				ReleaseDC(hwnd, loDc);
				iimg.Destroy();
			}
			return true;
		}
	}

	this->LeakedRect(loRet2);
	this->Leaked(FALSE);
	return false;
}



int CRTSP::DetectServerPort() 
{
	CAutoLock(this->m_oLock);
	if (this->m_nDetectServerPort <= 0)
	{
		this->m_nDetectServerPort = 18081;
	}
	return m_nDetectServerPort;
}

void CRTSP::DetectServerPort(int val)
{
	CAutoLock(this->m_oLock);
	m_nDetectServerPort = val;
}

CString CRTSP::DetectServerIP() 
{
	CAutoLock(this->m_oLock);
	if (this->m_strDetectServerIP.IsEmpty())
	{
		this->m_strDetectServerIP = "127.0.0.1";
	}
	return m_strDetectServerIP;
}

void CRTSP::DetectServerIP(CString val)
{
	CAutoLock(this->m_oLock);
	m_strDetectServerIP = val;
}

BOOL CRTSP::Leaked() 
{
	CAutoLock(this->m_oLock);
	return m_bLeaked;
}

void CRTSP::Leaked(BOOL val)
{
	CAutoLock(this->m_oLock);
	m_bLeaked = val;
}

std::vector<cv::Rect> CRTSP::LeakedRect()
{
	CAutoLock(this->m_oLock);
	return m_oLeakedRect;
}

void CRTSP::LeakedRect(std::vector<cv::Rect> val)
{
	CAutoLock(this->m_oLock);
	m_oLeakedRect = val;
}

int CRTSP::FrameInterval()
{
	CAutoLock(this->m_oLock);
	if (this->m_nFrameInterval <= 0)
	{
		this->m_nFrameInterval = 20;
	}
	return m_nFrameInterval;
}

void CRTSP::FrameInterval(int val)
{
	CAutoLock(this->m_oLock);
	m_nFrameInterval = val;
}

BOOL CRTSP::IgnorePerson()
{
	CAutoLock(this->m_oLock);
	return m_bIgnorePerson;
}

void CRTSP::IgnorePerson(BOOL val)
{
	CAutoLock(this->m_oLock);
	if (val < 0)
	{
		val = 0;
	}
	
	m_bIgnorePerson = val;
}

int CRTSP::Distance_c_max()
{

	return m_nDistance_c_max;
}

void CRTSP::Distance_c_max(int val)
{
	m_nDistance_c_max = val;
}

double CRTSP::Distance_ratio()
{
	CAutoLock(this->m_oLock);
	return m_nDistance_ratio;
}

void CRTSP::Distance_ratio(double val)
{
	CAutoLock(this->m_oLock);
	m_nDistance_ratio = val;
}

int CRTSP::DetectMinArea()
{
	CAutoLock(this->m_oLock);
	return m_nDetectMinArea;
}

void CRTSP::DetectMinArea(int val)
{
	CAutoLock(this->m_oLock);
	m_nDetectMinArea = val;
}

OnStatusChange CRTSP::StatusCallback()
{
	CAutoLock(this->m_oLock);
	return m_statusCallback;
}

void CRTSP::StatusCallback(OnStatusChange val)
{
	CAutoLock(this->m_oLock);
	m_statusCallback = val;
}

cv::Rect CRTSP::ROI()
{
	CAutoLock(this->m_oLock);
	return m_oROI;
}

void CRTSP::ROI(cv::Rect val)
{
	CAutoLock(this->m_oLock);
	m_oROI = val;
}

int CRTSP::SourceMode()
{
	CAutoLock(this->m_oLock);
	return m_nSourceMode;
}

void CRTSP::SourceMode(int val)
{
	CAutoLock(this->m_oLock);
	m_nSourceMode = val;
}

int CRTSP::CamIndex()
{
	CAutoLock(this->m_oLock);
	return m_nCamIndex;
}

void CRTSP::CamIndex(int val)
{
	CAutoLock(this->m_oLock);
	m_nCamIndex = val;
}

CString CRTSP::CamSrc()
{
	CAutoLock(this->m_oLock);
	return m_strCamSrc;
}

void CRTSP::CamSrc(CString val)
{
	CAutoLock(this->m_oLock);
	m_strCamSrc = val;
}

int CRTSP::DisplayGreenRect()
{
	CAutoLock(this->m_oLock);
	return m_nDisplayGreenRect;
}

void CRTSP::DisplayGreenRect(int val)
{
	CAutoLock(this->m_oLock);
	m_nDisplayGreenRect = val;
}

BOOL CRTSP::Ignore()
{
	CAutoLock(this->m_oLock);
	return m_nIgnore;
}

void CRTSP::Ignore(BOOL val)
{
	CAutoLock(this->m_oLock);
	m_nIgnore = val;
}

std::vector<std::vector<cv::Point>>  CRTSP::detect_overflap_merge(cv::Mat & img, std::vector<std::vector<cv::Point>>& contours)
{
	int 	height, width = 0;
	height = img.size().height;
	width = img.size().width;
	//cv::Mat1f mask(cv::Size(width,height));
	cv::Mat mask(height, width, CV_8UC3, Scalar(0, 0, 0));
	int lnMinArea = this->DetectMinArea();
	for (int i = 0; i < contours.size(); i++)
	{
		if (cv::contourArea(contours[i]) < lnMinArea)
		{
			continue;
		}

		cv::Rect loRect = cv::boundingRect(contours[i]);

		loRect.width = loRect.width + 0;
		loRect.height = loRect.height + 0;

		cv::rectangle(mask, loRect, cv::Scalar(255, 255, 255), cv::FILLED);
	}


	std::vector<std::vector<cv::Point>> loContours;
	std::vector<Vec4i> hierarchy;
	cv::Mat mask_gray;
	cv::cvtColor(mask, mask_gray, cv::COLOR_BGR2GRAY);
	cv::findContours(mask_gray, loContours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

#ifdef DEBUG
	cv::imwrite("./detect_overflap_merge_test.bmp", mask_gray);
#endif // DEBUG

	return loContours;
}

double CRTSP::compute_distance(const std::vector<cv::Point> & c)
{
	cv::Rect loRect = cv::boundingRect(c);
	int x, y, w, h = 0;
	x = loRect.x;
	y = loRect.y;
	w = loRect.width;
	h = loRect.height;

	int cx, cy = 0;
	cx = x + w / 2;
	cy = y + h / 2;

	int img_cx, img_cy = 0;
	img_cx = ROI_X;
	img_cy = ROI_Y;

	if (((x < img_cx) && img_cx < (x + w))
		&&
		((y < img_cy) && img_cy < (y + h)))
	{
		return 0;
	}

	double distance_c = (x - img_cx) * (x - img_cx) + (y - img_cy) * (y - img_cy);
	double distance_c2 = (x + w - img_cx) * (x + w - img_cx) + (y - img_cy) * (y - img_cy);
	double distance_c3 = (x - img_cx) * (x - img_cx) + (y + h - img_cy) * (y + h - img_cy);
	double distance_c4 = (x + w - img_cx) * (x + w - img_cx) + (y + h - img_cy) * (y + h - img_cy);

	if (distance_c > distance_c2)
		distance_c = distance_c2;

	if (distance_c > distance_c3)
		distance_c = distance_c3;

	if (distance_c > distance_c4)
		distance_c = distance_c4;

	distance_c = 1/(cv::contourArea(c)*1.0);
	return (distance_c);
}

bool CRTSP::less_cmp(const std::vector<cv::Point> & c1, const std::vector<cv::Point> & c2) {
	double dist1 = compute_distance(c1);
	double dist2 = compute_distance(c2);
	return dist1 < dist2;
}




std::vector<cv::Rect> CRTSP::detectLeak(cv::Mat & arefData1, 
	cv::Mat & arefData2, 
	int ln_zoom_size/* = 1*/, 
	int ab_only_detect /*= FALSE*/)
{
	int 	height, width = 0;
	int 	height1, width1 = 0;



	height = arefData1.size().height;
	width = arefData1.size().width;

	height1 = arefData2.size().height;
	width1 = arefData2.size().width;

	img_Width = int(width / ln_zoom_size);
	img_height = int(height / ln_zoom_size);
	Distance_c_max(pow((img_Width / 2.), 2) + pow((img_height / 2.), 2));
	int lnDistanceMax = this->Distance_c_max();
	ROI_X = img_Width / 2;
	ROI_Y = img_height / 2;

	cv::Size lo_zoom_size = cv::Size(int(width / ln_zoom_size), int(height / ln_zoom_size));

	cv::Mat lo_pic0, lo_pic1, lo_pic_diff;
	cv::resize(arefData1, lo_pic0, lo_zoom_size, 0.0, 0.0, cv::INTER_CUBIC);
	cv::resize(arefData2, lo_pic1, lo_zoom_size, 0.0, 0.0, cv::INTER_CUBIC);
	lo_pic_diff = lo_pic1.clone();

	cv::Mat Lim_0, Lim_1;
	cv::cvtColor(lo_pic0, Lim_0, cv::COLOR_BGR2GRAY);
	cv::cvtColor(lo_pic1, Lim_1, cv::COLOR_BGR2GRAY);

	int kernel_size = 5;
	int lnBoxFilterSize = 5;
	cv::Mat gauss_gray1, gauss_gray2;
	cv::GaussianBlur(Lim_0, gauss_gray1, cv::Size(kernel_size, kernel_size), 0);
	cv::GaussianBlur(Lim_1, gauss_gray2, cv::Size(kernel_size, kernel_size), 0);

	cv::Mat frameDelta, frameDelta_box;
	cv::absdiff(gauss_gray1, gauss_gray2, frameDelta);
	cv::boxFilter(frameDelta, frameDelta_box, -1, cv::Size(lnBoxFilterSize, lnBoxFilterSize));

	cv::Mat thresh, thresh_dilate;
	cv::threshold(frameDelta_box, thresh,7, 255, cv::THRESH_BINARY);
	cv::dilate(thresh, thresh_dilate, NULL, cv::Point(-1, -1), 2);

#ifdef _DEBUG
	cv::imwrite("diff.bmp", thresh_dilate);
#endif

	std::vector<std::vector<cv::Point>> contours;
	std::vector<Vec4i> hierarchy;
	cv::findContours(thresh_dilate, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

	std::vector<std::vector<cv::Point>> contours2 = detect_overflap_merge(lo_pic_diff, contours);

	sortstruct s(this);
	std::sort(contours2.begin(), contours2.end(), s);

	bool lbFoundLeak = false;
	std::vector<cv::Rect> loRet;

	double ldblDistanceRatio = this->Distance_ratio();

	int lnDetectMinArea = this->DetectMinArea();

	cv::Rect loRectROI = this->ROI();
	BOOL lbShouldJudgeROI = TRUE;
	if (loRectROI.x < 0|| loRectROI.y <0|| loRectROI.area()<=0||loRectROI.width<=0||loRectROI.height<=0)
	{
		lbShouldJudgeROI = FALSE;
	}
	int lnFoundCount = 0;
	for (int i = 0; i < contours2.size(); i++)
	{
		if (cv::contourArea(contours2[i]) < lnDetectMinArea)
		{
			continue;
		}

		if (compute_distance(contours2[i]) >lnDistanceMax / ldblDistanceRatio)
		{
			//continue;
		}		

		if (lnFoundCount > 5)
		{
			break;
		}

		lnFoundCount++;

		cv::Rect loRect = cv::boundingRect(contours2[i]);

		if (lbShouldJudgeROI)
		{
			double ldblArea = (loRect&loRectROI).area();
			if (ldblArea <= lnDetectMinArea)
			{
				continue;
			}
		}

		int x, y, w, h = 0;
		x = loRect.x;
		y = loRect.y;
		w = loRect.width;
		h = loRect.height;

		cv::Rect loRect2 = loRect;
		loRect2.x = loRect2.x*ln_zoom_size;
		loRect2.y = loRect2.y*ln_zoom_size;
		loRect2.width = loRect2.width*ln_zoom_size;
		loRect2.height = loRect2.height*ln_zoom_size;

		if (loRect2.y <= 20 && loRect2.height < 100)
		{
			continue;
		}

		loRet.push_back(loRect2);
#ifdef _DEBUG
		cv::rectangle(lo_pic_diff, loRect, Scalar(0, 0, 255), 8);
#endif

		lbFoundLeak = true;
	}

	if (lbFoundLeak)
	{
#ifdef _DEBUG
		
		time_t timer;
		char buffer[100] = { 0 };
		struct tm* tm_info;

		time(&timer);
		tm_info = localtime(&timer);
		strftime(buffer, 26, "%Y_%m_%d_%H_%M_%S", tm_info);

		CString lstrData;
		std::string lstrName;
		CString lstrIP;
		lstrIP.Format("%s_%d", this->IP(), this->m_nPort);
		lstrIP.Replace('.', '_');

		lstrData.Format("./pic_data/%s_pic0_%s.bmp", lstrIP, buffer);
		lstrName = lstrData.GetBuffer(0);
		cv::imwrite(lstrName, lo_pic0);

		lstrData.Format("./pic_data/%s_pic1_%s.bmp", lstrIP, buffer);
		lstrName = lstrData.GetBuffer(0);
		cv::imwrite(lstrName, lo_pic1);

		lstrData.Format("./pic_data/%s_diff_%s.bmp", lstrIP, buffer);
		lstrName = lstrData.GetBuffer(0);
		cv::imwrite(lstrName, lo_pic_diff);
#endif

		return loRet;
	}

	return std::vector<cv::Rect>();
}

//rtsp://admin:bohuaxinzhi123@192.168.1.64/h264/ch1/main
#include <opencv2\opencv.hpp>
CCriticalSection open_rtsp_stream_lock;
bool CRTSP::live(HWND hwnd)
{
	cv::VideoCapture vcap;
	cv::Mat image_resized;
	cv::Mat previous_image_large;
	cv::Mat image_large;
	cv::Mat current_image_large;
	cv::Mat image_large_thumb;
	CString lstrUrl;
	lstrUrl.Format("rtsp://%s:%s@%s:%d/mpeg4/ch1/main", this->UserName(), this->Password(), this->IP(), this->Port());
	CString lstrSrc = this->CamSrc();
	int lnCamIndex = this->CamIndex();
	const std::string videoStreamAddress = lstrUrl.GetBuffer(0);
	{

		while (true)
		{
			CAutoLock(open_rtsp_stream_lock);
			int lnSrcMode = this->SourceMode();
			if (lnSrcMode <= 0 || lnSrcMode > 2)
			{
				if (vcap.open(videoStreamAddress))
				{
					break;
				}
			}
			// read from src string
			else if (lnSrcMode == 1)
			{				
				if (vcap.open(lstrSrc.GetBuffer(0)))
				{
					break;
				}
			}
			// read from local camera
			else if (lnSrcMode == 2)
			{
				if (vcap.open(lnCamIndex))
				{
					break;
				}
			}
			
			return false;
		}
	}

	cv::Mat previous_gray;
	cv::Mat current_gray;
	cv::Mat current_gray_gauss;
	cv::Mat diff_gray;
	cv::Mat threshold_gray;
	cv::Mat threshold_gray_dilate;
	cv::Mat img_bin;
	cv::Mat out;


	cv::Mat back;
	cv::Mat fore;

	int thresval = 60;
	BOOL lbFirst = TRUE;


	cv::SimpleBlobDetector::Params params;
	params.minThreshold = 40;
	params.maxThreshold = 60;
	params.thresholdStep = 5;
	params.minArea = 100;
	params.minConvexity = 0.3;
	params.minInertiaRatio = 0.01;
	params.maxArea = 8000;
	params.maxConvexity = 10;
	params.filterByColor = false;
	params.filterByCircularity = false;

	cv::Ptr<cv::SimpleBlobDetector> detector = cv::SimpleBlobDetector::create(params);

	std::vector<cv::KeyPoint>               keyPoints;
	Ptr<BackgroundSubtractorMOG2> bg = createBackgroundSubtractorMOG2();
	FrameCallback  pFun = this->FunCallback();
	std::vector<std::vector<cv::Point>> contours;
	std::vector<Vec4i> hierarchy;
	Scalar loColorGreen = Scalar(0, 255, 0);
	Scalar loColorRed = Scalar(0, 0, 255);
	Scalar loColoryellow = Scalar(0, 255, 255);
	double ldblRatio = 8;
	DetectorbridgeV2 loBridge;
	cv::HOGDescriptor loOpencvDetector;
	loOpencvDetector.setSVMDetector(cv::HOGDescriptor::getDefaultPeopleDetector());
	ULONG64 lnStep = 0;
	int lnMinArea = this->DetectMinArea();
	BOOL lbDisplayGreen = this->DisplayGreenRect();
	std::vector<cv::Rect> loRectDiffs;
	int lnFrameInterval = this->FrameInterval();
	BOOL lnIgnore = this->Ignore();
	while (true)
	{

		BOOL lbShouldStopLive = FALSE;
		if (!vcap.read(image_large))
		{
			if (lbShouldStopLive)
			{
				return true;
			}
			cv::waitKey(10);
			vcap.release();
			int lnSrcMode = this->SourceMode();
			if (lnSrcMode <= 0 || lnSrcMode > 2)
			{
				if (vcap.open(videoStreamAddress))
				{
					//break;
				}
			}
			// read from src string
			else if (lnSrcMode == 1)
			{
				if (vcap.open(lstrSrc.GetBuffer(0)))
				{
					//break;
				}
			}
			// read from local camera
			else if (lnSrcMode == 2)
			{
				if (vcap.open(lnCamIndex))
				{
					//break;
				}
			}
			continue;
		}

		cv::waitKey(10);
		Sleep(0);

		int lnNewWidth = 0;
		int lnNewHeigth = 0;

		if (lnStep == ULONG64_MAX)
		{
			lnStep = 0;
		}

		if (lbFirst)
		{
			lbFirst = FALSE;
		}

		if (image_large.empty())
		{
			continue;
		}

		cv::Size size = image_large.size();
		lnNewWidth = size.width / ldblRatio;
		lnNewHeigth = size.height / ldblRatio;
		cv::resize(image_large, image_resized, cv::Size(lnNewWidth, lnNewHeigth));

		cv::cvtColor(image_resized, current_gray, COLOR_BGR2GRAY);
		cv::GaussianBlur(current_gray, current_gray_gauss, cv::Size(21, 21), 0);

		if (previous_gray.empty())
		{
			previous_image_large = image_large.clone();
			previous_gray = current_gray.clone();
			continue;
		}

		lnStep++;

		if (lnStep % lnFrameInterval == 0)
		{
			loRectDiffs.clear();
			current_image_large = image_large.clone();

			cv::absdiff(previous_gray, current_gray, diff_gray);
			cv::boxFilter(diff_gray, diff_gray, -1, cv::Size(15, 15));
			cv::threshold(diff_gray, threshold_gray, 0, 255, THRESH_BINARY);
			cv::dilate(threshold_gray, threshold_gray_dilate, NULL);

			contours.clear();
			hierarchy.clear();

			cv::findContours(threshold_gray_dilate, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

			std::vector<std::vector<cv::Point>>::iterator lpIt = contours.begin();

			bool lbDiffDetect = false;

			for (int i = 0; i < contours.size(); i++)
			{
				std::vector<cv::Point> loData = contours[i];
				if (cv::contourArea(loData) < lnMinArea)
				{
					continue;
				}

				cv::Rect loRect = cv::boundingRect(loData);


				if ((loRect.height >= lnNewHeigth*0.95) &&
					(loRect.width >= lnNewWidth*0.95))
				{
					//continue;
				}

				loRect.x = loRect.x*ldblRatio;
				loRect.y = loRect.y*ldblRatio;
				loRect.width = loRect.width*ldblRatio;
				loRect.height = loRect.height*ldblRatio;

				if (loRect.y <= 3 && loRect.height < 100)
				{
					loRect.height = loRect.height;
					continue;
				}

				if (cv::contourArea(loData) > lnMinArea)
				{
					{						
						loRectDiffs.push_back(loRect);
					}

				}

				lbDiffDetect = true;
			}			

			if (lbDiffDetect)
			{
				//if (lnStep % 40 == 0)
				{
					this->PushImage(previous_image_large.clone(), image_large.clone());
					::SetEvent(this->m_oNotifyDetect);
				}
			}
			else
			{
				this->Leaked(FALSE);
			}
		}

		std::vector<cv::Rect> loLeakedRect;

		BOOL lbLeaked = this->Leaked();
		if (lbLeaked)
		{
			loLeakedRect = this->LeakedRect();
		}

		RECT loRect;
		::GetClientRect(hwnd, &loRect);
		cv::Size winSize(loRect.right, loRect.bottom);
		cv::Mat cvImgTmp(winSize, CV_8UC3);

		for (int i=0;i<loRectDiffs.size();i++)
		{
			cv::Rect * lpRect = &loRectDiffs[i];
			if (lbDisplayGreen)
			{
				cv::rectangle(image_large, *lpRect, loColorGreen, 8);
			}			
		}

		if (lbLeaked)
		{
			for (int i = 0; i < loLeakedRect.size(); i++)
			{
				cv::Rect * lpRect = &loLeakedRect[i];
				if (!lnIgnore)
				{
					if (lbLeaked > 1)
					{
						cv::rectangle(image_large, *lpRect, loColorRed, 8);
					}
					else

					{
						cv::rectangle(image_large, *lpRect, loColorRed, 8);
					}
					
				}
				
			}
		}
	

		if (lbLeaked)
		{
			cv::Mat image = image_large;
			cv::Mat roi = image;
			cv::Mat color_red(roi.size(), CV_8UC3, cv::Scalar(0, 0, 125));
			cv::Mat color_yellow(roi.size(), CV_8UC3, cv::Scalar(0, 125, 125));
			double alpha = 0.3;
			if (!lnIgnore)
			{
				if (lbLeaked > 1)
				{
					//cv::addWeighted(color_red, alpha, roi, 1.0 - alpha, 0.0, roi);
				}
				else
				{
					//cv::addWeighted(color_yellow, alpha, roi, 1.0 - alpha, 0.0, roi);
				}
				
			}
		}

		if (this->StatusCallback() != NULL)
		{
			this->StatusCallback()(lbLeaked);
		}
		
		cv::resize(image_large, cvImgTmp, winSize);
		IplImage iplimg = IplImage(cvImgTmp);

		if (lbLeaked && !lnIgnore)
		{
			CString lstrData = "可能泄漏";
			if (lbLeaked>1)
			{
				lstrData = "可能着火";
			}
			cv::Point loPoint;
			loPoint.x = (loRect.right - loRect.left) / 2.0-80;
			loPoint.y = (loRect.bottom - loRect.top) / 2.0+20;
			//loBridge.PutText(&iplimg, lstrData, loPoint, loColorRed, cvScalar(40, 0, 0, 0));
		}

		CvvImage iimg;
		iimg.CopyOf(&iplimg);
		HDC loDc = ::GetDC(hwnd);
		iimg.DrawToHDC(loDc, &loRect);
		ReleaseDC(hwnd, loDc);
		iimg.Destroy();

		previous_gray = current_gray.clone();
		previous_image_large = current_image_large.clone();

	}
	return true;
}

HWND CRTSP::HWnd()
{
	CAutoLock(this->m_oLock);
	return m_hWnd;
}

void CRTSP::HWnd(HWND val)
{
	CAutoLock(this->m_oLock);
	m_hWnd = val;
}

UINT AFX_CDECL CRTSP::liveThread(LPVOID apParameter)
{
	if (NULL == apParameter)
	{
		return 0;
	}

	CRTSP * lpHandler = (CRTSP *)apParameter;

	while (true)
	{
		BOOL lbExit = lpHandler->ExitThread();

		if (lbExit)
		{
			break;
		}

		BOOL lbShouldStopPlay = lpHandler->ShouldStopLive();

		if (!lbShouldStopPlay)
		{
			lpHandler->live(lpHandler->HWnd());
		}

		Sleep(500);
	}

	return 0;
}

UINT AFX_CDECL CRTSP::DetectLeakThread(LPVOID apParameter)
{
	if (NULL == apParameter)
	{
		return 0;
	}

	CRTSP * lpHandler = (CRTSP *)apParameter;

	DetectorbridgeV2 loBridge;

	WaitForSingleObject(lpHandler->m_oNotifyDetect, INFINITE);

	while (true)
	{
		BOOL lbExit = lpHandler->ExitThread();

		if (lbExit)
		{
			break;
		}

		lpHandler->detectLeak(loBridge);
		WaitForSingleObject(lpHandler->m_oNotifyDetect, INFINITE);

	}

	return 0;
}



void CRTSP::ExitThread(BOOL val)
{
	CAutoLock(this->m_oLock);
	m_bExitThread = val;
}

BOOL CRTSP::ExitThread()
{
	CAutoLock(this->m_oLock);
	return m_bExitThread;
}

BOOL CRTSP::ShouldStopLive()
{
	CAutoLock(this->m_oLock);
	return m_bStopLive;
}

void CRTSP::ShouldStopLive(BOOL val)
{
	CAutoLock(this->m_oLock);
	m_bStopLive = val;
}

FrameCallback CRTSP::FunCallback()
{
	CAutoLock(this->m_oLock);
	return m_pcallback;
}

void CRTSP::FunCallback(FrameCallback val)
{
	CAutoLock(this->m_oLock);
	m_pcallback = val;
}

void CRTSP::PushImage(cv::Mat & arefData1, cv::Mat & arefData2)
{
	this->m_oLock.Lock();

	try
	{
		if (this->m_oImagesToDetect.size() > 10)
		{
			while (this->m_oImagesToDetect.size() > 0)
			{
				this->m_oImagesToDetect.pop();
			}
		}
		this->m_oImagesToDetect.push(arefData1.clone());
		this->m_oImagesToDetect.push(arefData2.clone());
	}
	catch (CMemoryException* e)
	{
		ASSERT(FALSE);
	}
	catch (CFileException* e)
	{
		ASSERT(FALSE);
	}
	catch (CException* e)
	{
		ASSERT(FALSE);
	}
	catch (...)
	{
		ASSERT(FALSE);
	}

	this->m_oLock.Unlock();

}

cv::Mat CRTSP::getHistogram(const cv::Mat &image, 
							  int anHistSize/* = 128*/)
{
/*
	int lnChannels = image.channels();
	cv::MatND hist;
	float hranges[2] = { 0 };
	hranges[0] = 0;
	hranges[1] = 1;

	cv::calcHist(&image,    // 输入图像
		1,         // 计算1张图片的直方图
		&lnChannels,  // 图像的通道数
		cv::Mat(), // 不实用图像作为掩码
		hist,      // 返回的直方图
		1,				// 1D的直方图
		&anHistSize,  // 容器的数量
		&hranges);   // 像素值的范围*/

	std::vector<Mat> bgr_planes;
	split(image, bgr_planes);
	
	if (bgr_planes.size()<=0)
	{
		return cv::Mat();
	}

	/// Establish the number of bins
	int histSize = anHistSize;

	/// Set the ranges ( for B,G,R) )
	float range[] = { 0, 256 };
	const float* histRange = { range };

	bool uniform = true; bool accumulate = false;

	Mat hist, g_hist, r_hist;

	int lnChannels = image.channels();

	/// Compute the histograms:
	calcHist(&bgr_planes[2], 1, 0, Mat(), hist, 1, &histSize, &histRange, uniform, accumulate);
	return hist;
}

void CRTSP::calcMeanStdVariant(cv::Mat & value, double & mean, double &std)
{
	if (value.empty())
	{
		return;
	}

	int lnCols = value.rows;

	double lnTotalValue = .0;
	for (int i=0;i<lnCols;i++)
	{
		lnTotalValue += value.at<float>(i);
	}

	double ldblMean = lnTotalValue / (lnCols*1.0);

	double ldblTotalVariant = .0;
	for (int i = 0; i < lnCols; i++)
	{
		double ldblDiff = (value.at<float>(i) - ldblMean);
		ldblDiff = ldblDiff*ldblDiff;
		ldblTotalVariant += ldblDiff;
	}

	ldblTotalVariant = ldblTotalVariant / (lnCols*1.0);
	double ldblstd = sqrt(ldblTotalVariant);

	mean = ldblMean;
	std = ldblstd;
}

int CRTSP::UserID()
{
	CAutoLock(this->m_oLock);
	return m_nUserID;
}

bool CRTSP::sortstruct::operator()(const std::vector<cv::Point> & m1, const std::vector<cv::Point> & m2)
{
	return this->m->less_cmp(m1, m2);
}
