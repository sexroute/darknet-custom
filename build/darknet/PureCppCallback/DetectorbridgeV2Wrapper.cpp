#include "stdafx.h"
#include "DetectorbridgeV2.hpp"

public ref class bbox_t_cli{
public:
	unsigned int x, y, w, h;	// (x,y) - top-left corner, (w, h) - width & height of bounded box
float prob;					// confidence - probability that the object was found correctly
unsigned int obj_id;		// class of object - from range [0, classes-1]
unsigned int track_id;		// tracking id for video (0 - untracked, 1 - inf - tracked object)
bbox_t_cli();

};

bbox_t_cli::bbox_t_cli()
{
	this->x = this->y = this->w = this->h = 0;
	this->prob = 0.f;
	this->obj_id = 0;
	this->track_id = 0;
}



public ref class DetectorbridgeV2Wrapper
{
public:
	static array<bbox_t_cli^>^ detect(unsigned char const *buffer, int len, int channels, float thresh , bool use_mean );
};

cli::array<bbox_t_cli^>^ DetectorbridgeV2Wrapper::detect(unsigned char const *buffer, int len, int channels, float thresh, bool use_mean)
{
	DetectorbridgeV2 loDetector;
	std::vector<bbox_t> loRet = loDetector.detect(buffer, len, channels, thresh, use_mean);
	cli::array<bbox_t_cli^>^ loRetx = gcnew cli::array<bbox_t_cli ^>(loRet.size());
	for (int i=0;i<loRet.size();i++)
	{
		loRetx[i] = gcnew bbox_t_cli();
		loRetx[i]->w = loRet[i].w;
		loRetx[i]->h = loRet[i].h;

		loRetx[i]->x = loRet[i].x;
		loRetx[i]->y = loRet[i].y;

		loRetx[i]->obj_id = loRet[i].obj_id;
		loRetx[i]->prob = loRet[i].prob;
		loRetx[i]->track_id = loRet[i].track_id;
	}

	return loRetx;
}
