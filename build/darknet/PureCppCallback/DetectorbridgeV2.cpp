#include "stdafx.h"
#include "DetectorbridgeV2.hpp"
#include "Jzon.h"
IDetector * gPDetector = NULL;
volatile LONGLONG  gPDetectorlock = 0;
CCriticalSection gDetectorlock;
DetectorbridgeV2::DetectorbridgeV2(std::string cfg_filename, std::string  weight_filename)
{
	this->init(cfg_filename, weight_filename, 0);
}

DetectorbridgeV2::DetectorbridgeV2()
{
	this->init("", "", 0);
}

DetectorbridgeV2::DetectorbridgeV2(std::string cfg_filename, std::string  weight_filename, int gpu_id)
{

	this->init(cfg_filename, weight_filename, gpu_id);
}

bool DetectorbridgeV2::init(std::string cfg_filename, std::string  weight_filename, int gpu_id)
{
	IDetector * lpDetctor = NULL;

	if (NULL == gPDetector)
	{
		CAutoLock(gDetectorlock);
		try
		{
			if (NULL == gPDetector)
			{
				if (cfg_filename.empty())
				{
					cfg_filename = "./cfg/yolo-voc.cfg";
				}

				if (weight_filename.empty())
				{
					weight_filename = "./yolo-voc.weights";
				}

				if (gPDetector == NULL)
				{
					int lnId = ::GetCurrentThreadId();
					gPDetector = (IDetector *)getInstane(cfg_filename, weight_filename, gpu_id);
					lpDetctor = gPDetector;
				}
			}
		
		}
		catch (CMemoryException* e)
		{
			return false;
		}
		catch (CFileException* e)
		{
			return false;
		}
		catch (CException* e)
		{
			return false;
		}
	}
	else
	{
		return true;
	}

	return (lpDetctor != NULL);
}

DetectorbridgeV2::~DetectorbridgeV2()
{

}

std::vector<bbox_t> DetectorbridgeV2::detectPerson(cv::Mat & mat, float thresh /*= 0.2*/, bool use_mean /*= false*/)
{
	std::vector<bbox_t> loRet;

	if (this->check())
	{
		std::vector<bbox_t> loRet2 = gPDetector->detect(mat, thresh, use_mean);

		for (int i = 0; i < loRet2.size(); i++)
		{
			if (loRet2[i].obj_id == 14)
			{
				loRet.push_back(loRet2[i]);
			}
		}
	}

	return loRet;
}

bool DetectorbridgeV2::detectPerson_simple(cv::Mat & mat, float thresh, bool use_mean)
{
	if (this->check())
	{

		try
		{
			std::vector<bbox_t> loRet2 = gPDetector->detect(mat, thresh, use_mean);

			for (int i = 0; i < loRet2.size(); i++)
			{
				if (loRet2[i].obj_id == 14)
				{
					return true;
				}
			}
		}
		catch (CMemoryException* e)
		{
			return false;
		}
		catch (CFileException* e)
		{
			return false;
		}
		catch (CException* e)
		{
			return false;
		}
	}

	return false;
}

#include <gdiplus.h>
#pragma comment (lib,"Gdiplus.lib")
using namespace System::Drawing;
using namespace System::Drawing::Imaging;


System::Drawing::Bitmap^ DetectorbridgeV2::MatToBitmap(const cv::Mat& img)
{
	PixelFormat fmt(PixelFormat::Format24bppRgb);
	System::Drawing::Bitmap ^bmpimg = gcnew System::Drawing::Bitmap(img.cols, img.rows, fmt); //unfortunately making this variable global didn't help
	BitmapData ^data = bmpimg->LockBits(System::Drawing::Rectangle(0, 0, img.cols, img.rows), ImageLockMode::WriteOnly, fmt);
	byte *dstData = reinterpret_cast<byte*>(data->Scan0.ToPointer());
	unsigned char *srcData = img.data;
	for (int row = 0; row < data->Height; ++row)
		memcpy(reinterpret_cast<void*>(&dstData[row*data->Stride]), reinterpret_cast<void*>(&srcData[row*img.step]), img.cols*img.channels());
	bmpimg->UnlockBits(data);
	return bmpimg;
}

bool DetectorbridgeV2::detectPerson_simple_remote(CString & astrFileName, int anPort,
	cv::Mat & mat, float thresh /*= 0.2*/, bool use_mean /*= false*/)
{

	try
	{

		cv::imwrite(astrFileName.GetBuffer(0), mat);
		System::String^ lstrImage0FileName = gcnew System::String(astrFileName);
		lstrImage0FileName = System::IO::Path::GetFullPath(lstrImage0FileName);
		array<unsigned char>^ bytes = System::Text::Encoding::ASCII->GetBytes(lstrImage0FileName);
		System::String^ lstrUtf8Img0 = System::Text::Encoding::UTF8->GetString(bytes);

		CString lstrCString;
		lstrCString.Format(_T("http://127.0.0.1:%d/diff?pic0=%s"), anPort, lstrUtf8Img0);

		System::String^ lstrUrl = gcnew System::String(lstrCString.GetBuffer(0));
		System::Net::WebClient^ loClient = gcnew System::Net::WebClient();
		System::IO::Stream^ loData = loClient->OpenRead(lstrUrl);
		System::IO::StreamReader^ loReader = gcnew System::IO::StreamReader(loData);
		System::String^ s = loReader->ReadToEnd();

		System::Int32 lnRet = 0;
		System::Int32::TryParse(s, lnRet);
		loReader->Close();
		if (lnRet > 0)
		{
			return true;
		}
	}
	catch (CMemoryException* e)
	{

	}
	catch (CFileException* e)
	{
	}
	catch (CException* e)
	{
	}
	catch (System::Exception^ e)
	{

	}

	return false;
}

bool DetectorbridgeV2::detectPerson_simple_remote_by_buffer(CString & astrFileName,
	CString& astrDetectServerIP,
	int anPort,
	cv::Mat & mat,
	float thresh /*= 0.2*/,
	bool use_mean /*= false*/,
	System::Drawing::Imaging::ImageFormat^ apForamt)
{
	System::Drawing::Bitmap^ loBitMap = nullptr;

	try
	{
		//cv::imwrite(astrFileName.GetBuffer(0), mat);
		loBitMap = this->MatToBitmap(mat);
		System::IO::MemoryStream^ loBufferInMemory = gcnew System::IO::MemoryStream();
		CString lstrImgFormat = "Bmp";
		if (apForamt == nullptr)
		{
			apForamt = System::Drawing::Imaging::ImageFormat::Bmp;
		}

		if (apForamt == System::Drawing::Imaging::ImageFormat::Bmp)
		{
			lstrImgFormat = "Bmp";
		}
		else if (apForamt == System::Drawing::Imaging::ImageFormat::Jpeg)
		{
			lstrImgFormat = "Jpeg";
		}
		else if (apForamt == System::Drawing::Imaging::ImageFormat::Png)
		{
			lstrImgFormat = "Png";
		}
		else if (apForamt == System::Drawing::Imaging::ImageFormat::Tiff)
		{
			lstrImgFormat = "Tiff";
		}
		else if (apForamt == System::Drawing::Imaging::ImageFormat::Wmf)
		{
			lstrImgFormat = "Wmf";
		}

		System::Drawing::Imaging::ImageFormat^ loFormat = apForamt;
		loBitMap->Save(loBufferInMemory, loFormat);
		//loBitMap->Save(gcnew System::String("test.bmp"));

		array<unsigned char>^ loBmpData = loBufferInMemory->ToArray();
#ifdef _DEBUG
		System::IO::File::WriteAllBytes("to_detect_person.bmp", loBmpData);
#endif // _DEBUG

		System::String^ lstrImage0FileName = gcnew System::String(astrFileName);
		lstrImage0FileName = System::IO::Path::GetFullPath(lstrImage0FileName);
		array<unsigned char>^ bytes = System::Text::Encoding::ASCII->GetBytes(lstrImage0FileName);
		System::String^ lstrUtf8Img0 = System::Text::Encoding::UTF8->GetString(bytes);

		CString lstrCString;
		if (astrDetectServerIP.IsEmpty())
		{
			astrDetectServerIP = "127.0.0.1";
		}
		lstrCString.Format(_T("http://%s:%d/diff?pic0=%s&format=%s&w=%d&h=%d&c=%d"),
			astrDetectServerIP,
			anPort,
			lstrUtf8Img0,
			lstrImgFormat,
			loBitMap->Width,
			loBitMap->Height,
			3);


		System::String^ lstrUrl = gcnew System::String(lstrCString.GetBuffer(0));
		System::Net::HttpWebRequest^ loClient = (System::Net::HttpWebRequest^)System::Net::WebRequest::Create(lstrUrl);
		loClient->Method = "POST";
		loClient->ContentType = "multipart/form-data";
		loClient->ContentLength = loBmpData->Length;
		System::IO::Stream^ loPostStream = loClient->GetRequestStream();
		loPostStream->Write(loBmpData, 0, loBmpData->Length);
		loPostStream->Flush();
		loPostStream->Close();

		System::IO::Stream^ loData = loClient->GetResponse()->GetResponseStream();
		System::IO::StreamReader^ loReader = gcnew System::IO::StreamReader(loData);
		System::String^ s = loReader->ReadToEnd();

		System::Int32 lnRet = 0;
		System::Int32::TryParse(s, lnRet);
		loReader->Close();
		delete loClient;
		if (lnRet > 0)
		{
			return true;
		}
	}
	catch (CMemoryException* e)
	{

	}
	catch (CFileException* e)
	{
	}
	catch (CException* e)
	{
	}
	catch (System::Exception^ e)
	{

	}
	finally{
		if (nullptr != loBitMap)
		{
			delete loBitMap;
		}
	}

	return false;
}

double DetectorbridgeV2::search_remote_by_buffer(CString & astrFileName,
	CString& astrDetectServerIP, 
	int anDetectServerPort, 
	cv::Mat & mat, 
	int anCamIndex,
	cv::Rect & arefRect,
	std::vector<int> & anFeature,
	float thresh /*= 0.2*/, 	
	bool use_mean /*= false*/, 	
System::Drawing::Imaging::ImageFormat^ apForamt /*= System::Drawing::Imaging::ImageFormat::Bmp*/)
{
	System::Drawing::Bitmap^ loBitMap = nullptr;

	try
	{
		//cv::imwrite(astrFileName.GetBuffer(0), mat);
		loBitMap = this->MatToBitmap(mat);
		System::IO::MemoryStream^ loBufferInMemory = gcnew System::IO::MemoryStream();
		CString lstrImgFormat = "bmp";
		if (apForamt == nullptr)
		{
			apForamt = System::Drawing::Imaging::ImageFormat::Bmp;
		}

		if (apForamt == System::Drawing::Imaging::ImageFormat::Bmp)
		{
			lstrImgFormat = "bmp";
		}
		else if (apForamt == System::Drawing::Imaging::ImageFormat::Jpeg)
		{
			lstrImgFormat = "jpeg";
		}
		else if (apForamt == System::Drawing::Imaging::ImageFormat::Png)
		{
			lstrImgFormat = "png";
		}
		else if (apForamt == System::Drawing::Imaging::ImageFormat::Tiff)
		{
			lstrImgFormat = "tiff";
		}
		else if (apForamt == System::Drawing::Imaging::ImageFormat::Wmf)
		{
			lstrImgFormat = "wmf";
		}

		System::Drawing::Imaging::ImageFormat^ loFormat = apForamt;
		loBitMap->Save(loBufferInMemory, loFormat);

		array<unsigned char>^ loBmpData = loBufferInMemory->ToArray();
#ifdef _DEBUG
		System::IO::File::WriteAllBytes("to_search.bmp", loBmpData);
#endif // _DEBUG

		System::String^ lstrImage0FileName = gcnew System::String(astrFileName);	
		array<unsigned char>^ bytes = System::Text::Encoding::ASCII->GetBytes(lstrImage0FileName);
		System::String^ lstrUtf8Img0 = System::Text::Encoding::UTF8->GetString(bytes);

		CString lstrCString;
		if (astrDetectServerIP.IsEmpty())
		{
			astrDetectServerIP = "127.0.0.1";
		}
		CString lstrImageUTF8FileName = lstrUtf8Img0;
		CString lstrFeature;
		
		for (int i=0;i<anFeature.size();i++)
		{		
			char buffer[20] = { 0 };
			lstrFeature.Append(_itoa(anFeature[i],buffer,10));
			lstrFeature.Append(",");
		}


		System::String^ lstrDotNetStringFeature= gcnew System::String(lstrFeature);
		bytes = System::Text::Encoding::ASCII->GetBytes(lstrDotNetStringFeature);
		lstrDotNetStringFeature = System::Text::Encoding::UTF8->GetString(bytes);

#ifdef _DEBUG
		lstrCString.Format(_T("http://%s:%d/search/?f=%s&w=%d&h=%d&c=%d&x=%d&y=%d&feature=%s&debug=true&format=%s"),
			astrDetectServerIP,
			anDetectServerPort,
			lstrImageUTF8FileName,
			loBitMap->Width,
			loBitMap->Height,
			anCamIndex,
			arefRect.x,
			arefRect.y,
			lstrDotNetStringFeature,
			lstrImgFormat);
#else
		lstrCString.Format(_T("http://%s:%d/search/?f=%s&w=%d&h=%d&c=%d&x=%d&y=%d&feature=%s&debug=true&format=%s"),
			astrDetectServerIP,
			anDetectServerPort,
			lstrImageUTF8FileName,
			loBitMap->Width,
			loBitMap->Height,
			anCamIndex,
			arefRect.x,
			arefRect.y,
			lstrDotNetStringFeature,
			lstrImgFormat);
#endif // _DEBUG



		System::String^ lstrUrl = gcnew System::String(lstrCString.GetBuffer(0));
		System::Net::HttpWebRequest^ loClient = (System::Net::HttpWebRequest^)System::Net::WebRequest::Create(lstrUrl);
		loClient->Method = "POST";
		loClient->ContentType = "multipart/form-data";
		loClient->ContentLength = loBmpData->Length;
		System::IO::Stream^ loPostStream = loClient->GetRequestStream();
		loPostStream->Write(loBmpData, 0, loBmpData->Length);
		loPostStream->Flush();
		loPostStream->Close();

		System::IO::Stream^ loData = loClient->GetResponse()->GetResponseStream();
		System::IO::StreamReader^ loReader = gcnew System::IO::StreamReader(loData);
		System::String^ s = loReader->ReadToEnd();

		System::Double lnRet = 0;
		System::Double::TryParse(s, lnRet);
		loReader->Close();
		delete loClient;
		
		return lnRet;
	}
	catch (CMemoryException* e)
	{

	}
	catch (CFileException* e)
	{
	}
	catch (CException* e)
	{
	}
	catch (System::Exception^ e)
	{

	}
	finally{
		if (nullptr != loBitMap)
		{
			delete loBitMap;
		}
	}

	return 0;
}

bool DetectorbridgeV2::detectPerson_simple_remote_by_buffer_v2(CString & astrFileName,
	int anPort,
	cv::Mat & mat,
	std::vector<bbox_t>& arefRet,
	float thresh /*= 0.2*/,
	bool use_mean /*= false*/,
	System::Drawing::Imaging::ImageFormat^ apForamt)
{
	System::Drawing::Bitmap^ loBitMap = nullptr;

	try
	{
		//cv::imwrite(astrFileName.GetBuffer(0), mat);
		loBitMap = this->MatToBitmap(mat);
		System::IO::MemoryStream^ loBufferInMemory = gcnew System::IO::MemoryStream();
		CString lstrImgFormat = "Bmp";
		if (apForamt == nullptr)
		{
			apForamt = System::Drawing::Imaging::ImageFormat::Bmp;
		}

		if (apForamt == System::Drawing::Imaging::ImageFormat::Bmp)
		{
			lstrImgFormat = "Bmp";
		}
		else if (apForamt == System::Drawing::Imaging::ImageFormat::Jpeg)
		{
			lstrImgFormat = "Jpeg";
		}
		else if (apForamt == System::Drawing::Imaging::ImageFormat::Png)
		{
			lstrImgFormat = "Png";
		}
		else if (apForamt == System::Drawing::Imaging::ImageFormat::Tiff)
		{
			lstrImgFormat = "Tiff";
		}
		else if (apForamt == System::Drawing::Imaging::ImageFormat::Wmf)
		{
			lstrImgFormat = "Wmf";
		}

		System::Drawing::Imaging::ImageFormat^ loFormat = apForamt;
		loBitMap->Save(loBufferInMemory, loFormat);
		//loBitMap->Save(gcnew System::String("test.bmp"));

		array<unsigned char>^ loBmpData = loBufferInMemory->ToArray();
#ifdef _DEBUG
		System::IO::File::WriteAllBytes("to_detect_person.bmp", loBmpData);
#endif // _DEBUG

		System::String^ lstrImage0FileName = gcnew System::String(astrFileName);
		lstrImage0FileName = System::IO::Path::GetFullPath(lstrImage0FileName);
		array<unsigned char>^ bytes = System::Text::Encoding::ASCII->GetBytes(lstrImage0FileName);
		System::String^ lstrUtf8Img0 = System::Text::Encoding::UTF8->GetString(bytes);

		CString lstrCString;
		lstrCString.Format(_T("http://127.0.0.1:%d/diff?pic0=%s&format=%s&w=%d&h=%d&c=%d&rect=1"),
			anPort,
			lstrUtf8Img0,
			lstrImgFormat,
			loBitMap->Width,
			loBitMap->Height,
			3);


		System::String^ lstrUrl = gcnew System::String(lstrCString.GetBuffer(0));
		System::Net::HttpWebRequest^ loClient = (System::Net::HttpWebRequest^)System::Net::WebRequest::Create(lstrUrl);
		loClient->Method = "POST";
		loClient->ContentType = "multipart/form-data";
		loClient->ContentLength = loBmpData->Length;
		System::IO::Stream^ loPostStream = loClient->GetRequestStream();
		loPostStream->Write(loBmpData, 0, loBmpData->Length);
		loPostStream->Flush();
		loPostStream->Close();

		System::IO::Stream^ loData = loClient->GetResponse()->GetResponseStream();
		System::IO::StreamReader^ loReader = gcnew System::IO::StreamReader(loData);
		System::String^ s = loReader->ReadToEnd();
		CString lstrCRet = s;
		std::string lstrRet = lstrCRet.GetBuffer(0);
		arefRet.clear();
		int lnRet = 0;
		try
		{
			Jzon::Parser loParser;
			Jzon::Node loNode = loParser.parseString(lstrRet);
			if (loNode.isArray())
			{
				for (Jzon::Node::iterator lpIt = loNode.begin(); lpIt != loNode.end(); lpIt++)
				{
					if (loNode.isNumber())
					{
						bbox_t loBox;
						loBox.w = lpIt->second.get("w").toInt();
						loBox.h = lpIt->second.get("h").toInt();
						loBox.x = lpIt->second.get("x").toInt();
						loBox.y = lpIt->second.get("y").toInt();
						loBox.obj_id = lpIt->second.get("obj_id").toInt();
						loBox.track_id = lpIt->second.get("track_id").toInt();
						loBox.prob = lpIt->second.get("prob").toFloat();
						arefRet.push_back(loBox);
						if (loBox.obj_id == 14)
						{
							lnRet = 1;
						}
					}
				}


			}
		}
		catch (CMemoryException* e)
		{

		}
		catch (CFileException* e)
		{
		}
		catch (CException* e)
		{
		}





		loReader->Close();

		delete loClient;
		if (lnRet > 0)
		{
			return true;
		}
	}
	catch (CMemoryException* e)
	{

	}
	catch (CFileException* e)
	{
	}
	catch (CException* e)
	{
	}
	catch (System::Exception^ e)
	{

	}
	finally{
		if (nullptr != loBitMap)
		{
			delete loBitMap;
		}
	}

	return false;
}

cli::array<System::Byte>^ DetectorbridgeV2::convertMatToImageByteArray(const cv::Mat& mat,
	System::Drawing::Imaging::ImageFormat^ apForamt /*= System::Drawing::Imaging::ImageFormat::Bmp*/)
{
	array<unsigned char>^ loBmpData = gcnew array<unsigned char>(0);
	if (!mat.empty())
	{
		System::Drawing::Bitmap^ loBitMap = DetectorbridgeV2::MatToBitmap(mat);
		System::IO::MemoryStream^ loBufferInMemory = gcnew System::IO::MemoryStream();
		CString lstrImgFormat = "Bmp";
		if (apForamt == nullptr)
		{
			apForamt = System::Drawing::Imaging::ImageFormat::Bmp;
		}

		if (apForamt == System::Drawing::Imaging::ImageFormat::Bmp)
		{
			lstrImgFormat = "Bmp";
		}
		else if (apForamt == System::Drawing::Imaging::ImageFormat::Jpeg)
		{
			lstrImgFormat = "Jpeg";
		}
		else if (apForamt == System::Drawing::Imaging::ImageFormat::Png)
		{
			lstrImgFormat = "Png";
		}
		else if (apForamt == System::Drawing::Imaging::ImageFormat::Tiff)
		{
			lstrImgFormat = "Tiff";
		}
		else if (apForamt == System::Drawing::Imaging::ImageFormat::Wmf)
		{
			lstrImgFormat = "Wmf";
		}

		System::Drawing::Imaging::ImageFormat^ loFormat = apForamt;
		loBitMap->Save(loBufferInMemory, loFormat);
		loBmpData = loBufferInMemory->ToArray();
		
	}
	return loBmpData;
}

bool DetectorbridgeV2::detectPerson_opencv(cv::HOGDescriptor & hog,
	cv::Mat & mat,
	std::vector<cv::Rect> & found,
	std::vector<cv::Rect> & found_filtered,
	cv::Scalar & loColor)
{
	found.clear();
	found_filtered.clear();
	hog.detectMultiScale(mat, found, 0, cv::Size(8, 8), cv::Size(32, 32), 1.05, 2);

	for (int i = 0; i < found.size(); i++)
	{
		int j = 0;
		cv::Rect r = found[i];
		for (j = 0; j < found.size(); j++)
			if (j != i && (r & found[j]) == r)
				break;
		if (j == found.size())
			found_filtered.push_back(r);
	}

	if (found_filtered.size() > 0)
	{
		return true;
	}

	return false;

	/*for (int i = 0; i < found_filtered.size(); i++)
	{
		cv::Rect r = found_filtered[i];
		// the HOG detector returns slightly larger rectangles than the real objects.
		// so we slightly shrink the rectangles to get a nicer output.
		r.x += cvRound(r.width*0.1);
		r.width = cvRound(r.width*0.8);
		r.y += cvRound(r.height*0.07);
		r.height = cvRound(r.height*0.8);

		cv::Rect loRect = r;
		/ *
		loRect.x = loRect.x*ldblRatio;
		loRect.y = loRect.y*ldblRatio;
		loRect.width = loRect.width*ldblRatio;
		loRect.height = loRect.height*ldblRatio* /;

		cv::rectangle(mat, loRect, loColor, 3);
	}*/
}

void DetectorbridgeV2::PutText(IplImage* image, const char *msg, CvPoint point, CvScalar color, CvScalar aoSize)
{
	if (this->check())
	{
		::PutText(image, msg, point, color, aoSize);
	}
}

std::vector<bbox_t> DetectorbridgeV2::detect(cv::Mat mat, float thresh, bool use_mean)
{
	if (this->check())
	{
		std::vector<bbox_t> loRet = gPDetector->detect(mat, thresh, use_mean);

		return loRet;
	}
	else
	{
		return std::vector<bbox_t>();
	}

}

std::vector<bbox_t> DetectorbridgeV2::detect(std::string filename, float thresh /*= 0.2*/, bool use_mean /*= false*/)
{
	if (this->check())
	{
		std::vector<bbox_t> loRet = gPDetector->detect(filename, thresh, use_mean);

		return loRet;
	}
	else
	{
		return std::vector<bbox_t>();
	}
}

std::vector<bbox_t> DetectorbridgeV2::detect(unsigned char const *buffer, int len, int channels, float thresh /*= 0.2*/, bool use_mean /*= false*/)
{
	if (this->check())
	{
		std::vector<bbox_t> loRet = gPDetector->detect(buffer, len, channels, thresh, use_mean);
		return loRet;
	}
	else
	{
		return std::vector<bbox_t>();
	}
}



bool DetectorbridgeV2::check()
{
	return this->init("", "", 0);
}