#pragma once
using namespace System::Threading;
public ref class CliAutoLock {
	Object^ m_pObject;
public:
	CliAutoLock(Object ^ pObject) : m_pObject(pObject) {
		Monitor::Enter(m_pObject);
	}
	~CliAutoLock() {
		Monitor::Exit(m_pObject);
	}
};
public ref class CPurePlayer
{
public:
	CPurePlayer();
	virtual ~CPurePlayer();
	
	HWND hWnd();
	void hWnd(HWND val);
	System::Threading::Thread^ Thread();
	void Thread(System::Threading::Thread^ val);
	System::String^ UserName();
	void UserName(System::String^ val);
	System::String^ Password();
	void Password(System::String^ val);
	System::String^ IP();
	void IP(System::String^ val);
	BOOL IgnorePerson();
	void IgnorePerson(BOOL val);
	int Port();
	void Port(int val);
	int DetectServerPort();
	void DetectServerPort(int val);
	System::String^ DetectServerIP();
	void DetectServerIP(System::String^ val);
	int UserID() ;
	void UserID(int val);
	BOOL ExitThread();
	void ExitThread(BOOL val);
	BOOL ShouldStopLive();
	void ShouldStopLive(BOOL val);
	double Distance_ratio();
	void Distance_ratio(double val);
	int SourceMode();
	void SourceMode(int val);
	int CamIndex();
	void CamIndex(int val);
	System::String^ CamSrc();
	void CamSrc(System::String^ val);
	BOOL DisplayGreenRect();
	void DisplayGreenRect(BOOL val);
	BOOL Ignore();
	void Ignore(BOOL val);
	void LiveThreadEntry();
	BOOL live(HWND ahWnd);
	BOOL stop();
	BOOL play();
	
	static CPurePlayer^ GetCPurePlayer(int64 ahHandle);
	static void RemoveCPurePlayer(int64 ahHandle);
private:
	System::Threading::Thread^ m_pThread;
	System::String^ m_strOpenRTSP_STREAM_LOCK = "OpenRTSP_STREAM_LOCK";
	System::String^ m_strUserName = "admin";
	static System::Collections::Generic::Dictionary<int64, CPurePlayer^>^ g_oPlayers
		= gcnew System::Collections::Generic::Dictionary<int64, CPurePlayer ^>();
	static System::String^ g_strPlayerMapLock = "m_strPlayerMapLock";
	System::String^ m_strPassword = "admin6666";
	System::String^ m_strIP = "127.0.0.1";
	System::String^ m_strShouldStopLock = "m_strShouldStopLock";
	BOOL m_bIgnorePerson = FALSE;
	int m_nPort = 8000;
	int m_nDetectServerPort = 18081;
	System::String^ m_strDetectServerIP = "127.0.0.1";
	int m_nUserID =-1;
	HWND m_hWnd= NULL;
	BOOL m_bExitThread = FALSE;
	BOOL m_bStopLive = FALSE;

	HANDLE m_oNotifyDetect = NULL;

	int img_Width =0;
	int img_height = 0;
	int m_nDistance_c_max = 0;
	int ROI_X = 0, ROI_Y = 0;
	int m_nFrameInterval = 20;
	double m_nDistance_ratio=1.0;
	BOOL m_bLeaked = FALSE;
	int m_nDetectMinArea= 100;
	int m_nSourceMode= 0;
	int m_nCamIndex = 0;
	System::String^ m_strCamSrc = "";
	
	BOOL m_nDisplayGreenRect = FALSE;
	BOOL m_nIgnore = FALSE;
};

