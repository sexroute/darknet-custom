#include "stdafx.h"
#include "app.h"

BOOL CApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	//Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

//	BOOL lbRet = NET_DVR_Init();          //Init SDK

	m_pMainWnd = new CMainWindow;
	m_pMainWnd->ShowWindow(SW_HIDE);
	m_pMainWnd->UpdateWindow();
	return TRUE;
}

int CApp::ExitInstance()
{
	NET_DVR_Cleanup();
	return CWinApp::ExitInstance();
}

BEGIN_MESSAGE_MAP(CMainWindow, CFrameWnd)
	ON_WM_PAINT()
END_MESSAGE_MAP()

CMainWindow::CMainWindow()
{
   BOOL lbRet = 	Create(NULL, _T("The Hello Application"));
}

void CMainWindow::OnPaint()
{
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);
	dc.DrawText(_T("Hello, MFC"), -1, &rect,
		DT_SINGLELINE | DT_CENTER | DT_VCENTER);
}

CApp theApp;