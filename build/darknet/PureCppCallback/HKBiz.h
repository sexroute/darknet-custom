#pragma once
#include "stdafx.h"
class HKBIZ
{
public:
	HKBIZ();
	virtual ~HKBIZ();
	static void CALLBACK fRealDataCallBack(LONG lRealHandle, DWORD dwDataType, BYTE *pBuffer, DWORD dwBufSize, void *pUser);
	static int CALLBACK yv12toYUV(char *outYuv, char *inYv12, int width, int height, int widthStep);
	static void CALLBACK InnerDecCBFun(long nPort,
		char * pBuf,
		long nSize,
		FRAME_INFO * pFrameInfo,
		long nReserved1,
		long nReserved2);
	LONG Port() const;
	void Port(LONG val);
	HWND Wnd() const;
	void Wnd(HWND val);
	static void Init();
	static BOOL m_bInited;
	int m_hRealPlayHandle;
private:
	LONG m_nPort;
	HWND m_hWnd;
};
