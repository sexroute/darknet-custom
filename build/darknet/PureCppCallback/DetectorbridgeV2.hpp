#pragma once
#include "stdafx.h"
class DetectorbridgeV2
{
	// TODO: Add your methods for this class here.
public:
	DetectorbridgeV2();
	DetectorbridgeV2(std::string cfg_filename, std::string weight_filename);
	DetectorbridgeV2(std::string cfg_filename, std::string weight_filename, int gpu_id);
	virtual ~DetectorbridgeV2();
	std::vector<bbox_t> detectPerson(cv::Mat & mat, float thresh = 0.2, bool use_mean = false);
	bool detectPerson_simple(cv::Mat & mat, float thresh = 0.2, bool use_mean = false);
	static System::Drawing::Bitmap^ MatToBitmap(const cv::Mat& img);
	bool detectPerson_simple_remote(CString & astrFileName, int anPort, cv::Mat & mat, float thresh = 0.2, bool use_mean = false);
	bool detectPerson_simple_remote_by_buffer(CString & astrFileName, 
		CString& astrDetectServerIP,
		int anDetectServerPort,
		cv::Mat & mat, 
		float thresh = 0.2, 
		bool use_mean = false, 
		System::Drawing::Imaging::ImageFormat^ apForamt = System::Drawing::Imaging::ImageFormat::Bmp);

	double search_remote_by_buffer(CString & astrFileName,
		CString& astrDetectServerIP,
		int anDetectServerPort,
		cv::Mat & mat,
		int anCamIndex,
		cv::Rect & arefRect,
		std::vector<int> & anFeature,
		float thresh = 0.2,		
		bool use_mean = false,		
		System::Drawing::Imaging::ImageFormat^ apForamt = System::Drawing::Imaging::ImageFormat::Bmp);

	bool detectPerson_simple_remote_by_buffer_v2(CString & astrFileName, int anPort, cv::Mat & mat, std::vector<bbox_t>& arefRet,float thresh = 0.2, bool use_mean = false,
		System::Drawing::Imaging::ImageFormat^ apForamt = System::Drawing::Imaging::ImageFormat::Bmp);

	static array<System::Byte>^ convertMatToImageByteArray(const cv::Mat& img, System::Drawing::Imaging::ImageFormat^ apForamt = System::Drawing::Imaging::ImageFormat::Bmp);

	bool detectPerson_opencv(cv::HOGDescriptor & hog, cv::Mat & mat,
		std::vector<cv::Rect> & found = std::vector<cv::Rect>(),
		std::vector<cv::Rect> & filtered = std::vector<cv::Rect>(),
		cv::Scalar & loColor = cv::Scalar(0, 255, 0));
	void PutText(IplImage*  image, const char *msg, CvPoint  point, CvScalar  color,CvScalar aoSize);
/*
	std::vector<bbox_t> detect(std::string image_filename, float thresh);
	std::vector<bbox_t> detect(image_t img, float thresh);
*/
	std::vector<bbox_t> detect(cv::Mat mat, float thresh = 0.2, bool use_mean = false);
	std::vector<bbox_t> detect(std::string filename, float thresh = 0.2, bool use_mean = false);
	std::vector<bbox_t> detect(unsigned char const *buffer, int len, int channels, float thresh = 0.2, bool use_mean = false);
	static bool init(std::string cfg_filename, std::string weight_filename, int gpu_id);
protected:
	
	bool check();
	void * getDetector();

};



