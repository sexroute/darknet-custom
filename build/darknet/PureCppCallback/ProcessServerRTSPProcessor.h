#pragma once
#include "stdafx.h"
#include "callback_export_api.h"
#include "..\..\..\src\interface.hpp"
#include "DetectorbridgeV2.hpp"
public  class ProcessServerRTSPProcessor
{
	struct sortstruct
	{
		// sortstruct needs to know its containing object
		ProcessServerRTSPProcessor* m;
		sortstruct(ProcessServerRTSPProcessor* p) : m(p) {};

		// this is our sort function, which makes use
		// of some non-static data (sortascending)
		bool operator() (const std::vector<cv::Point> & m1, const std::vector<cv::Point> & m2);
	};
public:
	static CString MakeUuidString();
	ProcessServerRTSPProcessor();
	double correlation(cv::Mat &mat1, cv::Mat &mat2);
	virtual ~ProcessServerRTSPProcessor();

	CString UserName();
	void UserName(CString val);
	CString Password();
	void Password(CString val);
	CString IP();
	void IP(CString val);
	int Port();
	void Port(int val);
	int UserID();
	void UserID(int val);
	virtual bool startLive();
	virtual void stopLive();
	virtual bool detectLeak(DetectorbridgeV2& loBridge);
	virtual std::vector<cv::Rect> detectLeak(cv::Mat & arefData1, cv::Mat & arefData2, int ln_zoom_size = 1, int ab_only_detect = FALSE);
	virtual bool detectLeakV2(DetectorbridgeV2& loBridge, std::vector<bbox_t> & arefRet);
	virtual bool pushAlarmPacketDataToMSQ(cv::Mat & imageOriginal, cv::Mat & detected,int anAlarmType, CString & astrUrl);
	virtual bool pushNormalPacketDataToMSQ(cv::Mat & imageOriginal,CString & astrUrl);
	virtual bool pushHeartBeatMSQ(CString & astrUrl);
	virtual bool pushConfigMSQ(CString & astrUrl);
	std::vector<int> getMapIndex(cv::Mat & arefWholeMap, cv::Rect & arefRect,int anSplitCount);
	std::vector<std::vector<cv::Point>> detect_overflap_merge(cv::Mat & img, std::vector<std::vector<cv::Point>>& contours);
	double compute_distance(const std::vector<cv::Point> & c);
	bool less_cmp(const std::vector<cv::Point> & m1, const std::vector<cv::Point> & m2);
	bool live(HWND hwnd);
	HWND HWnd();
	void HWnd(HWND val);
	static  UINT AFX_CDECL liveThread(LPVOID apParameter);
	static  UINT AFX_CDECL DetectLeakThread(LPVOID apParameter);
	BOOL ExitThread();
	void ExitThread(BOOL val);
	BOOL ShouldStopLive();
	void ShouldStopLive(BOOL val);
	FrameCallback FunCallback();
	void FunCallback(FrameCallback val);

	void PushImage(cv::Mat & arefData1, cv::Mat & arefData2);

	cv::MatND getHistogram(const cv::Mat &image, int anHistSize = 256);

	void calcMeanStdVariant(cv::Mat & value, double & mean, double &std);

	int DetectServerPort();
	void DetectServerPort(int val);
	CString DetectServerIP();
	void DetectServerIP(CString val);
	BOOL Leaked();
	void Leaked(BOOL val);
	std::vector<cv::Rect> LeakedRect();
	void LeakedRect(std::vector<cv::Rect> val);
	int FrameInterval();
	void FrameInterval(int val);
	BOOL IgnorePerson();
	void IgnorePerson(BOOL val);
	int Distance_c_max();
	void Distance_c_max(int val);
	double Distance_ratio();
	void Distance_ratio(double val);
	int DetectMinArea();
	void DetectMinArea(int val);
	OnStatusChange StatusCallback();
	void StatusCallback(OnStatusChange val);
	cv::Rect ROI();
	void ROI(cv::Rect val);
	int SourceMode();
	void SourceMode(int val);
	int CamIndex();
	void CamIndex(int val);
	CString CamSrc();
	void CamSrc(CString val);
	int DisplayGreenRect();
	void DisplayGreenRect(int val);
	BOOL Ignore();
	void Ignore(BOOL val);
	CString MSQURL();
	void MSQURL(CString val);
	int ChannelIndex();
	void ChannelIndex(int val);
	int TotalChannels();
	void TotalChannels(int val);
	CString UUID();
	void UUID(CString val);
	cv::Mat LoadHistogram(System::String^ astrFilePath);
private:
	CWinThread* m_pThread;
	CWinThread* m_pThread_DetectLeak;
	CString m_strUserName;
	CString m_strPassword;
	CString m_strIP;
	CString m_strUUID;
	BOOL m_bIgnorePerson;
	int m_nPort;
	int m_nDetectServerPort;
	CString m_strDetectServerIP;
	int m_nUserID;
	HWND m_hWnd;
	BOOL m_bExitThread;
	BOOL m_bStopLive;
	CCriticalSection m_oLock;
	HANDLE m_oNotifyDetect;
	FrameCallback m_pcallback;
	OnStatusChange m_statusCallback;
	std::queue<cv::Mat> m_oImagesToDetect;
	int img_Width;
	int img_height;
	int m_nDistance_c_max;
	int ROI_X, ROI_Y;
	int m_nFrameInterval;
	double m_nDistance_ratio;
	LONG m_bLeaked;
	int m_nDetectMinArea;
	int m_nSourceMode;
	int m_nCamIndex;
	CString m_strCamSrc;
	cv::Rect m_oROI;
	std::vector<cv::Rect> m_oLeakedRect;
	cv::Mat m_oSampleHistogram;
	cv::Mat m_oFakeHistogram;
	BOOL m_nDisplayGreenRect;
	BOOL m_nIgnore;
	CString m_strMSQURL;
	int m_nChannelIndex;
	int m_nTotalChannels;
	static IDetector * ProcessServerRTSPProcessor::g_Detector;
	static CCriticalSection ProcessServerRTSPProcessor::g_lock;
	static CCriticalSection ProcessServerRTSPProcessor::open_rtsp_stream_lock;
	
	
};

