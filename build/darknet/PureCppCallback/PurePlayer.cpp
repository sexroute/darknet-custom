#include "stdafx.h"
#include "PurePlayer.h"
#include "CvvImage.h"

using namespace cv;
using namespace Gdiplus;
CPurePlayer::CPurePlayer()
{
	this->m_strShouldStopLock = gcnew System::String("");
}


CPurePlayer::~CPurePlayer()
{
	this->stop();
}

HWND CPurePlayer::hWnd()
{
	CliAutoLock(this);
	return m_hWnd;
}

void CPurePlayer::hWnd(HWND val)
{
	CliAutoLock(this);
	m_hWnd = val;
}

System::Threading::Thread^ CPurePlayer::Thread()
{
	CliAutoLock(this);
	return m_pThread;
}

void CPurePlayer::Thread(System::Threading::Thread^ val)
{
	CliAutoLock(this);
	m_pThread = val;
}

System::String^ CPurePlayer::UserName()
{
	CliAutoLock(this);
	return m_strUserName;
}

void CPurePlayer::UserName(System::String^ val)
{
	CliAutoLock(this);
	m_strUserName = val;
}

System::String^ CPurePlayer::Password()
{
	CliAutoLock(this);
	return m_strPassword;
}

void CPurePlayer::Password(System::String^ val)
{
	CliAutoLock(this);
	m_strPassword = val;
}

System::String^ CPurePlayer::IP()
{
	CliAutoLock(this);
	return m_strIP;
}

void CPurePlayer::IP(System::String^ val)
{
	CliAutoLock(this);
	m_strIP = val;
}

BOOL CPurePlayer::IgnorePerson()
{
	CliAutoLock(this);
	return m_bIgnorePerson;
}

void CPurePlayer::IgnorePerson(BOOL val)
{
	CliAutoLock(this);
	m_bIgnorePerson = val;
}

int CPurePlayer::Port()
{
	CliAutoLock(this);
	return m_nPort;
}

void CPurePlayer::Port(int val)
{
	CliAutoLock(this);
	m_nPort = val;
}

int CPurePlayer::DetectServerPort()
{
	CliAutoLock(this);
	return m_nDetectServerPort;
}

void CPurePlayer::DetectServerPort(int val)
{
	CliAutoLock(this);
	m_nDetectServerPort = val;
}

System::String^ CPurePlayer::DetectServerIP()
{
	CliAutoLock(this);
	return m_strDetectServerIP;
}

void CPurePlayer::DetectServerIP(System::String^ val)
{
	CliAutoLock(this);
	m_strDetectServerIP = val;
}

int CPurePlayer::UserID() 
{
	CliAutoLock(this);
	return m_nUserID;
}

void CPurePlayer::UserID(int val)
{
	CliAutoLock(this);
	m_nUserID = val;
}

BOOL CPurePlayer::ExitThread()
{
	CliAutoLock(this);
	return m_bExitThread;
}

void CPurePlayer::ExitThread(BOOL val)
{
	CliAutoLock(this);
	m_bExitThread = val;
}

BOOL CPurePlayer::ShouldStopLive()
{
	CliAutoLock(this->m_strShouldStopLock);
	return m_bStopLive;
}

void CPurePlayer::ShouldStopLive(BOOL val)
{
	CliAutoLock(this->m_strShouldStopLock);
	m_bStopLive = val;
}

double CPurePlayer::Distance_ratio()
{
	CliAutoLock(this);
	return m_nDistance_ratio;
}

void CPurePlayer::Distance_ratio(double val)
{
	CliAutoLock(this);
	m_nDistance_ratio = val;
}

int CPurePlayer::SourceMode()
{
	CliAutoLock(this);
	return m_nSourceMode;
}

void CPurePlayer::SourceMode(int val)
{
	CliAutoLock(this);
	m_nSourceMode = val;
}

int CPurePlayer::CamIndex()
{
	CliAutoLock(this);
	return m_nCamIndex;
}

void CPurePlayer::CamIndex(int val)
{
	CliAutoLock(this);
	m_nCamIndex = val;
}

System::String^ CPurePlayer::CamSrc()
{
	CliAutoLock(this);
	return m_strCamSrc;
}

void CPurePlayer::CamSrc(System::String^ val)
{
	CliAutoLock(this);
	m_strCamSrc = val;
}

BOOL CPurePlayer::DisplayGreenRect()
{
	CliAutoLock(this);
	return m_nDisplayGreenRect;
}

void CPurePlayer::DisplayGreenRect(BOOL val)
{
	CliAutoLock(this);
	m_nDisplayGreenRect = val;
}

BOOL CPurePlayer::Ignore()
{
	CliAutoLock(this);
	return m_nIgnore;
}

void CPurePlayer::Ignore(BOOL val)
{
	CliAutoLock(this);
	m_nIgnore = val;
}

void CPurePlayer::LiveThreadEntry()
{
	BOOL lbShouldStopLive = this->ShouldStopLive();

	while (!lbShouldStopLive)
	{
		HWND lhWnd = this->hWnd();
		this->live(lhWnd);
		lbShouldStopLive = this->ShouldStopLive();
	}
}

BOOL CPurePlayer::live(HWND ahWnd)
{
	cv::VideoCapture vcap;
	cv::Mat image_resized;
	cv::Mat previous_image_large;
	cv::Mat image_large;
	cv::Mat current_image_large;
	cv::Mat image_large_thumb;
	CString lstrUrl;
	lstrUrl.Format("rtsp://%s:%s@%s:%d/mpeg4/ch1/main", this->UserName(), this->Password(), this->IP(), this->Port());
	CString lstrSrc = this->CamSrc();
	int lnCamIndex = this->CamIndex();
	BOOL lbShouldStop = this->ShouldStopLive();
	const std::string videoStreamAddress = lstrUrl.GetBuffer(0);
	{

		while (!lbShouldStop)
		{
			CliAutoLock(this->m_strOpenRTSP_STREAM_LOCK);
			int lnSrcMode = this->SourceMode();
			if (lnSrcMode <= 0 || lnSrcMode > 2)
			{
				if (vcap.open(videoStreamAddress))
				{
					break;
				}
			}
			// read from src string
			else if (lnSrcMode == 1)
			{
				if (vcap.open(lstrSrc.GetBuffer(0)))
				{
					break;
				}
			}
			// read from local camera
			else if (lnSrcMode == 2)
			{
				if (vcap.open(lnCamIndex))
				{
					break;
				}
			}

			return false;
		}
	}

	cv::Mat previous_gray;
	cv::Mat current_gray;
	cv::Mat current_gray_gauss;
	cv::Mat diff_gray;
	cv::Mat threshold_gray;
	cv::Mat threshold_gray_dilate;
	cv::Mat img_bin;
	cv::Mat out;


	cv::Mat back;
	cv::Mat fore;

	int thresval = 60;
	BOOL lbFirst = TRUE;
	
	double ldblRatio = 8;
	ULONG64 lnStep = 0;
	BOOL lnIgnore = this->Ignore();
	
	

	while (!lbShouldStop)
	{

		try
		{
			lbShouldStop = this->ShouldStopLive();
			if (!vcap.read(image_large))
			{
				if (lbShouldStop)
				{
					return true;
				}
				cv::waitKey(10);
				vcap.release();
				int lnSrcMode = this->SourceMode();
				if (lnSrcMode <= 0 || lnSrcMode > 2)
				{
					if (vcap.open(videoStreamAddress))
					{
						//break;
					}
				}
				// read from src string
				else if (lnSrcMode == 1)
				{
					if (vcap.open(lstrSrc.GetBuffer(0)))
					{
						//break;
					}
				}
				// read from local camera
				else if (lnSrcMode == 2)
				{
					if (vcap.open(lnCamIndex))
					{
						//break;
					}
				}
				continue;
			}

			cv::waitKey(10);
			Sleep(0);

			RECT loRect;
			BOOL lbRet = ::GetClientRect(ahWnd, &loRect);
			if ( ::IsWindow(ahWnd))
			{
				cv::Size winSize(loRect.right, loRect.bottom);
				cv::Mat cvImgTmp(winSize, CV_8UC3);
				cv::resize(image_large, cvImgTmp, winSize);
				IplImage iplimg = IplImage(cvImgTmp);

				CvvImage iimg;
				iimg.CopyOf(&iplimg);
				HDC loDc = ::GetDC(ahWnd);
				iimg.DrawToHDC(loDc, &loRect);
				ReleaseDC(ahWnd, loDc);
				iimg.Destroy();
			}


			lbShouldStop = this->ShouldStopLive();
		}
		catch (CMemoryException* e)
		{
			cv::waitKey(10);
			Sleep(0);
		}
		catch (CFileException* e)
		{
			cv::waitKey(10);
			Sleep(0);
		}
		catch (CException* e)
		{
			cv::waitKey(10);
			Sleep(0);
		}
		catch (...)
		{
			cv::waitKey(10);
			Sleep(0);
		}
	}
	return true;
}

BOOL CPurePlayer::stop()
{
	CliAutoLock(this);
	if (this->Thread() != nullptr)
	{
		this->ShouldStopLive(TRUE);
		boolean lbExited = FALSE;
		try
		{
			//lbExited = this->Thread()->Join(100);
		}
		catch (CMemoryException* e)
		{
			
		}
		catch (CFileException* e)
		{
		}
		catch (CException* e)
		{
		}
		catch (System::Exception^ e)
		{

		}

		if (!lbExited)
		{
			try
			{
				
				//this->Thread()->Abort();
			}
			catch (CMemoryException* e)
			{

			}
			catch (CFileException* e)
			{
			}
			catch (CException* e)
			{
			}
			catch (System::Exception^ e)
			{

			}
		}

		this->Thread(nullptr);
	}

	return TRUE;
}

BOOL CPurePlayer::play()
{
	CliAutoLock(this);
	if (this->hWnd() == NULL || !::IsWindow(this->hWnd()))
	{
		return FALSE;
	}

	if (this->Thread() == nullptr)
	{
		System::Threading::Thread^ lpThread = gcnew System::Threading::Thread(gcnew ThreadStart(this, &CPurePlayer::LiveThreadEntry));
		this->Thread(lpThread);
		lpThread->Name = "CPurePlayer";
		lpThread->Start();
	}
}

CPurePlayer^ CPurePlayer::GetCPurePlayer(int64 ahHandle)
{
	CliAutoLock loLock(CPurePlayer::g_strPlayerMapLock);
	CPurePlayer^ lpRet = nullptr;
	if (CPurePlayer::g_oPlayers->ContainsKey(ahHandle))
	{
		lpRet = CPurePlayer::g_oPlayers[ahHandle];
	}
	else
	{
		lpRet = gcnew CPurePlayer();
		CPurePlayer::g_oPlayers->Add(ahHandle, lpRet);
	}
	return lpRet;
}

void CPurePlayer::RemoveCPurePlayer(int64 ahHandle)
{
	CliAutoLock loLock(CPurePlayer::g_strPlayerMapLock);
	CPurePlayer^ lpRet = nullptr;
	if (CPurePlayer::g_oPlayers->ContainsKey(ahHandle))
	{
		CPurePlayer::g_oPlayers->Remove(ahHandle);
	}
}
