#include "stdafx.h"
#include "HKBiz.h"
#define USECOLOR

CCriticalSection gLock;
HKBIZ::HKBIZ()
{
	this->Port(-1);
}

HKBIZ::~HKBIZ()
{
	
}

void HKBIZ::fRealDataCallBack(LONG lRealHandle,
	DWORD dwDataType,
	BYTE * pBuffer,
	DWORD dwBufSize,
	void * pUser)
{
	if (pUser == NULL)
	{
		return;
	}

	HKBIZ * lpHKBiz = (HKBIZ *)pUser;

	DWORD dRet = 0;
	BOOL inData = FALSE;

	LONG nPort = lpHKBiz->Port();

	
	switch (dwDataType)
	{
	case NET_DVR_SYSHEAD:

		if (!PlayM4_GetPort(&nPort))
		{
			break;
		}
		{
			CString lstrData;
			lstrData.Format(_T("Port:%d\r\n"), nPort);
			OutputDebugString(lstrData);
		}

		lpHKBiz->Port(nPort);

		nPort = lpHKBiz->Port();

		if (!PlayM4_OpenStream(nPort, pBuffer, dwBufSize, 1024 * 1024))
		{
			dRet = PlayM4_GetLastError(nPort);
			break;
		}

		//设置解码回调函数 只解码不显示
		if (!PlayM4_SetDecCallBack(nPort, HKBIZ::InnerDecCBFun))
		{
			dRet = PlayM4_GetLastError(nPort);
			break;
		}


		//打开视频解码
		if (!PlayM4_Play(nPort, (HWND)lpHKBiz->Wnd()))
		{
			dRet = PlayM4_GetLastError(nPort);
			break;
		}
		break;

	case NET_DVR_STREAMDATA:
		inData = PlayM4_InputData(nPort, pBuffer, dwBufSize);
		while (!inData)
		{
			Sleep(10);
			inData = PlayM4_InputData(nPort, pBuffer, dwBufSize);
			OutputDebugString("PlayM4_InputData failed \n");
		}
		break;
	default:
		inData = PlayM4_InputData(nPort, pBuffer, dwBufSize);
		while (!inData)
		{
			Sleep(10);
			inData = PlayM4_InputData(nPort, pBuffer, dwBufSize);
			OutputDebugString("PlayM4_InputData failed \n");
		}
		break;
	}
}

int CALLBACK HKBIZ::yv12toYUV(char *outYuv, char *inYv12, int width, int height, int widthStep)
{
	return 0;
	int col, row;
	unsigned int Y, U, V;
	int tmp;
	int idx;
	int lnTotalBufferSize = 0;
	//printf("widthStep=%d.\n",widthStep);  

	for (row = 0; row < height; row++)
	{
		idx = row * widthStep;
		int rowptr = row*width;

		for (col = 0; col < width; col++)
		{
			//int colhalf=col>>1;  
			tmp = (row / 2)*(width / 2) + (col / 2);
			//         if((row==1)&&( col>=1400 &&col<=1600))  
			//         {   
			//          printf("col=%d,row=%d,width=%d,tmp=%d.\n",col,row,width,tmp);  
			//          printf("row*width+col=%d,width*height+width*height/4+tmp=%d,width*height+tmp=%d.\n",row*width+col,width*height+width*height/4+tmp,width*height+tmp);  
			//         }   
			Y = (unsigned int)inYv12[row*width + col];
			U = (unsigned int)inYv12[width*height + width*height / 4 + tmp];
			V = (unsigned int)inYv12[width*height + tmp];
			//         if ((col==200))  
			//         {   
			//         printf("col=%d,row=%d,width=%d,tmp=%d.\n",col,row,width,tmp);  
			//         printf("width*height+width*height/4+tmp=%d.\n",width*height+width*height/4+tmp);  
			//         return ;  
			//         }  
			if ((idx + col * 3 + 2) > (1200 * widthStep))
			{
				//printf("row * widthStep=%d,idx+col*3+2=%d.\n",1200 * widthStep,idx+col*3+2);  
			}
			outYuv[idx + col * 3] = Y;
			outYuv[idx + col * 3 + 1] = U;
			outYuv[idx + col * 3 + 2] = V;
			lnTotalBufferSize += 3;
		}
	}

	return lnTotalBufferSize;
	//printf("col=%d,row=%d.\n",col,row);  
}

void CALLBACK HKBIZ::InnerDecCBFun(long nPort, char * pBuf, long nSize, FRAME_INFO * pFrameInfo, long nReserved1, long nReserved2)
{
	long lFrameType = pFrameInfo->nType;
	if (lFrameType == T_YV12)
	{
#ifdef USECOLOR
		//int start = clock();  
		IplImage* pImgYCrCb = cvCreateImage(cvSize(pFrameInfo->nWidth, pFrameInfo->nHeight), 8, 3);//得到图像的Y分量    
		int lnTotalBufferSize = HKBIZ::yv12toYUV(pImgYCrCb->imageData, pBuf, pFrameInfo->nWidth, pFrameInfo->nHeight, pImgYCrCb->widthStep);//得到全部RGB图像  
		IplImage* pImg = cvCreateImage(cvSize(pFrameInfo->nWidth, pFrameInfo->nHeight), 8, 3);
		cvCvtColor(pImgYCrCb, pImg, CV_YCrCb2RGB);
		//int end = clock();  
#else  
		IplImage* pImg = cvCreateImage(cvSize(pFrameInfo->nWidth, pFrameInfo->nHeight), 8, 1);
		memcpy(pImg->imageData, pBuf, pFrameInfo->nWidth*pFrameInfo->nHeight);
#endif  

#ifdef USECOLOR  
		cvReleaseImage(&pImgYCrCb);
		cvReleaseImage(&pImg);
#else  
		cvReleaseImage(&pImg);
#endif 
	}
}

LONG HKBIZ::Port() const
{
	CAutoLock(gLock);
	return m_nPort;
}

void HKBIZ::Port(LONG val)
{
	CAutoLock(gLock);
	m_nPort = val;
}



HWND HKBIZ::Wnd() const
{
	CAutoLock(gLock);
	return m_hWnd;
}

void HKBIZ::Wnd(HWND val)
{
	CAutoLock(gLock);
	m_hWnd = val;
}

void HKBIZ::Init()
{
	if (HKBIZ::m_bInited==FALSE)
	{
		CAutoLock(gLock);
		if (HKBIZ::m_bInited == FALSE)
		{
			NET_DVR_Init();
			//theApp.m_pMainWnd->ShowWindow(SW_SHOW);			
			HKBIZ::m_bInited = TRUE;
		}
	}
}

BOOL HKBIZ::m_bInited = FALSE;
