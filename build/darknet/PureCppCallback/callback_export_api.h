#ifndef _CALLBACK_EXPORT_H_ 
#define _CALLBACK_EXPORT_H_

#if defined(CALLBACK_EXPORT)
#define CALLBACK_EXPORT_API  extern "C" __declspec(dllexport) 
#else 
#define CALLBACK_EXPORT_API  extern "C" __declspec(dllimport) 
#endif
typedef void(__stdcall * FrameCallback)(int width,
	int height,
	int stride, byte * buffer, int length);

typedef void(__stdcall * OnStatusChange)(int anStatus);
CALLBACK_EXPORT_API bool __stdcall live(int UserID, HWND handle, int userData, int & appRealHandle, int & errcode);
CALLBACK_EXPORT_API int __stdcall login(const char * strUserName, const char * strPassword, const char * strIP, int port);

CALLBACK_EXPORT_API bool __stdcall live_v2(int UserID, HWND handle, int userData, int & appRealHandle, int & errcode, FrameCallback callback);
CALLBACK_EXPORT_API bool __stdcall process(int UserID, HWND handle, int userData, int & appRealHandle, int & errcode, FrameCallback callback);
CALLBACK_EXPORT_API int __stdcall login_v2(const char * strUserName,
	const char * strPassword,
	const char * strIP,
	int port,
	int rtsp_port,
	const char * strDetectServerIP,
	int detect_server_port,
	int frame_Interval,
	int ignore_person,
	double distance_ratio,
	int min_area,
	OnStatusChange callback,
	int ROI_X,
	int ROI_Y,
	int ROI_W,
	int ROI_H,
	const char * strSrc,
	int nCamIndex,
	int nSrcMode,
	int nDisplayGreenRect,
	int nIgnore);

CALLBACK_EXPORT_API int __stdcall process_login(int uid,
	const char * strUserName,
	const char * strPassword,
	const char * strIP,
	int port,
	int rtsp_port,
	const char * strDetectServerIP,
	int detect_server_port,
	int frame_Interval,
	int ignore_person,
	double distance_ratio,
	int min_area,
	OnStatusChange callback,
	int ROI_X,
	int ROI_Y,
	int ROI_W,
	int ROI_H,
	const char * strSrc,
	int nCamIndex,
	int nSrcMode,
	int nDisplayGreenRect,
	int nIgnore,
	const char * strMSQURL,
	int anChannIndex,
	int anTotalChannels);

CALLBACK_EXPORT_API int __stdcall pure_play(HWND handle,
	const char * strUserName,
	const char * strPassword,
	const char * strIP,
	int port,
	int rtsp_port,
	const char * strDetectServerIP,
	int detect_server_port,
	int frame_Interval,
	int ignore_person,
	double distance_ratio,
	int min_area,
	OnStatusChange callback,
	int ROI_X,
	int ROI_Y,
	int ROI_W,
	int ROI_H,
	const char * strSrc,
	int nCamIndex,
	int nSrcMode,
	int nDisplayGreenRect,
	int nIgnore);
CALLBACK_EXPORT_API int stop_pure_play(HWND handle);
CALLBACK_EXPORT_API bool init(const char * cfg_filename, const char *  weight_filename, int gpu_id = 0);
CALLBACK_EXPORT_API bool detect_person_v2(unsigned char* img_bytes, int anImgLength, int w, int h, int c = 3, float threhold = 0.2);
CALLBACK_EXPORT_API bool detect_person(const char * cfg_filename, float threhold = 0.2);
CALLBACK_EXPORT_API void quit();

#endif