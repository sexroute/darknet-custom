#include "stdafx.h"
#include "ProcessServerRTSPProcessor.h"
#include "stdafx.h"
#include "RTSP.h"
#include "CvvImage.h"
#include "callback_export_api.h"
#include <gdiplus.h>
#include <gdiplusheaders.h>
#include "..\..\..\src\interface.hpp"
#include "rpcdce.h"
#include "DetectorbridgeV2.hpp"
//rtsp://admin:bohuaxinzhi123@192.168.1.64/h264/ch1/main
#include <opencv2\opencv.hpp>
#pragma comment( lib "Rpcrt4.lib")
using namespace cv;
using namespace Gdiplus;

IDetector * ProcessServerRTSPProcessor::g_Detector = NULL;
CCriticalSection ProcessServerRTSPProcessor::g_lock;

CString ProcessServerRTSPProcessor::MakeUuidString()
{
	CString strGUID;
	GUID    m_guid;
	::CoCreateGuid(&m_guid);
	LPOLESTR wszCLSID = NULL;
	StringFromCLSID(m_guid, &wszCLSID);
	int nLen = lstrlenW(wszCLSID);

	TCHAR* szCLSID = new TCHAR[nLen + 1];

#ifndef _UNICODE
	wcstombs(szCLSID, wszCLSID, nLen);
	szCLSID[nLen] = 0;
#else
	lstrcpy(szCLSID, wszCLSID);
#endif

	CoTaskMemFree(wszCLSID);

	strGUID = szCLSID;
	delete szCLSID;

	return strGUID;
}

ProcessServerRTSPProcessor::ProcessServerRTSPProcessor()
{
	this->DisplayGreenRect(FALSE);
	this->IgnorePerson(TRUE);
	this->ExitThread(0);
	this->ShouldStopLive(1);
	this->FunCallback(NULL);
	this->Port(-1);
	this->Leaked(FALSE);
	this->FrameInterval(20);
	this->Distance_ratio(2.0L);
	this->DetectMinArea(100);
	this->StatusCallback(NULL);
	this->Ignore(FALSE);
	this->UUID(MakeUuidString());
	this->m_oSampleHistogram = this->LoadHistogram("./true_sample/");
	this->m_oFakeHistogram = this->LoadHistogram("./fake_sample/");
	CString lstrEventName;
	CString lstrUnique = MakeUuidString();
	lstrEventName.Format(_T("Local\%s"), lstrUnique);
	this->m_oNotifyDetect = CreateEvent(NULL, FALSE, FALSE, lstrEventName);
	this->m_pThread = AfxBeginThread(ProcessServerRTSPProcessor::liveThread, this, 0, 0, CREATE_SUSPENDED);
	CString lstrThreadName;
	lstrThreadName.Format(_T("RSTP:%s"), "test");
	SetThreadName(this->m_pThread->m_nThreadID, lstrThreadName.GetBuffer(0));
	lstrThreadName.Format(_T("DetectLeakThread:%s"), "test");
	this->m_pThread_DetectLeak = AfxBeginThread(ProcessServerRTSPProcessor::DetectLeakThread, this, 0, 0, CREATE_SUSPENDED);
	SetThreadName(this->m_pThread->m_nThreadID, lstrThreadName.GetBuffer(0));
	this->m_pThread->m_bAutoDelete = TRUE;
	this->m_pThread->ResumeThread();
	this->m_pThread_DetectLeak->ResumeThread();
	
}

double ProcessServerRTSPProcessor::correlation(cv::Mat &mat1, cv::Mat &mat2)
{

	// convert data-type to "float"
	cv::Mat im_float_1= mat1.clone();
	cv::Mat im_float_2 = mat2.clone();

	int n_pixels = im_float_1.rows * im_float_1.cols;

	// Compute mean and standard deviation of both images
	cv::Scalar im1_Mean, im1_Std, im2_Mean, im2_Std;
	meanStdDev(im_float_1, im1_Mean, im1_Std);
	meanStdDev(im_float_2, im2_Mean, im2_Std);

	// Compute covariance and correlation coefficient
	double covar = (im_float_1 - im1_Mean).dot(im_float_2 - im2_Mean) / n_pixels;
	double correl = covar / (im1_Std[0] * im2_Std[0]);

	return correl;
}

ProcessServerRTSPProcessor::~ProcessServerRTSPProcessor()
{
	try
	{
		this->ExitThread(1);
		SetEvent(this->m_oNotifyDetect);
		DWORD ldwRret = WaitForSingleObject(this->m_pThread->m_hThread, 1000);
		System::String^ lstrUUID = gcnew System::String(this->UUID());
		EricZhao::AlarmImageRabbitExchangHandler::releaseSender(lstrUUID);
		if (ldwRret == WAIT_TIMEOUT)
		{
			try
			{
				TerminateThread(this->m_pThread->m_hThread, -1);
			}
			catch (CMemoryException* e)
			{

			}
			catch (CFileException* e)
			{
			}
			catch (CException* e)
			{
			}

			try
			{
				CloseHandle(this->m_pThread->m_hThread);
			}
			catch (CMemoryException* e)
			{

			}
			catch (CFileException* e)
			{
			}
			catch (CException* e)
			{
			}

			try
			{
				//delete this->m_pThread;
			}
			catch (CMemoryException* e)
			{

			}
			catch (CFileException* e)
			{
			}
			catch (CException* e)
			{
			}
		}
	}
	catch (CMemoryException* e)
	{

	}
	catch (CFileException* e)
	{
	}
	catch (CException* e)
	{
	}
	catch (...)
	{
	}


	this->m_pThread = NULL;
}

CString ProcessServerRTSPProcessor::UserName()
{
	CAutoLock(this->m_oLock);
	return m_strUserName;
}

void ProcessServerRTSPProcessor::UserName(CString val)
{
	CAutoLock(this->m_oLock);
	m_strUserName = val;
}

CString ProcessServerRTSPProcessor::Password()
{
	CAutoLock(this->m_oLock);
	return m_strPassword;
}

void ProcessServerRTSPProcessor::Password(CString val)
{
	CAutoLock(this->m_oLock);
	m_strPassword = val;
}

CString ProcessServerRTSPProcessor::IP()
{
	CAutoLock(this->m_oLock);
	return m_strIP;
}

void ProcessServerRTSPProcessor::IP(CString val)
{
	CAutoLock(this->m_oLock);
	m_strIP = val;
}

int ProcessServerRTSPProcessor::Port()
{
	CAutoLock(this->m_oLock);
	return m_nPort;
}

void ProcessServerRTSPProcessor::Port(int val)
{
	CAutoLock(this->m_oLock);
	m_nPort = val;
}

void ProcessServerRTSPProcessor::UserID(int val)
{
	CAutoLock(this->m_oLock);
	m_nUserID = val;
}

bool ProcessServerRTSPProcessor::startLive()
{
	this->ShouldStopLive(FALSE);
	return true;
}

void ProcessServerRTSPProcessor::stopLive()
{
	this->ShouldStopLive(TRUE);
}
bool ProcessServerRTSPProcessor::detectLeakV2(DetectorbridgeV2& loBridge, std::vector<bbox_t> & arefRet)
{
	cv::Mat current_image_large;
	cv::Mat previous_image_large;

	{
		CAutoLock(this->m_oLock);
		{
			if (this->m_oImagesToDetect.size() < 2)
			{
				return false;
			}
		}

		current_image_large = this->m_oImagesToDetect.front();
		this->m_oImagesToDetect.pop();

		previous_image_large = this->m_oImagesToDetect.front();
		this->m_oImagesToDetect.pop();
	}
	int lnDetectServerPort = this->DetectServerPort();
	CString lstrIP, lstrData, lstrName;
	lstrIP.Format("%s_%d", this->IP(), this->m_nPort);
	lstrData.Format("./pic_temp/%s_pic0.bmp", lstrIP);
	lstrName = lstrData.GetBuffer(0);

	bool lbPerson = loBridge.detectPerson_simple_remote_by_buffer_v2(lstrName,
		lnDetectServerPort,
		current_image_large,
		arefRet, 0.2f, false, System::Drawing::Imaging::ImageFormat::Jpeg);

	{
		CString lstrLog;
		lstrLog.Format(_T("Current Person:%d\r\n"), lbPerson);
		OutputDebugString(lstrLog);
	}

	if (!lbPerson)
	{
		std::vector<cv::Rect> loRet2 = this->detectLeak(previous_image_large, current_image_large);
	}

	return false;
}

bool ProcessServerRTSPProcessor::pushAlarmPacketDataToMSQ(cv::Mat & imageOriginal, 
	cv::Mat & detected,int anAlarmType, CString & astrUrl)
{
	try
	{
		EricZhao::AlarmImagePacket^ loPacket = gcnew EricZhao::AlarmImagePacket();
		loPacket->PacketType = EricZhao::PacketTypeValues::ALARM_IMAGE_DATA;
		loPacket->LeakMode = anAlarmType;
		if (!imageOriginal.empty())
		{
			loPacket->OriginalImage = DetectorbridgeV2::convertMatToImageByteArray(imageOriginal, System::Drawing::Imaging::ImageFormat::Jpeg);
		}
		
		loPacket->DetectImage = DetectorbridgeV2::convertMatToImageByteArray(detected, System::Drawing::Imaging::ImageFormat::Jpeg);
		loPacket->DateTime = System::DateTime::Now;
		loPacket->Channels = this->TotalChannels();
		loPacket->VideoChannelIndex = this->ChannelIndex();
		System::String^ lstrUrl = gcnew System::String(astrUrl.GetBuffer(0));
		System::String^ lstrUUID = gcnew System::String(this->UUID());
		EricZhao::AlarmImageRabbitExchangHandler::SingleSend(loPacket, lstrUUID,lstrUrl, EricZhao::PacketTypeValues::ALARM_IMAGE_DATA);
		return true;
	}
	catch (CMemoryException* e)
	{
		
	}
	catch (CFileException* e)
	{
	}
	catch (CException* e)
	{
	}
	return false;
}

bool ProcessServerRTSPProcessor::pushNormalPacketDataToMSQ(cv::Mat & imageOriginal, CString & astrUrl)
{
	try
	{
		EricZhao::AlarmImagePacket^ loPacket = gcnew EricZhao::AlarmImagePacket();
		loPacket->PacketType = EricZhao::PacketTypeValues::NORMAL_DATA;
		loPacket->LeakMode = 0;
		loPacket->OriginalImage = DetectorbridgeV2::convertMatToImageByteArray(imageOriginal, System::Drawing::Imaging::ImageFormat::Jpeg);
		loPacket->DateTime = System::DateTime::Now;
		loPacket->Channels = this->TotalChannels();
		loPacket->VideoChannelIndex = this->ChannelIndex();
		System::String^ lstrUrl = gcnew System::String( astrUrl.GetBuffer(0));
		System::String^ lstrUUID = gcnew System::String(this->UUID());
		EricZhao::AlarmImageRabbitExchangHandler::SingleSend(loPacket, lstrUUID, lstrUrl, EricZhao::PacketTypeValues::NORMAL_DATA);
		return true;
	}
	catch (CMemoryException* e)
	{
		
	}
	catch (CFileException* e)
	{
	}
	catch (CException* e)
	{
	}
	return false;
}

bool ProcessServerRTSPProcessor::pushHeartBeatMSQ(CString & astrUrl)
{
	try
	{
		EricZhao::AlarmImagePacket^ loPacket = gcnew EricZhao::AlarmImagePacket();
		loPacket->PacketType = EricZhao::PacketTypeValues::HEART_BEAT;
		loPacket->LeakMode = 0;
		loPacket->DateTime = System::DateTime::Now;
		loPacket->Channels = this->TotalChannels();
		loPacket->VideoChannelIndex = this->ChannelIndex();
		System::String^ lstrUrl = gcnew System::String(astrUrl.GetBuffer(0));
		System::String^ lstrUUID = gcnew System::String(this->UUID());
		EricZhao::AlarmImageRabbitExchangHandler::SingleSend(loPacket, lstrUUID, lstrUrl, EricZhao::PacketTypeValues::ALARM_IMAGE_DATA);
		return true;
	}
	catch (CMemoryException* e)
	{

	}
	catch (CFileException* e)
	{
	}
	catch (CException* e)
	{
	}
	return false;
}

bool ProcessServerRTSPProcessor::pushConfigMSQ(CString & astrUrl)
{
	try
	{
		EricZhao::AlarmImagePacket^ loPacket = gcnew EricZhao::AlarmImagePacket();
		loPacket->PacketType = EricZhao::PacketTypeValues::CONFIG;
		loPacket->LeakMode = 0;
		loPacket->DateTime = System::DateTime::Now;
		loPacket->Channels = this->TotalChannels();
		loPacket->VideoChannelIndex = this->ChannelIndex();
		System::String^ lstrUrl = gcnew System::String(astrUrl.GetBuffer(0));
		System::String^ lstrUUID = gcnew System::String(this->UUID());
		EricZhao::AlarmImageRabbitExchangHandler::SingleSend(loPacket, lstrUUID, lstrUrl, EricZhao::PacketTypeValues::ALARM_IMAGE_DATA);
		return true;
	}
	catch (CMemoryException* e)
	{

	}
	catch (CFileException* e)
	{
	}
	catch (CException* e)
	{
	}
	return false;
}

std::vector<int> ProcessServerRTSPProcessor::getMapIndex(cv::Mat & arefWholeMap, cv::Rect & arefRect, int anSplitCount)
{
	if (anSplitCount <1)
	{
		anSplitCount = 1;
	}

	int lnTotalWidth = arefWholeMap.size().width;
	int lnTotalHeight = arefWholeMap.size().height;

	int lnWidthStep = lnTotalWidth /anSplitCount;
	if (lnWidthStep<1)
	{
		lnWidthStep = 1;
	}
	int lnHeightStep = lnTotalHeight / anSplitCount;
	if (lnHeightStep < 1)
	{
		lnHeightStep = 1;
	}
	std::vector<int> loRet;
	for (int i=0;i<anSplitCount;i++)
	{
		for (int j=0;j<anSplitCount;j++)
		{
			cv::Rect loRect;
			loRect.x = j*lnWidthStep;
			loRect.y = i*lnHeightStep;

			if (j == anSplitCount - 1)
			{
				if (anSplitCount*lnWidthStep<lnTotalWidth)
				{
					loRect.width = lnTotalWidth - (anSplitCount - 1)*lnWidthStep;
				}
				else
				{
					loRect.width = lnWidthStep;
				}
			}
			else
			{
				loRect.width = lnWidthStep;
			}

			if (i == anSplitCount - 1)
			{
				if (anSplitCount*lnHeightStep < lnTotalWidth)
				{
					loRect.width = lnTotalWidth - (anSplitCount - 1)*lnHeightStep;
				}
				else
				{
					loRect.height = lnHeightStep;
				}
			}
			else
			{
				loRect.height = lnHeightStep;
			}
			
			cv::Rect loComputed = loRect & arefRect;

			if (loComputed.area()>0)
			{
				int lnIndex = i*anSplitCount + j;

				loRet.push_back(lnIndex);
			}
		}
	}


	return loRet;
}

bool ProcessServerRTSPProcessor::detectLeak(DetectorbridgeV2& loBridge)
{
	cv::Mat current_image_large_original;
	cv::Mat current_image_large;
	cv::Mat previous_image_large;
	Scalar loColorGreen = Scalar(0, 255, 0);
	Scalar loColorRed = Scalar(0, 0, 255);
	Scalar loColoryellow = Scalar(0, 255, 255);
	{
		CAutoLock(this->m_oLock);
		{
			if (this->m_oImagesToDetect.size() < 2)
			{
				return false;
			}
		}

		current_image_large = this->m_oImagesToDetect.front();
		current_image_large_original = current_image_large.clone();
		this->m_oImagesToDetect.pop();

		previous_image_large = this->m_oImagesToDetect.front();
		this->m_oImagesToDetect.pop();
	}

	int lnDetectServerPort = this->DetectServerPort();
	CString lstrDetectServerIP = this->DetectServerIP();
	CString lstrIP, lstrData, lstrName;
	lstrIP.Format("%s_%d", this->IP(), this->m_nPort);
	lstrData.Format("./pic_temp/%s_pic0.bmp", lstrIP);
	lstrName = lstrData.GetBuffer(0);
	CString lstrMSQURL = this->MSQURL();
	int lnCamIndex = this->CamIndex();
	cv::Mat loHistCurretn = this->getHistogram(current_image_large);
	double ldblCurrentStd = 0;
	double ldblMean = 0;
	std::vector<cv::Rect> loRet2;
	if (current_image_large.cols <= 0
		|| current_image_large.rows <= 0
		|| previous_image_large.cols <= 0
		|| previous_image_large.rows <= 0)
	{
#ifdef DEBUG
		cv::imwrite("./error_current_image_large.bmp", current_image_large);
		cv::imwrite("./error_previous_image_large.bmp", previous_image_large);
#endif

		this->LeakedRect(loRet2);
		this->Leaked(FALSE);
		return false;
	}
	else
	{
#ifdef DEBUG
		cv::imwrite("./good_current_image_large.bmp", current_image_large);
		cv::imwrite("./good_previous_image_large.bmp", previous_image_large);
#endif
	}
	this->calcMeanStdVariant(loHistCurretn, ldblMean, ldblCurrentStd);

	bool lbPerson = loBridge.detectPerson_simple_remote_by_buffer(lstrName,
		lstrDetectServerIP,
		lnDetectServerPort,
		current_image_large,
		0.2f,
		false,
		System::Drawing::Imaging::ImageFormat::Jpeg);

	{
		CString lstrLog;
		lstrLog.Format(_T("Current Person:%d\r\n"), lbPerson);
		OutputDebugString(lstrLog);
	}

	BOOL lbIgnorePerson = this->IgnorePerson();

	if (!lbPerson || !lbIgnorePerson)
	{
		loRet2 = this->detectLeak(previous_image_large, current_image_large);
		int lnLeakMode = 0;
		if (loRet2.size()>0)
		{
		
			HWND hwnd = this->HWnd();
			RECT loRect;
			::GetClientRect(hwnd, &loRect);
			cv::Size winSize(loRect.right, loRect.bottom);
			cv::Mat cvImgTmp(winSize, CV_8UC3);



			std::vector<cv::Rect> loRetFiltered;
			int lnLeakFound = 0;
			for (int i = 0; i < loRet2.size(); i++)
			{
				cv::Rect * lpRect = &loRet2[i];			

				cv::Mat loSubMat = current_image_large(*lpRect);

				cv::Mat loHistCurretnSub /*= this->getHistogram(loSubMat)*/;

				bool lbldblCorrelationSatisfied = false;
#ifdef _DEBUG
				CString lstrName;
				lstrName.Format("sample_leak_bmp/cam_%d/all/leak_%d_%d.bmp", this->CamIndex(), lpRect->x, lpRect->y);
				cv::imwrite(lstrName.GetBuffer(0), loSubMat);
#endif // _DEBUG


				if (!this->m_oSampleHistogram.empty())
				{
					/*double ldblCorrelation = this->correlation(loHistCurretnSub, this->m_oSampleHistogram);
					ldblCorrelation = fabs(ldblCorrelation);
					if (ldblCorrelation < 0.5)
					{
						//continue;
					}
					
					if (ldblCorrelation>0.7)
					{
						lbldblCorrelationSatisfied = true;
					}*/
					
				}

				
				if (!this->m_oFakeHistogram.empty() && !lbldblCorrelationSatisfied)
				{
				/*	double ldblCorrelation = this->correlation(loHistCurretnSub, this->m_oFakeHistogram);
					ldblCorrelation = fabs(ldblCorrelation);
					if (ldblCorrelation > 0.5)
					{
					//	continue;
					}*/
				}

				if (!lbldblCorrelationSatisfied)
				{
					double ldblRatio = (lpRect->width / (lpRect->height*1.0));
					if (ldblRatio > 0.7 && ldblRatio <1.2)
					{
					//	continue;
					}
				}

				if (lpRect->area()>800)
				{
					//continue;
				}
				lstrName.Format("leak_%d_%d_index",lpRect->x, lpRect->y);				
				std::vector<int> loFeatures = this->getMapIndex(current_image_large, *lpRect,10);
				for (int i = 0; i < loFeatures.size();i++)
				{
					CString lstrTemp;
					lstrTemp.Format(_T("_%d"), loFeatures.at(i));
					lstrName.Append(lstrTemp);
				}

				double lbRet = loBridge.search_remote_by_buffer(lstrName,
					lstrDetectServerIP,
					8830,
					loSubMat,
					lnCamIndex,
					*lpRect,
					loFeatures,
					0.2f,					
					false,					
					System::Drawing::Imaging::ImageFormat::Jpeg);

				if (lbRet<0.8)
				{
					continue;
				}
				
				lnLeakFound++;

				loRetFiltered.push_back(loRet2[i]);
			
				double ldblCurrentStdSub = 0;
				double ldblMeanSub = 0;
				this->calcMeanStdVariant(loHistCurretnSub, ldblMeanSub, ldblCurrentStdSub);
				double ldblDiff = fabs(ldblMeanSub - ldblMean);
				if (ldblDiff> ldblCurrentStd*2)
				{
					this->Leaked(2);
					lnLeakMode = 2;
					//cv::rectangle(current_image_large, *lpRect, loColorRed, 8);
				}
				else
				{
					//cv::rectangle(current_image_large, *lpRect, loColoryellow, 8);
				}




#ifdef _DEBUG
				lstrName.Format("sample_leak_bmp/cam_%d/leak_%d_%d_filtered.bmp", this->CamIndex(), lpRect->x, lpRect->y);
				cv::imwrite(lstrName.GetBuffer(0), loSubMat);
#endif // _DEBUG
			}

			if (lnLeakFound)
			{
				this->Leaked(TRUE);
				lnLeakMode = 1;
				this->LeakedRect(loRet2);
			}
#ifdef _DEBUG
			cv::imwrite("current_image_large.bmp", current_image_large);
#endif
			
			return true;
		}
	}

	this->LeakedRect(loRet2);
	this->Leaked(FALSE);
	return false;
}



int ProcessServerRTSPProcessor::DetectServerPort()
{
	CAutoLock(this->m_oLock);
	if (this->m_nDetectServerPort <= 0)
	{
		this->m_nDetectServerPort = 18081;
	}
	return m_nDetectServerPort;
}

void ProcessServerRTSPProcessor::DetectServerPort(int val)
{
	CAutoLock(this->m_oLock);
	m_nDetectServerPort = val;
}

CString ProcessServerRTSPProcessor::DetectServerIP()
{
	CAutoLock(this->m_oLock);
	if (this->m_strDetectServerIP.IsEmpty())
	{
		this->m_strDetectServerIP = "127.0.0.1";
	}
	return m_strDetectServerIP;
}

void ProcessServerRTSPProcessor::DetectServerIP(CString val)
{
	CAutoLock(this->m_oLock);
	m_strDetectServerIP = val;
}

BOOL ProcessServerRTSPProcessor::Leaked()
{
	long llRet = 0;
	llRet = InterlockedAdd(&this->m_bLeaked, 0);
	return llRet;
}

void ProcessServerRTSPProcessor::Leaked(BOOL val)
{	
	InterlockedExchange(&this->m_bLeaked, val);
	m_bLeaked = val;
}

std::vector<cv::Rect> ProcessServerRTSPProcessor::LeakedRect()
{
	CAutoLock(this->m_oLock);
	return m_oLeakedRect;
}

void ProcessServerRTSPProcessor::LeakedRect(std::vector<cv::Rect> val)
{
	CAutoLock(this->m_oLock);
	m_oLeakedRect = val;
}

int ProcessServerRTSPProcessor::FrameInterval()
{
	CAutoLock(this->m_oLock);
	if (this->m_nFrameInterval <= 0)
	{
		this->m_nFrameInterval = 20;
	}
	return m_nFrameInterval;
}

void ProcessServerRTSPProcessor::FrameInterval(int val)
{
	CAutoLock(this->m_oLock);
	m_nFrameInterval = val;
}

BOOL ProcessServerRTSPProcessor::IgnorePerson()
{
	CAutoLock(this->m_oLock);
	return m_bIgnorePerson;
}

void ProcessServerRTSPProcessor::IgnorePerson(BOOL val)
{
	CAutoLock(this->m_oLock);
	if (val < 0)
	{
		val = 0;
	}

	m_bIgnorePerson = val;
}

int ProcessServerRTSPProcessor::Distance_c_max()
{

	return m_nDistance_c_max;
}

void ProcessServerRTSPProcessor::Distance_c_max(int val)
{
	m_nDistance_c_max = val;
}

double ProcessServerRTSPProcessor::Distance_ratio()
{
	CAutoLock(this->m_oLock);
	return m_nDistance_ratio;
}

void ProcessServerRTSPProcessor::Distance_ratio(double val)
{
	CAutoLock(this->m_oLock);
	m_nDistance_ratio = val;
}

int ProcessServerRTSPProcessor::DetectMinArea()
{
	CAutoLock(this->m_oLock);
	return m_nDetectMinArea;
}

void ProcessServerRTSPProcessor::DetectMinArea(int val)
{
	CAutoLock(this->m_oLock);
	m_nDetectMinArea = val;
}

OnStatusChange ProcessServerRTSPProcessor::StatusCallback()
{
	CAutoLock(this->m_oLock);
	return m_statusCallback;
}

void ProcessServerRTSPProcessor::StatusCallback(OnStatusChange val)
{
	CAutoLock(this->m_oLock);
	m_statusCallback = val;
}

cv::Rect ProcessServerRTSPProcessor::ROI()
{
	CAutoLock(this->m_oLock);
	return m_oROI;
}

void ProcessServerRTSPProcessor::ROI(cv::Rect val)
{
	CAutoLock(this->m_oLock);
	m_oROI = val;
}

int ProcessServerRTSPProcessor::SourceMode()
{
	CAutoLock(this->m_oLock);
	return m_nSourceMode;
}

void ProcessServerRTSPProcessor::SourceMode(int val)
{
	CAutoLock(this->m_oLock);
	m_nSourceMode = val;
}

int ProcessServerRTSPProcessor::CamIndex()
{
	CAutoLock(this->m_oLock);
	return m_nCamIndex;
}

void ProcessServerRTSPProcessor::CamIndex(int val)
{
	CAutoLock(this->m_oLock);
	m_nCamIndex = val;
}

CString ProcessServerRTSPProcessor::CamSrc()
{
	CAutoLock(this->m_oLock);
	return m_strCamSrc;
}

void ProcessServerRTSPProcessor::CamSrc(CString val)
{
	CAutoLock(this->m_oLock);
	m_strCamSrc = val;
}

int ProcessServerRTSPProcessor::DisplayGreenRect()
{
	CAutoLock(this->m_oLock);
	return m_nDisplayGreenRect;
}

void ProcessServerRTSPProcessor::DisplayGreenRect(int val)
{
	CAutoLock(this->m_oLock);
	m_nDisplayGreenRect = val;
}

BOOL ProcessServerRTSPProcessor::Ignore()
{
	CAutoLock(this->m_oLock);
	return m_nIgnore;
}

void ProcessServerRTSPProcessor::Ignore(BOOL val)
{
	CAutoLock(this->m_oLock);
	m_nIgnore = val;
}

CString ProcessServerRTSPProcessor::MSQURL()
{
	CAutoLock(this->m_oLock);
	return m_strMSQURL;
}

void ProcessServerRTSPProcessor::MSQURL(CString val)
{
	CAutoLock(this->m_oLock);
	m_strMSQURL = val;
}

int ProcessServerRTSPProcessor::ChannelIndex()
{
	CAutoLock(this->m_oLock);
	return m_nChannelIndex;
}

void ProcessServerRTSPProcessor::ChannelIndex(int val)
{
	CAutoLock(this->m_oLock);
	m_nChannelIndex = val;
}

int ProcessServerRTSPProcessor::TotalChannels()
{
	CAutoLock(this->m_oLock);
	return m_nTotalChannels;
}

void ProcessServerRTSPProcessor::TotalChannels(int val)
{
	CAutoLock(this->m_oLock);
	m_nTotalChannels = val;
}

CString ProcessServerRTSPProcessor::UUID()
{
	CAutoLock(this->m_oLock);
	return m_strUUID;
}

void ProcessServerRTSPProcessor::UUID(CString val)
{
	CAutoLock(this->m_oLock);
	m_strUUID = val;
}

cv::Mat ProcessServerRTSPProcessor::LoadHistogram(
	System::String^ astrFilePath)
{
	cv::Mat loRet = cv::Mat();
	try
	{
		System::String^ lstrPathName = astrFilePath;
		System::Collections::Generic::IEnumerable<System::String^>^ loFiles = System::IO::Directory::EnumerateFiles(lstrPathName);
		
		int lnSize = 0;
		for each(System::String^ lstrFileName in loFiles)
		{
			CString lstrCFileName = lstrFileName;
			cv::Mat loPerPic = cv::imread(lstrCFileName.GetBuffer(0));
			cv::Mat loHistgoramPerPic = this->getHistogram(loPerPic);
			if (loRet.empty())
			{
				loRet = loHistgoramPerPic;
			}
			else
			{
				loRet += loHistgoramPerPic;
			}			
			lnSize++;
		}

		if (lnSize>0)
		{
			loRet = loRet / lnSize;
		}
	}
	catch (CMemoryException* e)
	{
		
	}
	catch (CFileException* e)
	{
	}
	catch (CException* e)
	{
	}
	catch (System::Exception^ e)
	{

	}
	
	return loRet;
}

std::vector<std::vector<cv::Point>>  ProcessServerRTSPProcessor::detect_overflap_merge(cv::Mat & img, std::vector<std::vector<cv::Point>>& contours)
{
	int 	height, width = 0;
	height = img.size().height;
	width = img.size().width;
	//cv::Mat1f mask(cv::Size(width,height));
	cv::Mat mask(height, width, CV_8UC3, Scalar(0, 0, 0));
	int lnMinArea = this->DetectMinArea();
	for (int i = 0; i < contours.size(); i++)
	{
		if (cv::contourArea(contours[i]) < lnMinArea)
		{
			continue;
		}

		cv::Rect loRect = cv::boundingRect(contours[i]);

		loRect.width = loRect.width + 0;
		loRect.height = loRect.height + 0;

		cv::rectangle(mask, loRect, cv::Scalar(255, 255, 255), cv::FILLED);
	}


	std::vector<std::vector<cv::Point>> loContours;
	std::vector<Vec4i> hierarchy;
	cv::Mat mask_gray;
	cv::cvtColor(mask, mask_gray, cv::COLOR_BGR2GRAY);
	cv::findContours(mask_gray, loContours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

#ifdef DEBUG
	cv::imwrite("./detect_overflap_merge_test.bmp", mask_gray);
#endif // DEBUG

	return loContours;
}

double ProcessServerRTSPProcessor::compute_distance(const std::vector<cv::Point> & c)
{
	cv::Rect loRect = cv::boundingRect(c);
	int x, y, w, h = 0;
	x = loRect.x;
	y = loRect.y;
	w = loRect.width;
	h = loRect.height;

	int cx, cy = 0;
	cx = x + w / 2;
	cy = y + h / 2;

	int img_cx, img_cy = 0;
	img_cx = ROI_X;
	img_cy = ROI_Y;

	if (((x < img_cx) && img_cx < (x + w))
		&&
		((y < img_cy) && img_cy < (y + h)))
	{
		return 0;
	}

	double distance_c = (x - img_cx) * (x - img_cx) + (y - img_cy) * (y - img_cy);
	double distance_c2 = (x + w - img_cx) * (x + w - img_cx) + (y - img_cy) * (y - img_cy);
	double distance_c3 = (x - img_cx) * (x - img_cx) + (y + h - img_cy) * (y + h - img_cy);
	double distance_c4 = (x + w - img_cx) * (x + w - img_cx) + (y + h - img_cy) * (y + h - img_cy);

	if (distance_c > distance_c2)
		distance_c = distance_c2;

	if (distance_c > distance_c3)
		distance_c = distance_c3;

	if (distance_c > distance_c4)
		distance_c = distance_c4;

	distance_c = 1 / (cv::contourArea(c)*1.0);
	return (distance_c);
}

bool ProcessServerRTSPProcessor::less_cmp(const std::vector<cv::Point> & c1, const std::vector<cv::Point> & c2) {
	double dist1 = compute_distance(c1);
	double dist2 = compute_distance(c2);
	return dist1 < dist2;
}




std::vector<cv::Rect> ProcessServerRTSPProcessor::detectLeak(cv::Mat & arefData1,
	cv::Mat & arefData2,
	int ln_zoom_size/* = 1*/,
	int ab_only_detect /*= FALSE*/)
{
	int 	height, width = 0;
	int 	height1, width1 = 0;



	height = arefData1.size().height;
	width = arefData1.size().width;

	height1 = arefData2.size().height;
	width1 = arefData2.size().width;

	img_Width = int(width / ln_zoom_size);
	img_height = int(height / ln_zoom_size);
	Distance_c_max(pow((img_Width / 2.), 2) + pow((img_height / 2.), 2));
	int lnDistanceMax = this->Distance_c_max();
	ROI_X = img_Width / 2;
	ROI_Y = img_height / 2;

	cv::Size lo_zoom_size = cv::Size(int(width / ln_zoom_size), int(height / ln_zoom_size));

	cv::Mat lo_pic0, lo_pic1, lo_pic_diff;
	cv::resize(arefData1, lo_pic0, lo_zoom_size, 0.0, 0.0, cv::INTER_CUBIC);
	cv::resize(arefData2, lo_pic1, lo_zoom_size, 0.0, 0.0, cv::INTER_CUBIC);
	lo_pic_diff = lo_pic1.clone();

	cv::Mat Lim_0, Lim_1;
	cv::cvtColor(lo_pic0, Lim_0, cv::COLOR_BGR2GRAY);
	cv::cvtColor(lo_pic1, Lim_1, cv::COLOR_BGR2GRAY);

	int kernel_size = 5;
	int lnBoxFilterSize = 5;
	cv::Mat gauss_gray1, gauss_gray2;
	cv::GaussianBlur(Lim_0, gauss_gray1, cv::Size(kernel_size, kernel_size), 0);
	cv::GaussianBlur(Lim_1, gauss_gray2, cv::Size(kernel_size, kernel_size), 0);

	cv::Mat frameDelta, frameDelta_box;
	cv::absdiff(gauss_gray1, gauss_gray2, frameDelta);
	cv::boxFilter(frameDelta, frameDelta_box, -1, cv::Size(lnBoxFilterSize, lnBoxFilterSize));

	cv::Mat thresh, thresh_dilate;
	cv::threshold(frameDelta_box, thresh, 7, 255, cv::THRESH_BINARY);
	cv::dilate(thresh, thresh_dilate, NULL, cv::Point(-1, -1), 2);

#ifdef _DEBUG
	cv::imwrite("diff.bmp", thresh_dilate);
#endif

	std::vector<std::vector<cv::Point>> contours;
	std::vector<Vec4i> hierarchy;
	cv::findContours(thresh_dilate, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

	std::vector<std::vector<cv::Point>> contours2 = detect_overflap_merge(lo_pic_diff, contours);

	sortstruct s(this);
	std::sort(contours2.begin(), contours2.end(), s);

	bool lbFoundLeak = false;
	std::vector<cv::Rect> loRet;

	double ldblDistanceRatio = this->Distance_ratio();

	int lnDetectMinArea = this->DetectMinArea();

	cv::Rect loRectROI = this->ROI();
	BOOL lbShouldJudgeROI = TRUE;
	if (loRectROI.x < 0 || loRectROI.y <0 || loRectROI.area() <= 0 || loRectROI.width <= 0 || loRectROI.height <= 0)
	{
		lbShouldJudgeROI = FALSE;
	}
	int lnFoundCount = 0;
	for (int i = 0; i < contours2.size(); i++)
	{
		if (cv::contourArea(contours2[i]) < lnDetectMinArea)
		{
			continue;
		}

		if (cv::contourArea(contours2[i]) > 1000)
		{
			//continue;
		}

		if (compute_distance(contours2[i]) >lnDistanceMax / ldblDistanceRatio)
		{
			//continue;
		}

		if (lnFoundCount > 5)
		{
		//	break;
		}

		lnFoundCount++;

		cv::Rect loRect = cv::boundingRect(contours2[i]);

		if (lbShouldJudgeROI)
		{
			double ldblArea = (loRect&loRectROI).area();
			if (ldblArea <= lnDetectMinArea)
			{
				continue;
			}
		}

		int x, y, w, h = 0;
		x = loRect.x;
		y = loRect.y;
		w = loRect.width;
		h = loRect.height;

		cv::Rect loRect2 = loRect;
		loRect2.x = loRect2.x*ln_zoom_size;
		loRect2.y = loRect2.y*ln_zoom_size;
		loRect2.width = loRect2.width*ln_zoom_size;
		loRect2.height = loRect2.height*ln_zoom_size;

		if (loRect2.y <= 20 && loRect2.height < 100)
		{
			continue;
		}

		loRet.push_back(loRect2);
#ifdef _DEBUG
		cv::rectangle(lo_pic_diff, loRect, Scalar(0, 0, 255), 8);
#endif

		lbFoundLeak = true;
	}

	if (lbFoundLeak)
	{
#ifdef _DEBUG

		time_t timer;
		char buffer[100] = { 0 };
		struct tm* tm_info;

		time(&timer);
		tm_info = localtime(&timer);
		strftime(buffer, 26, "%Y_%m_%d_%H_%M_%S", tm_info);

		CString lstrData;
		std::string lstrName;
		CString lstrIP;
		lstrIP.Format("%s_%d", this->IP(), this->m_nPort);
		lstrIP.Replace('.', '_');

		lstrData.Format("./pic_data/%s_pic0_%s.bmp", lstrIP, buffer);
		lstrName = lstrData.GetBuffer(0);
		cv::imwrite(lstrName, lo_pic0);

		lstrData.Format("./pic_data/%s_pic1_%s.bmp", lstrIP, buffer);
		lstrName = lstrData.GetBuffer(0);
		cv::imwrite(lstrName, lo_pic1);

		lstrData.Format("./pic_data/%s_diff_%s.bmp", lstrIP, buffer);
		lstrName = lstrData.GetBuffer(0);
		cv::imwrite(lstrName, lo_pic_diff);
#endif

		return loRet;
	}

	return std::vector<cv::Rect>();
}


CCriticalSection ProcessServerRTSPProcessor::open_rtsp_stream_lock;
bool ProcessServerRTSPProcessor::live(HWND hwnd)
{
	cv::VideoCapture vcap;
	cv::Mat image_filtered;
	cv::Mat image_resized;
	cv::Mat previous_image_large;
	cv::Mat previous_image_large_resized;
	cv::Mat image_large;
	cv::Mat current_image_large;
	cv::Mat current_image_large_send;
	cv::Mat image_large_thumb;
	CString lstrUrl;
	lstrUrl.Format("rtsp://%s:%s@%s:%d/mpeg4/ch1/main", this->UserName(), this->Password(), this->IP(), this->Port());
	CString lstrSrc = this->CamSrc();
	int lnCamIndex = this->CamIndex();
	CString lstrMSQURL = this->MSQURL();
	const std::string videoStreamAddress = lstrUrl.GetBuffer(0);
	{

		while (true)
		{
			CAutoLock(open_rtsp_stream_lock);
			int lnSrcMode = this->SourceMode();
			if (lnSrcMode <= 0 || lnSrcMode > 2)
			{
				if (vcap.open(videoStreamAddress))
				{
					break;
				}
			}
			// read from src string
			else if (lnSrcMode == 1)
			{
				if (vcap.open(lstrSrc.GetBuffer(0)))
				{
					break;
				}
			}
			// read from local camera
			else if (lnSrcMode == 2)
			{
				if (vcap.open(lnCamIndex))
				{
					break;
				}
			}

			return false;
		}
	}

	cv::Mat previous_gray;
	cv::Mat current_gray;
	cv::Mat current_gray_gauss;
	cv::Mat diff_gray;
	cv::Mat diff_gray_filter;
	cv::Mat threshold_gray;
	cv::Mat threshold_gray_dilate;
	cv::Mat img_bin;
	cv::Mat out;


	cv::Mat back;
	cv::Mat fore;

	int thresval = 60;
	BOOL lbFirst = TRUE;

	std::vector<std::vector<cv::Point>> contours;
	std::vector<Vec4i> hierarchy;
	Scalar loColorGreen = Scalar(0, 255, 0);
	Scalar loColorRed = Scalar(0, 0, 255);
	Scalar loColoryellow = Scalar(0, 255, 255);
	DetectorbridgeV2 loBridge;
	ULONG64 lnStep = 0;
	std::vector<cv::Rect> loRectDiffs;

	BOOL lbSaveDebugImage = TRUE;



	int lnBoxFillterSize = 7;
	int lnGaussFilterSize = 15;
	double ldblZoomInRatio = 3;
	BOOL lbDisplayWholeColor = FALSE;

	FrameCallback  pFun = this->FunCallback();
	int lnFrameInterval = this->FrameInterval();
	int lnMinArea = this->DetectMinArea();
	BOOL lbDisplayGreen = this->DisplayGreenRect();
	BOOL lnIgnore = this->Ignore();
	OnStatusChange callBack = this->StatusCallback();


	while (true)
	{

		BOOL lbShouldStopLive = FALSE;
		if (!vcap.read(image_large))
		{
			if (lbShouldStopLive)
			{
				return true;
			}
			cv::waitKey(10);
			vcap.release();
			int lnSrcMode = this->SourceMode();
			if (lnSrcMode <= 0 || lnSrcMode > 2)
			{
				if (vcap.open(videoStreamAddress))
				{
					//break;
				}
			}
			// read from src string
			else if (lnSrcMode == 1)
			{
				if (vcap.open(lstrSrc.GetBuffer(0)))
				{
					//break;
				}
			}
			// read from local camera
			else if (lnSrcMode == 2)
			{
				if (vcap.open(lnCamIndex))
				{
					//break;
				}
			}
			continue;
		}

		cv::waitKey(10);
		Sleep(0);

		int lnNewWidth = 0;
		int lnNewHeigth = 0;

		int lnNewWidthSend = 0;
		int lnNewHeigthSend = 0;

		double ldblRatioSend = 2;

		if (lnStep == ULONG64_MAX)
		{
			lnStep = 0;
		}

		if (lbFirst)
		{
			lbFirst = FALSE;
		}

		if (image_large.empty())
		{
			continue;
		}


		try
		{
	
			cv::Size size = image_large.size();
			lnNewWidth = size.width / ldblZoomInRatio;
			lnNewHeigth = size.height / ldblZoomInRatio;

			lnNewWidthSend = size.width / ldblRatioSend;
			lnNewHeigthSend = size.height / ldblRatioSend;

			cv::boxFilter(image_large, image_filtered, -1, cv::Size(lnBoxFillterSize, lnBoxFillterSize));
			cv::resize(image_filtered, image_resized, cv::Size(lnNewWidth, lnNewHeigth), 0, 0, 2);
			cv::cvtColor(image_resized, current_gray, COLOR_BGR2GRAY);
			if (previous_gray.empty())
			{
				previous_image_large = image_large.clone();
				previous_gray = current_gray.clone();
				continue;
			}

			lnStep++;			
			current_image_large_send = image_large.clone();

			if (lnStep % lnFrameInterval == 0)
			{
#ifdef _DEBUG
				CString lstrFileName;

				if (lbSaveDebugImage)
				{
					lstrFileName.Format("./test/%d_0current.bmp", lnStep);
					cv::imwrite(lstrFileName.GetBuffer(0), image_large);

					lstrFileName.Format("./test/%d_1previous.bmp", lnStep);
					cv::imwrite(lstrFileName.GetBuffer(0), previous_image_large);
				}



				cv::boxFilter(previous_image_large, image_filtered, -1, cv::Size(lnBoxFillterSize, lnBoxFillterSize));
				cv::resize(image_filtered, previous_image_large_resized, cv::Size(lnNewWidth, lnNewHeigth), 0, 0, 2);
				cv::cvtColor(previous_image_large_resized, previous_gray, COLOR_BGR2GRAY);\
					

#endif // _DEBUG

				loRectDiffs.clear();
				current_image_large = image_large.clone();

				cv::absdiff(previous_gray, current_gray, diff_gray);
				cv::boxFilter(diff_gray, diff_gray_filter, -1, cv::Size(lnBoxFillterSize, lnBoxFillterSize));
				cv::threshold(diff_gray_filter, threshold_gray, 1, 255, THRESH_TRIANGLE);
				cv::dilate(threshold_gray, threshold_gray_dilate, NULL);

#ifdef _DEBUG

				if (lbSaveDebugImage)
				{
					lstrFileName;
					lstrFileName.Format("./test/%d_2gray_current.bmp", lnStep);
					cv::imwrite(lstrFileName.GetBuffer(0), current_gray);

					lstrFileName.Format("./test/%d_3gray_previous.bmp", lnStep);
					cv::imwrite(lstrFileName.GetBuffer(0), previous_gray);

					lstrFileName.Format("./test/%d_4diff.bmp", lnStep);
					cv::imwrite(lstrFileName.GetBuffer(0), diff_gray);

					lstrFileName.Format("./test/%d_5diff_filter.bmp", lnStep);
					cv::imwrite(lstrFileName.GetBuffer(0), diff_gray_filter);

					lstrFileName.Format("./test/%d_6diff_threshold_gray.bmp", lnStep);
					cv::imwrite(lstrFileName.GetBuffer(0), threshold_gray);

					lstrFileName.Format("./test/%d_7diff_threshold_gray_dilate.bmp", lnStep);
					cv::imwrite(lstrFileName.GetBuffer(0), threshold_gray_dilate);
				}

				cv::namedWindow("current");
				cv::imshow("current", image_large);

				cv::namedWindow("previous");
				cv::imshow("previous", previous_image_large);

				cv::namedWindow("diff");
				cv::imshow("diff", threshold_gray_dilate);

#endif // _DEBUG

				contours.clear();
				hierarchy.clear();

				cv::findContours(threshold_gray_dilate, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
				std::vector<std::vector<cv::Point>>::iterator lpIt = contours.begin();

				bool lbDiffDetect = false;

				for (int i = 0; i < contours.size(); i++)
				{
					std::vector<cv::Point> loData = contours[i];
					if (cv::contourArea(loData) < lnMinArea)
					{
						continue;
					}

					cv::Rect loRect = cv::boundingRect(loData);


					if ((loRect.height >= lnNewHeigth*0.95) &&
						(loRect.width >= lnNewWidth*0.95))
					{
						//continue;
					}

					loRect.x = loRect.x*ldblZoomInRatio;
					loRect.y = loRect.y*ldblZoomInRatio;
					loRect.width = loRect.width*ldblZoomInRatio;
					loRect.height = loRect.height*ldblZoomInRatio;

					if (loRect.y <= 3 && loRect.height < 100)
					{
						loRect.height = loRect.height;
						continue;
					}

					if (cv::contourArea(loData) > lnMinArea)
					{
						{
							loRectDiffs.push_back(loRect);
						}

					}

					lbDiffDetect = true;
				}

				if (lbDiffDetect)
				{
					//if (lnStep % 40 == 0)
					{
						this->PushImage(previous_image_large, image_large);
						::SetEvent(this->m_oNotifyDetect);
					}
				}
				else
				{
					
				}
			}//if (lnStep % lnFrameInterval == 0)

			std::vector<cv::Rect> loLeakedRect;

			BOOL lbLeaked = this->Leaked();
			if (lbLeaked)
			{
				loLeakedRect = this->LeakedRect();
				this->Leaked(FALSE);
			}

			RECT loRect;
			::GetClientRect(hwnd, &loRect);
			cv::Size winSize(loRect.right, loRect.bottom);
			cv::Mat cvImgTmp(winSize, CV_8UC3);
			if (lbDisplayGreen)
			{
				for (int i = 0; i < loRectDiffs.size(); i++)
				{
					cv::Rect * lpRect = &loRectDiffs[i];

					cv::rectangle(image_large, *lpRect, loColorGreen, 8);

				}
			}

			if (lbLeaked)
			{
				for (int i = 0; i < loLeakedRect.size(); i++)
				{
					cv::Rect * lpRect = &loLeakedRect[i];
					if (!lnIgnore)
					{
						if (lbLeaked > 1)
						{
							cv::rectangle(image_large, *lpRect, loColorRed, 8);
						}
						else

						{
							cv::rectangle(image_large, *lpRect, loColoryellow, 8);
						}

					}

				}				
			}


			if (lbLeaked)
			{
				if (lbDisplayWholeColor)
				{
					cv::Mat image = image_large;
					cv::Mat roi = image;
					cv::Mat color_red(roi.size(), CV_8UC3, cv::Scalar(0, 0, 125));
					cv::Mat color_yellow(roi.size(), CV_8UC3, cv::Scalar(0, 125, 125));
					double alpha = 0.3;
					if (!lnIgnore)
					{
						if (lbLeaked > 1)
						{
							cv::addWeighted(color_red, alpha, roi, 1.0 - alpha, 0.0, roi);
						}
						else
						{
							cv::addWeighted(color_yellow, alpha, roi, 1.0 - alpha, 0.0, roi);
						}

					}
				}				
			}

			if (callBack != NULL)
			{
				callBack(lbLeaked);
			}

			cv::resize(image_large, cvImgTmp, winSize);
			IplImage iplimg = IplImage(cvImgTmp);

			if (lbLeaked && !lnIgnore)
			{
				CString lstrData = "可能泄漏";
				if (lbLeaked > 1)
				{
					lstrData = "可能着火";
				}
				cv::Point loPoint;
				loPoint.x = (loRect.right - loRect.left) / 2.0 - 80;
				loPoint.y = (loRect.bottom - loRect.top) / 2.0 + 20;
				//loBridge.PutText(&iplimg, lstrData, loPoint, loColorRed, cvScalar(40, 0, 0, 0));
				
				cv::Mat image_resized_alarmed;
				cv::resize(current_image_large_send, image_resized, cv::Size(lnNewWidthSend, lnNewHeigthSend), 0, 0, 2);
				cv::resize(image_large, image_resized_alarmed, cv::Size(lnNewWidthSend, lnNewHeigthSend), 0, 0, 2);
				
				this->pushAlarmPacketDataToMSQ(current_image_large_send, 
					image_resized_alarmed, 
					lbLeaked, 
					lstrMSQURL);

			}
			else
			{
				cv::resize(current_image_large_send, image_resized, cv::Size(lnNewWidthSend, lnNewHeigthSend), 0, 0, 2);
				this->pushNormalPacketDataToMSQ(image_resized, lstrMSQURL);
			}

#ifdef _DEBUG
			CString lstrData;
			lstrData.Format("%d", lnStep);
			cv::Point loPoint;
			loPoint.x = 40;
			loPoint.y =80;
			loBridge.PutText(&iplimg, lstrData, loPoint, loColorRed, cvScalar(40, 0, 0, 0));
#endif

			CvvImage iimg;
			iimg.CopyOf(&iplimg);
			HDC loDc = ::GetDC(hwnd);
			iimg.DrawToHDC(loDc, &loRect);
			ReleaseDC(hwnd, loDc);
			iimg.Destroy();

			previous_gray = current_gray.clone();
			previous_image_large = current_image_large.clone();
		}
		catch (CMemoryException* e)
		{
			try
			{
				vcap.release();
			}
			catch (CMemoryException* e)
			{
				
			}
			catch (CFileException* e)
			{
			}
			catch (CException* e)
			{
			}
			
			return false;
		}
		catch (CFileException* e)
		{
			try
			{
				vcap.release();
			}
			catch (CMemoryException* e)
			{

			}
			catch (CFileException* e)
			{
			}
			catch (CException* e)
			{
			}
			return false;
		}
		catch (CException* e)
		{
			try
			{
				vcap.release();
			}
			catch (CMemoryException* e)
			{

			}
			catch (CFileException* e)
			{
			}
			catch (CException* e)
			{
			}
			return false;
		}	

	}
	return true;
}

HWND ProcessServerRTSPProcessor::HWnd()
{
	CAutoLock(this->m_oLock);
	return m_hWnd;
}

void ProcessServerRTSPProcessor::HWnd(HWND val)
{
	CAutoLock(this->m_oLock);
	m_hWnd = val;
}

UINT AFX_CDECL ProcessServerRTSPProcessor::liveThread(LPVOID apParameter)
{
	if (NULL == apParameter)
	{
		return 0;
	}

	ProcessServerRTSPProcessor * lpHandler = (ProcessServerRTSPProcessor *)apParameter;

	while (true)
	{
		BOOL lbExit = lpHandler->ExitThread();

		if (lbExit)
		{
			break;
		}

		BOOL lbShouldStopPlay = lpHandler->ShouldStopLive();

		if (!lbShouldStopPlay)
		{
			lpHandler->live(lpHandler->HWnd());
		}

		Sleep(500);
	}

	return 0;
}

UINT AFX_CDECL ProcessServerRTSPProcessor::DetectLeakThread(LPVOID apParameter)
{
	if (NULL == apParameter)
	{
		return 0;
	}

	ProcessServerRTSPProcessor * lpHandler = (ProcessServerRTSPProcessor *)apParameter;

	DetectorbridgeV2 loBridge;

	WaitForSingleObject(lpHandler->m_oNotifyDetect, INFINITE);

	while (true)
	{
		BOOL lbExit = lpHandler->ExitThread();

		if (lbExit)
		{
			break;
		}

		lpHandler->detectLeak(loBridge);
		WaitForSingleObject(lpHandler->m_oNotifyDetect, INFINITE);

	}

	return 0;
}



void ProcessServerRTSPProcessor::ExitThread(BOOL val)
{
	CAutoLock(this->m_oLock);
	m_bExitThread = val;
}

BOOL ProcessServerRTSPProcessor::ExitThread()
{
	CAutoLock(this->m_oLock);
	return m_bExitThread;
}

BOOL ProcessServerRTSPProcessor::ShouldStopLive()
{
	CAutoLock(this->m_oLock);
	return m_bStopLive;
}

void ProcessServerRTSPProcessor::ShouldStopLive(BOOL val)
{
	CAutoLock(this->m_oLock);
	m_bStopLive = val;
}

FrameCallback ProcessServerRTSPProcessor::FunCallback()
{
	CAutoLock(this->m_oLock);
	return m_pcallback;
}

void ProcessServerRTSPProcessor::FunCallback(FrameCallback val)
{
	CAutoLock(this->m_oLock);
	m_pcallback = val;
}

void ProcessServerRTSPProcessor::PushImage(cv::Mat & arefData1, cv::Mat & arefData2)
{
	this->m_oLock.Lock();

	try
	{
		if (this->m_oImagesToDetect.size() > 10)
		{
			while (this->m_oImagesToDetect.size() > 0)
			{
				this->m_oImagesToDetect.pop();
			}
		}
		this->m_oImagesToDetect.push(arefData1.clone());
		this->m_oImagesToDetect.push(arefData2.clone());
	}
	catch (CMemoryException* e)
	{
		ASSERT(FALSE);
	}
	catch (CFileException* e)
	{
		ASSERT(FALSE);
	}
	catch (CException* e)
	{
		ASSERT(FALSE);
	}
	catch (...)
	{
		ASSERT(FALSE);
	}

	this->m_oLock.Unlock();

}

cv::Mat ProcessServerRTSPProcessor::getHistogram(const cv::Mat &image,
	int anHistSize/* = 128*/)
{
	/*
	int lnChannels = image.channels();
	cv::MatND hist;
	float hranges[2] = { 0 };
	hranges[0] = 0;
	hranges[1] = 1;

	cv::calcHist(&image,    // 输入图像
	1,         // 计算1张图片的直方图
	&lnChannels,  // 图像的通道数
	cv::Mat(), // 不实用图像作为掩码
	hist,      // 返回的直方图
	1,				// 1D的直方图
	&anHistSize,  // 容器的数量
	&hranges);   // 像素值的范围*/

	std::vector<Mat> bgr_planes;
	split(image, bgr_planes);

	if (bgr_planes.size() <= 0)
	{
		return cv::Mat();
	}

	/// Establish the number of bins
	int histSize = anHistSize;

	/// Set the ranges ( for B,G,R) )
	float range[] = { 0, 256 };
	const float* histRange = { range };

	bool uniform = true; bool accumulate = false;

	Mat hist, g_hist, r_hist;

	int lnChannels = image.channels();

	/// Compute the histograms:
	calcHist(&bgr_planes[2], 1, 0, Mat(), hist, 1, &histSize, &histRange, uniform, accumulate);
	return hist;
}

void ProcessServerRTSPProcessor::calcMeanStdVariant(cv::Mat & value, double & mean, double &std)
{
	if (value.empty())
	{
		return;
	}

	int lnCols = value.rows;

	double lnTotalValue = .0;
	for (int i = 0; i<lnCols; i++)
	{
		lnTotalValue += value.at<float>(i);
	}

	double ldblMean = lnTotalValue / (lnCols*1.0);

	double ldblTotalVariant = .0;
	for (int i = 0; i < lnCols; i++)
	{
		double ldblDiff = (value.at<float>(i) - ldblMean);
		ldblDiff = ldblDiff*ldblDiff;
		ldblTotalVariant += ldblDiff;
	}

	ldblTotalVariant = ldblTotalVariant / (lnCols*1.0);
	double ldblstd = sqrt(ldblTotalVariant);

	mean = ldblMean;
	std = ldblstd;
}

int ProcessServerRTSPProcessor::UserID()
{
	CAutoLock(this->m_oLock);
	return m_nUserID;
}

bool ProcessServerRTSPProcessor::sortstruct::operator()(const std::vector<cv::Point> & m1, const std::vector<cv::Point> & m2)
{
	return this->m->less_cmp(m1, m2);
}
