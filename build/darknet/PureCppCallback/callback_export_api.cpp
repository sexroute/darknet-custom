#include "stdafx.h"
#include "callback_export_api.h"
#include "stdafx.h"
#include "callback_export_api.h"
#include "HKBiz.h"
#include <map>
#include "RTSP.h"
#include "DetectorbridgeV2.hpp"
#include "ProcessServerRTSPProcessor.h"
#include "PurePlayer.h"

CString safeToCString(const char * strSrc)
{
	CString lstrRet = _T("");
	if (NULL == strSrc)
	{
		return lstrRet;
	}


	int lnLength = strlen(strSrc);
	if (lnLength > 102300)
	{
		lnLength = 102300;
	}

	std::vector<char>loBuffer(lnLength + 10);
	char * CamSrc = &loBuffer.front();
	strncpy(CamSrc, strSrc, lnLength);
	lstrRet = CamSrc;
	return lstrRet;
}

System::String^ safeToString(const char * strSrc)
{
	System::String^ lstrRet = gcnew System::String("");
	if (NULL == strSrc)
	{
		return lstrRet;
	}

	
	int lnLength = strlen(strSrc);
	if (lnLength > 102300)
	{
		lnLength = 102300;
	}

	std::vector<char>loBuffer(lnLength + 10);
	char * CamSrc = &loBuffer.front();
	strncpy(CamSrc, strSrc, lnLength);
	lstrRet = gcnew System::String(CamSrc);
	return lstrRet;
}
CALLBACK_EXPORT_API  bool __stdcall live(int UserID, 
	HWND handle,
	int userData,
	int & appRealHandle,
	int & errorCode)
{


	NET_DVR_CLIENTINFO ClientInfo;
	ClientInfo.hPlayWnd = handle;
	ClientInfo.lChannel =  1;
	ClientInfo.lLinkMode = 0;
	ClientInfo.sMultiCastIP = NULL;
	
	HKBIZ * lpHKBiz = new HKBIZ();
	lpHKBiz->Wnd(handle);
	BOOL lbRet = ::IsWindow(lpHKBiz->Wnd());
	appRealHandle = NET_DVR_RealPlay_V30(UserID, &ClientInfo, HKBIZ::fRealDataCallBack, (void *)lpHKBiz, FALSE);

	
	errorCode = NET_DVR_GetLastError();
	if (appRealHandle<0)
	{
		errorCode =  NET_DVR_GetLastError();		
		return false;
	}

	return true;
}
std::map<CString, int> g_logins;
CCriticalSection g_loginLock;
int g_loginId = 0;
CALLBACK_EXPORT_API int __stdcall login(const char * strUserName, const char * strPassword, const char * strIP, int port)
{
	if (NULL == strUserName || NULL == strPassword || NULL == strIP)
	{
		return -1;
	}

	char DeviceIP[16] = { 0 };
	char cUserName[100] = { 0 };
	char cPassword[100] = { 0 };

	int lnDeviceIP = strlen(strIP);
	int lncUserName = strlen(cUserName);
	int lncPassword = strlen(cPassword);

	if (lnDeviceIP > MAX_NAMELEN - 1)
	{
		lnDeviceIP = MAX_NAMELEN - 1;
	}

	if (lncUserName > MAX_NAMELEN - 1)
	{
		lncUserName = MAX_NAMELEN - 1;
	}

	if (lncPassword > PASSWD_LEN - 1)
	{
		lncPassword = PASSWD_LEN - 1;
	}

	strncpy(DeviceIP, strIP, lnDeviceIP);
	strncpy(cUserName, strUserName, lncUserName);
	strncpy(cPassword, strPassword, lncPassword);

	CString lstrKey;
	lstrKey.Format("%s:%d %s %s", strIP, port, strUserName, strPassword);

	CAutoLock(g_loginLock);

	int lUserID = 0;

	if (g_logins.find(lstrKey) == g_logins.end())
	{
		g_logins[lstrKey] = g_loginId++;
	}
	
	lUserID = g_loginId[lstrKey];

	return lUserID;
	/*HKBIZ::Init();

	if (NULL == strUserName || NULL == strPassword || NULL == strIP)
	{
		return -1;
	}

	char DeviceIP[16] = { 0 };
	char cUserName[100] = { 0 };
	char cPassword[100] = { 0 };

	int lnDeviceIP = strlen(strIP);
	int lncUserName = strlen(cUserName);
	int lncPassword = strlen(cPassword);

	if (lnDeviceIP>MAX_NAMELEN-1)
	{
		lnDeviceIP = MAX_NAMELEN - 1;
	}

	if (lncUserName > MAX_NAMELEN - 1)
	{
		lncUserName = MAX_NAMELEN - 1;
	}

	if (lncPassword > PASSWD_LEN - 1)
	{
		lncPassword = PASSWD_LEN - 1;
	}

	strncpy(DeviceIP,  strIP, lnDeviceIP);
	strncpy(cUserName, strUserName, lncUserName);
	strncpy(cPassword, strPassword, lncPassword);

	NET_DVR_DEVICEINFO_V30 devInfo;

	int lUserID = NET_DVR_Login_V30(DeviceIP, port, cUserName, cPassword, &devInfo);

	return lUserID;*/
}

std::map<int, CRTSP> g_oPlayer;
CCriticalSection  g_oLock;

std::map<int, ProcessServerRTSPProcessor> g_oProcessor;
CCriticalSection  g_oProcessorLock;

CALLBACK_EXPORT_API bool __stdcall live_v2(int UserID,
	HWND handle,
	int userData,
	int & appRealHandle,
	int & errcode, 
	FrameCallback callback)
{

	if (UserID >= 0)
	{
		CAutoLock(g_oLock);
		std::map<int, CRTSP>::iterator lpIt = g_oPlayer.find(UserID);
		if (lpIt != g_oPlayer.end())
		{
			CRTSP * lpRtspPlayer = &lpIt->second;
			lpRtspPlayer->HWnd(handle);
			lpRtspPlayer->ShouldStopLive(FALSE);
			lpRtspPlayer->FunCallback(callback);
			return true;
		}
	}

	return false;
}

CALLBACK_EXPORT_API bool __stdcall process(int UserID, 
	HWND handle, 
	int userData, 
	int & appRealHandle, 
	int & errcode, 
	FrameCallback callback)
{
	if (UserID >= 0)
	{
		CAutoLock(g_oProcessorLock);
		std::map<int, ProcessServerRTSPProcessor>::iterator lpIt = g_oProcessor.find(UserID);
		if (lpIt != g_oProcessor.end())
		{
			ProcessServerRTSPProcessor * lpRtspPlayer = &lpIt->second;
			lpRtspPlayer->HWnd(handle);
			lpRtspPlayer->ShouldStopLive(FALSE);
			lpRtspPlayer->FunCallback(callback);
			return true;
		}
	}

	return false;
}

int g_uid = 0;

CALLBACK_EXPORT_API int __stdcall process_login(
	int uid, 
	const char * strUserName,
	const char * strPassword,
	const char * strIP,
	int port,
	int rtsp_port,
	const char * strDetectServerIP,
	int detect_server_port,
	int frame_Interval,
	int ignore_person,
	double distance_ratio,
	int min_area,
	OnStatusChange callback,
	int ROI_X,
	int ROI_Y,
	int ROI_W,
	int ROI_H,
	const char * strSrc,
	int nCamIndex,
	int nSrcMode,
	int nDisplayGreenRect,
	int nIgnore,
	const char * strMSQURL,
	int anChannIndex,
	int anTotalChannels)
{
	int lnUserID = uid;

	if (lnUserID >= 0)
	{
		cv::Rect loRect;
		loRect.x = ROI_X;
		loRect.y = ROI_Y;
		loRect.width = ROI_W;
		loRect.height = ROI_H;
		CAutoLock(g_oProcessorLock);
		ProcessServerRTSPProcessor * lpRtspPlayer = &g_oProcessor[lnUserID];
		lpRtspPlayer->UserName(strUserName);
		lpRtspPlayer->Password(strPassword);
		lpRtspPlayer->IP(strIP);
		lpRtspPlayer->Port(rtsp_port);
		lpRtspPlayer->DetectServerPort(detect_server_port);
		lpRtspPlayer->DetectServerIP(strDetectServerIP);
		lpRtspPlayer->FrameInterval(frame_Interval);
		lpRtspPlayer->IgnorePerson(ignore_person);
		lpRtspPlayer->Distance_ratio(distance_ratio);
		lpRtspPlayer->DetectMinArea(min_area);
		lpRtspPlayer->StatusCallback(callback);
		lpRtspPlayer->ROI(loRect);
		lpRtspPlayer->CamIndex(nCamIndex);
		lpRtspPlayer->DisplayGreenRect(nDisplayGreenRect);
		lpRtspPlayer->Ignore(nIgnore);
		lpRtspPlayer->CamSrc(safeToCString(strSrc));
		lpRtspPlayer->MSQURL(safeToCString(strMSQURL));
		lpRtspPlayer->SourceMode(nSrcMode);
		lpRtspPlayer->ChannelIndex(anChannIndex);
		lpRtspPlayer->TotalChannels(anTotalChannels);
	}

	return lnUserID;
}
CALLBACK_EXPORT_API int __stdcall  stop_pure_play(HWND handle)
{
	CPurePlayer^ lpRtspPlayer = CPurePlayer::GetCPurePlayer((int64)handle);
	if (nullptr != lpRtspPlayer)
	{
		lpRtspPlayer->stop();
	}

	CPurePlayer::RemoveCPurePlayer((int64)handle);

	return 0;
}

CALLBACK_EXPORT_API int __stdcall pure_play(
	HWND handle,
	const char * strUserName,
	const char * strPassword,
	const char * strIP, 
	int port, 
	int rtsp_port, 
	const char * strDetectServerIP, 
	int detect_server_port,
	int frame_Interval, 
	int ignore_person, 
	double distance_ratio, 
	int min_area, 
	OnStatusChange callback, 
	int ROI_X, 
	int ROI_Y, 
	int ROI_W, 
	int ROI_H, 
	const char * strSrc, 
	int nCamIndex,
	int nSrcMode, 
	int nDisplayGreenRect, 
	int nIgnore)
{
	CPurePlayer^ lpRtspPlayer = CPurePlayer::GetCPurePlayer((int)handle);
	lpRtspPlayer->UserName(safeToString(strUserName));
	lpRtspPlayer->Password(safeToString(strPassword));
	lpRtspPlayer->IP(safeToString(strIP));
	lpRtspPlayer->Port(rtsp_port);
	lpRtspPlayer->DetectServerPort(detect_server_port);
	lpRtspPlayer->DetectServerIP(safeToString(strDetectServerIP));
	lpRtspPlayer->IgnorePerson(ignore_person);
	lpRtspPlayer->Distance_ratio(distance_ratio);
	lpRtspPlayer->CamIndex(nCamIndex);
	lpRtspPlayer->DisplayGreenRect(nDisplayGreenRect);
	lpRtspPlayer->Ignore(nIgnore);
	lpRtspPlayer->hWnd(handle);
	lpRtspPlayer->SourceMode(nSrcMode);
	lpRtspPlayer->CamIndex(nCamIndex);
	lpRtspPlayer->DisplayGreenRect(nDisplayGreenRect);
	lpRtspPlayer->Ignore(nIgnore);
	lpRtspPlayer->CamSrc(safeToString(strSrc));
	lpRtspPlayer->play();
	return TRUE;
}

CALLBACK_EXPORT_API int __stdcall login_v2(const char * strUserName,
	const char * strPassword,
	const char * strIP,
	int port,
	int rtsp_port,
	const char * strDetectServerIP,
	int detect_server_port,
	int frame_Interval,
	int ignore_person,
	double distance_ratio,
	int min_area,
	OnStatusChange callback,
	int ROI_X,
	int ROI_Y,
	int ROI_W,
	int ROI_H,
	const char * strSrc,
	int nCamIndex,
	int nSrcMode,
	int nDisplayGreenRect,
	int nIgnore)
{
	int lnUserID = 0;

	if (nSrcMode <=0)
	{
		lnUserID = login(strUserName, strPassword, strIP, port);
	}
	else
	{
		lnUserID = g_uid ++;
	}
	

	if (lnUserID >= 0)
	{
		cv::Rect loRect;
		loRect.x = ROI_X;
		loRect.y = ROI_Y;
		loRect.width = ROI_W;
		loRect.height = ROI_H;
		CAutoLock(g_oLock);
		CRTSP * lpRtspPlayer =& g_oPlayer[lnUserID];
		lpRtspPlayer->UserName(strUserName);
		lpRtspPlayer->Password(strPassword);
		lpRtspPlayer->IP(strIP);
		lpRtspPlayer->Port(rtsp_port);
		lpRtspPlayer->DetectServerPort(detect_server_port);
		lpRtspPlayer->DetectServerIP(strDetectServerIP);
		lpRtspPlayer->FrameInterval(frame_Interval);
		lpRtspPlayer->IgnorePerson(ignore_person);
		lpRtspPlayer->Distance_ratio(distance_ratio);
		lpRtspPlayer->DetectMinArea(min_area);
		lpRtspPlayer->StatusCallback(callback);
		lpRtspPlayer->ROI(loRect);
		lpRtspPlayer->CamIndex(nCamIndex);
		lpRtspPlayer->DisplayGreenRect(nDisplayGreenRect);
		lpRtspPlayer->Ignore(nIgnore);
		lpRtspPlayer->CamSrc(safeToCString(strSrc));
		lpRtspPlayer->SourceMode(nSrcMode);
		
	}

	return lnUserID;
}

CALLBACK_EXPORT_API bool detect_person(const char * fileName, float threhold )
{
	std::string lstrFileName;
	if (NULL == fileName)
	{
		lstrFileName = "";
	}

	DetectorbridgeV2 loDetector;

	std::vector<bbox_t> loRet = loDetector.detect(fileName, threhold);

	for (size_t i = 0; i < loRet.size(); i++)
	{
		if (loRet[i].obj_id == 14)
		{
			return true;
		}
	}

	return false;
}

CALLBACK_EXPORT_API void quit()
{
/*
	CAutoLock(g_oLock);
	std::map<int, CRTSP>::iterator lpIt = g_oPlayer.begin();
	for (lpIt = g_oPlayer.begin();lpIt != g_oPlayer.end();lpIt++)
	{
		lpIt->second.stopLive();
	}*/

	exit(0);
}

CALLBACK_EXPORT_API bool detect_person_v2(unsigned char* img_bytes, int anImgLength, int w, int h, int c/*=3*/, float threhold/*= 0.2*/)
{
	if (NULL == img_bytes || anImgLength<=0|| w<=0||h<=0|| c<=0)
	{
		return false;
	}

	DetectorbridgeV2 loDetector;
	std::vector<bbox_t> loRet = loDetector.detect(img_bytes, anImgLength, c);

	for (size_t i = 0; i < loRet.size(); i++)
	{
		if (loRet[i].obj_id == 14)
		{
			return true;
		}
	}

	return false;
}

CALLBACK_EXPORT_API bool init(const char * cfg_filename, const char * weight_filename, int gpu_id)
{
	std::string lstrFileName ;
	std::string lstrWeightName ;

	if (NULL == cfg_filename)
	{
		lstrFileName = "";
	
	}

	if ( NULL == weight_filename )
	{
		lstrWeightName = "";
	}

	if ( gpu_id < 0)
	{
		gpu_id = 0;
	}

	return	DetectorbridgeV2::init(lstrFileName, lstrWeightName, gpu_id);

}

