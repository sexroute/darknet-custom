// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#ifdef _DEBUG
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
// Replace _NORMAL_BLOCK with _CLIENT_BLOCK if you want the
// allocations to be of _CLIENT_BLOCK type
#else
#define DBG_NEW new
#endif
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // Exclude rarely-used stuff from Windows headers
#endif



#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions


#include <afxdisp.h>        // MFC Automation classes



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxcontrolbars.h>     // MFC support for ribbons and control bars

#include <afxinet.h>
#include <iostream>
#include <ostream>
#include <iterator>
#include <fstream>
#include <vector>
#include <algorithm>
#include <vector>
#include <map>
#include "include/HCNetSDK.h"
#include "include/PlayM4.h"
#include <opencv2/opencv.hpp>
#include "opencv2/video/background_segm.hpp"  
#include <string>
#include "..\..\..\src\interface.hpp"
#pragma comment(lib,"../x64/detector.lib")
#pragma comment(lib,"../x64/HCNetSDK.lib")
#pragma comment(lib,"../x64/PlayCtrl.lib")

class CAutoLock_class
{
	CCriticalSection * m_pLock;
private:


public:

	CAutoLock_class()
	{
		this->m_pLock = NULL;
	}

	CAutoLock_class(CCriticalSection & arefLock, BOOL abAddLock = TRUE)
	{
		this->m_pLock = &arefLock;

		if (abAddLock)
		{
			this->m_pLock->Lock();
		}
	}

	BOOL Attach(CCriticalSection & arefLock, BOOL abAddLock = TRUE)
	{
		ASSERT(this->m_pLock == NULL);

		if (this->m_pLock != NULL)
		{
			this->m_pLock->Unlock();
		}

		this->m_pLock = &arefLock;

		if (abAddLock)
		{
			this->m_pLock->Lock();
		}
	}

	~CAutoLock_class()
	{
		if (NULL != this->m_pLock)
		{
			this->m_pLock->Unlock();
		}

		this->m_pLock = NULL;
	}
};

#define CAutoLock(x) CAutoLock_class lolock(x)


class CApp : public CWinApp
{
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
};

const DWORD MS_VC_EXCEPTION = 0x406D1388;

#pragma pack(push,8)
typedef struct tagTHREADNAME_INFO
{
	DWORD dwType; // Must be 0x1000.
	LPCSTR szName; // Pointer to name (in user addr space).
	DWORD dwThreadID; // Thread ID (-1=caller thread).
	DWORD dwFlags; // Reserved for future use, must be zero.
} THREADNAME_INFO;
#pragma pack(pop)

void SetThreadName(DWORD dwThreadID, char* threadName);

extern CApp theApp;
