﻿using System;
using System.Collections.Generic;
using System.Drawing;
using detectorbridge;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using EricZhao.UiThread;
using System.IO;
using static haikang_video.Custom_hk_api;
using System.Drawing.Imaging;
using System.Threading;
using Dse;
using static Dse.FDse_define;
using static Dse.FixDef_Daq;
using static Dse.FixDef_Field;
using System.Diagnostics;
using SenderApplication;
using System.Messaging;
using EricZhao;

namespace haikang_video
{
    public class Biz
    {
        public Biz()
        {
            
        }
        int m_nStartIndex = 0;
        public int StartIndex
        {
            get { lock (m_strLockSendThreadStop) return m_nStartIndex; }
            set { lock (m_strLockSendThreadStop) m_nStartIndex = value; }
        }

        String m_strMiddlewareIP = "127.0.0.1";
        public  System.String MiddlewareIP
        {
            get { lock (m_strLockSendThreadStop) return m_strMiddlewareIP; }
            set { lock (m_strLockSendThreadStop) m_strMiddlewareIP = value; }
        }
        int m_nMiddlewarePort = 17001;
        public int MiddlewarePort
        {
            get { lock (m_strLockSendThreadStop) return m_nMiddlewarePort; }
            set { lock (m_strLockSendThreadStop) m_nMiddlewarePort = value; }
        }

        int m_nKeepSeconds = 40;
        public int KeepSeconds
        {
            get { lock (m_strLockSendThreadStop) if (m_nKeepSeconds < 0) { m_nKeepSeconds = 0; }; return m_nKeepSeconds; }
            set { lock (m_strLockSendThreadStop) m_nKeepSeconds = value; }
        }
        String m_strCompany = "";
        public System.String Company
        {
            get { lock (m_strLockSendThreadStop) return m_strCompany; }
            set { lock (m_strLockSendThreadStop) m_strCompany = value; }
        }
        String m_strFactory = "";
        public System.String Factory
        {
            get { lock (m_strLockSendThreadStop) return m_strFactory; }
            set { lock (m_strLockSendThreadStop) m_strFactory = value; }
        }
        String m_strPlant = "";
        public System.String Plant
        {
            get { lock (m_strLockSendThreadStop) return m_strPlant; }
            set { lock (m_strLockSendThreadStop) m_strPlant = value; }
        }
        String m_strDetectServerIP = "127.0.0.1";
        public System.String DetectServerIP
        {
            get { lock (this) return m_strDetectServerIP; }
            set { lock (this) m_strDetectServerIP = value; }
        }

        int m_nAlarmStatus = 0;
        String m_strAlarmLock = "";
        public int AlarmStatus
        {
            get { lock (m_strAlarmLock) return m_nAlarmStatus; }
            set { lock (m_strAlarmLock) m_nAlarmStatus = value; }
        }
        int m_nDetectServerPort = 18081;
        public int DetectServerPort
        {
            get { lock (this) return m_nDetectServerPort; }
            set { lock (this) m_nDetectServerPort = value; }
        }

        int m_nFrameInterval = 20;
        public int FrameInterval
        {
            get { lock (this) return m_nFrameInterval; }
            set { lock (this) { if (m_nFrameInterval >= 0) { m_nFrameInterval = value; } } }
        }

        int m_nIgnorePerson = 0;
        public int IgnorePerson
        {
            get { lock (this) return m_nIgnorePerson; }
            set { lock (this) m_nIgnorePerson = value; }
        }

        double distance_ratio = 2.0;
        public double Distance_ratio
        {
            get { lock (this) return distance_ratio; }
            set { lock (this) distance_ratio = value; }
        }

        int area_min = 100;
        public int Area_min
        {
            get { lock (this) return area_min; }
            set { lock (this) area_min = value; }
        }
        int m_nRTSP_PORT = 0;
        public int RTSP_PORT
        {
            get { lock (this) return m_nRTSP_PORT; }
            set { lock (this) m_nRTSP_PORT = value; }
        }

        bool m_nIsFirst = true;
        public bool IsFirst
        {
            get { lock (this) return m_nIsFirst; }
            set { lock (this) m_nIsFirst = value; }
        }


        int m_nROI_X = 0;
        public int ROI_X
        {
            get { lock (this) return m_nROI_X; }
            set { lock (this) m_nROI_X = value; }
        }
        int m_nROI_Y = 0;
        public int ROI_Y
        {
            get { lock (this) return m_nROI_Y; }
            set { lock (this) m_nROI_Y = value; }
        }
        int m_nROI_W = 0;
        public int ROI_W
        {
            get { lock (this) return m_nROI_W; }
            set { lock (this) m_nROI_W = value; }
        }
        int m_nROI_H = 0;
        public int ROI_H
        {
            get { lock (this) return m_nROI_H; }
            set { lock (this) m_nROI_H = value; }
        }
        int m_nIndex = 0;
        public int Index
        {
            get
            {
                lock (this) { return m_nIndex; }
            }
            set
            {
                lock (this) { m_nIndex = value; }
            }
        }
        CustomPictureBox m_oPicPox = null;
        private  String m_Str_PicBox_Lock = "m_Str_PicBox_Lock";
        public CustomPictureBox PicPox
        {
            get { lock (this.m_Str_PicBox_Lock) return m_oPicPox; }
            set { lock (this.m_Str_PicBox_Lock) m_oPicPox = value; }
        }
        private int m_lUserID = -1;
        private String m_lUserID_lock = "m_lUserID_lock";
        public int UserID
        {
            get { lock (this.m_lUserID_lock) return m_lUserID; }
            set { lock (this.m_lUserID_lock) m_lUserID = value; }
        }

        String m_strCamSrc = "";
        public System.String CamSrc
        {
            get { lock (this) return m_strCamSrc; }
            set { lock (this) m_strCamSrc = value; }
        }
        int m_nCamMode = 0;
        public int CamMode
        {
            get { lock (this) return m_nCamMode; }
            set { lock (this) m_nCamMode = value; }
        }
        int m_nCamIndex = 0;
        public int CamIndex
        {
            get { lock (this) return m_nCamIndex; }
            set { lock (this) m_nCamIndex = value; }
        }
        private Int32 m_lRealHandle = -1;
        public System.Int32 RealHandle
        {
            get { lock (this) return m_lRealHandle; }
            set { lock (this) m_lRealHandle = value; }
        }

        int m_nIgnore = 0;
        public int Ignore
        {
            get { lock (this) return m_nIgnore; }
            set { lock (this) m_nIgnore = value; }
        }
        int m_nDisplayGreenRect = 0;
        public int DisplayGreenRect
        {
            get { lock (this) return m_nDisplayGreenRect; }
            set { lock (this) m_nDisplayGreenRect = value; }
        }
        System.Threading.Thread m_oSendThread = null;
         Boolean m_oSendThreadStop = false;
         String m_strLockSendThreadStop = "m_strLockSendThreadStop";
        public  System.Boolean SendThreadStop
        {
            get { lock (m_strLockSendThreadStop) return m_oSendThreadStop; }
            set { lock (m_strLockSendThreadStop) m_oSendThreadStop = value; }
        }

        System.Threading.Thread m_oCaptureThread = null;
        Boolean m_bStop = false;
        public System.Boolean Stop
        {
            get { lock (this) return m_bStop; }
            set { lock (this) m_bStop = value; }
        }
        private static bool m_bInitSDK = false;

        string DVRIPAddress = "192.168.1.64"; //设备IP地址或者域名
        public string IP
        {
            get { lock (this) return DVRIPAddress; }
            set { lock (this) DVRIPAddress = value; }
        }
        Int16 DVRPortNumber = 8000;//设备服务端口号
        public System.Int16 Port
        {
            get { lock (this) return DVRPortNumber; }
            set { lock (this) DVRPortNumber = value; }
        }
        private uint iLastErr = 0;
        private string str = "";

        string DVRUserName = "admin";//设备登录用户名
        public string UserName
        {
            get { lock (this) return DVRUserName; }
            set { lock (this) DVRUserName = value; }
        }
        string DVRPassword = "bohuaxinzhi123";//设备登录密码
        public string Password
        {
            get { lock (this) return DVRPassword; }
            set { lock (this) DVRPassword = value; }
        }
        protected static detectorbridge.Detectorbridge moDetector = null;
        protected static String moDetector_locker = "detectorbridge.Detectorbridge moDetector";

        private String m_strCompareImagePath0 = "";
        public System.String CompareImagePath0
        {
            get { lock (this) return m_strCompareImagePath0; }
            set { lock (this) m_strCompareImagePath0 = value; }
        }
        private String m_strCompareImagePath1 = "";
        public System.String CompareImagePath1
        {
            get { lock (this) return m_strCompareImagePath1; }
            set { lock (this) m_strCompareImagePath1 = value; }
        }

        private String m_strCompareDiffPath = "";
        public System.String CompareDiffPath
        {
            get { lock (this) return m_strCompareDiffPath; }
            set { lock (this) m_strCompareDiffPath = value; }
        }
        String m_strTempDir = "./tmp";
        public System.String TempDir
        {
            get { lock (this) return m_strTempDir; }
            set { lock (this) m_strTempDir = value; }
        }
        String m_strBaseDir = "./data";
        public System.String BaseDir
        {
            get { lock (this) return m_strBaseDir; }
            set { lock (this) m_strBaseDir = value; }
        }

        IntPtr m_hWnd = new IntPtr(0);
        public System.IntPtr hWnd
        {
            get { return m_hWnd; }
            set { m_hWnd = value; }
        }

        private delegate void CaptureBmp_delegate(int index);


        private String m_strCompareImagePath0_bak = "";
        public System.String CompareImagePath0_bak
        {
            get { lock (this) return m_strCompareImagePath0_bak; }
            set { lock (this) m_strCompareImagePath0_bak = value; }
        }
        private String m_strCompareImagePath1_bak = "";
        public System.String CompareImagePath1_bak
        {
            get { lock (this) return m_strCompareImagePath1_bak; }
            set { lock (this) m_strCompareImagePath1_bak = value; }
        }

        private String m_strCompareDiffPath_bak = "";
        public System.String CompareDiffPath_bak
        {
            get { lock (this) return m_strCompareDiffPath_bak; }
            set { lock (this) m_strCompareDiffPath_bak = value; }
        }

        String m_strAviBakPath = "";
        public System.String AviBakPath
        {
            get { lock (this) return m_strAviBakPath; }
            set { lock (this) m_strAviBakPath = value; }
        }
        private String m_strAviPath = "";
        public System.String AviPath
        {
            get { lock (this) return m_strAviPath; }
            set { lock (this) m_strAviPath = value; }
        }

        private int m_nStatus = 0;
        private String m_strStatusLock = "m_strStatusLock";
        public int Status
        {
            get { lock (this.m_strStatusLock) return m_nStatus; }
            set { lock (this.m_strStatusLock) m_nStatus = value; }
        }

        Queue<byte[]> m_oQueue = null;
        String m_StrQueueLock = "m_StrQueueLock";
        public Queue<byte[]> Queue
        {
            get { lock (this.m_StrQueueLock) return m_oQueue; }
            set { lock (this.m_StrQueueLock) m_oQueue = value; }
        }
        private void initDetector()
        {
            lock (Biz.moDetector_locker)
            {
                if (Biz.moDetector == null)
                {
                    Biz.moDetector = new Detectorbridge();
                }
            }
        }


        HKHelper m_oHkHelper = null;
        public detectorbridge.HKHelper HkHelper
        {
            get { lock (this) return m_oHkHelper; }
            set { lock (this) m_oHkHelper = value; }
        }
        private void init()
        {
            lock (Biz.moDetector_locker)
            {
                if (Biz.m_bInitSDK == false)
                {
                    Biz.m_bInitSDK = CHCNetSDK.NET_DVR_Init();
                    CHCNetSDK.NET_DVR_SetLogToFile(3, "HKSdkLog\\", true);
                }

                if (this.Queue == null)
                {
                    this.Queue = new Queue<byte[]>();
                }

                if (this.OnImageCallback == null)
                {
                    this.OnImageCallback = new HKHelper.DecodeImageBufferCallBack(this.onImageCallBack);
                }

                if (this.HkHelper == null)
                {
                    this.HkHelper = new HKHelper();
                    this.HkHelper.setImageDecodedCallBack(this.OnImageCallback);
                }
            }
        }


        private void startCapture_thread()
        {
            this.stopCapture_thread();

            this.init();

            lock (this)
            {
                this.Stop = false;
                this.m_oCaptureThread = new System.Threading.Thread(thread_capture);
                this.m_oCaptureThread.IsBackground = true;
                this.m_oCaptureThread.Name = "Capture_thread:" + this.Index;
                this.m_oCaptureThread.Start();
            }
        }


        private void startCapture_thread2()
        {
            return;
            this.stopCapture_thread();

            this.init();

            lock (this)
            {
                this.Stop = false;
                this.m_oCaptureThread = new System.Threading.Thread(thread_capture2);
                this.m_oCaptureThread.IsBackground = true;
                this.m_oCaptureThread.Name = "Capture_thread:" + this.Index;
                this.m_oCaptureThread.Start();
            }
        }

        private void startSender_thread()
        {
            return;
            this.stopCapture_thread();

            lock (this.m_strLockSendThreadStop)
            {
                this.SendThreadStop = false;
                this.m_oSendThread = new System.Threading.Thread(send_thread_entry);
                this.m_oSendThread.IsBackground = true;
                this.m_oSendThread.Name = "Sender_thread:" + this.Index;
                this.m_oSendThread.Start();
            }
        }

        private bool CaptureImage(int anHandle, byte[] loBuffer, byte[] loBuffer2, byte[] loBuffer3)
        {
            try
            {
                string sJpegPicFileName;
                //图片保存路径和文件名 the path and file name to save
                sJpegPicFileName = anHandle + "_" + DateTime.Now + ".jpg";


                sJpegPicFileName = sJpegPicFileName.Replace(':', '_');

                int lChannel = 1; //通道号 Channel number

                CHCNetSDK.NET_DVR_JPEGPARA lpJpegPara = new CHCNetSDK.NET_DVR_JPEGPARA();
                lpJpegPara.wPicQuality = 0; //图像质量 Image quality
                lpJpegPara.wPicSize = 0xff; //抓图分辨率 Picture size: 2- 4CIF，0xff- Auto(使用当前码流分辨率)，抓图分辨率需要设备支持，更多取值请参考SDK文档

                if (loBuffer == null)
                {
                    loBuffer = new byte[1024 * 1024];
                }
                CaptureBmp_delegate delegatex = new CaptureBmp_delegate(CaptureBmp);


                uint lnLength = (uint)loBuffer.Length;
                uint lnReturned = 0;
                uint lnReturned2 = 0;
                bool lbPersonIn = false;
                bool lbPersonIn2 = false;


                //JPEG抓图 Capture a JPEG picture
                String lstrDebug = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + "|capture begin:" + "| userid:" + this.UserID;
                ThreadUiController.outputDebugString(lstrDebug + "\r\n");
                bool lbRet = CHCNetSDK.NET_DVR_CaptureJPEGPicture_NEW(UserID, lChannel, ref lpJpegPara, loBuffer, lnLength, ref lnReturned);


                if (!lbRet)
                {
                    iLastErr = CHCNetSDK.NET_DVR_GetLastError();

                    str = "NET_DVR_CaptureJPEGPicture failed, error code= " + iLastErr;
                    ThreadUiController.outputDebugString(str);
                    return false;
                }

                lstrDebug = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + "|capture end:" + "| userid:" + this.UserID;
                ThreadUiController.outputDebugString(lstrDebug + "\r\n");

                {
                    delegatex.Invoke(0);
                    lbPersonIn = this.detect(loBuffer, (int)lnReturned);
                }

                lstrDebug = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + "|capture begin:" + "| userid:" + this.UserID;
                ThreadUiController.outputDebugString(lstrDebug + "\r\n");
                System.Threading.Thread.Sleep(200);
                lbRet = CHCNetSDK.NET_DVR_CaptureJPEGPicture_NEW(UserID, lChannel, ref lpJpegPara, loBuffer2, lnLength, ref lnReturned2);

                lstrDebug = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + "|capture end:" + "| userid:" + this.UserID;
                ThreadUiController.outputDebugString(lstrDebug + "\r\n");
                if (!lbRet)
                {
                    iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                    str = "NET_DVR_CaptureJPEGPicture failed, error code= " + iLastErr;
                    ThreadUiController.outputDebugString(str);
                    return false;
                }


                {
                    lbPersonIn2 = this.detect(loBuffer, (int)lnReturned);
                    lstrDebug = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + "|detect first:" + lbPersonIn + "|detect second:" + lbPersonIn2;
                    ThreadUiController.outputDebugString(lstrDebug);
                    if (!lbPersonIn && !lbPersonIn && !this.IsFirst)
                    {

                        delegatex.Invoke(1);
                        bool lbLeak = this.detectleak();
                        return lbLeak;
                    }

                }

                if (this.IsFirst)
                {
                    this.IsFirst = false;
                }

            }
            catch (Exception e)
            {
                ThreadUiController.log(e.Message, ThreadUiController.LOG_LEVEL.ERROR);
            }

            return false;
        }

        private bool CaptureImage2(int anHandle, byte[] loBuffer, byte[] loBuffer2, byte[] loBuffer3)
        {
            try
            {
                string sJpegPicFileName;
                //图片保存路径和文件名 the path and file name to save
                sJpegPicFileName = anHandle + "_" + DateTime.Now + ".jpg";


                sJpegPicFileName = sJpegPicFileName.Replace(':', '_');

                int lChannel = 1; //通道号 Channel number

                CaptureBmp_delegate delegatex = new CaptureBmp_delegate(CaptureBmp);


                uint lnLength = (uint)loBuffer.Length;
                uint lnReturned = 0;
                uint lnReturned2 = 0;
                bool lbPersonIn = false;
                bool lbPersonIn2 = false;


                //JPEG抓图 Capture a JPEG picture
                String lstrDebug = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + "|capture begin:" + "| userid:" + this.UserID;
                ThreadUiController.outputDebugString(lstrDebug + "\r\n");

                if (this.Queue.Count <= 3)
                {
                    return false;
                }

                byte[] loBufferInqueue = this.Queue.Dequeue();

                if (loBufferInqueue == null || loBufferInqueue.Length == 0)
                {
                    str = "Queue is empty,wait for next run";
                    ThreadUiController.outputDebugString(str);
                    return false;
                }

                {

                    lbPersonIn = this.detect(loBufferInqueue, (int)loBufferInqueue.Length);
                    
                }


                byte[] loBufferInqueue2 = this.Queue.Dequeue();

                if (loBufferInqueue2 == null || loBufferInqueue2.Length == 0)
                {
                    iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                    str = "Queue is empty,wait for next run";
                    ThreadUiController.outputDebugString(str);
                    return false;
                }


                {
                    lbPersonIn2 = this.detect(loBufferInqueue2, (int)loBufferInqueue2.Length);
                    lstrDebug = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + "|detect first:" + lbPersonIn + "|detect second:" + lbPersonIn2;
                    ThreadUiController.outputDebugString(lstrDebug);
                    if (!lbPersonIn && !lbPersonIn && !this.IsFirst)
                    {
                        this.CaptureBmp2(0, loBufferInqueue);
                        this.CaptureBmp2(1, loBufferInqueue2);
                        bool lbLeak = this.detectleak();
                        return lbLeak;
                    }

                }

                if (this.IsFirst)
                {
                    this.IsFirst = false;
                }

            }
            catch (Exception e)
            {
                ThreadUiController.log(e.Message, ThreadUiController.LOG_LEVEL.ERROR);
            }

            return false;
        }


        private bool detectleak()
        {
            try
            {
                String lstrCString =
                              String.Format("http://127.0.0.1:1808{0}/diff?pic0={1}&pic1={2}&pic_diff={3}",
                              this.Index + 1, this.CompareImagePath0.Replace('\\', '/'), this.CompareImagePath1.Replace('\\', '/'), this.CompareDiffPath.Replace('\\', '/')
                              );

                String lstrUrl = lstrCString;
                System.Net.WebClient loClient = new System.Net.WebClient();
                System.IO.Stream loData = loClient.OpenRead(lstrUrl);
                System.IO.StreamReader loReader = new StreamReader(loData);
                String s = loReader.ReadToEnd();
                int lnRet = 0;
                Int32.TryParse(s, out lnRet);
                if (lnRet > 0)
                {
                    File.Copy(this.CompareImagePath0, this.CompareImagePath0_bak);
                    File.Copy(this.CompareImagePath1, this.CompareImagePath1_bak);
                    File.Copy(this.CompareDiffPath, this.CompareDiffPath_bak);
                    //File.Copy(this.AviPath, this.AviPathb);
                    return true;
                }

                return false;
            }
            catch (Exception e)
            {
                ThreadUiController.log(e.Message, ThreadUiController.LOG_LEVEL.ERROR);
                return false;
            }
        }


        private bool detect(byte[] arefImg, int anLengthReal)
        {
            this.initDetector();

            unsafe
            {
                fixed (byte* charPtr = &arefImg[0])
                {
                    List<bbox_t_csharp> loRet2 = Biz.moDetector.detect(charPtr, (int)anLengthReal, 0);

                    int lnPerson = Biz.moDetector.detectPerson(charPtr, (int)anLengthReal, 0.4f);

                    if (lnPerson > 0)
                    {
                        return true;
                    }

                    String lstrDebug = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + "| userid:" + this.UserID;

                    ThreadUiController.outputDebugString(lstrDebug + "|is Person:" + lnPerson + "\r\n");
                }
            }

            return false;
        }

        AutoResetEvent waitHandle = new AutoResetEvent(false);
        private void thread_capture2()
        {
           
            byte[] loBuffer = new byte[1024 * 1024];
            byte[] loBuffer2 = new byte[1024 * 1024];
            byte[] loBuffer3 = new byte[1024 * 1024];

            while (true)
            {
               
                lock (this)
                {
                    if (this.Stop)
                    {
                        return;
                    }

                    bool lbRet = this.CaptureImage2(this.UserID, loBuffer, loBuffer2, loBuffer3);
                    this.alarm(lbRet);
                    waitHandle.WaitOne();
                }
            }
        }
         Dse.FixComm m_oFixComm = null;
        public  Dse.FixComm FixComm
        {
            get { return m_oFixComm; }
            set {  m_oFixComm = value; }
        }
        private  void send_thread_entry()
        {
            this.FixComm = new Dse.FixComm();
            bool lbRet = this.FixComm.Link(this.MiddlewareIP, "" + this.MiddlewarePort);
            Dictionary<String, Packet> loLaskPackets = new Dictionary<string, Packet>();
            while (true)
            {

              
                {
                    if (this.SendThreadStop)
                    {
                        return;
                    }
                }

                Packet lpPacket = null;
                bool lbNeedCheckAndRemovePacket = false;

                if(this.StatusList.Count>0)
                {
                    lpPacket = this.StatusList[0];
                    lbNeedCheckAndRemovePacket = true;
                }

                if(lpPacket == null)
                {
                   if(loLaskPackets.Count>0)
                    {
                        foreach(KeyValuePair<String,Packet> lpIt in loLaskPackets)
                        {
                            lpPacket = lpIt.Value;
                        }
                    }
                }


                if(lbRet)
                {
                   lbRet = this.doSend(lpPacket);
                }

                if(lbRet)
                {
                    if(lbNeedCheckAndRemovePacket)
                    {
                        this.StatusList.RemoveAt(0);
                    }
                }
               

                try
                {
                    Thread.Sleep(100);
                }
                catch (Exception e) { }
            }
        }

     

   
        private bool doSend(Packet apPacket)
        {
         
            try
            {
                if(null == apPacket)
                {
                    return false;
                }

                System.Collections.Generic.List<Packet> loRet = this.StatusList;

                int lnStatus = this.LastAlarmStatus;
                Packet loValue = null;
                DateTime loDateTime = DateTime.Now;
                        

                Dse.FixComm lpFixComm = this.FixComm;               
                {
                    bool lbRet = lpFixComm.CreateHead(SERVICE_SERVICE_ON_RECEIVEPROC);
                    if (lbRet)
                    {

                        lpFixComm.Set(FIELD_SYSTEM_COMPANY, apPacket.Company);
                        lpFixComm.Set(FIELD_SYSTEM_FACTORY, apPacket.Factory);
                        lpFixComm.Set(FIELD_SERVICE_PLANTID, apPacket.Plant);
                        lpFixComm.Set(FIELD_SERVICE_ON_SEND_STARTID, apPacket.StartIndex);
                        lpFixComm.Set(FIELD_SERVICE_CHANNTYPE, CHANNTYPE.GE_ALLPROC);
                        lpFixComm.Set(FIELD_SERVICE_ON_SEND_PROCCHANNNUM, 1);
                        using (ValueCache loBuffer = new ValueCache())
                        {

                            loBuffer.Ensure(sizeof(float) * 1);
                            loBuffer.Clear();
                            loBuffer.Add((float)apPacket.Value);
                            var buf = loBuffer.ToArray();
                            Debug.Assert(buf.Length == (sizeof(float) * 1));
                            lpFixComm.Set(FIELD_SERVICE_ON_SEND_ALLPROCVALUE, buf);
                        }

                        lpFixComm.Set(FIELD_SERVICE_TIME, loDateTime);

                        if (lpFixComm.More())
                        {
                            return true;
                        }
                    }
                }
               

            }
            catch(Exception e)
            {

            }

            return false;
        }

        private void thread_capture()
        {

            byte[] loBuffer = new byte[1024 * 1024];
            byte[] loBuffer2 = new byte[1024 * 1024];
            byte[] loBuffer3 = new byte[1024 * 1024];
            while (true)
            {
                List<Int32> loList = new List<int>();
                lock (this)
                {
                    if (this.Stop)
                    {
                        return;
                    }
                }

                bool lbRet = this.CaptureImage(this.UserID, loBuffer, loBuffer2, loBuffer3);
                alarm_delegate loAlarm = new alarm_delegate(alarm);
                loAlarm.Invoke(lbRet);
            }
        }


        public Boolean login()
        {
            CHCNetSDK.NET_DVR_DEVICEINFO_V30 DeviceInfo = new CHCNetSDK.NET_DVR_DEVICEINFO_V30();
            UserID = CHCNetSDK.NET_DVR_Login_V30(IP, Port, UserName, Password, ref DeviceInfo);

            if (UserID < 0)
            {
                iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                str = IP + ":" + Port + ":" + UserName + ":" + Password + "NET_DVR_Login_V30 failed, error code= " + iLastErr; //登录失败，输出错误号
                                                                                                                               // MessageBox.Show(str);
                ThreadUiController.log(str, ThreadUiController.LOG_LEVEL.ERROR);
                ThreadUiController.outputDebugString(str);
                return false;
            }

            return true;
        }

        private delegate void alarm_delegate(bool abAlarm);
        private void alarm(bool abAlarm)
        {
            return;
            lock (this)
            {
                if (abAlarm)
                {
                    this.Status = 1;
                }
                else
                {
                    this.Status = 0;
                }

                // return;

                if (this.PicPox == null)
                {
                    return;
                }

                if (abAlarm)
                {

                    if (this.PicPox.Controls != null && this.PicPox.Controls.Count > 0)
                    {
                        Control c = this.PicPox.Controls[0];
                        if (c.GetType() == typeof(CustomPictureBox))
                        {
                            ((CustomPictureBox)c).UserID = this.UserID;
                        }

                        ThreadUiController.ShowControl(this.PicPox.Controls[0]);
                        //                     this.PicPox.Controls[0].Visible = true;
                        //                     this.PicPox.Controls[0].BackColor = Color.Red;
                    }

                }
                else
                {
                    if (this.PicPox.Controls != null && this.PicPox.Controls.Count > 0)
                    {
                        Control c = this.PicPox.Controls[0];
                        if (c.GetType() == typeof(CustomPictureBox))
                        {
                            ((CustomPictureBox)c).UserID = -1;
                        }

                        ThreadUiController.HideControl(this.PicPox.Controls[0]);
                    }
                }

            }

        }

        private void CaptureBmp2(int index, byte[] loBuffer)
        {
            string sBmpPicFileName;
            //图片保存路径和文件名 the path and file name to save

            string myExeDir = this.BaseDir;

            String myTempDir = this.TempDir;

            myTempDir = Detectorbridge.MakeSureImagePathExist(myTempDir);

            myExeDir = Detectorbridge.MakeSureImagePathExist(myExeDir);

            sBmpPicFileName = myTempDir + "\\" + this.UserID + "_" + index + ".bmp";

            //  string myExeDir = new FileInfo(Assembly.GetEntryAssembly().Location).Directory.ToString();

            if (index == 0)
            {
                this.CompareImagePath0 = sBmpPicFileName;
            }
            else
            {
                this.CompareImagePath1 = sBmpPicFileName;
            }

            this.CompareDiffPath = myTempDir + "\\" + "diff.bmp";
            this.AviPath = myTempDir + "\\" + this.UserID + "_diff.avi";

            long milliseconds = DateTimeOffset.Now.Ticks;

            this.CompareImagePath0_bak = myExeDir + "\\" + this.UserID + "_0_" + milliseconds + "_.bmp";
            this.CompareImagePath1_bak = myExeDir + "\\" + this.UserID + "_1_" + milliseconds + "_.bmp";
            this.m_strCompareDiffPath_bak = myExeDir + "\\" + this.UserID + "_diff_" + milliseconds + "_.bmp";

            this.AviBakPath = myExeDir + "\\" + this.UserID + "_diff.avi";

            File.WriteAllBytes(sBmpPicFileName, loBuffer);

        }
        private void CaptureBmp(int index)
        {
            string sBmpPicFileName;
            //图片保存路径和文件名 the path and file name to save


            string myExeDir = this.BaseDir;

            String myTempDir = this.TempDir;

            myTempDir = Detectorbridge.MakeSureImagePathExist(myTempDir);

            myExeDir = Detectorbridge.MakeSureImagePathExist(myExeDir);

            sBmpPicFileName = myTempDir + "\\" + this.UserID + "_" + index + ".bmp";

            //  string myExeDir = new FileInfo(Assembly.GetEntryAssembly().Location).Directory.ToString();

            if (index == 0)
            {
                this.CompareImagePath0 = sBmpPicFileName;
            }
            else
            {
                this.CompareImagePath1 = sBmpPicFileName;
            }

            this.CompareDiffPath = myTempDir + "\\" + "diff.bmp";
            this.AviPath = myTempDir + "\\" + this.UserID + "_diff.avi";

            long milliseconds = DateTimeOffset.Now.Ticks;

            this.CompareImagePath0_bak = myExeDir + "\\" + this.UserID + "_0_" + milliseconds + "_.bmp";
            this.CompareImagePath1_bak = myExeDir + "\\" + this.UserID + "_1_" + milliseconds + "_.bmp";
            this.m_strCompareDiffPath_bak = myExeDir + "\\" + this.UserID + "_diff_" + milliseconds + "_.bmp";

            this.AviBakPath = myExeDir + "\\" + this.UserID + "_diff.avi";

            if (!CHCNetSDK.NET_DVR_CapturePicture(RealHandle, sBmpPicFileName))
            {
                iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                str = "NET_DVR_CapturePicture failed, error code= " + iLastErr;
                ThreadUiController.outputDebugString(str);

                return;
            }
            else
            {
                str = "Successful to capture the BMP file and the saved file is " + sBmpPicFileName;

            }
        }


        private void cbDrawFun(int lRealHandle, IntPtr hDc, uint dwUser)
        {
            return;

            Graphics g = null;

            if (null == hDc)
            {
                return;
            }

            try
            {
                g = Graphics.FromHdc(hDc);
            }
            catch (Exception e)
            {
                return;
            }

            try
            {
                if (this.Status > 0)
                {
                    if (this.Status == 2)
                    {


                        SolidBrush opaqueBrush = new SolidBrush(Color.FromArgb(100, 255, 255, 0));
                        using (opaqueBrush)
                        {
                            g.FillRectangle(opaqueBrush, 0, 0, this.PicPox.Width, this.PicPox.Height);
                            g.Dispose();
                        }
                    }
                    else if (this.Status == 1)
                    {


                        SolidBrush opaqueBrush = new SolidBrush(Color.FromArgb(100, 255, 0, 0));
                        using (opaqueBrush)
                        {
                            g.FillRectangle(opaqueBrush, 0, 0, this.PicPox.Width, this.PicPox.Height);
                            g.Dispose();
                        }
                    }
                }

            }
            catch (Exception e)
            {

            }
            finally
            {
                if (null != g)
                {
                    g.Dispose();
                }
            }


        }


        public Boolean live(PictureBox pictureBox)
        {
            if (null == pictureBox)
            {
                return false;
            }

            CHCNetSDK.NET_DVR_PREVIEWINFO lpPreviewInfo = new CHCNetSDK.NET_DVR_PREVIEWINFO();
            lpPreviewInfo.hPlayWnd = pictureBox.Handle;//预览窗口
            lpPreviewInfo.lChannel = 1;//预te览的设备通道
            lpPreviewInfo.dwStreamType = 0;//码流类型：0-主码流，1-子码流，2-码流3，3-码流4，以此类推
            lpPreviewInfo.dwLinkMode = 0;//连接方式：0- TCP方式，1- UDP方式，2- 多播方式，3- RTP方式，4-RTP/RTSP，5-RSTP/HTTP 
            lpPreviewInfo.bBlocked = false; //0- 非阻塞取流，1- 阻塞取流
            lpPreviewInfo.dwDisplayBufNum = 15; //播放库播放缓冲区最大缓冲帧数

            CHCNetSDK.REALDATACALLBACK RealData = new CHCNetSDK.REALDATACALLBACK(OnRealDataCallBackAll);//预览实时流回调函数
            IntPtr pUser = new IntPtr();//用户数据

            //打开预览 Start live view 
            RealHandle = CHCNetSDK.NET_DVR_RealPlay_V40(UserID, ref lpPreviewInfo, null/*RealData*/, pUser);
            if (RealHandle < 0)
            {
                uint iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                String str = "NET_DVR_RealPlay_V40 failed, error code= " + iLastErr; //预览失败，输出错误号
                MessageBox.Show(str);
                return false;
            }
            else
            {


                return true;
            }
        }

        public Boolean live(CustomPictureBox pictureBox)
        {
            if (null == pictureBox)
            {
                return false;
            }
            pictureBox.UserID = this.UserID;
            CHCNetSDK.NET_DVR_PREVIEWINFO lpPreviewInfo = new CHCNetSDK.NET_DVR_PREVIEWINFO();
            lpPreviewInfo.hPlayWnd = pictureBox.Handle;//预览窗口
            this.hWnd = pictureBox.Handle;
            lpPreviewInfo.lChannel = 1;//预te览的设备通道
            lpPreviewInfo.dwStreamType = 0;//码流类型：0-主码流，1-子码流，2-码流3，3-码流4，以此类推
            lpPreviewInfo.dwLinkMode = 0;//连接方式：0- TCP方式，1- UDP方式，2- 多播方式，3- RTP方式，4-RTP/RTSP，5-RSTP/HTTP 
            lpPreviewInfo.bBlocked = false; //0- 非阻塞取流，1- 阻塞取流
            lpPreviewInfo.dwDisplayBufNum = 15; //播放库播放缓冲区最大缓冲帧数

            CHCNetSDK.REALDATACALLBACK RealData = new CHCNetSDK.REALDATACALLBACK(OnRealDataCallBackAll);//预览实时流回调函数
            IntPtr pUser = new IntPtr();//用户数据
            RealHandle = CHCNetSDK.NET_DVR_RealPlay_V40(UserID, ref lpPreviewInfo, null/*RealData*/, pUser);

            //打开预览 Start live view 

            if (RealHandle < 0)
            {
                uint iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                String str = "NET_DVR_RealPlay_V40 failed, error code= " + iLastErr; //预览失败，输出错误号
                MessageBox.Show(str);
                return false;
            }
            else
            {
                //bool lbRet = CHCNetSDK.NET_DVR_RigisterDrawFun(m_lRealHandle, (cbDrawFun), 0);
                //  bool lbRet = CHCNetSDK.NET_DVR_SetStandardDataCallBack(this.RealHandle, this.STDDATACALLBACK, (uint)this.m_lUserID);
                return true;
            }
        }

        public Boolean login3()
        {

            this.UserID = Custom_hk_api.login(this.UserName, this.Password, this.IP, this.Port);

            if (UserID < 0)
            {
                iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                str = IP + ":" + Port + ":" + UserName + ":" + Password + "NET_DVR_Login_V30 failed, error code= " + iLastErr; //登录失败，输出错误号
                                                                                                                               // MessageBox.Show(str);
                ThreadUiController.log(str, ThreadUiController.LOG_LEVEL.ERROR);
                ThreadUiController.outputDebugString(str);
                return false;
            }

            return true;
        }

        private System.Windows.Forms.Timer m_oStatusTimer = new System.Windows.Forms.Timer();
        public Boolean live3(CustomPictureBox pictureBox)
        {
            if (null == pictureBox)
            {
                return false;
            }
            int lnErrocCode = 0;
            // HwndSource hwndSource = HwndSource.FromHwnd(pictureBox.Handle);
            bool lbRet = Custom_hk_api.live(this.UserID, pictureBox.Handle, 0, ref this.m_lRealHandle, ref lnErrocCode);
            return lbRet;
        }
        int m_nLastAlarmStatus = 0;
        String m_strLastAlarmStatusLock = "m_strLastAlarmStatusLock";
        public int LastAlarmStatus
        {
            get { lock(m_strLastAlarmStatusLock)return m_nLastAlarmStatus; }
            set { lock (m_strLastAlarmStatusLock) m_nLastAlarmStatus = value; }
        }

     

        List<Packet> m_oStatusList = new List<Packet>();
        public System.Collections.Generic.List<Packet> StatusList
        {
            get { return m_oStatusList; }
            set { m_oStatusList = value; }
        }

        RabbitMQ m_oRabbitMq = new RabbitMQ();
        public void RegSend(int anStatus)
        {
            try
            {
                Debug.WriteLine(String.Format("RegSend {0}:", anStatus));
                Packet loPacket = new Packet();
                loPacket.Company = this.Company;
                loPacket.Factory = this.Factory;
                loPacket.Plant = this.Plant;
                loPacket.StartIndex = this.StartIndex;
                loPacket.Value = anStatus;
                loPacket.DateTime = DateTime.Now;
                loPacket.Middleware = this.MiddlewareIP;
                loPacket.Port = this.MiddlewarePort;
                m_oRabbitMq.Send(loPacket);
            }
            catch(Exception e)
            {

            }

        }

        DateTime m_oLastSendTime = new DateTime();
        public  void onStatusCallback(int anStatus)
        {
            int lnLastStatus = this.AlarmStatus;
            if(lnLastStatus!=anStatus)
            {
                this.RegSend(anStatus);
            }

            TimeSpan loSpan = DateTime.Now - m_oLastSendTime;
            if(loSpan.TotalSeconds>1)
            {
                this.RegSend(anStatus);
                this.m_oLastSendTime = DateTime.Now;
            }
            this.AlarmStatus = anStatus;
        }

            byte[] data = new byte[1920*1080*3];
        public unsafe void onFrameCallback(int width,
                int height,
                int stride, 
                ref byte buffer,
                int length)
        {
            fixed (byte* p = &buffer)
            {
                Bitmap bitmap = new Bitmap(width, height, PixelFormat.Format24bppRgb);
                BitmapData bmData = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadWrite, bitmap.PixelFormat);
                IntPtr pNative = bmData.Scan0;               
                Marshal.Copy(new IntPtr(p), data, 0, length);
                Marshal.Copy(data, 0, pNative, length);
                bitmap.UnlockBits(bmData);
                MemoryStream loBuffer = new MemoryStream();
                bitmap.Save(loBuffer, ImageFormat.Bmp);
                byte[] loBufferTempx = loBuffer.ToArray();
                this.Queue.Enqueue(loBufferTempx);

                if(this.Queue.Count>2)
                {
                    waitHandle.Set();
                }
            }


        }



        framecallback callback = null;

        OnStatusChange status_callback = null;

        public Boolean login4()
        {
            this.status_callback = new OnStatusChange(this.onStatusCallback);         

            this.UserID = Custom_hk_api.login_v2(this.UserName,
                this.Password,
                this.IP,
                this.Port, 
                this.RTSP_PORT,
                this.DetectServerIP,
                this.DetectServerPort,
                this.FrameInterval,
                this.IgnorePerson,
                this.distance_ratio,
                this.area_min,
                this.status_callback,
                this.ROI_X,
                this.ROI_Y,
                this.ROI_W,
                this.ROI_H,
                this.CamSrc,
                this.CamIndex,
                this.CamMode,
                this.DisplayGreenRect,
                this.Ignore);

            if (UserID < 0)
            {
                iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                str = IP + ":" + Port + ":" + UserName + ":" + Password + "NET_DVR_Login_V30 failed, error code= " + iLastErr; //登录失败，输出错误号
                                                                                                                               // MessageBox.Show(str);
                ThreadUiController.log(str, ThreadUiController.LOG_LEVEL.ERROR);
                ThreadUiController.outputDebugString(str);
                return false;
            }

            return true;
        }

        private void OnTick(object sender, EventArgs e)
        {
            this.onStatusCallback(this.AlarmStatus);
        }


        public Boolean live4(CustomPictureBox pictureBox)
        {
            this.callback = new framecallback(onFrameCallback);
            if (null == pictureBox)
            {
                return false;
            }
            int lnErrocCode = 0;
            // HwndSource hwndSource = HwndSource.FromHwnd(pictureBox.Handle);
           // this.live(pictureBox);
            bool lbRet = Custom_hk_api.live_v2(this.UserID, 
                pictureBox.Handle,
                0, 
                ref this.m_lRealHandle,
                ref lnErrocCode, 
                this.callback);
            this.m_oStatusTimer.Interval = 1;
            this.m_oStatusTimer.Enabled = true;
            this.m_oStatusTimer.Tick += this.OnTick;
            return lbRet;
        }

        public Boolean live2(CustomPictureBox pictureBox)
        {
            if (null == pictureBox)
            {
                return false;
            }
            pictureBox.UserID = this.UserID;
            CHCNetSDK.NET_DVR_PREVIEWINFO lpPreviewInfo = new CHCNetSDK.NET_DVR_PREVIEWINFO();
            lpPreviewInfo.hPlayWnd = pictureBox.Handle;//预览窗口
            this.hWnd = pictureBox.Handle;
            lpPreviewInfo.lChannel = 1;//预te览的设备通道
            lpPreviewInfo.dwStreamType = 0;//码流类型：0-主码流，1-子码流，2-码流3，3-码流4，以此类推
            lpPreviewInfo.dwLinkMode = 0;//连接方式：0- TCP方式，1- UDP方式，2- 多播方式，3- RTP方式，4-RTP/RTSP，5-RSTP/HTTP 
            lpPreviewInfo.bBlocked = false; //0- 非阻塞取流，1- 阻塞取流
            lpPreviewInfo.dwDisplayBufNum = 15; //播放库播放缓冲区最大缓冲帧数

            CHCNetSDK.REALDATACALLBACK RealData = new CHCNetSDK.REALDATACALLBACK(OnRealDataCallBackAll);//预览实时流回调函数
            IntPtr pUser = new IntPtr();//用户数据
            RealHandle = CHCNetSDK.NET_DVR_RealPlay_V40(UserID, ref lpPreviewInfo, RealData/*RealData*/, pUser);

            CHCNetSDK.NET_DVR_CLIENTINFO ClientInfo = new CHCNetSDK.NET_DVR_CLIENTINFO();
            ClientInfo.lChannel = 1;        //Channel number 设备通道号  
            ClientInfo.hPlayWnd = this.hWnd;     //窗口为空，设备SDK不解码只取流  
            ClientInfo.lLinkMode = 0;       //Main Stream  
                                            // ClientInfo.sMultiCastIP = NULL;
                                            // RealHandle = CHCNetSDK.NET_DVR_RealPlay_V30(UserID, ref ClientInfo, null, pUser, 1);
                                            //打开预览 Start live view 

            if (RealHandle < 0)
            {
                uint iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                String str = "NET_DVR_RealPlay_V40 failed, error code= " + iLastErr; //预览失败，输出错误号
                                                                                     //   MessageBox.Show(str);
                return false;
            }
            else
            {
                // bool lbRet = CHCNetSDK.NET_DVR_RigisterDrawFun(m_lRealHandle, (cbDrawFun), 0);
                //  bool lbRet = CHCNetSDK.NET_DVR_SetStandardDataCallBack(this.RealHandle, this.STDDATACALLBACK, (uint)this.m_lUserID);
                return true;
            }
        }

        public void stopLive(System.Windows.Forms.PictureBox pictureBox)
        {
            if (null == pictureBox)
            {
                return;
            }

            IntPtr Handle = pictureBox.Handle;
            if (!CHCNetSDK.NET_DVR_StopRealPlay((int)Handle))
            {
                iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                str = "NET_DVR_StopRealPlay failed, error code= " + iLastErr;

                return;
            }
        }


        public void OnRealDataCallBackRTSP(int lRealHandle, uint dwDataType, ref byte pBuffer, uint dwBufSize, uint dwUser)
        {
        }

        public void OnRealDataCallBackAll(Int32 lRealHandle, UInt32 dwDataType, ref byte pBuffer, UInt32 dwBufSize, IntPtr pUser)
        {

            unsafe
            {
                fixed (byte* lpBuffer = &pBuffer)
                {
                    this.HkHelper.DecCBFun(lRealHandle, dwDataType, lpBuffer, dwBufSize, pUser, (IntPtr)0, null);
                }

            }
        }

        HKHelper.DecodeImageBufferCallBack m_oOnImageCallback = null;
        public detectorbridge.HKHelper.DecodeImageBufferCallBack OnImageCallback
        {
            get { lock (this) return m_oOnImageCallback; }
            set { lock (this) m_oOnImageCallback = value; }
        }
        public void onImageCallBack(byte[] apBuffer, int anLength)
        {
            System.Diagnostics.Debug.WriteLine(anLength + "");
        }

        Boolean isStarted = false;
        public System.Boolean IsStarted
        {
            get { lock (this) return isStarted; }
            set { lock (this) isStarted = value; }
        }

        protected bool inner_start()
        {
            {
                this.init();
                bool lbRet = this.login();
                this.isStarted = lbRet;
                if (!lbRet)
                {

                    return false;
                }
                this.live(this.PicPox);
                this.startCapture_thread();
                return true;
            }
        }

        protected bool inner_start2()
        {
            {
                this.init();
                bool lbRet = this.login();
                this.isStarted = lbRet;
                if (!lbRet)
                {

                    return false;
                }
                this.live2(this.PicPox);
                this.startCapture_thread2();
                return true;
            }
        }

        protected bool inner_start3()
        {
            {
                this.init();
                bool lbRet = this.login3();
                this.isStarted = lbRet;
                if (!lbRet)
                {

                    return false;
                }
                this.live3(this.PicPox);
                this.startCapture_thread2();
                return true;
            }
        }

        protected bool inner_start4()
        {
            {
                this.init();
                bool lbRet = this.login4();
                this.isStarted = lbRet;
                if (!lbRet)
                {

                    return false;
                }
                this.live4(this.PicPox);
                this.startSender_thread();
                return true;
            }
        }
        public bool start()
        {
            return this.inner_start4();
        }

        private void stopSender_thread()
        {
            lock (this.m_strLockSendThreadStop)
            {
                this.SendThreadStop = true;
            }
            if (this.m_oSendThread != null)
            {
                this.SendThreadStop = true;

                Boolean lbSucceedExit = false;

                try
                {
                    lbSucceedExit = this.m_oSendThread.Join(2000);

                }
                catch (Exception e)
                {

                }

                try
                {
                    if (!lbSucceedExit)
                    {
                        this.m_oSendThread.Abort();
                    }

                }
                catch (Exception e)
                {

                }


            }

            lock (this.m_strLockSendThreadStop)
            {
                this.m_oSendThread = null;
            }

        }

        private void stopCapture_thread()
        {
            lock (this)
            {
                this.Stop = true;
            }
            if (this.m_oCaptureThread != null)
            {
                this.Stop = true;

                Boolean lbSucceedExit = false;

                try
                {
                    lbSucceedExit = this.m_oCaptureThread.Join(2000);

                }
                catch (Exception e)
                {

                }

                try
                {
                    if (!lbSucceedExit)
                    {
                        this.m_oCaptureThread.Abort();
                    }

                }
                catch (Exception e)
                {

                }


            }

            lock (this)
            {
                this.m_oCaptureThread = null;
            }

        }
    }
}
