﻿using System;
using System.Collections.Generic;

using System.Windows.Forms;
using EricZhao.UiThread;
using System.Threading;
using static haikang_video.Custom_hk_api;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;
using EricZhao;

namespace haikang_video
{
    static class Program
    {
        static MainWindow m_oMainWindow = null;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
       
        static void Main()
        {
            MiniDump.RegistUnhandledExceptionHandler();
            Application.EnableVisualStyles();
           
            MainWindow.ShouldQuit = false;
            m_oMainWindow = new MainWindow();
            //  Application.SetCompatibleTextRenderingDefault(false);
            
            Application.Run(m_oMainWindow);
        }

    }
}
