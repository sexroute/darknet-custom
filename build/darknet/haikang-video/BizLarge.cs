﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows;

using System.Text;
using detectorbridge;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using EricZhao.UiThread;
using System.IO;
using System.Reflection;
using System.Messaging;
using static haikang_video.Custom_hk_api;
using System.Drawing.Imaging;

namespace haikang_video
{
    public class BizLarge
    {
        private static int m_nGUID = 0;
        private static String ServerBizLockObject = "ServerBiz.ServerBizLockObject";
        public static int GenUID()
        {
            lock (ServerBizLockObject)
            {
                int lnRet = m_nGUID++;

                return lnRet;
            }
        }
        public BizLarge()
        {
            this.UserID = BizLarge.GenUID();
        }
        String m_strDetectServerIP = "127.0.0.1";
        public System.String DetectServerIP
        {
            get { lock (this) return m_strDetectServerIP; }
            set { lock (this) m_strDetectServerIP = value; }
        }
        int m_nDetectServerPort = 18081;
        public int DetectServerPort
        {
            get { lock (this) return m_nDetectServerPort; }
            set { lock (this) m_nDetectServerPort = value; }
        }

        int m_nIgnorePerson = 0;
        public int IgnorePerson
        {
            get { lock (this) return m_nIgnorePerson; }
            set { lock (this) m_nIgnorePerson = value; }
        }

        int m_nAlarmStatus = 0;
        public int AlarmStatus
        {
            get { lock (this) return m_nAlarmStatus; }
            set { lock (this) m_nAlarmStatus = value; }
        }

        double distance_ratio = 2.0;
        public double Distance_ratio
        {
            get { lock (this) return distance_ratio; }
            set { lock (this) distance_ratio = value; }
        }

        int area_min = 100;
        public int Area_min
        {
            get { lock (this) return area_min; }
            set { lock (this) area_min = value; }
        }
        int m_nFrameInterval = 20;
        public int FrameInterval
        {
            get { lock (this) return m_nFrameInterval; }
            set { lock (this) { if (m_nFrameInterval >= 0) { m_nFrameInterval = value; } } }
        }
        int m_nRTSP_PORT = 0;
        public int RTSP_PORT
        {
            get { lock (this) return m_nRTSP_PORT; }
            set { lock (this) m_nRTSP_PORT = value; }
        }

        String m_strCamSrc = "";
        public System.String CamSrc
        {
            get { lock (this) return m_strCamSrc; }
            set { lock (this) m_strCamSrc = value; }
        }
        int m_nCamMode = 0;
        public int CamMode
        {
            get { lock (this) return m_nCamMode; }
            set { lock (this) m_nCamMode = value; }
        }
        int m_nCamIndex = 0;
        public int CamIndex
        {
            get { lock (this) return m_nCamIndex; }
            set { lock (this) m_nCamIndex = value; }
        }

        bool m_nIsFirst = true;
        public bool IsFirst
        {
            get { lock (this) return m_nIsFirst; }
            set { lock (this) m_nIsFirst = value; }
        }

        String msqUrl = "";
        public System.String MsqUrl
        {
            get { lock (this) return msqUrl; }
            set { lock (this) msqUrl = value; }
        }
        int m_nIgnore = 0;
        public int Ignore
        {
            get { lock (this) return m_nIgnore; }
            set { lock (this) m_nIgnore = value; }
        }

        int m_nDisplayGreenRect = 0;
        public int DisplayGreenRect
        {
            get { lock (this) return m_nDisplayGreenRect; }
            set { lock (this) m_nDisplayGreenRect = value; }
        }

        int m_nIndex = 0;
        public int Index
        {
            get
            {
                lock (this) { return m_nIndex; }
            }
            set
            {
                lock (this) { m_nIndex = value; }
            }
        }

        int m_nROI_X = 0;
        public int ROI_X
        {
            get { lock (this) return m_nROI_X; }
            set { lock (this) m_nROI_X = value; }
        }
        int m_nROI_Y = 0;
        public int ROI_Y
        {
            get { lock (this) return m_nROI_Y; }
            set { lock (this) m_nROI_Y = value; }
        }
        int m_nROI_W = 0;
        public int ROI_W
        {
            get { lock (this) return m_nROI_W; }
            set { lock (this) m_nROI_W = value; }
        }
        int m_nROI_H = 0;
        public int ROI_H
        {
            get { lock (this) return m_nROI_H; }
            set { lock (this) m_nROI_H = value; }
        }
        CustomPictureBox m_oPicPox = null;
        private String m_Str_PicBox_Lock = "m_Str_PicBox_Lock";
        public CustomPictureBox PicPox
        {
            get { lock (this.m_Str_PicBox_Lock) return m_oPicPox; }
            set { lock (this.m_Str_PicBox_Lock) m_oPicPox = value; }
        }
        private int m_lUserID = -1;
        private String m_lUserID_lock = "m_lUserID_lock";
        public int UserID
        {
            get { lock (this.m_lUserID_lock) return m_lUserID; }
            set { lock (this.m_lUserID_lock) m_lUserID = value; }
        }
        private Int32 m_lRealHandle = -1;
        public System.Int32 RealHandle
        {
            get { lock (this) return m_lRealHandle; }
            set { lock (this) m_lRealHandle = value; }
        }
        System.Threading.Thread m_oCaptureThread = null;
        Boolean m_bStop = false;
        public System.Boolean Stop
        {
            get { lock (this) return m_bStop; }
            set { lock (this) m_bStop = value; }
        }
        private static bool m_bInitSDK = false;

        string DVRIPAddress = "192.168.1.64"; //设备IP地址或者域名
        public string IP
        {
            get { lock (this) return DVRIPAddress; }
            set { lock (this) DVRIPAddress = value; }
        }
        Int16 DVRPortNumber = 8000;//设备服务端口号
        public System.Int16 Port
        {
            get { lock (this) return DVRPortNumber; }
            set { lock (this) DVRPortNumber = value; }
        }
        private uint iLastErr = 0;
        private string str = "";

        string DVRUserName = "admin";//设备登录用户名
        public string UserName
        {
            get { lock (this) return DVRUserName; }
            set { lock (this) DVRUserName = value; }
        }
        string DVRPassword = "bohuaxinzhi123";//设备登录密码
        public string Password
        {
            get { lock (this) return DVRPassword; }
            set { lock (this) DVRPassword = value; }
        }
        protected static detectorbridge.Detectorbridge moDetector = null;
        protected static String moDetector_locker = "detectorbridge.Detectorbridge moDetector";

        private String m_strCompareImagePath0 = "";
        public System.String CompareImagePath0
        {
            get { lock (this) return m_strCompareImagePath0; }
            set { lock (this) m_strCompareImagePath0 = value; }
        }
        private String m_strCompareImagePath1 = "";
        public System.String CompareImagePath1
        {
            get { lock (this) return m_strCompareImagePath1; }
            set { lock (this) m_strCompareImagePath1 = value; }
        }

        private String m_strCompareDiffPath = "";
        public System.String CompareDiffPath
        {
            get { lock (this) return m_strCompareDiffPath; }
            set { lock (this) m_strCompareDiffPath = value; }
        }
        String m_strTempDir = "./tmp";
        public System.String TempDir
        {
            get { lock (this) return m_strTempDir; }
            set { lock (this) m_strTempDir = value; }
        }
        String m_strBaseDir = "./data";
        public System.String BaseDir
        {
            get { lock (this) return m_strBaseDir; }
            set { lock (this) m_strBaseDir = value; }
        }

        IntPtr m_hWnd = new IntPtr(0);
        public System.IntPtr hWnd
        {
            get { return m_hWnd; }
            set { m_hWnd = value; }
        }

        private delegate void CaptureBmp_delegate(int index);


        private String m_strCompareImagePath0_bak = "";
        public System.String CompareImagePath0_bak
        {
            get { lock (this) return m_strCompareImagePath0_bak; }
            set { lock (this) m_strCompareImagePath0_bak = value; }
        }
        private String m_strCompareImagePath1_bak = "";
        public System.String CompareImagePath1_bak
        {
            get { lock (this) return m_strCompareImagePath1_bak; }
            set { lock (this) m_strCompareImagePath1_bak = value; }
        }

        private String m_strCompareDiffPath_bak = "";
        public System.String CompareDiffPath_bak
        {
            get { lock (this) return m_strCompareDiffPath_bak; }
            set { lock (this) m_strCompareDiffPath_bak = value; }
        }

        String m_strAviBakPath = "";
        public System.String AviBakPath
        {
            get { lock (this) return m_strAviBakPath; }
            set { lock (this) m_strAviBakPath = value; }
        }
        private String m_strAviPath = "";
        public System.String AviPath
        {
            get { lock (this) return m_strAviPath; }
            set { lock (this) m_strAviPath = value; }
        }

        private int m_nStatus = 0;
        private String m_strStatusLock = "m_strStatusLock";
        public int Status
        {
            get { lock (this.m_strStatusLock) return m_nStatus; }
            set { lock (this.m_strStatusLock) m_nStatus = value; }
        }

        Queue<byte[]> m_oQueue = null;
        public Queue<byte[]> Queue
        {
            get { lock (this) return m_oQueue; }
            set { lock (this) m_oQueue = value; }
        }
        private void initDetector()
        {
            lock (BizLarge.moDetector_locker)
            {
                if (BizLarge.moDetector == null)
                {
                    BizLarge.moDetector = new Detectorbridge();
                }
            }
        }


        HKHelper m_oHkHelper = null;
        public detectorbridge.HKHelper HkHelper
        {
            get { lock (this) return m_oHkHelper; }
            set { lock (this) m_oHkHelper = value; }
        }
        private void init()
        {
            lock (BizLarge.moDetector_locker)
            {
                if (BizLarge.m_bInitSDK == false)
                {
                    BizLarge.m_bInitSDK = true;
                }

                if (this.Queue == null)
                {
                    this.Queue = new Queue<byte[]>();
                }

                if (this.OnImageCallback == null)
                {
                    this.OnImageCallback = new HKHelper.DecodeImageBufferCallBack(this.onImageCallBack);
                }

                if (this.HkHelper == null)
                {
                    this.HkHelper = new HKHelper();
                    this.HkHelper.setImageDecodedCallBack(this.OnImageCallback);
                }
            }
        }


        private void startCapture_thread()
        {
            this.stopCapture_thread();

            this.init();

            lock (this)
            {
                this.Stop = false;

            }
        }


        private void startCapture_thread2()
        {

            this.stopCapture_thread();

            this.init();

            lock (this)
            {
                this.Stop = false;

            }
        }




        private bool detectleak()
        {
            try
            {
                String lstrCString =
                              String.Format("http://127.0.0.1:1808{0}/diff?pic0={1}&pic1={2}&pic_diff={3}",
                              this.Index + 1, this.CompareImagePath0.Replace('\\', '/'), this.CompareImagePath1.Replace('\\', '/'), this.CompareDiffPath.Replace('\\', '/')
                              );

                String lstrUrl = lstrCString;
                System.Net.WebClient loClient = new System.Net.WebClient();
                System.IO.Stream loData = loClient.OpenRead(lstrUrl);
                System.IO.StreamReader loReader = new StreamReader(loData);
                String s = loReader.ReadToEnd();
                int lnRet = 0;
                Int32.TryParse(s, out lnRet);
                if (lnRet > 0)
                {
                    File.Copy(this.CompareImagePath0, this.CompareImagePath0_bak);
                    File.Copy(this.CompareImagePath1, this.CompareImagePath1_bak);
                    File.Copy(this.CompareDiffPath, this.CompareDiffPath_bak);
                    //File.Copy(this.AviPath, this.AviPathb);
                    return true;
                }

                return false;
            }
            catch (Exception e)
            {
                ThreadUiController.log(e.Message, ThreadUiController.LOG_LEVEL.ERROR);
                return false;
            }
        }


        private bool detect(byte[] arefImg, int anLengthReal)
        {
            this.initDetector();

            unsafe
            {
                fixed (byte* charPtr = &arefImg[0])
                {
                    List<bbox_t_csharp> loRet2 = BizLarge.moDetector.detect(charPtr, (int)anLengthReal, 0);

                    int lnPerson = BizLarge.moDetector.detectPerson(charPtr, (int)anLengthReal, 0.4f);

                    if (lnPerson > 0)
                    {
                        return true;
                    }

                    String lstrDebug = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff") + "| userid:" + this.UserID;

                    ThreadUiController.outputDebugString(lstrDebug + "|is Person:" + lnPerson + "\r\n");
                }
            }

            return false;
        }

        private delegate void alarm_delegate(bool abAlarm);
        private void alarm(bool abAlarm)
        {
            lock (this)
            {
                if (abAlarm)
                {
                    this.Status = 1;
                }
                else
                {
                    this.Status = 0;
                }

                // return;

                if (this.PicPox == null)
                {
                    return;
                }

                if (abAlarm)
                {

                    if (this.PicPox.Controls != null && this.PicPox.Controls.Count > 0)
                    {
                        Control c = this.PicPox.Controls[0];
                        if (c.GetType() == typeof(CustomPictureBox))
                        {
                            ((CustomPictureBox)c).UserID = this.UserID;
                        }

                        ThreadUiController.ShowControl(this.PicPox.Controls[0]);
                        //                     this.PicPox.Controls[0].Visible = true;
                        //                     this.PicPox.Controls[0].BackColor = Color.Red;
                    }

                }
                else
                {
                    if (this.PicPox.Controls != null && this.PicPox.Controls.Count > 0)
                    {
                        Control c = this.PicPox.Controls[0];
                        if (c.GetType() == typeof(CustomPictureBox))
                        {
                            ((CustomPictureBox)c).UserID = -1;
                        }

                        ThreadUiController.HideControl(this.PicPox.Controls[0]);
                    }
                }

            }

        }

        private void CaptureBmp2(int index, byte[] loBuffer)
        {
            string sBmpPicFileName;
            //图片保存路径和文件名 the path and file name to save

            string myExeDir = this.BaseDir;

            String myTempDir = this.TempDir;

            myTempDir = Detectorbridge.MakeSureImagePathExist(myTempDir);

            myExeDir = Detectorbridge.MakeSureImagePathExist(myExeDir);

            sBmpPicFileName = myTempDir + "\\" + this.UserID + "_" + index + ".bmp";

            //  string myExeDir = new FileInfo(Assembly.GetEntryAssembly().Location).Directory.ToString();

            if (index == 0)
            {
                this.CompareImagePath0 = sBmpPicFileName;
            }
            else
            {
                this.CompareImagePath1 = sBmpPicFileName;
            }

            this.CompareDiffPath = myTempDir + "\\" + "diff.bmp";
            this.AviPath = myTempDir + "\\" + this.UserID + "_diff.avi";

            long milliseconds = DateTimeOffset.Now.Ticks;

            this.CompareImagePath0_bak = myExeDir + "\\" + this.UserID + "_0_" + milliseconds + "_.bmp";
            this.CompareImagePath1_bak = myExeDir + "\\" + this.UserID + "_1_" + milliseconds + "_.bmp";
            this.m_strCompareDiffPath_bak = myExeDir + "\\" + this.UserID + "_diff_" + milliseconds + "_.bmp";

            this.AviBakPath = myExeDir + "\\" + this.UserID + "_diff.avi";

            File.WriteAllBytes(sBmpPicFileName, loBuffer);

        }



        private void cbDrawFun(int lRealHandle, IntPtr hDc, uint dwUser)
        {
            return;

            Graphics g = null;

            if (null == hDc)
            {
                return;
            }

            try
            {
                g = Graphics.FromHdc(hDc);
            }
            catch (Exception e)
            {
                return;
            }

            try
            {
                if (this.Status > 0)
                {
                    if (this.Status == 2)
                    {


                        SolidBrush opaqueBrush = new SolidBrush(Color.FromArgb(100, 255, 255, 0));
                        using (opaqueBrush)
                        {
                            g.FillRectangle(opaqueBrush, 0, 0, this.PicPox.Width, this.PicPox.Height);
                            g.Dispose();
                        }
                    }
                    else if (this.Status == 1)
                    {


                        SolidBrush opaqueBrush = new SolidBrush(Color.FromArgb(100, 255, 0, 0));
                        using (opaqueBrush)
                        {
                            g.FillRectangle(opaqueBrush, 0, 0, this.PicPox.Width, this.PicPox.Height);
                            g.Dispose();
                        }
                    }
                }

            }
            catch (Exception e)
            {

            }
            finally
            {
                if (null != g)
                {
                    g.Dispose();
                }
            }


        }


        byte[] data = new byte[1920 * 1080 * 3];
        public unsafe void onFrameCallback(int width,
                int height,
                int stride,
                ref byte buffer,
                int length)
        {



            fixed (byte* p = &buffer)
            {
                Bitmap bitmap = new Bitmap(width, height, PixelFormat.Format24bppRgb);
                BitmapData bmData = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadWrite, bitmap.PixelFormat);
                IntPtr pNative = bmData.Scan0;
                Marshal.Copy(new IntPtr(p), data, 0, length);
                Marshal.Copy(data, 0, pNative, length);
                bitmap.UnlockBits(bmData);
                MemoryStream loBuffer = new MemoryStream();
                bitmap.Save(loBuffer, ImageFormat.Bmp);
                byte[] loBufferTempx = loBuffer.ToArray();
                this.Queue.Enqueue(loBufferTempx);
            }


        }
        public void onStatusCallback(int anStatus)
        {
            this.AlarmStatus = (anStatus);
        }


        framecallback callback = null;
        OnStatusChange status_callback = null;
        public Boolean login()
        {
            return true;
        }

        private System.Windows.Forms.Timer m_oStatusTimer = new System.Windows.Forms.Timer();
        public Boolean live(PictureBox pictureBox)
        {
            this.callback = new framecallback(onFrameCallback);
            if (null == pictureBox)
            {
                return false;
            }
            int lnErrocCode = 0;
            Custom_hk_api.pure_play(pictureBox.Handle, this.UserName,
               this.Password,
               this.IP,
               this.Port,
               this.RTSP_PORT,
               this.DetectServerIP,
               this.DetectServerPort,
               this.FrameInterval,
               this.IgnorePerson,
               this.distance_ratio,
               this.area_min,
               this.status_callback,
               this.ROI_X,
               this.ROI_Y,
               this.ROI_W,
               this.ROI_H,
               this.CamSrc,
               this.CamIndex,
               this.CamMode,
               this.DisplayGreenRect,
               this.Ignore);
            return true;
        }
        public void stopLive(System.Windows.Forms.PictureBox pictureBox)
        {
            if (null == pictureBox)
            {
                return;
            }

            Custom_hk_api.stop_pure_play(pictureBox.Handle);

        }


        public void OnRealDataCallBackRTSP(int lRealHandle, uint dwDataType, ref byte pBuffer, uint dwBufSize, uint dwUser)
        {
        }

        public void OnRealDataCallBackAll(Int32 lRealHandle, UInt32 dwDataType, ref byte pBuffer, UInt32 dwBufSize, IntPtr pUser)
        {

            unsafe
            {
                fixed (byte* lpBuffer = &pBuffer)
                {
                    this.HkHelper.DecCBFun(lRealHandle, dwDataType, lpBuffer, dwBufSize, pUser, (IntPtr)0, null);
                }

            }
        }

        HKHelper.DecodeImageBufferCallBack m_oOnImageCallback = null;
        public detectorbridge.HKHelper.DecodeImageBufferCallBack OnImageCallback
        {
            get { lock (this) return m_oOnImageCallback; }
            set { lock (this) m_oOnImageCallback = value; }
        }
        public void onImageCallBack(byte[] apBuffer, int anLength)
        {
            System.Diagnostics.Debug.WriteLine(anLength + "");
        }

        Boolean isStarted = false;
        public System.Boolean IsStarted
        {
            get { lock (this) return isStarted; }
            set { lock (this) isStarted = value; }
        }

        protected bool inner_start()
        {
            {
                this.init();
                bool lbRet = this.login();
                this.isStarted = lbRet;
                if (!lbRet)
                {
                    return false;
                }

                this.live(this.PicPox);

                return true;
            }
        }
        public bool start()
        {
            return this.inner_start();
        }

        private void stopCapture_thread()
        {
            lock (this)
            {
                this.Stop = true;
            }
            if (this.m_oCaptureThread != null)
            {
                this.Stop = true;

                Boolean lbSucceedExit = false;

                try
                {
                    lbSucceedExit = this.m_oCaptureThread.Join(2000);

                }
                catch (Exception e)
                {

                }

                try
                {
                    if (!lbSucceedExit)
                    {
                        this.m_oCaptureThread.Abort();
                    }

                }
                catch (Exception e)
                {

                }


            }

            lock (this)
            {
                this.m_oCaptureThread = null;
            }

        }
    }
}
