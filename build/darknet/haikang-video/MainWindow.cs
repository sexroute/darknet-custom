﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using detectorbridge;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using EricZhao.UiThread;
using System.Linq;
using MetroFramework.Forms;
using static haikang_video.Custom_hk_api;
using EricZhao;

namespace haikang_video
{
    public partial class MainWindow : MetroForm
    {

        static Boolean  m_bShouldQuit = false;
        public static System.Boolean ShouldQuit
        {
            get { return m_bShouldQuit; }
            set { m_bShouldQuit = value; }
        }
        public MainWindow()
        {
            InitializeComponent();
            this.StyleManager = metroStyleManager1;

        }

        Dictionary<Int32, Int32> m_oHandleMap = new Dictionary<int, int>();

        String m_strSystemUserName = "admin";
        public System.String SystemUserName
        {
            get { return m_strSystemUserName; }
            set { m_strSystemUserName = value; }
        }
        String m_strSystemPassword = "admin6666";
        public System.String SystemPassword
        {
            get { return m_strSystemPassword; }
            set { m_strSystemPassword = value; }
        }

        internal void Exit()
        {
           
        }

        private void registLive(Int32 handle)
        {
            lock(this)
            {
                if(this.m_oHandleMap.ContainsKey(handle))
                {
                    return;
                }
                this.m_oHandleMap.Add(handle, handle);
            }
        }

        private void unRegist(Int32 handle)
        {
            lock(this)
            {
                if (!this.m_oHandleMap.ContainsKey(handle))
                {
                    return;
                }
                this.m_oHandleMap.Remove(handle);
            }
        }

        private int calcColumnCount(int anInput)
        {
            if(anInput<=1)
            {
                return 1;
            }

            if (anInput <= 2)
            {
                return 2;
            }

            if (anInput <= 4)
            {
                return 2;
            }

            if (anInput <= 9)
            {
                return 3;
            }

            if (anInput <= 10)
            {
                return 5;
            }

            if (anInput <= 16)
            {
                return 4;
            }

            if (anInput <= 25)
            {
                return 5;
            }

            if (anInput <= 48)
            {
                return 8;
            }

            return 4;
        }
        private void initTableLayout()
        {
            this.tableLayoutPanel1.RowStyles.Clear();

           // this.tableLayoutPanel1.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.Dock = DockStyle.Fill;
           // this.tableLayoutPanel1.Padding = GetCorrectionPadding(tableLayoutPanel1, 5);

            int lnPicBox = this.m_nPicboxCount;

            TableLayoutPanel panel = this.tableLayoutPanel1;

            int lnColumn = this.calcColumnCount(this.m_nPicboxCount);            
            int lnRowCount = this.m_nPicboxCount/lnColumn;
            panel.ColumnCount = lnColumn;

            int lnColPercent = (int)(1 / (lnColumn * 1.0) * 100.0);
            int lnRowPercent = (int)(1 / (lnRowCount * 1.0) * 100.0);

            panel.ColumnStyles.Clear();

            for (int i = 0; i < lnColumn; i++)
            {
                panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, lnColPercent));
            }

            for (int i = 0; i < lnRowCount ; i++)
            {
                panel.RowStyles.Add(new RowStyle(SizeType.Percent, lnRowPercent));
            }

            for (int i=0;i< lnRowCount; i++)
            {
                for (int j=0;j<lnColumn;j++)
                {

                    CustomPictureBox pb = new CustomPictureBox();
                    pb.BackColor = Color.Black;
                    pb.Dock = DockStyle.Fill;
                    pb.Name = "index:" + i + "" + j+"1";
                    pb.Margin = new Padding(0);

                    CustomPictureBox pb2 = new CustomPictureBox();
                    pb2.BackColor = Color.Red;
                    pb2.Dock = DockStyle.Bottom;
                    pb2.Name = "index:" + i + "" + j+":2";
                    pb2.Margin = new Padding(0);

                    pb.DoubleClick += new System.EventHandler(this.OnPic_DoubleClick);
                    pb2.DoubleClick += new System.EventHandler(this.OnPic_DoubleClick);

                    //if (i==0 && j ==0)
                    {
                        pb.Controls.Add(pb2);
                        pb2.Visible = false;
                    }

                     panel.Controls.Add(pb, j, i);
                }
            }

            panel.ResumeLayout();
        }

       detectorbridge.Detectorbridge moDetector = null;
       List<CustomPictureBox> m_oPics = new List<CustomPictureBox>();
       private void initUI()
        {
            this.StyleManager.Style = MetroFramework.MetroColorStyle.Purple;
            this.StyleManager.Theme = MetroFramework.MetroThemeStyle.Light;
            this.initTableLayout();
            this.m_oPics.Clear();
            foreach (var pb in this.tableLayoutPanel1.Controls.OfType<CustomPictureBox>())
            {
               this.m_oPics.Add(pb);
            }
        }

        private void Start()
        {
            int lnPicIndex = 0;

            Custom_hk_api.init("", "", 0);

            foreach (KeyValuePair<string, Biz> entry in m_oCameras)
            {
                if(lnPicIndex<this.m_oPics.Count)
                {
                    entry.Value.PicPox = this.m_oPics[lnPicIndex];
                }
                if(!entry.Value.IsStarted)
                {
                    entry.Value.Index = lnPicIndex;
                    entry.Value.start();
                }

                lnPicIndex++;
            }
        }

        Padding GetCorrectionPadding(TableLayoutPanel TLP, int minimumPadding)
        {
            int minPad = minimumPadding;
            Rectangle netRect = TLP.ClientRectangle;
            netRect.Inflate(-minPad, -minPad);

            int w = netRect.Width / TLP.ColumnCount;
            int h = netRect.Height / TLP.RowCount;

            int deltaX = (netRect.Width - w * TLP.ColumnCount) / 2;
            int deltaY = (netRect.Height - h * TLP.RowCount) / 2;

            int OddX = (netRect.Width - w * TLP.ColumnCount) % 2;
            int OddY = (netRect.Height - h * TLP.RowCount) % 2;

            return new Padding(minPad + deltaX, minPad + deltaY,
                               minPad + deltaX + OddX, minPad + deltaY + OddY);
        }

        private void GoFullscreen(bool fullscreen)
        {
            if(!ShouldFullScreen)
            {
                return;
            }
            if (fullscreen)
            {
                if((this.Bounds != Screen.PrimaryScreen.Bounds))
                {
                    this.WindowState = FormWindowState.Normal;
                    if(this.PasswordFormShowed || this.ShowMaxWindow)
                    {
                        this.TopMost = false;
                    }else
                    {
                        this.TopMost = true;
                    }
                   
                    this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    this.Bounds = Screen.PrimaryScreen.Bounds;
                }

            }
            else
            {
                this.WindowState = FormWindowState.Maximized;
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            }
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {

            this.GoFullscreen(true);
            this.loadSettings();
            this.initUI();
            this.Start();
            
        }

        Dictionary<String, Biz> m_oCameras = new Dictionary<string, Biz>();
        protected String m_strSettingFile = "./settings.ini";
        protected int m_nPicboxCount = 0;

        private int calcPicBoxCount(int anInput)
        {
            if(anInput<=1)
            {
                return 1;
            }

            return anInput;

            if (anInput <= 2)
            {
                return 2;
            }

            if (anInput <= 4)
            {
                return 4;
            }

            if (anInput <= 9)
            {
                return 9;
            }

            if (anInput <= 10)
            {
                return 10;
            }

            if (anInput <= 16)
            {
                return 16;
            }

            if (anInput <= 25)
            {
                return 25;
            }

            return 16;
        }
        private void loadSettings()
        {
            this.m_oCameras.Clear();
            IniFile loFile = new IniFile(this.m_strSettingFile);
            int lnTemp = loFile.IniReadIntValue("global", "window", 10, true);
            String baseDir = loFile.IniReadStringValue("global", "base_dir", "./pic_data", true);
            String tempDir = loFile.IniReadStringValue("global", "temp_dir", "./pic_temp", true);
            this.m_nPicboxCount = this.calcPicBoxCount(lnTemp);
            this.SystemUserName = loFile.IniReadStringValue("global", "system_username", "admin", true);
            this.SystemPassword = loFile.IniReadStringValue("global", "system_password", "admin6666", true);
            this.ShouldFullScreen = loFile.IniReadBooleanValue("global", "full_screen", false, true);
            String lstrTitle = loFile.IniReadStringValue("global", "title", "视频图像识别系统", true);
            this.Text = lstrTitle;
            if (this.m_nPicboxCount<=0)
            {
                this.m_nPicboxCount = 1;
            }
            int i = 0;
            while(true)
            {

                String lstrSection = "cam_" + i;
                String lstrIP = loFile.IniReadStringValue(lstrSection, "ip", "",false);
                if(String.IsNullOrEmpty(lstrIP))
                {
                    break;
                }
                int lnPort = loFile.IniReadIntValue(lstrSection, "port", 0,false);
                if(lnPort == 0)
                {
                    break;
                }

                int lnRtspPort = loFile.IniReadIntValue(lstrSection, "rtsp_port", 554, false);
                if (lnRtspPort == 0)
                {
                    break;
                }


                String detect_server_ip = loFile.IniReadStringValue(lstrSection, "detect_server_ip", "127.0.0.1", true);
                int detect_server_port = loFile.IniReadIntValue(lstrSection, "detect_server_port", 18081, true);

                String middleware_ip = loFile.IniReadStringValue(lstrSection, "middleware_ip", "127.0.0.1", true);
                int middleware_port = loFile.IniReadIntValue(lstrSection, "middleware_port", 17001, true);

                String company = loFile.IniReadStringValue(lstrSection, "company", "大连石化", true);
                String factory = loFile.IniReadStringValue(lstrSection, "factory", "三催化", true);
                String plant = loFile.IniReadStringValue(lstrSection, "plant", "原料泵P-2201-1", true);

                int startIndex = loFile.IniReadIntValue(lstrSection, "start_index", 0, true);


                String lstrUser = loFile.IniReadStringValue(lstrSection, "user", "admin", true);
                String lstrPassword = loFile.IniReadStringValue(lstrSection, "password", "admin6666", true);

                int lnFrameInterval = loFile.IniReadIntValue(lstrSection, "frame_interval", 20, true);
                int lnIgnore_Person = loFile.IniReadIntValue(lstrSection, "ignore_person", 1, true);

                int lnMinArea = loFile.IniReadIntValue(lstrSection, "area_min", 100, true);
                double ldblDistanceRatio = loFile.IniReadDoubleValue(lstrSection, "distance_ratio", 4.0, true);
                int lnValueKeepSeconds = loFile.IniReadIntValue(lstrSection, "value_keep_seconds", 40, true);

                int lnROI_X = loFile.IniReadIntValue(lstrSection, "ROI_X", -1, true);
                int lnROI_Y = loFile.IniReadIntValue(lstrSection, "ROI_Y", -1, true);
                int lnROI_W = loFile.IniReadIntValue(lstrSection, "ROI_W", -1, true);
                int lnROI_H = loFile.IniReadIntValue(lstrSection, "ROI_H", -1, true);

                String lstrCamSrc = loFile.IniReadStringValue(lstrSection, "cam_src", "", true);
                int lnCamIndex = loFile.IniReadIntValue(lstrSection, "cam_index", 0, true);
                int lnCamMode = loFile.IniReadIntValue(lstrSection, "cam_mode", 0, true);

                int lnDisplayGreenRect = loFile.IniReadIntValue(lstrSection, "green_rect", 0, true);

                int lnIgnore = loFile.IniReadIntValue(lstrSection, "ignore", 0, true);


                String lstrKey = lstrIP + ":" + lnPort + ":"+ System.Guid.NewGuid() ;
                Biz loBiz = new Biz();
                loBiz.IP = lstrIP;
                loBiz.Port = (Int16)lnPort;
                loBiz.UserName = lstrUser;
                loBiz.Password = lstrPassword;
                loBiz.BaseDir = baseDir;
                loBiz.TempDir = tempDir;
                loBiz.RTSP_PORT = lnRtspPort;
                loBiz.DetectServerIP = detect_server_ip;
                loBiz.DetectServerPort = detect_server_port;
                loBiz.FrameInterval = lnFrameInterval;
                loBiz.IgnorePerson = lnIgnore_Person;
                loBiz.Distance_ratio = ldblDistanceRatio;
                loBiz.KeepSeconds = lnValueKeepSeconds;
                loBiz.Area_min = lnMinArea;
                loBiz.Company = company;
                loBiz.Factory = factory;
                loBiz.Plant = plant;
                loBiz.MiddlewareIP = middleware_ip;
                loBiz.MiddlewarePort = middleware_port;
                loBiz.StartIndex = startIndex;
                loBiz.CamMode = lnCamMode;
                loBiz.CamIndex = lnCamIndex;
                loBiz.CamSrc = lstrCamSrc;
                loBiz.DisplayGreenRect = lnDisplayGreenRect;
                loBiz.Ignore = lnIgnore;

                loBiz.ROI_H = lnROI_H;
                loBiz.ROI_X = lnROI_X;
                loBiz.ROI_W = lnROI_W;
                loBiz.ROI_Y = lnROI_Y;

                if (!this.m_oCameras.ContainsKey(lstrKey))
                {
                    this.m_oCameras.Add(lstrKey, loBiz);
                }
                i++;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        Boolean m_bShowMaxWindow = false;
        public System.Boolean ShowMaxWindow
        {
            get { return m_bShowMaxWindow; }
            set { m_bShowMaxWindow = value; }
        }
        private void OnPic_DoubleClick(object sender, EventArgs e)
        {
            if (sender.GetType() == typeof(CustomPictureBox))
            {
                Biz loRemoteBiz = null;
                int lnPicIndex = 0;
                foreach (KeyValuePair<string, Biz> entry in m_oCameras)
                {
                   if(entry.Value.PicPox == sender)
                    {
                        loRemoteBiz = entry.Value;
                        break;
                    }
                }

                if(loRemoteBiz == null)
                {
                    return;
                }
                ShowMaxWindow = true;
                this.TopMost = false;
                MaxCemra MaxWindow = new MaxCemra();
                MaxWindow.RemoteBiz = new BizLarge() ;
                MaxWindow.RemoteBiz.UserName = loRemoteBiz.UserName;
                MaxWindow.RemoteBiz.Password = loRemoteBiz.Password;
                MaxWindow.RemoteBiz.Port = loRemoteBiz.Port;
                MaxWindow.RemoteBiz.IP = loRemoteBiz.IP;
                MaxWindow.RemoteBiz.UserID = loRemoteBiz.UserID;
                MaxWindow.RemoteBiz.RTSP_PORT = loRemoteBiz.RTSP_PORT;
                MaxWindow.RemoteBiz.DetectServerIP = loRemoteBiz.DetectServerIP;
                MaxWindow.RemoteBiz.DetectServerPort = loRemoteBiz.DetectServerPort;
                MaxWindow.RemoteBiz.FrameInterval = loRemoteBiz.FrameInterval;
                MaxWindow.RemoteBiz.IgnorePerson = loRemoteBiz.IgnorePerson;
                MaxWindow.RemoteBiz.Distance_ratio = loRemoteBiz.Distance_ratio;
                MaxWindow.RemoteBiz.Area_min = loRemoteBiz.Area_min;

                MaxWindow.RemoteBiz.CamMode = loRemoteBiz.CamMode;
                MaxWindow.RemoteBiz.CamIndex = loRemoteBiz.CamIndex; ;
                MaxWindow.RemoteBiz.CamSrc = loRemoteBiz.CamSrc; ;
                MaxWindow.RemoteBiz.DisplayGreenRect = loRemoteBiz.DisplayGreenRect; ;
                MaxWindow.RemoteBiz.Ignore = loRemoteBiz.Ignore; ;
                MaxWindow.WindowState = FormWindowState.Maximized;
                MaxWindow.Show();
            }           
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.QuitApp();
        }

        public bool ShouldFullScreen = false;
        public bool PasswordFormShowed = false;
        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(!MainWindow.ShouldQuit)
            {
                PasswordInput loPasswordForm = new PasswordInput();
                this.TopMost = false;
                this.PasswordFormShowed = true;
                loPasswordForm.ShowDialog();
                this.PasswordFormShowed = false;
                if (loPasswordForm.Password != null && loPasswordForm.Password.CompareTo(this.SystemPassword) == 0)
                {
                    return;
                }
                e.Cancel = true;
            }

        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            this.GoFullscreen(true);
        }



        private void QuitApp()
        {

            MiniDump.TerminateProcess();

            try
            {
                if (System.Windows.Forms.Application.MessageLoop)
                {
                    // WinForms app
                    System.Windows.Forms.Application.Exit();
                    System.Environment.Exit(1);
                }
                else
                {
                    // Console app
                    System.Environment.Exit(1);
                }
            }
            catch(Exception e)
            {

            }


        }


        private void MainWindow_KeyPress(object sender, KeyPressEventArgs e)
        {
            char keychar = e.KeyChar;
            if ((keychar == 'q'))
            {
                this.QuitApp();
            }
        }

        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            System.Windows.Forms.Keys lnCode = e.KeyCode;
            if ((lnCode == Keys.Q))
            {
                this.QuitApp();
            }
        }
    }
}
