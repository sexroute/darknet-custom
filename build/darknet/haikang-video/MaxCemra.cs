﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace haikang_video
{
    public partial class MaxCemra : MetroForm
    {

        public MaxCemra()
        {
            InitializeComponent();
        }

        BizLarge m_oRemoteBiz = null;
        public BizLarge RemoteBiz
        {
            get { return m_oRemoteBiz; }
            set { m_oRemoteBiz = value; }
        }




        private void MaxCemra_Load(object sender, EventArgs e)
        {
            this.play();
        }

        Boolean m_bLive = false;
        Boolean m_blogin = false;



        private void MaxCemra_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.m_oRemoteBiz != null)
            {
                this.m_oRemoteBiz.stopLive(this.pictureBox1);
                this.m_oRemoteBiz = null;
            }

            if (this.m_oRemoteBiz != null)
            {
                this.m_oRemoteBiz = null;
            }
        }

        private void play()
        {
            this.m_oRemoteBiz.live(this.pictureBox1);

        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

       
    }
}
