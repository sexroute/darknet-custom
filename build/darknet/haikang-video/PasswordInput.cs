﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace haikang_video
{
    public partial class PasswordInput : MetroForm
    {
        public PasswordInput()
        {
            InitializeComponent();
        }

        private void PasswordInput_Load(object sender, EventArgs e)
        {
            this.CenterToParent();
            this.AcceptButton = this.btnOK;
        }

        String m_strPassword = "";
        public System.String Password
        {
            get { return m_strPassword; }
            set { m_strPassword = value; }
        }
        private void metroButton1_Click(object sender, EventArgs e)
        {
            this.Password = this.metroTextBox1.Text;
            this.Hide();
        }

        private void PasswordInput_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Escape)
            {
                this.Hide();
            }
        }

        private void metroTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Hide();
            }
        }
    }
}
