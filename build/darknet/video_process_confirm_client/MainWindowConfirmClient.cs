﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using EricZhao;
using EricZhao.UiThread;
using MetroFramework.Forms;
using RabbitMQ.Client.Events;
/***
 * 流程：1.读取配置，实时更新，也算是心跳
 *       2.读取视频帧，显示报警帧
 *       3.双击放大后，显示原帧，并持续更新
 */
namespace video_process_confirm_client
{
    public partial class MainWindowConfirmClient : MetroForm
    {
        String m_strUUID = System.Guid.NewGuid().ToString();
        public System.String UUID
        {
            get { lock (this) return m_strUUID; }
            set { lock (this) m_strUUID = value; }
        }

        public MainWindowConfirmClient()
        {
            InitializeComponent();
        }
        EricZhao.AlarmImageRabbitExchangHandler m_oAlarmMQ = new EricZhao.AlarmImageRabbitExchangHandler(EricZhao.PacketTypeValues.ALARM_IMAGE_DATA);
        EricZhao.AlarmImageRabbitExchangHandler m_oNormalMQ = new EricZhao.AlarmImageRabbitExchangHandler(EricZhao.PacketTypeValues.NORMAL_DATA);
        protected String m_strSettingFile = "./settings.ini";

        static Boolean m_bShouldQuit = false;
        public static System.Boolean ShouldQuit
        {
            get { return m_bShouldQuit; }
            set { m_bShouldQuit = value; }
        }

        String m_strTitle = "";
        public System.String Title
        {
            get { return m_strTitle; }
            set { m_strTitle = value; }
        }
        String m_strSystemUserName = "admin";
        public System.String SystemUserName
        {
            get { return m_strSystemUserName; }
            set { m_strSystemUserName = value; }
        }
        String m_strSystemPassword = "admin6666";
        public System.String SystemPassword
        {
            get { return m_strSystemPassword; }
            set { m_strSystemPassword = value; }
        }


        Boolean m_bReadNormalData = false;
        public Boolean ReadNormalData
        {
            get { return m_bReadNormalData; }
            set { m_bReadNormalData = value; }
        }

        private Boolean m_oAutoPush = false;
        public System.Boolean AutoPush
        {
            get { lock(this) return m_oAutoPush; }
            set { lock (this) m_oAutoPush = value; }
        }

        String m_strRemoteMsgUrl = "";
        public System.String RemoteMsgUrl
        {
            get { lock (this) return m_strRemoteMsgUrl; }
            set { lock (this) m_strRemoteMsgUrl = value; }
        }

        Boolean m_bAutoSendRemoteMsg = false;
        public System.Boolean AutoSendRemoteMsg
        {
            get { lock (this) return m_bAutoSendRemoteMsg; }
            set { lock (this) m_bAutoSendRemoteMsg = value; }
        }
        String m_strMSQURL = "amqp://guest:guest@127.0.0.1:5672";
        public System.String MSQURL
        {
            get { return m_strMSQURL; }
            set { m_strMSQURL = value; }
        }
        public bool ShouldFullScreen = false;
        public bool PasswordFormShowed = false;

        Boolean m_bShowMaxWindow = false;
        public System.Boolean ShowMaxWindow
        {
            get { return m_bShowMaxWindow; }
            set { m_bShowMaxWindow = value; }
        }
        List<CustomPictureBoxConfirmClient> m_oPics = new List<CustomPictureBoxConfirmClient>();
        private void initUI()
        {
            this.StyleManager = this.metroStyleManager1;
            this.StyleManager.Style = MetroFramework.MetroColorStyle.Blue;
            this.StyleManager.Theme = MetroFramework.MetroThemeStyle.Light;
          
            foreach(var pb in this.m_oPics)
            {
                MainWindowConfirmClient.UnRegistWindow(pb.ChannelIndex, pb);
            }
            this.m_oPics.Clear();
            MainWindowConfirmClient.m_oCameras.Clear();
            this.initTableLayout();    
                   
            foreach (var pb in this.tableLayoutPanel1.Controls.OfType<CustomPictureBoxConfirmClient>())
            {
                pb.PicDisplayType = CustomPictureBoxConfirmClient.DisplayType.both;
                this.m_oPics.Add(pb);
                MainWindowConfirmClient.RegisWindow(pb.ChannelIndex,pb);
            }

        }

        private void QuitApp()
        {

            MiniDump.TerminateProcess();

            try
            {
                if (System.Windows.Forms.Application.MessageLoop)
                {
                    // WinForms app
                    System.Windows.Forms.Application.Exit();
                    System.Environment.Exit(1);
                }
                else
                {
                    // Console app
                    System.Environment.Exit(1);
                }
            }
            catch (Exception e)
            {

            }


        }

        private int calcPicBoxCount(int anInput)
        {
            if (anInput <= 1)
            {
                return 1;
            }

            return anInput;

            if (anInput <= 2)
            {
                return 2;
            }

            if (anInput <= 4)
            {
                return 4;
            }

            if (anInput <= 9)
            {
                return 9;
            }

            if (anInput <= 10)
            {
                return 10;
            }

            if (anInput <= 16)
            {
                return 16;
            }

            if (anInput <= 25)
            {
                return 25;
            }

            return 16;
        }

        public void regAlarmCongfirmed(int anIndex)
        {
            this.autoSendRemoteMessage(anIndex);

            foreach (var pb in this.m_oPics)
            {
                if(pb.ChannelIndex == anIndex)
                {
                    pb.AlarmCanceled = 0;
                    pb.Invalidate();
                }
            }
        }

        public void updateAlarmStatus(int anIndex,int anStatus)
        {
            foreach (var pb in this.m_oPics)
            {
                if (pb.ChannelIndex == anIndex)
                {
                    int lnPreviousStatus = pb.AlarmCanceled;
                    pb.AlarmCanceled = anStatus;         
                    if(lnPreviousStatus!=anStatus)
                    {
                        pb.Invalidate();
                    }         
                }
            }
        }


        DateTime m_oStartTime = new DateTime();
        public System.DateTime StartTime
        {
            get { return m_oStartTime; }
            set { m_oStartTime = value; }
        }
        private void MainWindow_Load(object sender, EventArgs e)
        {
          
            this.loadSettings();
            this.initUI();
            this.start();
        }

       AlarmImageRabbitExchangHandler m_oConfirmCancelReciver = null;


        private void GoFullscreen(bool fullscreen)
        {
            if (!ShouldFullScreen)
            {
                return;
            }
            if (fullscreen)
            {
                if ((this.Bounds != Screen.PrimaryScreen.Bounds))
                {
                    this.WindowState = FormWindowState.Normal;
                    if (this.PasswordFormShowed || this.ShowMaxWindow)
                    {
                        this.TopMost = false;
                    }
                    else
                    {
                        this.TopMost = true;
                    }

                    this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                    this.Bounds = Screen.PrimaryScreen.Bounds;
                }

            }
            else
            {
                this.WindowState = FormWindowState.Maximized;
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            }
        }

        Dictionary<int, ServerBiz> m_oCameras_send = new Dictionary<int, ServerBiz>();

        private void loadSettings()
        {
            this.StartTime = DateTime.Now;
            IniFile loFile = new IniFile(this.m_strSettingFile);
            int lnTemp = loFile.IniReadIntValue("process_confirm_client_global", "window", 10, true);
            String baseDir = loFile.IniReadStringValue("process_confirm_client_global", "base_dir", "./pic_data", true);
            String tempDir = loFile.IniReadStringValue("process_confirm_client_global", "temp_dir", "./pic_temp", true);
            this.TotalChannels = this.calcPicBoxCount(lnTemp);
            this.SystemUserName = loFile.IniReadStringValue("process_confirm_client_global", "system_username", "admin", true);
            this.SystemPassword = loFile.IniReadStringValue("process_confirm_client_global", "system_password", "admin6666", true);
            this.ShouldFullScreen = loFile.IniReadBooleanValue("process_confirm_client_global", "full_screen", false, true);
            String lstrTitle = loFile.IniReadStringValue("process_confirm_client_global", "title", "视频图像确认系统", true);
           
            String lstrMSQURL = loFile.IniReadStringValue("process_confirm_client_global", "message_queue_url", "amqp://guest:guest@127.0.0.1:5672", true);

            this.MSQURL = lstrMSQURL;
            this.ReadNormalData = loFile.IniReadBooleanValue("process_confirm_client_global", "read_normal_data",false, true);
            this.AutoPush = loFile.IniReadBooleanValue("process_confirm_client_global", "auto_push", false, true);
            this.RemoteMsgUrl = loFile.IniReadStringValue("process_confirm_client_global", "remote_msg_url", "http://10.168.1.123:8090/?", true);
            this.AutoSendRemoteMsg = loFile.IniReadBooleanValue("process_confirm_client_global", "auto_send_remote_msg", false, true);
            this.RealTimeToolStripMenuItem.Checked = this.ReadNormalData;
            this.ToolStripMenuItemAutoPush.Checked = this.AutoPush;
            this.ToolStripMenuItemAutoSendMsg.Checked = this.AutoSendRemoteMsg;
            this.Text = lstrTitle;
            this.Title = lstrTitle;
            if (this.TotalChannels <= 0)
            {
                this.TotalChannels = 1;
            }

            int i = 0;
            while (true)
            {

                String lstrSection = "cam_" + i;
                String lstrIP = loFile.IniReadStringValue(lstrSection, "ip", "", false);
                if (String.IsNullOrEmpty(lstrIP))
                {
                    break;
                }
                int lnPort = loFile.IniReadIntValue(lstrSection, "port", 0, false);
                if (lnPort == 0)
                {
                    break;
                }

                int lnRtspPort = loFile.IniReadIntValue(lstrSection, "rtsp_port", 554, false);
                if (lnRtspPort == 0)
                {
                    break;
                }


                String detect_server_ip = loFile.IniReadStringValue(lstrSection, "detect_server_ip", "127.0.0.1", true);
                int detect_server_port = loFile.IniReadIntValue(lstrSection, "detect_server_port", 18081, true);

                String middleware_ip = loFile.IniReadStringValue(lstrSection, "middleware_ip", "127.0.0.1", true);
                int middleware_port = loFile.IniReadIntValue(lstrSection, "middleware_port", 17001, true);

                String company = loFile.IniReadStringValue(lstrSection, "company", "大连石化", true);
                String factory = loFile.IniReadStringValue(lstrSection, "factory", "三催化", true);
                String plant = loFile.IniReadStringValue(lstrSection, "plant", "原料泵P-2201-1", true);

                int startIndex = loFile.IniReadIntValue(lstrSection, "start_index", 0, true);


                String lstrUser = loFile.IniReadStringValue(lstrSection, "user", "admin", true);
                String lstrPassword = loFile.IniReadStringValue(lstrSection, "password", "admin6666", true);

                int lnFrameInterval = loFile.IniReadIntValue(lstrSection, "frame_interval", 20, true);
                int lnIgnore_Person = loFile.IniReadIntValue(lstrSection, "ignore_person", 1, true);

                int lnMinArea = loFile.IniReadIntValue(lstrSection, "area_min", 100, true);
                double ldblDistanceRatio = loFile.IniReadDoubleValue(lstrSection, "distance_ratio", 4.0, true);
                int lnValueKeepSeconds = loFile.IniReadIntValue(lstrSection, "value_keep_seconds", 40, true);

                int lnROI_X = loFile.IniReadIntValue(lstrSection, "ROI_X", -1, true);
                int lnROI_Y = loFile.IniReadIntValue(lstrSection, "ROI_Y", -1, true);
                int lnROI_W = loFile.IniReadIntValue(lstrSection, "ROI_W", -1, true);
                int lnROI_H = loFile.IniReadIntValue(lstrSection, "ROI_H", -1, true);

                String lstrCamSrc = loFile.IniReadStringValue(lstrSection, "cam_src", "", true);
                int lnCamIndex = loFile.IniReadIntValue(lstrSection, "cam_index", 0, true);
                int lnCamMode = loFile.IniReadIntValue(lstrSection, "cam_mode", 0, true);

                int lnDisplayGreenRect = loFile.IniReadIntValue(lstrSection, "green_rect", 0, true);

                int lnIgnore = loFile.IniReadIntValue(lstrSection, "ignore", 0, true);
                String lstrChannID = loFile.IniReadStringValue(lstrSection, "channid", "泄漏监测", true);

                int lnAlarmCanceled = loFile.IniReadIntValue(lstrSection, "confirm_alarm_canceled", 1, true);
                int lnLeakMode = loFile.IniReadIntValue(lstrSection, "confirm_leak_mode", 0, true);


               
                ServerBiz loBiz = new ServerBiz();
                loBiz.IP = lstrIP;
                loBiz.Port = (Int16)lnPort;
                loBiz.UserName = lstrUser;
                loBiz.Password = lstrPassword;
                loBiz.BaseDir = baseDir;
                loBiz.TempDir = tempDir;
                loBiz.RTSP_PORT = lnRtspPort;
                loBiz.DetectServerIP = detect_server_ip;
                loBiz.DetectServerPort = detect_server_port;
                loBiz.FrameInterval = lnFrameInterval;
                loBiz.IgnorePerson = lnIgnore_Person;
                loBiz.Distance_ratio = ldblDistanceRatio;
                loBiz.KeepSeconds = lnValueKeepSeconds;
                loBiz.Area_min = lnMinArea;
                loBiz.Company = company;
                loBiz.Factory = factory;
                loBiz.Plant = plant;
                loBiz.MiddlewareIP = middleware_ip;
                loBiz.MiddlewarePort = middleware_port;
                loBiz.StartIndex = startIndex;
                loBiz.CamMode = lnCamMode;
                loBiz.CamIndex = lnCamIndex;
                loBiz.CamSrc = lstrCamSrc;
                loBiz.DisplayGreenRect = lnDisplayGreenRect;
                loBiz.Ignore = lnIgnore;
                loBiz.ChannID = lstrChannID;
                loBiz.AlarmCanceled = lnAlarmCanceled;
               

                loBiz.ROI_H = lnROI_H;
                loBiz.ROI_X = lnROI_X;
                loBiz.ROI_W = lnROI_W;
                loBiz.ROI_Y = lnROI_Y;
                loBiz.MSQ_Url = lstrMSQURL;
                loBiz.ChannelIndex = i;
                if (!this.m_oCameras_send.ContainsKey(i))
                {
                    this.m_oCameras_send.Add(i, loBiz);
                }
                i++;
            }

        }

        static  Dictionary<int, Dictionary<IntPtr, CustomPictureBoxConfirmClient>>  m_oCameras
            = new Dictionary<int, Dictionary<IntPtr, CustomPictureBoxConfirmClient>>();
        public static System.Collections.Generic.Dictionary<int, 
            System.Collections.Generic.Dictionary<System.IntPtr,
            video_process_confirm_client.CustomPictureBoxConfirmClient>> Cameras
        {
            get { return m_oCameras; }
            set { m_oCameras = value; }
        }

        public static void RegisWindow(int anIndex,CustomPictureBoxConfirmClient apBox)
        {
            if( null == apBox)
            {
                return;
            }
            if (m_oCameras.ContainsKey(anIndex))
            {
                Dictionary<IntPtr, CustomPictureBoxConfirmClient> loWindows = Cameras[anIndex];
                if (loWindows == null)
                {
                    loWindows = new Dictionary<IntPtr, CustomPictureBoxConfirmClient>();
                }
                if (!loWindows.ContainsKey(apBox.Handle))
                {
                    loWindows.Add(apBox.Handle, apBox);
                }

            }else
            {
                Dictionary<IntPtr, CustomPictureBoxConfirmClient> loWindows = new Dictionary<IntPtr, CustomPictureBoxConfirmClient>();
                loWindows.Add(apBox.Handle, apBox);
                m_oCameras.Add(anIndex, loWindows);
            }
        }

        public static void UnRegistWindow(int anIndex, CustomPictureBoxConfirmClient apBox)
        {
            if ( null == apBox)
            {
                return;
            }

            if (Cameras.ContainsKey(anIndex))
            {
                Dictionary<IntPtr, CustomPictureBoxConfirmClient> loWindows = Cameras[anIndex];
                if (loWindows != null)
                {
                    if (loWindows.ContainsKey(apBox.Handle))
                    {
                        loWindows.Remove(apBox.Handle);
                    }
                }
            }
        }
        private void OnPic_DoubleClick(object sender, EventArgs e)
        {
            if (sender.GetType() == typeof(CustomPictureBoxConfirmClient))
            {
                int lnIndex = -1;
                Image lpImage = null;
                foreach (var pb in m_oPics)
                {
                    if (pb == sender)
                    {
                        lnIndex = pb.ChannelIndex;
                        lpImage = pb.Image;
                        break;
                    }
                }

                if(lnIndex>=0)
                {
                    ShowMaxWindow = true;
                    this.TopMost = false;
                    MaxCemraConfirmClient MaxWindow = new MaxCemraConfirmClient();
                    MaxWindow.initPicturebox(lpImage);
                    MaxWindow.ChannIndex = lnIndex;
                    MaxWindow.MSG_URL = this.MSQURL;
                    MaxWindow.WindowState = FormWindowState.Maximized;
                    MaxWindow.ParentWindow = this;
                    MaxWindow.Show();
                }


            }
        }

        private int calcColumnCount(int anInput)
        {
            if (anInput <= 1)
            {
                return 1;
            }

            if (anInput <= 2)
            {
                return 2;
            }

            if (anInput <= 4)
            {
                return 2;
            }

            if (anInput <= 9)
            {
                return 3;
            }

            if (anInput <= 10)
            {
                return 5;
            }

            if (anInput <= 16)
            {
                return 4;
            }

            if (anInput <= 25)
            {
                return 5;
            }

            if (anInput <= 48)
            {
                return 8;
            }

            return 4;
        }
        public  void RemoveRow( TableLayoutPanel panel, int rowIndex)
        {
            panel.RowStyles.RemoveAt(rowIndex);

            for (int columnIndex = 0; columnIndex < panel.ColumnCount; columnIndex++)
            {
                var control = panel.GetControlFromPosition(columnIndex, rowIndex);
                if (null != control)
                {
                    panel.Controls.Remove(control);
                }
                    
            }

            for (int i = rowIndex + 1; i < panel.RowCount; i++)
            {
                for (int columnIndex = 0; columnIndex < panel.ColumnCount; columnIndex++)
                {
                    var control = panel.GetControlFromPosition(columnIndex, i);
                    if(null!=control)
                    {
                        panel.SetRow(control, i - 1);
                    }
                   
                }
            }

            panel.RowCount--;
        }
        private void initTableLayout()
        {
            for(int i=0;i<this.tableLayoutPanel1.RowCount;i++)
            {
                this.RemoveRow(this.tableLayoutPanel1, i);
            }
            this.tableLayoutPanel1.Controls.Clear();
           
            this.tableLayoutPanel1.RowStyles.Clear();

            // this.tableLayoutPanel1.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.Dock = DockStyle.Fill;
            // this.tableLayoutPanel1.Padding = GetCorrectionPadding(tableLayoutPanel1, 5);

            int lnPicBox = this.TotalChannels;

            TableLayoutPanel panel = this.tableLayoutPanel1;

            int lnColumn = this.calcColumnCount(lnPicBox);
            int lnRowCount = lnPicBox / lnColumn;
            panel.ColumnCount = lnColumn;

            int lnColPercent = (int)(1 / (lnColumn * 1.0) * 100.0);
            int lnRowPercent = (int)(1 / (lnRowCount * 1.0) * 100.0);

            panel.ColumnStyles.Clear();

            for (int i = 0; i < lnColumn; i++)
            {
                panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, lnColPercent));
            }

            for (int i = 0; i < lnRowCount; i++)
            {
                panel.RowStyles.Add(new RowStyle(SizeType.Percent, lnRowPercent));
            }
            int lnChannIndex = 0;
            for (int i = 0; i < lnRowCount; i++)
            {
                for (int j = 0; j < lnColumn; j++)
                {

                    CustomPictureBoxConfirmClient pb = new CustomPictureBoxConfirmClient();
                    
                    pb.BackColor = Color.Black;
                    pb.Dock = DockStyle.Fill;
                    pb.Name = "index:" + i + "" + j + "1";
                    pb.Margin = new Padding(0);
                    pb.ChannelIndex = lnChannIndex;
                   
                    lnChannIndex++;
                    pb.DoubleClick += new System.EventHandler(this.OnPic_DoubleClick);             

                    panel.Controls.Add(pb, j, i);
                }
            }

            panel.ResumeLayout();
        }

        private void toggleUI(Boolean abStart)
        {
            this.btnStart.Enabled = !abStart;
            this.btnStop.Enabled = abStart;
        }

        Boolean m_oNormalMQ_inited = false;
        public System.Boolean NormalMQ_inited
        {
            get { lock(this)return m_oNormalMQ_inited; }
            set { lock (this) m_oNormalMQ_inited = value; }
        }
        private void startReadNormalData()
        {
            lock(this)
            {
                if (!this.NormalMQ_inited)
                {
                    m_oNormalMQ.m_OnMessageReceived += this.OnNormalMessageReicved;
                    m_oNormalMQ.URL = this.MSQURL;
                }

                this.NormalMQ_inited = true;

            }
            m_oNormalMQ.Read();

        }

        private void stopReadNormalData()
        {
            lock (this)
            {
                if (this.NormalMQ_inited)
                {
                    this.m_oNormalMQ.closeAllResources();
                    this.NormalMQ_inited = false;
                }
            }                
        }

        private void start()
        {
            m_oAlarmMQ.m_OnMessageReceived += this.OnMessageReicved;
            m_oAlarmMQ.URL = this.MSQURL;
            m_oAlarmMQ.Read();
            if(this.ReadNormalData)
            {
                this.startReadNormalData();
            }
            this.toggleUI(true);

            System.Threading.Thread lpThread = new System.Threading.Thread(this.checkStatus);
            lpThread.IsBackground = true;
            lpThread.Start();
        }

        int m_nTotalChannels = 0;
        public int TotalChannels
        {
            get { lock(this) return m_nTotalChannels; }
            set { lock (this) m_nTotalChannels = value; }
        }

        Boolean m_bNeedUpdateUI = false;
        public System.Boolean NeedUpdateUI
        {
            get { lock (this) return m_bNeedUpdateUI; }
            set { lock (this) m_bNeedUpdateUI = value; }
        }
        DateTime m_oLastHeartBeatTime = new DateTime();
        public System.DateTime LastHeartBeatTime
        {
            get { lock (this) return m_oLastHeartBeatTime; }
            set { lock (this) m_oLastHeartBeatTime = value; }
        }
        public void OnNormalMessageReicved(object sender, BasicDeliverEventArgs e)
        {
            if (e != null)
            {
                try
                {
                    var body = e.Body;
                    AlarmImagePacket loPacket = AlarmImagePacket.toObject(body);

                    switch (loPacket.PacketType)
                    {
                        case PacketTypeValues.CONFIG:
                            {
                                int lnTotalChannels = loPacket.Channels;
                                if (lnTotalChannels != this.TotalChannels)
                                {
                                    this.TotalChannels = lnTotalChannels;
                                    this.NeedUpdateUI = true;
                                }
                            }
                            break;
                        case PacketTypeValues.HEART_BEAT:
                            {
                                this.LastHeartBeatTime = loPacket.DateTime;
                            }
                            break;

                        case PacketTypeValues.ALARM_IMAGE_DATA:
                            {
                                int lnTotalChannels = loPacket.Channels;
                                if (lnTotalChannels != this.TotalChannels)
                                {
                                    this.TotalChannels = lnTotalChannels;
                                    this.NeedUpdateUI = true;
                                }

                                int lnChannIndex = loPacket.VideoChannelIndex;

                                List<CustomPictureBoxConfirmClient> loAlarms = new List<CustomPictureBoxConfirmClient>();
                                List<CustomPictureBoxConfirmClient> loNormals = new List<CustomPictureBoxConfirmClient>();

                                if (MainWindowConfirmClient.Cameras.ContainsKey(lnChannIndex))
                                {
                                    Dictionary<IntPtr, CustomPictureBoxConfirmClient> loDict = MainWindowConfirmClient.Cameras[lnChannIndex];
                                    if (null != loDict)
                                    {
                                        foreach (KeyValuePair<IntPtr, CustomPictureBoxConfirmClient> lpIt in loDict)
                                        {
                                            if (lpIt.Value != null)
                                            {
                                                if (lpIt.Value.PicDisplayType == CustomPictureBoxConfirmClient.DisplayType.only_normal)
                                                {
                                                    loNormals.Add(lpIt.Value);
                                                }
                                                else if (lpIt.Value.PicDisplayType == CustomPictureBoxConfirmClient.DisplayType.only_alarm)
                                                {
                                                    loAlarms.Add(lpIt.Value);
                                                }else
                                                {
                                                    loNormals.Add(lpIt.Value);
                                                    loAlarms.Add(lpIt.Value);
                                                }
                                            }
                                        }
                                    }
                                }


                                if (loPacket.DetectImage != null && loPacket.DetectImage.Length > 0)
                                {
                                    using (Stream stream = new MemoryStream(loPacket.DetectImage))
                                    {
                                        Bitmap loBitMap = new Bitmap(stream);
                                        foreach (CustomPictureBoxConfirmClient pic in loAlarms)
                                        {
                                            pic.SetPicture(loBitMap);
                                        }
                                    }
                                }

                                if (loPacket.OriginalImage != null && loPacket.OriginalImage.Length > 0)
                                {
                                    using (Stream stream = new MemoryStream(loPacket.OriginalImage))
                                    {
                                        Bitmap loBitMap = new Bitmap(stream);
                                        foreach (CustomPictureBoxConfirmClient pic in loNormals)
                                        {
                                            pic.SetPicture(loBitMap);
                                        }
                                    }
                                }
                            }
                            break;
                        case PacketTypeValues.NORMAL_DATA:
                            {
                                int lnTotalChannels = loPacket.Channels;
                                if (lnTotalChannels != this.TotalChannels)
                                {
                                    this.TotalChannels = lnTotalChannels;
                                    this.NeedUpdateUI = true;
                                }

                                if (loPacket.OriginalImage != null && loPacket.OriginalImage.Length > 0)
                                {

                                    List<CustomPictureBoxConfirmClient> loAlarms = new List<CustomPictureBoxConfirmClient>();
                                    List<CustomPictureBoxConfirmClient> loNormals = new List<CustomPictureBoxConfirmClient>();
                                    int lnChannIndex = loPacket.VideoChannelIndex;
                                    if (MainWindowConfirmClient.Cameras.ContainsKey(lnChannIndex))
                                    {
                                        Dictionary<IntPtr, CustomPictureBoxConfirmClient> loDict = MainWindowConfirmClient.Cameras[lnChannIndex];
                                        if (null != loDict)
                                        {
                                            foreach (KeyValuePair<IntPtr, CustomPictureBoxConfirmClient> lpIt in loDict)
                                            {

                                                if (lpIt.Value != null)
                                                {
                                                    if (lpIt.Value.PicDisplayType == CustomPictureBoxConfirmClient.DisplayType.only_normal)
                                                    {
                                                        loNormals.Add(lpIt.Value);
                                                    }
                                                    else if (lpIt.Value.PicDisplayType == CustomPictureBoxConfirmClient.DisplayType.only_alarm)
                                                    {
                                                        loAlarms.Add(lpIt.Value);
                                                    }
                                                    else
                                                    {
                                                        loNormals.Add(lpIt.Value);
                                                        loAlarms.Add(lpIt.Value);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    using (Stream stream = new MemoryStream(loPacket.OriginalImage))
                                    {
                                        Bitmap loBitMap = new Bitmap(stream);
                                        foreach (CustomPictureBoxConfirmClient pic in loNormals)
                                        {
                                            pic.SetPicture(loBitMap);
                                        }
                                    }
                                }
                            }
                            break;

                    }


                }
                catch (Exception ex)
                {

                }

            }
        }

        

        public void autoSendRemoteMessage(int anChannIndex)
        {
            if(!this.AutoSendRemoteMsg)
            {
                return;
            }
            try
            {
                if (this.m_oCameras_send.ContainsKey(anChannIndex))
                {
                    ServerBiz loBiz = this.m_oCameras_send[anChannIndex];

                    if (null != loBiz)
                    {
                        String lstrMsg = loBiz.Company + " " + loBiz.Factory + " " + loBiz.Plant + " 视频报警";

                        try
                        {
                            byte[] encodeBytes = System.Text.Encoding.UTF8.GetBytes(lstrMsg);

                            string inputString = System.Text.Encoding.UTF8.GetString(encodeBytes);

                            inputString = HttpUtility.UrlEncode(inputString);

                            String lstrMsgUrl = this.RemoteMsgUrl + inputString;

                            System.Diagnostics.Debug.WriteLine(lstrMsgUrl);
                            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(lstrMsgUrl);
                            request.Method = "GET";
                            String test = String.Empty;
                            
                            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                            {
                                Stream dataStream = response.GetResponseStream();
                                StreamReader reader = new StreamReader(dataStream);
                                test = reader.ReadToEnd();
                                reader.Close();
                                dataStream.Close();
                            }

                        }
                        catch (Exception e)
                        {

                        }

                    }
                }
            }
            catch(Exception e)
            {

            }
            
        }

        public void autoPush(int anChannIndex,String astrUUID,String astrMSG_URL)
        {
            if(this.AutoPush)
            {
                AlarmImagePacket loPacket = new AlarmImagePacket();
                loPacket.PacketType = PacketTypeValues.CONFIRM_LEAK;
                loPacket.DateTime = DateTime.Now;
                loPacket.LeakMode = 1;
                loPacket.VideoChannelIndex = anChannIndex;
                loPacket.PacketSource = Environment.MachineName + ":" + Packet.GetLocalIPAddress();
                AlarmImageRabbitExchangHandler.SingleSend(loPacket, astrUUID, astrMSG_URL);
                this.regAlarmCongfirmed(anChannIndex);
            }else
            {
                this.autoSendRemoteMessage(anChannIndex);
            }

        }
        public void OnMessageReicved(object sender, BasicDeliverEventArgs e)
        {
            if (e != null)
            {
                try
                {
                    var body = e.Body;
                    AlarmImagePacket loPacket = AlarmImagePacket.toObject(body);

                    switch(loPacket.PacketType)
                    {
                        case PacketTypeValues.CONFIG:
                            {
                                int lnTotalChannels = loPacket.Channels;
                                if(lnTotalChannels!=this.TotalChannels)
                                {
                                    this.TotalChannels = lnTotalChannels;
                                    this.NeedUpdateUI = true;
                                }
                            }
                            break;
                        case PacketTypeValues.HEART_BEAT:
                            {
                                this.LastHeartBeatTime = loPacket.DateTime;
                            }
                            break;

                        case PacketTypeValues.ALARM_IMAGE_DATA:
                            {
                                int lnTotalChannels = loPacket.Channels;
                                if (lnTotalChannels != this.TotalChannels)
                                {
                                    this.TotalChannels = lnTotalChannels;
                                    this.NeedUpdateUI = true;
                                }

                                int lnChannIndex = loPacket.VideoChannelIndex;                               

                                this.autoPush(lnChannIndex, this.UUID, this.MSQURL);
                               

                                List<CustomPictureBoxConfirmClient> loAlarms = new List<CustomPictureBoxConfirmClient>();
                                List<CustomPictureBoxConfirmClient> loNormals = new List<CustomPictureBoxConfirmClient>();

                                if (MainWindowConfirmClient.Cameras.ContainsKey(lnChannIndex))
                                {
                                    Dictionary<IntPtr, CustomPictureBoxConfirmClient> loDict = MainWindowConfirmClient.Cameras[lnChannIndex];
                                    if(null!= loDict)
                                    {
                                        foreach(KeyValuePair<IntPtr,CustomPictureBoxConfirmClient> lpIt in loDict)
                                        {
                                            if(lpIt.Value!=null )
                                            {

                                                if (lpIt.Value != null)
                                                {
                                                    if (lpIt.Value.PicDisplayType == CustomPictureBoxConfirmClient.DisplayType.only_normal)
                                                    {
                                                      //  loNormals.Add(lpIt.Value);
                                                    }
                                                    else if (lpIt.Value.PicDisplayType == CustomPictureBoxConfirmClient.DisplayType.only_alarm)
                                                    {
                                                        loAlarms.Add(lpIt.Value);
                                                    }
                                                    else
                                                    {
                                                      //  loNormals.Add(lpIt.Value);
                                                        loAlarms.Add(lpIt.Value);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (loPacket.OriginalImage != null && loPacket.OriginalImage.Length > 0)
                                {
                                    using (Stream stream = new MemoryStream(loPacket.OriginalImage))
                                    {
                                        Bitmap loBitMap = new Bitmap(stream);
                                        foreach (CustomPictureBoxConfirmClient pic in loNormals)
                                        {
                                            pic.SetPicture(loBitMap);
                                        }
                                    }
                                }

                                if (loPacket.DetectImage!=null && loPacket.DetectImage.Length>0)
                                {
                                    using (Stream stream = new MemoryStream(loPacket.DetectImage))
                                    {
                                        Bitmap loBitMap = new Bitmap(stream);
                                       foreach(CustomPictureBoxConfirmClient pic in loAlarms)
                                        {
                                            pic.SetPicture(loBitMap);
                                        }
                                    }
                                }

                                                          
                            }
                            break;

                        case PacketTypeValues.CANCEL_ALL_ALARM:
                            {
                                int lnIndex = loPacket.VideoChannelIndex;
                                this.updateAlarmStatus(lnIndex, 1);
                            }
                            break;

                        case PacketTypeValues.NOTIFY_STATUS:
                            {
                                int lnIndex = loPacket.VideoChannelIndex;
                                int lnStatus = loPacket.Status;
                                this.updateAlarmStatus(lnIndex,lnStatus);
                            }
                            break;

                        case PacketTypeValues.NORMAL_DATA:
                            {
                                if (loPacket.OriginalImage != null && loPacket.OriginalImage.Length > 0)
                                {

                                    List<CustomPictureBoxConfirmClient> loAlarms = new List<CustomPictureBoxConfirmClient>();
                                    List<CustomPictureBoxConfirmClient> loNormals = new List<CustomPictureBoxConfirmClient>();
                                    int lnChannIndex = loPacket.VideoChannelIndex;
                                    if (MainWindowConfirmClient.Cameras.ContainsKey(lnChannIndex))
                                    {
                                        Dictionary<IntPtr, CustomPictureBoxConfirmClient> loDict = MainWindowConfirmClient.Cameras[lnChannIndex];
                                        if (null != loDict)
                                        {
                                            foreach (KeyValuePair<IntPtr, CustomPictureBoxConfirmClient> lpIt in loDict)
                                            {
                                                if (lpIt.Value != null)
                                                {

                                                    if (lpIt.Value != null)
                                                    {
                                                        if (lpIt.Value.PicDisplayType == CustomPictureBoxConfirmClient.DisplayType.only_normal)
                                                        {
                                                            loNormals.Add(lpIt.Value);
                                                        }
                                                        else if (lpIt.Value.PicDisplayType == CustomPictureBoxConfirmClient.DisplayType.only_alarm)
                                                        {
                                                            loAlarms.Add(lpIt.Value);
                                                        }
                                                        else
                                                        {
                                                            loNormals.Add(lpIt.Value);
                                                            loAlarms.Add(lpIt.Value);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    using (Stream stream = new MemoryStream(loPacket.OriginalImage))
                                    {
                                        Bitmap loBitMap = new Bitmap(stream);
                                        foreach (CustomPictureBoxConfirmClient pic in loNormals)
                                        {
                                            pic.SetPicture(loBitMap);
                                        }
                                    }
                                }
                            }
                            break;

                    }
                   

                }
                catch (Exception ex)
                {

                }

            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            this.start();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            m_oAlarmMQ.OnStop();
            this.toggleUI(false);
        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        Boolean m_bQuit = false;
        public System.Boolean Quit
        {
            get { lock(this)return m_bQuit; }
            set { lock (this) m_bQuit = value; }
        }
        private void checkStatus()
        {
            while(!this.Quit)
            {
                try
                {
                    if (this.ConnectStatus != EricZhao.AlarmImageRabbitExchangHandler.Status.CONNECTED)
                    {
                        this.m_oAlarmMQ.Read();
                    }

                   
                }catch(Exception e)
                {

                }

                try
                {
                    System.Threading.Thread.Sleep(1000);
                }
                catch (Exception e)
                {

                }

            }
        }

        private int m_nConnectStatus = EricZhao.AlarmImageRabbitExchangHandler.Status.DISCONNECTED;
        public int ConnectStatus
        {
            get { return m_nConnectStatus; }
            set { m_nConnectStatus = value; }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            if(this.NeedUpdateUI)
            {
              //  this.initUI();
                this.NeedUpdateUI = false;
            }

            this.ConnectStatus = this.m_oAlarmMQ.ConnectStatus;          
            String lstrStatus = EricZhao.AlarmImageRabbitExchangHandler.Status.getStatus(this.ConnectStatus);
          

            this.toolStripStatusLabel2.Text ="连接状态 : "+ lstrStatus;
            TimeSpan span = DateTime.Now - this.StartTime;
            string formatted = string.Format("{0}:{1}:{2}:{3}", span.Days,span.Hours,span.Minutes,span.Seconds);
            this.toolStripStatusLabel1.Text = "已运行 : " + span.ToString("hh': 'mm': 'ss''");

            this.Update();
        }

        private void MainWindowConfirmClient_FormClosed(object sender, FormClosedEventArgs e)
        {
            MiniDump.TerminateProcess();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            this.GoFullscreen(true);
        }

        private void MainWindowConfirmClient_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!MainWindowConfirmClient.ShouldQuit)
            {
                PasswordInput loPasswordForm = new PasswordInput();
                ShowMaxWindow = true;
                this.TopMost = false;
                this.PasswordFormShowed = true;
                loPasswordForm.ShowDialog();
                this.PasswordFormShowed = false;
                if (loPasswordForm.Password != null && loPasswordForm.Password.CompareTo(this.SystemPassword) == 0)
                {
                    return;
                }
                e.Cancel = true;
            }
        }

        private void savesettings()
        {
            IniFile loFile = new IniFile(this.m_strSettingFile);
            loFile.initWriteBooleanValue("process_confirm_client_global", "read_normal_data", this.ReadNormalData);
            loFile.initWriteBooleanValue("process_confirm_client_global", "auto_push", this.AutoPush);
            loFile.initWriteBooleanValue("process_confirm_client_global", "auto_send_remote_msg", this.AutoSendRemoteMsg);
        }

        private void RealTimeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.RealTimeToolStripMenuItem.Checked = !this.RealTimeToolStripMenuItem.Checked;
        }

        private void RealTimeToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            this.ReadNormalData = this.RealTimeToolStripMenuItem.Checked;
            this.savesettings();
            if (this.ReadNormalData)
            {
                this.startReadNormalData();
            }
            else
            {
                this.stopReadNormalData();
            }
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void ToolStripMenuItemAutoPush_Click(object sender, EventArgs e)
        {
           
            ConfirmWindow loConfirmWindow = new ConfirmWindow();
            if(!this.ToolStripMenuItemAutoPush.Checked)
            {
                loConfirmWindow.Text = "是否确认启用自动推送?";
            }
            else
            {
                loConfirmWindow.Text = "是否取消自动推送?";
            }
            loConfirmWindow.ShowDialog(this);
            if (loConfirmWindow.ConfirmedSend)
            {
                this.ToolStripMenuItemAutoPush.Checked = !this.ToolStripMenuItemAutoPush.Checked;
            }
                
        }

        private void ToolStripMenuItemAutoPush_CheckStateChanged(object sender, EventArgs e)
        {

        }

        private void ToolStripMenuItemAutoPush_CheckedChanged(object sender, EventArgs e)
        {
            this.AutoPush = this.ToolStripMenuItemAutoPush.Checked;
            this.savesettings();          
        }

        private void ToolStripMenuItemAutoSendMsg_Click(object sender, EventArgs e)
        {
            ConfirmWindow loConfirmWindow = new ConfirmWindow();
            if (!this.ToolStripMenuItemAutoPush.Checked)
            {
                loConfirmWindow.Text = "是否确认自动发送远程消息?";
            }
            else
            {
                loConfirmWindow.Text = "是否取消自动发送远程消息?";
            }
            loConfirmWindow.ShowDialog(this);
            if (loConfirmWindow.ConfirmedSend)
            {
                this.ToolStripMenuItemAutoSendMsg.Checked = !this.ToolStripMenuItemAutoSendMsg.Checked;
            }

        }

        private void ToolStripMenuItemAutoSendMsg_CheckedChanged(object sender, EventArgs e)
        {
            this.AutoSendRemoteMsg = this.ToolStripMenuItemAutoSendMsg.Checked;
            this.savesettings();
        }

      

        private void ToolStripMenuItemDataChoose_Click(object sender, EventArgs e)
        {
            this.Focus();
            this.ToolStripMenuItemDataChoose.ShowDropDown();
        }

        private void ToolStripMenuItemConfirmSetting_Click(object sender, EventArgs e)
        {
            this.Focus();
            this.ToolStripMenuItemConfirmSetting.ShowDropDown();
        }

        private void ToolStripMenuItemNoticeSetting_Click(object sender, EventArgs e)
        {
            this.Focus();
            this.ToolStripMenuItemNoticeSetting.ShowDropDown();
           
        }
    }
}
