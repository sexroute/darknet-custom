﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using EricZhao.UiThread;
using System.Linq;
using EricZhao;

namespace video_process_confirm_client
{
    public partial class CustomPictureBoxConfirmClient : PictureBox
    {
        public CustomPictureBoxConfirmClient()
        {
            InitializeComponent();
            this.SizeMode = PictureBoxSizeMode.StretchImage;
            this.m_oInvoker = new Method_invoker(this.cloneBmp2);
            this.DoubleBuffered = true;
        }

        private void InitializeComponent()
        {
           // throw new NotImplementedException();

        }

        private bool shifted = false;

        private int m_lUserID = -1;
        public int UserID
        {
            get { lock(this)return m_lUserID; }
            set { lock (this) m_lUserID = value; }
        }

        string DVRIPAddress = "192.168.1.64"; //设备IP地址或者域名
        public string IP
        {
            get { lock (this) return DVRIPAddress; }
            set { lock (this) DVRIPAddress = value; }
        }
        Int16 DVRPortNumber = 8000;//设备服务端口号
        public System.Int16 Port
        {
            get { lock (this) return DVRPortNumber; }
            set { lock (this) DVRPortNumber = value; }
        }
        private uint iLastErr = 0;
        private string str = "";

        string DVRUserName = "admin";//设备登录用户名
        public string UserName
        {
            get { lock (this) return DVRUserName; }
            set { lock (this) DVRUserName = value; }
        }
        string DVRPassword = "admin6666";//设备登录密码
        public string Password
        {
            get { lock (this) return DVRPassword; }
            set { lock (this) DVRPassword = value; }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
          /*  if (e.Button == MouseButtons.Left && this.Image != null)
            {
                this.shifted = true;
                this.Invalidate();
            }*/

            base.OnMouseDown(e);
        }

        Bitmap m_oBitMap = null;
        public System.Drawing.Bitmap BitMap
        {
            get { lock (this) return m_oBitMap; }
            set { lock (this) m_oBitMap = value; }
        }
        protected override void OnMouseUp(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && this.Image != null)
            {
                this.shifted = false;
                this.Invalidate();
            }

            base.OnMouseUp(e);
        }

        protected override void OnPaint(PaintEventArgs e)
        {       
            try
            {
                base.OnPaint(e);
            } catch(Exception e2)

            {

            }
           
            try
            {
                if (this.AlarmCanceled == 0)
                {
                    using (Font myFont = new Font("Arial", 60))
                    {
                        e.Graphics.DrawString("待确认", myFont, Brushes.Blue, new Point(5, 15));
                    }
                }
            }
            catch(Exception e2)
            {

            }
          
        }

        public event EventHandler ImageChanged = null;


        private Bitmap m_oCurrentBitMap = null;
        public System.Drawing.Bitmap CurrentBitMap
        {
            get
            {
                lock (this) return m_oCurrentBitMap;
            }
            set
            {
                lock (this) m_oCurrentBitMap = value;
            }
        }

        private void mainthreadUpdatePicture()
        {
            if (this.Image != null)
            {
                try
                {
                    this.Image.Dispose();
                }
                catch(Exception e)
                {

                }
                
            }

            this.Image = this.m_oCurrentBitMap;

        }
        public void SetPicture(Bitmap img)
        {
            if (null == img)
            {
                return;
            }

            this.CurrentBitMap = img;

            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(
                delegate ()
                {
                    this.mainthreadUpdatePicture();
                }));

            }


            else
            {
                this.mainthreadUpdatePicture();
            }
        }

        int m_nChannelIndex = 0;
        public int ChannelIndex
        {
            get { lock(this)return m_nChannelIndex; }
            set { lock (this) m_nChannelIndex = value; }
        }
        public delegate void Method_invoker(IntPtr hdc);

        public Method_invoker m_oInvoker = null;

        public void cloneBmp2(IntPtr hdc)
        {
            try
            {
                Bitmap bitMap = Bitmap.FromHbitmap(hdc);
                String lstrImagePath = "./xxxxx_" + DateTime.Now.ToFileTime() + "_test.bmp";
                bitMap.Save(lstrImagePath, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            catch(Exception e)
            {
                ThreadUiController.outputDebugString(e.Message);
            }

        }

        public void cloneBmp(IntPtr hdc)
        {
            try
            {
                var ctr = this;
                Image bmp = new Bitmap(ctr.Width, ctr.Height);
                var gg = Graphics.FromImage(bmp);
                var rect = ctr.RectangleToScreen(ctr.ClientRectangle);
                gg.CopyFromScreen(rect.Location, Point.Empty, ctr.Size);
                String lstrImagePath = "./xxxxx_" + DateTime.Now.ToFileTime() + "_test.bmp";
                bmp.Save(lstrImagePath);
            }
            catch (Exception e)
            {
                ThreadUiController.outputDebugString(e.Message);
            }

        }

        public enum DisplayType
        {
            only_alarm  =0,
            only_normal,           
            both
        }

        DisplayType m_nDisplayType = 0; // 0:only alarm 1:only normal 2:all
        public DisplayType PicDisplayType
        {
            get { lock(this) return m_nDisplayType; }
            set { lock (this) m_nDisplayType = value; }
        }

        int m_nAlarmCanceled = 1;
        public int AlarmCanceled
        {
            get { lock (this) return m_nAlarmCanceled; }
            set { lock (this) m_nAlarmCanceled = value; }
        }
       
    }
}
