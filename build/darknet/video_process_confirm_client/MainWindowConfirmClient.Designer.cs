﻿namespace video_process_confirm_client
{
    partial class MainWindowConfirmClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindowConfirmClient));
            this.btnStop = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.metroStyleManager1 = new MetroFramework.Components.MetroStyleManager(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ToolStripMenuItemDataChoose = new System.Windows.Forms.ToolStripMenuItem();
            this.RealTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemConfirmSetting = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemAutoPush = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemNoticeSetting = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItemAutoSendMsg = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(826, 395);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 3;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(745, 395);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Silver;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(377, 218);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // metroStyleManager1
            // 
            this.metroStyleManager1.Owner = null;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 300;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemDataChoose,
            this.ToolStripMenuItemConfirmSetting,
            this.ToolStripMenuItemNoticeSetting});
            this.menuStrip1.Location = new System.Drawing.Point(20, 60);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(922, 25);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // ToolStripMenuItemDataChoose
            // 
            this.ToolStripMenuItemDataChoose.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RealTimeToolStripMenuItem});
            this.ToolStripMenuItemDataChoose.Name = "ToolStripMenuItemDataChoose";
            this.ToolStripMenuItemDataChoose.ShowShortcutKeys = false;
            this.ToolStripMenuItemDataChoose.Size = new System.Drawing.Size(68, 21);
            this.ToolStripMenuItemDataChoose.Text = "数据选择";
            this.ToolStripMenuItemDataChoose.Click += new System.EventHandler(this.ToolStripMenuItemDataChoose_Click);
            // 
            // RealTimeToolStripMenuItem
            // 
            this.RealTimeToolStripMenuItem.Name = "RealTimeToolStripMenuItem";
            this.RealTimeToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.RealTimeToolStripMenuItem.Text = "实时正常数据";
            this.RealTimeToolStripMenuItem.CheckedChanged += new System.EventHandler(this.RealTimeToolStripMenuItem_CheckedChanged);
            this.RealTimeToolStripMenuItem.Click += new System.EventHandler(this.RealTimeToolStripMenuItem_Click);
            // 
            // ToolStripMenuItemConfirmSetting
            // 
            this.ToolStripMenuItemConfirmSetting.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemAutoPush});
            this.ToolStripMenuItemConfirmSetting.Name = "ToolStripMenuItemConfirmSetting";
            this.ToolStripMenuItemConfirmSetting.Size = new System.Drawing.Size(68, 21);
            this.ToolStripMenuItemConfirmSetting.Text = "确认设置";
            this.ToolStripMenuItemConfirmSetting.Click += new System.EventHandler(this.ToolStripMenuItemConfirmSetting_Click);
            // 
            // ToolStripMenuItemAutoPush
            // 
            this.ToolStripMenuItemAutoPush.Name = "ToolStripMenuItemAutoPush";
            this.ToolStripMenuItemAutoPush.Size = new System.Drawing.Size(124, 22);
            this.ToolStripMenuItemAutoPush.Text = "自动推送";
            this.ToolStripMenuItemAutoPush.CheckedChanged += new System.EventHandler(this.ToolStripMenuItemAutoPush_CheckedChanged);
            this.ToolStripMenuItemAutoPush.CheckStateChanged += new System.EventHandler(this.ToolStripMenuItemAutoPush_CheckStateChanged);
            this.ToolStripMenuItemAutoPush.Click += new System.EventHandler(this.ToolStripMenuItemAutoPush_Click);
            // 
            // ToolStripMenuItemNoticeSetting
            // 
            this.ToolStripMenuItemNoticeSetting.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemAutoSendMsg});
            this.ToolStripMenuItemNoticeSetting.Name = "ToolStripMenuItemNoticeSetting";
            this.ToolStripMenuItemNoticeSetting.Size = new System.Drawing.Size(68, 21);
            this.ToolStripMenuItemNoticeSetting.Text = "通知设置";
            this.ToolStripMenuItemNoticeSetting.Click += new System.EventHandler(this.ToolStripMenuItemNoticeSetting_Click);
            // 
            // ToolStripMenuItemAutoSendMsg
            // 
            this.ToolStripMenuItemAutoSendMsg.Name = "ToolStripMenuItemAutoSendMsg";
            this.ToolStripMenuItemAutoSendMsg.Size = new System.Drawing.Size(196, 22);
            this.ToolStripMenuItemAutoSendMsg.Text = "自动调用接口发送通知";
            this.ToolStripMenuItemAutoSendMsg.CheckedChanged += new System.EventHandler(this.ToolStripMenuItemAutoSendMsg_CheckedChanged);
            this.ToolStripMenuItemAutoSendMsg.Click += new System.EventHandler(this.ToolStripMenuItemAutoSendMsg_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(20, 421);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(922, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(0, 17);
            // 
            // MainWindowConfirmClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(962, 463);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainWindowConfirmClient";
            this.Text = "Client";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindowConfirmClient_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainWindowConfirmClient_FormClosed);
            this.Load += new System.EventHandler(this.MainWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.metroStyleManager1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Components.MetroStyleManager metroStyleManager1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemDataChoose;
        private System.Windows.Forms.ToolStripMenuItem RealTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemConfirmSetting;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemAutoPush;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemNoticeSetting;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemAutoSendMsg;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
    }
}

