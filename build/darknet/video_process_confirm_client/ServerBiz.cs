﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace video_process_confirm_client
{
    public class ServerBiz
    {
        private static int m_nGUID = 0;
        private static String ServerBizLockObject = "video_process_confirm_client.ServerBiz.ServerBizLockObject";
        public static int GenUID()
        {
            lock (ServerBizLockObject)
            {
                int lnRet = m_nGUID++;

                return lnRet;
            }
        }

        private Boolean m_bEnableAlarm = false;
        public System.Boolean EnableAlarm
        {
            get { lock (this) return m_bEnableAlarm; }
            set { lock (this) m_bEnableAlarm = value; }
        }

        public ServerBiz()
        {
            this.UserID = ServerBiz.GenUID();
        }
        int m_nStartIndex = 0;
        public int StartIndex
        {
            get { lock (this) return m_nStartIndex; }
            set { lock (this) m_nStartIndex = value; }
        }

        String m_strMiddlewareIP = "127.0.0.1";
        public System.String MiddlewareIP
        {
            get { lock (this) return m_strMiddlewareIP; }
            set { lock (this) m_strMiddlewareIP = value; }
        }
        int m_nMiddlewarePort = 17001;
        public int MiddlewarePort
        {
            get { lock (this) return m_nMiddlewarePort; }
            set { lock (this) m_nMiddlewarePort = value; }
        }

        int m_nKeepSeconds = 40;
        public int KeepSeconds
        {
            get { lock (this) if (m_nKeepSeconds < 0) { m_nKeepSeconds = 0; }; return m_nKeepSeconds; }
            set { lock (this) m_nKeepSeconds = value; }
        }
        String m_strCompany = "";
        public System.String Company
        {
            get { lock (this) return m_strCompany; }
            set { lock (this) m_strCompany = value; }
        }
        String m_strFactory = "";
        public System.String Factory
        {
            get { lock (this) return m_strFactory; }
            set { lock (this) m_strFactory = value; }
        }
        String m_strPlant = "";
        public System.String Plant
        {
            get { lock (this) return m_strPlant; }
            set { lock (this) m_strPlant = value; }
        }
        String m_strDetectServerIP = "127.0.0.1";
        public System.String DetectServerIP
        {
            get { lock (this) return m_strDetectServerIP; }
            set { lock (this) m_strDetectServerIP = value; }
        }

        int m_nAlarmStatus = 0;
        String m_strAlarmLock = "";
        public int AlarmStatus
        {
            get { lock (m_strAlarmLock) return m_nAlarmStatus; }
            set { lock (m_strAlarmLock) m_nAlarmStatus = value; }
        }

        
        int m_nDetectServerPort = 18081;
        public int DetectServerPort
        {
            get { lock (this) return m_nDetectServerPort; }
            set { lock (this) m_nDetectServerPort = value; }
        }

        int m_nFrameInterval = 20;
        public int FrameInterval
        {
            get { lock (this) return m_nFrameInterval; }
            set { lock (this) { if (m_nFrameInterval >= 0) { m_nFrameInterval = value; } } }
        }

        int m_nIgnorePerson = 0;
        public int IgnorePerson
        {
            get { lock (this) return m_nIgnorePerson; }
            set { lock (this) m_nIgnorePerson = value; }
        }

        double distance_ratio = 2.0;
        public double Distance_ratio
        {
            get { lock (this) return distance_ratio; }
            set { lock (this) distance_ratio = value; }
        }

        int area_min = 100;
        public int Area_min
        {
            get { lock (this) return area_min; }
            set { lock (this) area_min = value; }
        }
        int m_nRTSP_PORT = 0;
        public int RTSP_PORT
        {
            get { lock (this) return m_nRTSP_PORT; }
            set { lock (this) m_nRTSP_PORT = value; }
        }

        bool m_nIsFirst = true;
        public bool IsFirst
        {
            get { lock (this) return m_nIsFirst; }
            set { lock (this) m_nIsFirst = value; }
        }


        int m_nROI_X = 0;
        public int ROI_X
        {
            get { lock (this) return m_nROI_X; }
            set { lock (this) m_nROI_X = value; }
        }
        int m_nROI_Y = 0;
        public int ROI_Y
        {
            get { lock (this) return m_nROI_Y; }
            set { lock (this) m_nROI_Y = value; }
        }
        int m_nROI_W = 0;
        public int ROI_W
        {
            get { lock (this) return m_nROI_W; }
            set { lock (this) m_nROI_W = value; }
        }

        int m_nROI_H = 0;
        public int ROI_H
        {
            get { lock (this) return m_nROI_H; }
            set { lock (this) m_nROI_H = value; }
        }
        int m_nAlarmCanceled = 1;
        public int AlarmCanceled
        {
            get { lock (this) return m_nAlarmCanceled; }
            set { lock (this) m_nAlarmCanceled = value; }
        }
        int m_nConfrimedStatus = 0;
        public int ConfrimedStatus
        {
            get { lock (this) return m_nConfrimedStatus; }
            set { lock (this) m_nConfrimedStatus = value; }
        }
        int m_nIndex = 0;
        public int Index
        {
            get
            {
                lock (this) { return m_nIndex; }
            }
            set
            {
                lock (this) { m_nIndex = value; }
            }
        }
    
        private int m_lUserID = -1;
        private String m_lUserID_lock = "m_lUserID_lock";
        public int UserID
        {
            get { lock (this.m_lUserID_lock) return m_lUserID; }
            set { lock (this.m_lUserID_lock) m_lUserID = value; }
        }

        String m_strCamSrc = "";
        public System.String CamSrc
        {
            get { lock (this) return m_strCamSrc; }
            set { lock (this) m_strCamSrc = value; }
        }
        int m_nCamMode = 0;
        public int CamMode
        {
            get { lock (this) return m_nCamMode; }
            set { lock (this) m_nCamMode = value; }
        }
        int m_nCamIndex = 0;
        public int CamIndex
        {
            get { lock (this) return m_nCamIndex; }
            set { lock (this) m_nCamIndex = value; }
        }
        private Int32 m_lRealHandle = -1;
        public System.Int32 RealHandle
        {
            get { lock (this) return m_lRealHandle; }
            set { lock (this) m_lRealHandle = value; }
        }
        String msqUrl = "";
        public System.String MSQ_Url
        {
            get { lock (this) return msqUrl; }
            set { lock (this) msqUrl = value; }
        }

        int m_nTotalChannels = 0;
        public int TotalChannels
        {
            get { lock (this) return m_nTotalChannels; }
            set { lock (this) m_nTotalChannels = value; }
        }
        int m_nChannelIndex = 0;
        public int ChannelIndex
        {
            get { lock (this) return m_nChannelIndex; }
            set { lock (this) m_nChannelIndex = value; }
        }
        int m_nIgnore = 0;
        public int Ignore
        {
            get { lock (this) return m_nIgnore; }
            set { lock (this) m_nIgnore = value; }
        }

        String m_strChannID = "泄漏监测";
        public System.String ChannID
        {
            get { lock (this) return m_strChannID; }
            set { lock (this) m_strChannID = value; }
        }
        int m_nDisplayGreenRect = 0;
        public int DisplayGreenRect
        {
            get { lock (this) return m_nDisplayGreenRect; }
            set { lock (this) m_nDisplayGreenRect = value; }
        }


 
        Boolean m_bStop = false;
        public System.Boolean Stop
        {
            get { lock (this) return m_bStop; }
            set { lock (this) m_bStop = value; }
        }
        private static bool m_bInitSDK = false;

        string DVRIPAddress = "192.168.1.64"; //设备IP地址或者域名
        public string IP
        {
            get { lock (this) return DVRIPAddress; }
            set { lock (this) DVRIPAddress = value; }
        }
        Int16 DVRPortNumber = 8000;//设备服务端口号
        public System.Int16 Port
        {
            get { lock (this) return DVRPortNumber; }
            set { lock (this) DVRPortNumber = value; }
        }
        private uint iLastErr = 0;
        private string str = "";

        string DVRUserName = "admin";//设备登录用户名
        public string UserName
        {
            get { lock (this) return DVRUserName; }
            set { lock (this) DVRUserName = value; }
        }
        string DVRPassword = "bohuaxinzhi123";//设备登录密码
        public string Password
        {
            get { lock (this) return DVRPassword; }
            set { lock (this) DVRPassword = value; }
        }
  
        private String m_strCompareImagePath0 = "";
        public System.String CompareImagePath0
        {
            get { lock (this) return m_strCompareImagePath0; }
            set { lock (this) m_strCompareImagePath0 = value; }
        }
        private String m_strCompareImagePath1 = "";
        public System.String CompareImagePath1
        {
            get { lock (this) return m_strCompareImagePath1; }
            set { lock (this) m_strCompareImagePath1 = value; }
        }

        private String m_strCompareDiffPath = "";
        public System.String CompareDiffPath
        {
            get { lock (this) return m_strCompareDiffPath; }
            set { lock (this) m_strCompareDiffPath = value; }
        }
        String m_strTempDir = "./tmp";
        public System.String TempDir
        {
            get { lock (this) return m_strTempDir; }
            set { lock (this) m_strTempDir = value; }
        }
        String m_strBaseDir = "./data";
        public System.String BaseDir
        {
            get { lock (this) return m_strBaseDir; }
            set { lock (this) m_strBaseDir = value; }
        }

        IntPtr m_hWnd = new IntPtr(0);
        public System.IntPtr hWnd
        {
            get { return m_hWnd; }
            set { m_hWnd = value; }
        }

        private delegate void CaptureBmp_delegate(int index);


        private String m_strCompareImagePath0_bak = "";
        public System.String CompareImagePath0_bak
        {
            get { lock (this) return m_strCompareImagePath0_bak; }
            set { lock (this) m_strCompareImagePath0_bak = value; }
        }
        private String m_strCompareImagePath1_bak = "";
        public System.String CompareImagePath1_bak
        {
            get { lock (this) return m_strCompareImagePath1_bak; }
            set { lock (this) m_strCompareImagePath1_bak = value; }
        }

        private String m_strCompareDiffPath_bak = "";
        public System.String CompareDiffPath_bak
        {
            get { lock (this) return m_strCompareDiffPath_bak; }
            set { lock (this) m_strCompareDiffPath_bak = value; }
        }

        String m_strAviBakPath = "";
        public System.String AviBakPath
        {
            get { lock (this) return m_strAviBakPath; }
            set { lock (this) m_strAviBakPath = value; }
        }
        private String m_strAviPath = "";
        public System.String AviPath
        {
            get { lock (this) return m_strAviPath; }
            set { lock (this) m_strAviPath = value; }
        }

        private int m_nStatus = 0;
        private String m_strStatusLock = "m_strStatusLock";
        public int Status
        {
            get { lock (this.m_strStatusLock) return m_nStatus; }
            set { lock (this.m_strStatusLock) m_nStatus = value; }
        }

        Queue<byte[]> m_oQueue = null;
        String m_StrQueueLock = "m_StrQueueLock";
        public Queue<byte[]> Queue
        {
            get { lock (this.m_StrQueueLock) return m_oQueue; }
            set { lock (this.m_StrQueueLock) m_oQueue = value; }
        }
    }
    }
