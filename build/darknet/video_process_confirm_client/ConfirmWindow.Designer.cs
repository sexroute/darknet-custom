﻿namespace video_process_confirm_client
{
    partial class ConfirmWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroButtonConfirm = new MetroFramework.Controls.MetroButton();
            this.metroButtonCancel = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // metroButtonConfirm
            // 
            this.metroButtonConfirm.Location = new System.Drawing.Point(153, 102);
            this.metroButtonConfirm.Name = "metroButtonConfirm";
            this.metroButtonConfirm.Size = new System.Drawing.Size(75, 23);
            this.metroButtonConfirm.TabIndex = 0;
            this.metroButtonConfirm.Text = "确认";
            this.metroButtonConfirm.UseSelectable = true;
            this.metroButtonConfirm.Click += new System.EventHandler(this.metroButtonConfirm_Click);
            // 
            // metroButtonCancel
            // 
            this.metroButtonCancel.Location = new System.Drawing.Point(234, 102);
            this.metroButtonCancel.Name = "metroButtonCancel";
            this.metroButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.metroButtonCancel.TabIndex = 1;
            this.metroButtonCancel.Text = "取消";
            this.metroButtonCancel.UseSelectable = true;
            this.metroButtonCancel.Click += new System.EventHandler(this.metroButtonCancel_Click);
            // 
            // ConfirmWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(323, 148);
            this.Controls.Add(this.metroButtonCancel);
            this.Controls.Add(this.metroButtonConfirm);
            this.Name = "ConfirmWindow";
            this.Text = "是否确认推送";
            this.Load += new System.EventHandler(this.ConfirmWindow_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroButton metroButtonConfirm;
        private MetroFramework.Controls.MetroButton metroButtonCancel;
    }
}