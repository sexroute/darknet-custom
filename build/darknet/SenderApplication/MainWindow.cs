﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EricZhao;
using EricZhao.UiThread;
using MetroFramework.Forms;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace SenderApplication
{
    public partial class MainWindow :MetroForm
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        EricZhao.RabbitMQ loMq = new EricZhao.RabbitMQ();
       
        private void btnStart_Click(object sender, EventArgs e)
        {
            loMq.m_OnMessageReceived += this.OnMessageReicved;
            loMq.Read();
            this.toggleUI(true);
        }

        Dictionary<String, Packet> m_oLastPacketsRegistor = new Dictionary<string, Packet>();

        public void OnMessageReicved(object sender, BasicDeliverEventArgs e)
        {
            if (e != null)
            {
                try
                {
                    var body = e.Body;
                    String message = Encoding.UTF8.GetString(body);
                    System.Diagnostics.Debug.WriteLine(" [x] Received {0}", message);
                   
                    Packet loPacket =JsonConvert.DeserializeObject<Packet>(message);
                    message =  JsonConvert.SerializeObject(loPacket, Formatting.Indented);
                    ThreadUiController.SetControlText(this.richTextBox1, message);
                    String lstrKey = loPacket.Key;
                    Packet loLastPacket = null;
                    if (this.m_oLastPacketsRegistor.ContainsKey(lstrKey))
                    {
                        loLastPacket = this.m_oLastPacketsRegistor[lstrKey];
                        if(loLastPacket.Value>0)
                        {
                            DateTime loLastDateTime = loLastPacket.DateTime;
                            DateTime loDateTimeCurrent = loPacket.DateTime;
                            TimeSpan loSpan = loDateTimeCurrent - loLastDateTime;
                            if(loSpan.TotalSeconds<loPacket.KeepSecond)
                            {
                                loPacket.Value = loLastPacket.Value;
                            }
                            else
                            {
                                this.m_oLastPacketsRegistor[lstrKey] = loPacket;
                            }
                        }else
                        {
                            this.m_oLastPacketsRegistor[lstrKey] = loPacket;
                        }
                    }
                    else
                    {
                        this.m_oLastPacketsRegistor.Add(lstrKey, loPacket);
                    }
                    this.loMq.doSendToMiddleware(loPacket);
                    


                }
                catch (Exception ex)
                {

                }

            }
        }

        private void toggleUI(Boolean abStart)
        {
            this.btnStart.Enabled = !abStart;
            this.btnStop.Enabled = abStart;
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            this.btnStart_Click(null, null);
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            loMq.OnStop();
            this.toggleUI(false);
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (System.Windows.Forms.Application.MessageLoop)
            {
                // WinForms app
                System.Windows.Forms.Application.Exit();
                System.Environment.Exit(1);
            }
            else
            {
                // Console app
                System.Environment.Exit(1);
            }
        }
    }
}
