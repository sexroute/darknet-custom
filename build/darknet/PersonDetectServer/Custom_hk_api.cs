﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
namespace PersonDetectServer
{
    class Custom_hk_api
    {
        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate void framecallback(int width,
    int height,
    int stride, ref byte buffer, int length);

        [DllImport(@"PureCppCallback.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern bool live(int UserID, IntPtr handle, int userData, ref int  appRealHandle, ref int errcode);

        [DllImport(@"PureCppCallback.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int login(string strUserName, string strPassword, string strIP, int port);

        [DllImport(@"PureCppCallback.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern bool live_v2(int UserID, IntPtr handle, int userData, ref int appRealHandle, ref int errcode, framecallback callback);

        [DllImport(@"PureCppCallback.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern int login_v2(string strUserName, string strPassword, string strIP, int port,int rtsp_port);

        [DllImport(@"PureCppCallback.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern bool init(string cfg_filename, string weight_filename, int gpu_id);

        [DllImport(@"PureCppCallback.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern bool detect_person(string cfg_filename, float threshold=0.2f);

        [DllImport(@"PureCppCallback.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern bool detect_person_v2(byte[] bytes, int anImgLength, int w, int h, int c = 3, float threhold = 0.2f);
    }
}
