﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Threading;
using System.Diagnostics;
using detectorbridge;
using System.Web;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PersonDetectServer
{
    class SimpleHTTPServer
    {
        private readonly string[] _indexFiles = {
        "index.html",
        "index.htm",
        "default.html",
        "default.htm"
    };

        private static IDictionary<string, string> _mimeTypeMappings = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase) {
        #region extension to MIME type list
        {".asf", "video/x-ms-asf"},
        {".asx", "video/x-ms-asf"},
        {".avi", "video/x-msvideo"},
        {".bin", "application/octet-stream"},
        {".cco", "application/x-cocoa"},
        {".crt", "application/x-x509-ca-cert"},
        {".css", "text/css"},
        {".deb", "application/octet-stream"},
        {".der", "application/x-x509-ca-cert"},
        {".dll", "application/octet-stream"},
        {".dmg", "application/octet-stream"},
        {".ear", "application/java-archive"},
        {".eot", "application/octet-stream"},
        {".exe", "application/octet-stream"},
        {".flv", "video/x-flv"},
        {".gif", "image/gif"},
        {".hqx", "application/mac-binhex40"},
        {".htc", "text/x-component"},
        {".htm", "text/html"},
        {".html", "text/html"},
        {".ico", "image/x-icon"},
        {".img", "application/octet-stream"},
        {".iso", "application/octet-stream"},
        {".jar", "application/java-archive"},
        {".jardiff", "application/x-java-archive-diff"},
        {".jng", "image/x-jng"},
        {".jnlp", "application/x-java-jnlp-file"},
        {".jpeg", "image/jpeg"},
        {".jpg", "image/jpeg"},
        {".js", "application/x-javascript"},
        {".mml", "text/mathml"},
        {".mng", "video/x-mng"},
        {".mov", "video/quicktime"},
        {".mp3", "audio/mpeg"},
        {".mpeg", "video/mpeg"},
        {".mpg", "video/mpeg"},
        {".msi", "application/octet-stream"},
        {".msm", "application/octet-stream"},
        {".msp", "application/octet-stream"},
        {".pdb", "application/x-pilot"},
        {".pdf", "application/pdf"},
        {".pem", "application/x-x509-ca-cert"},
        {".pl", "application/x-perl"},
        {".pm", "application/x-perl"},
        {".png", "image/png"},
        {".prc", "application/x-pilot"},
        {".ra", "audio/x-realaudio"},
        {".rar", "application/x-rar-compressed"},
        {".rpm", "application/x-redhat-package-manager"},
        {".rss", "text/xml"},
        {".run", "application/x-makeself"},
        {".sea", "application/x-sea"},
        {".shtml", "text/html"},
        {".sit", "application/x-stuffit"},
        {".swf", "application/x-shockwave-flash"},
        {".tcl", "application/x-tcl"},
        {".tk", "application/x-tcl"},
        {".txt", "text/plain"},
        {".war", "application/java-archive"},
        {".wbmp", "image/vnd.wap.wbmp"},
        {".wmv", "video/x-ms-wmv"},
        {".xml", "text/xml"},
        {".xpi", "application/x-xpinstall"},
        {".zip", "application/zip"},
        #endregion
    };
        private Thread _serverThread;
        private string _rootDirectory;
        private HttpListener _listener;
        private int _port;

        public int Port
        {
            get { return _port; }
            private set { }
        }

        /// <summary>
        /// Construct server with given port.
        /// </summary>
        /// <param name="path">Directory path to serve.</param>
        /// <param name="port">Port of the server.</param>
        public SimpleHTTPServer(string path, int port)
        {
            this.Initialize(path, port);
        }


        /// <summary>
        /// Construct server with given port.
        /// </summary>
        
        /// <param name="port">Port of the server.</param>
        public SimpleHTTPServer( int port)
        {
            this.Initialize("./", port);
        }

        /// <summary>
        /// Construct server with suitable port.
        /// </summary>
        /// <param name="path">Directory path to serve.</param>
        public SimpleHTTPServer(string path)
        {
            //get an empty port
            TcpListener l = new TcpListener(IPAddress.Loopback, 0);
            l.Start();
            int port = ((IPEndPoint)l.LocalEndpoint).Port;
            l.Stop();
            this.Initialize(path, port);
        }

        /// <summary>
        /// Stop server and dispose all functions.
        /// </summary>
        public void Stop()
        {
            _serverThread.Abort();
            _listener.Stop();
        }

        private void Listen()
        {
            string lstrCurrentDirectory = Assembly.GetEntryAssembly().Location;
            lstrCurrentDirectory = System.IO.Path.GetDirectoryName(lstrCurrentDirectory);
            Directory.SetCurrentDirectory(lstrCurrentDirectory);
            _listener = new HttpListener();

            _listener.Prefixes.Add("http://*:" + _port.ToString() + "/");
            _listener.Start();
            PersonDetectServer.Custom_hk_api.init("", "", 0);
            while (true)
            {
                try
                {
                    HttpListenerContext context = _listener.GetContext();
                    Process_post(context);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private int getIntValue(String astrData)
        {
            int lnRet = 0;
            try
            {

                lnRet = Int32.Parse(astrData);

            }catch(Exception e)
            {

            }

            return lnRet;
        }

        private void Process_post_need_rect(HttpListenerContext context)
        {
            string filename = context.Request.Url.AbsolutePath;
            string param1 = HttpUtility.ParseQueryString(context.Request.Url.Query).Get("pic0");
            String lstr_w = HttpUtility.ParseQueryString(context.Request.Url.Query).Get("w");
            String lstr_h = HttpUtility.ParseQueryString(context.Request.Url.Query).Get("h");
            String lstr_c = HttpUtility.ParseQueryString(context.Request.Url.Query).Get("c");
            String lstr_format = HttpUtility.ParseQueryString(context.Request.Url.Query).Get("format");
            String lstr_rect= HttpUtility.ParseQueryString(context.Request.Url.Query).Get("rect");

            if(!String.IsNullOrEmpty(lstr_rect))
            {
                this.Process_post_need_rect(context);
                return;
            }
            int lnLength = (int)context.Request.ContentLength64;

            if (!String.IsNullOrEmpty(param1))
            {
                filename = param1;
            }

            if (!File.Exists(filename))
            {
                filename = filename.Substring(1);

                if (string.IsNullOrEmpty(filename))
                {
                    foreach (string indexFile in _indexFiles)
                    {
                        if (File.Exists(Path.Combine(_rootDirectory, indexFile)))
                        {
                            filename = indexFile;
                            break;
                        }
                    }
                }

                filename = Path.Combine(_rootDirectory, filename);
            }

            if (lnLength > 0)
            {
                using (var output = context.Request.InputStream)
                {
                    byte[] lpBuffer = new byte[lnLength];
                    output.Read(lpBuffer, 0, lnLength);
                    StringBuilder loSb = new StringBuilder();
                    String lstrRet = "[]";
                    JArray array = new JArray();
                    unsafe
                    {
                        bbox_t_cli[] loRet = null;
                        fixed (byte* packet = lpBuffer)
                        {
                            loRet = DetectorbridgeV2Wrapper.detect(packet, lpBuffer.Length, getIntValue(lstr_c), 0.2f, false);
                        }

                        if (null != loRet)
                        {
                            for(int i=0;i<loRet.Length;i++)
                            {
                                dynamic jsonObject = new JObject();
                                jsonObject.w = loRet[i].w;
                                jsonObject.h = loRet[i].h;
                                jsonObject.x = loRet[i].x;
                                jsonObject.y = loRet[i].y;

                                jsonObject.track_id = loRet[i].track_id;
                                jsonObject.obj_id = loRet[i].obj_id;
                                jsonObject.prob = loRet[i].prob;
                                array.Add(jsonObject);
                            }
                        }

                        lstrRet = array.ToString();
                    }

                    byte[] loRetx = System.Text.Encoding.UTF8.GetBytes(lstrRet);
                    context.Response.OutputStream.Write(loRetx, 0, loRetx.Length);
                    Console.WriteLine("0:" + filename);
                }

            }
            else
            {
                this.Process(context);
            }

            context.Response.OutputStream.Close();
        }

        private void Process_post(HttpListenerContext context)
        {
            string filename = context.Request.Url.AbsolutePath;
            string param1 = HttpUtility.ParseQueryString(context.Request.Url.Query).Get("pic0");
            String lstr_w = HttpUtility.ParseQueryString(context.Request.Url.Query).Get("w");
            String lstr_h = HttpUtility.ParseQueryString(context.Request.Url.Query).Get("h");
            String lstr_c = HttpUtility.ParseQueryString(context.Request.Url.Query).Get("c");
            String lstr_format = HttpUtility.ParseQueryString(context.Request.Url.Query).Get("format");

            int lnLength = (int)context.Request.ContentLength64;

            if (!String.IsNullOrEmpty(param1))
            {
                filename = param1;
            }

            if (!File.Exists(filename))
            {
                filename = filename.Substring(1);

                if (string.IsNullOrEmpty(filename))
                {
                    foreach (string indexFile in _indexFiles)
                    {
                        if (File.Exists(Path.Combine(_rootDirectory, indexFile)))
                        {
                            filename = indexFile;
                            break;
                        }
                    }
                }

                filename = Path.Combine(_rootDirectory, filename);
            }

            if ( lnLength>0)
            {
                using (var output = context.Request.InputStream)
                {
                    byte[] lpBuffer = new byte[lnLength];
                    byte[] lpBufferToDetect = new byte[lnLength];
                    using (MemoryStream loMemStream = new MemoryStream())
                    {
                       int lnReaded = output.Read(lpBuffer, 0, lnLength);
                        while(lnReaded>0)
                        {                           
                            loMemStream.Write(lpBuffer, 0, lnReaded);
                            loMemStream.Flush();
                            lnReaded = output.Read(lpBuffer, 0, lnLength);
                        }

                        lpBufferToDetect = loMemStream.ToArray();
                    }



                    bool lbPersonIn =   PersonDetectServer.Custom_hk_api.detect_person_v2(lpBufferToDetect, 
                        lpBuffer.Length, 
                        getIntValue(lstr_w),
                        getIntValue(lstr_h), 
                        getIntValue(lstr_c));


                    if (lbPersonIn)
                    {
                        byte[] loRetx = System.Text.Encoding.UTF8.GetBytes("1");
                        context.Response.OutputStream.Write(loRetx, 0, loRetx.Length);
                        Console.WriteLine("1:" + filename);

                    }
                    else
                    {
                        byte[] loRetx = System.Text.Encoding.UTF8.GetBytes("0");
                        context.Response.OutputStream.Write(loRetx, 0, loRetx.Length);
                        Console.WriteLine("0:" + filename);

                    }
                }
                
            }
            else
            {
                this.Process(context);
            }

            context.Response.OutputStream.Close();
        }

        private void Process(HttpListenerContext context)
        {
            string filename = context.Request.Url.AbsolutePath;
            string param1 = HttpUtility.ParseQueryString(context.Request.Url.Query).Get("pic0");
            if(!String.IsNullOrEmpty(param1))
            {
                filename = param1;
            }
           
            if (!File.Exists(filename))
            {
                filename = filename.Substring(1);

                if (string.IsNullOrEmpty(filename))
                {
                    foreach (string indexFile in _indexFiles)
                    {
                        if (File.Exists(Path.Combine(_rootDirectory, indexFile)))
                        {
                            filename = indexFile;
                            break;
                        }
                    }
                }

                filename = Path.Combine(_rootDirectory, filename);
            }

               

            if (File.Exists(filename))
            {

                bool lbPersonIn = PersonDetectServer.Custom_hk_api.detect_person(filename, 0.2f);

                context.Response.AddHeader("Date", DateTime.Now.ToString("r"));
                context.Response.AddHeader("Last-Modified", System.IO.File.GetLastWriteTime(filename).ToString("r"));
                if(lbPersonIn)
                {
                    byte[] loRetx = System.Text.Encoding.UTF8.GetBytes("1");
                    context.Response.OutputStream.Write(loRetx,0, loRetx.Length);
                    Console.WriteLine("1:" + filename);

                }
                else
                {
                    byte[] loRetx = System.Text.Encoding.UTF8.GetBytes("0");
                    context.Response.OutputStream.Write(loRetx, 0, loRetx.Length);
                    Console.WriteLine("0:" + filename);

                }

                context.Response.OutputStream.Flush();

                /*try
                {
                    Stream input = new FileStream(filename, FileMode.Open);

                    //Adding permanent http response headers
                    string mime;
                    context.Response.ContentType = _mimeTypeMappings.TryGetValue(Path.GetExtension(filename), out mime) ? mime : "application/octet-stream";
                    context.Response.ContentLength64 = input.Length;
                    context.Response.AddHeader("Date", DateTime.Now.ToString("r"));
                    context.Response.AddHeader("Last-Modified", System.IO.File.GetLastWriteTime(filename).ToString("r"));

                    byte[] buffer = new byte[1024 * 16];
                    int nbytes;
                    while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                        context.Response.OutputStream.Write(buffer, 0, nbytes);
                    input.Close();

                    context.Response.StatusCode = (int)HttpStatusCode.OK;
                    context.Response.OutputStream.Flush();
                }
                catch (Exception ex)
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                }*/

            }
            else
            {
                byte[] loRetx = System.Text.Encoding.UTF8.GetBytes("0");
                context.Response.OutputStream.Write(loRetx, 0, loRetx.Length);
                context.Response.OutputStream.Flush();
                Console.WriteLine("0:"+filename);
            }

            context.Response.OutputStream.Close();
        }

        private void Initialize(string path, int port)
        {
            this._rootDirectory = path;
            this._port = port;
            _serverThread = new Thread(this.Listen);
            _serverThread.Start();
        }


    }
}
