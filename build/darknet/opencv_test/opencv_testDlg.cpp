
// opencv_testDlg.cpp : implementation file
//

#include "stdafx.h"
#include "opencv_test.h"
#include "opencv_testDlg.h"
#include "afxdialogex.h"
#include <opencv2\opencv.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Copencv_testDlg dialog



Copencv_testDlg::Copencv_testDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_OPENCV_TEST_DIALOG, pParent)
	, m_nStep(0)
	, m_dblRatio(0)
	, m_nKernelSize(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Copencv_testDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	this->m_nStep = 1;
	this->m_dblRatio = 1.0;
	this->m_nKernelSize = 1.0;
	DDX_Text(pDX, IDC_EDIT_PIC_NUMBER, m_nStep);
	DDV_MinMaxInt(pDX, m_nStep, 1, 9999);
	DDX_Text(pDX, IDC_EDIT_PIC_ZOM_RATIO, m_dblRatio);
	DDV_MinMaxDouble(pDX, m_dblRatio, 1.0, 99.0);
	DDX_Text(pDX, IDC_EDIT_PIC_ZOM_KERNEL_SIZE, m_nKernelSize);
	DDV_MinMaxInt(pDX, m_nKernelSize, 0, 99);
}

BEGIN_MESSAGE_MAP(Copencv_testDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &Copencv_testDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// Copencv_testDlg message handlers

BOOL Copencv_testDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	this->m_nStep = 1;
	this->UpdateData();
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void Copencv_testDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void Copencv_testDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR Copencv_testDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

using namespace cv;

void Copencv_testDlg::OnBnClickedOk()
{
	this->UpdateData(TRUE);
	
	cv::Mat image_resized = cv::Mat();
	cv::Mat image_large = cv::Mat();
	cv::Mat current_image_large = cv::Mat();
	cv::Mat previous_image_large = cv::Mat();
	cv::Mat previous_image_large_noise = cv::Mat();

	cv::Mat image_large_noise = cv::Mat();

	cv::Mat current_image_large_send = cv::Mat();
	cv::Mat image_large_thumb = cv::Mat();
	cv::Mat previous_gray = cv::Mat();
	cv::Mat current_gray = cv::Mat();
	cv::Mat current_gray_gauss = cv::Mat();
	cv::Mat diff_gray = cv::Mat();
	cv::Mat diff_gray_filter = cv::Mat();
	cv::Mat threshold_gray = cv::Mat();
	cv::Mat threshold_gray_dilate = cv::Mat();
	cv::Mat img_bin = cv::Mat();
	cv::Mat out = cv::Mat();

	Scalar loColorGreen = Scalar(0, 255, 0);
	Scalar loColorRed = Scalar(0, 0, 255);
	Scalar loColoryellow = Scalar(0, 255, 255);
	double ldblRatio = this->m_dblRatio;
	double ldblRatioSend = 1;
	int lnNewWidth = 0;
	int lnNewHeigth = 0;
	int lnStep = this->m_nStep;
	int lnNewWidthSend = 0;
	int lnNewHeigthSend = 0;
	int lnKernelSize = this->m_nKernelSize;

	CString lstrFileName;
	lstrFileName.Format("./test/%d_0current.bmp", lnStep);

	image_large_noise = cv::imread(lstrFileName.GetBuffer(0));
	cv::boxFilter(image_large_noise, image_large, -1, cv::Size(lnKernelSize, lnKernelSize));

	lstrFileName.Format("./test/%d_1previous.bmp", lnStep);
	previous_image_large_noise = cv::imread(lstrFileName.GetBuffer(0));

	cv::boxFilter(previous_image_large_noise, previous_image_large, -1, cv::Size(lnKernelSize, lnKernelSize));

	cv::Size size = image_large.size();
	lnNewWidth = size.width / ldblRatio;
	lnNewHeigth = size.height / ldblRatio;

	lnNewWidthSend = size.width / ldblRatioSend;
	lnNewHeigthSend = size.height / ldblRatioSend;

	cv::resize(image_large, image_resized, cv::Size(lnNewWidth, lnNewHeigth), 0, 0, 2);

	cv::cvtColor(image_resized, current_gray, COLOR_BGR2GRAY);

	if (lnKernelSize ==0)
	{
		current_gray_gauss = current_gray.clone();
	}
	else
	{
		//cv::GaussianBlur(current_gray, current_gray_gauss, cv::Size(lnKernelSize, lnKernelSize), 0);
	}

	lstrFileName.Format("./test/%d_0current.bmp", lnStep);
	//cv::imwrite(lstrFileName.GetBuffer(0), image_large);

	cv::Mat image_resized_previous;
	cv::resize(previous_image_large, image_resized_previous, cv::Size(lnNewWidth, lnNewHeigth), 0, 0, 2);

	cv::cvtColor(image_resized_previous, previous_gray, COLOR_BGR2GRAY);

	lstrFileName.Format("./test/%d_1previous.bmp", lnStep);
	//cv::imwrite(lstrFileName.GetBuffer(0), previous_image_large);

	current_image_large = image_large.clone();
	cv::Mat previous_gray_gauss = cv::Mat();

	if (lnKernelSize ==0)
	{
		previous_gray_gauss = previous_gray.clone();
	}
	else
	{
		//cv::GaussianBlur(previous_gray, previous_gray_gauss, cv::Size(lnKernelSize, lnKernelSize), 0);
	}
	
	cv::absdiff(previous_gray, current_gray, diff_gray);
	if (lnKernelSize==0)
	{
		diff_gray_filter = diff_gray.clone();
	}
	else
	{
		cv::boxFilter(diff_gray, diff_gray_filter, -1, cv::Size(lnKernelSize, lnKernelSize));
	}
	
	cv::threshold(diff_gray_filter, threshold_gray,1, 255, THRESH_TRIANGLE);
	cv::dilate(threshold_gray, threshold_gray_dilate, NULL);



	lstrFileName.Format("./test/%d_2gray_new_current.bmp", lnStep);
	cv::imwrite(lstrFileName.GetBuffer(0), current_gray);

	lstrFileName.Format("./test/%d_3gray_new_previous.bmp", lnStep);
	cv::imwrite(lstrFileName.GetBuffer(0), previous_gray);

	lstrFileName.Format("./test/%d_4diff_new.bmp", lnStep);
	cv::imwrite(lstrFileName.GetBuffer(0), diff_gray);

	lstrFileName.Format("./test/%d_5diff_new_filter.bmp", lnStep);
	cv::imwrite(lstrFileName.GetBuffer(0), diff_gray_filter);

	lstrFileName.Format("./test/%d_6diff_new_threshold_gray.bmp", lnStep);
	cv::imwrite(lstrFileName.GetBuffer(0), threshold_gray);

	lstrFileName.Format("./test/%d_7diff_new_threshold_gray_dilate.bmp", lnStep);
	cv::imwrite(lstrFileName.GetBuffer(0), threshold_gray_dilate);

	cv::namedWindow("current");
	cv::imshow("current", image_large_noise);

	cv::namedWindow("previous");
	cv::imshow("previous", previous_image_large_noise);

	cv::namedWindow("diff");
	cv::imshow("diff", threshold_gray_dilate);


}
