﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataReader
{
    public enum ReadDataRetType
    {
        ERROR_CODE_NO_ERROR = 0,
        ERROR_CODE_NO_MORE_DATA = 1,
        ERROR_CODE_DAT_FILE_NOT_FOUND = 2,
        ERROR_CODE_REC_FILE_NOT_FOUND = 3,
        ERROR_CODE_UNKOWN_DAT_ERROR = 4,
        ERROR_CODE_UNKOWN_DATA_ERROR = 5,
        ERROR_CODE_CHANNEL_NOT_EXIST = 6,
    }

    public interface IReaderService
    {
        String Foo(String value);
        IntPtr ShowSettingsWindow();
        void StopReadBuffer();
        bool StartReadBuffer(String astrFileName, bool abForceStopRunning = true);
        Boolean IsChannEnable(int anChannIndex);
        void EnableChann(String astrFileName, int anChannIndex, bool abEnable);
        string ReadRawHeader(String astrFilePath);
        string ReadHeader(String astrFilePath);

        void SetLoopWhenReadToEnd(Boolean abLoopWhenReadToEnd);
        Boolean GetLoopWhenReadToEnd();

        ReadDataRetType BufferedRead(string path,
            ref int anBlockIndex,
            int annChannIndex,
            ref int annChannDataRead,
            ref int annDataTobeRead,
            List<double> loData);

        ReadDataRetType ReadData(
           string path,
           ref int anBlockIndex,
           int annChannIndex,
           ref int annChannDataRead,
           ref int annDataTobeRead,
           List<double> loData);
    };
}

