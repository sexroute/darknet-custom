﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dse;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using static Dse.FDse_define;
using static Dse.FixDef_Daq;
using static Dse.FixDef_Field;
namespace EricZhao
{
    public class AlarmImageRabbitMQHandler : IMessageQueue, IDisposable
    {
        public AlarmImageRabbitMQHandler(int anType = 2)
        {
            init(anQueueType: anType);
        }
        ConnectionFactory m_oConnctionFactory = new ConnectionFactory();
        public ConnectionFactory ConnctionFactory
        {
            get { lock (this) return m_oConnctionFactory; }
            set { lock (this) m_oConnctionFactory = value; }
        }
        IConnection conn = null;
        public IConnection Conn
        {
            get { lock (this) return conn; }
            set { lock (this) conn = value; }
        }
        IModel channel = null;
        public IModel Channel
        {
            get { lock (this) return channel; }
            set { lock (this) channel = value; }
        }

        EventingBasicConsumer m_oConsumer = null;
        public EventingBasicConsumer Consumer
        {
            get { lock (this) return m_oConsumer; }
            set { lock (this) m_oConsumer = value; }
        }

        public void OnStart(string args)
        {
            this.init(args :args);
        }
        String m_StrAlarmQueueName = "dse_alarm_image_packet";
        public System.String AlarmQueueName
        {
            get { return m_StrAlarmQueueName; }
            set { m_StrAlarmQueueName = value; }
        }

        String m_StrNormalQueueName = "dse_normal_image_packet";
        public System.String NormalQueueName
        {
            get { return m_StrNormalQueueName; }
            set { m_StrNormalQueueName = value; }
        }
        String m_strRoutingKey = "m_strRoutingKey";
        public System.String RoutingKey
        {
            get { lock (this) return m_strRoutingKey; }
            set { lock (this) m_strRoutingKey = value; }
        }
        public void Send(AlarmImagePacket obj)
        {
            try
            {
                String lstrQueueName = this.AlarmQueueName;
                if(obj.PacketType == 1)
                {
                    lstrQueueName = this.NormalQueueName;
                }

                if (obj != null && this.init(obj.PacketType))
                {
                    byte[] messageBodyBytes = AlarmImagePacket.toArray(obj);
                    this.channel.BasicPublish("",
                      lstrQueueName, null,
                       messageBodyBytes);
                }
            }
            catch (Exception e)
            {

            }
        }

        static AlarmImageRabbitMQHandler m_oAlarmSender = new AlarmImageRabbitMQHandler(2);
        static AlarmImageRabbitMQHandler m_oNormalSender = new AlarmImageRabbitMQHandler(1);
       
        public static void SingleSend(AlarmImagePacket obj)
        {
            if(null!=obj)
            {
                if(obj.PacketType == 1)
                {
                    AlarmImageRabbitMQHandler.m_oNormalSender.Send(obj);
                }else
                {
                    AlarmImageRabbitMQHandler.m_oAlarmSender.Send(obj);
                }
            }
        
           
        }

        public void Read(int QueueType =2)
        {
            if (this.init(anQueueType: QueueType) && this.InitReader(QueueType))
            {

            }
        }   

        public event EventHandler<BasicDeliverEventArgs> m_OnMessageReceived = null;

        public void OnMessageReicved(object sender, BasicDeliverEventArgs e)
        {
            if (e != null)
            {
                try
                {
                    var body = e.Body;
                    AlarmImagePacket lpRet = AlarmImagePacket.toObject(body);
                    Debug.Assert(lpRet != null);

                }
                catch (Exception ex)
                {

                }

            }
        }

        public void closeAllResources()
        {
            try
            {
                if (this.Consumer != null)
                {
                    this.Consumer = null;
                }
            }
            catch (System.Exception ex)
            {

            }

            try
            {
                if (this.channel != null)
                {
                    this.channel.Close();
                    this.channel.Dispose();
                    this.channel = null;
                }
            }
            catch (System.Exception ex)
            {

            }

            try
            {
                if (this.Conn != null)
                {
                    this.Conn.Close();
                    this.Conn.Dispose();
                    this.Conn = null;
                }
            }
            catch (System.Exception ex)
            {

            }
        }

        public void Dispose()
        {
            this.closeAllResources();
        }

        protected Boolean InitReader(int QueueType = 2,
            Boolean abForeReconnect = false
            )
        {
            if (this.Consumer != null && !abForeReconnect)
            {
                return true;
            }
            try
            {

                this.Consumer = new EventingBasicConsumer(this.channel);

                if (this.m_OnMessageReceived == null)
                {
                    this.Consumer.Received += this.OnMessageReicved;
                }
                else
                {
                    this.Consumer.Received += this.m_OnMessageReceived;
                }

            }
            catch (Exception e)
            {
                this.closeAllResources();
                return false;
            }

            try
            {
                String lstrQueueName = this.AlarmQueueName;

                if(QueueType == 1)
                {
                    lstrQueueName = this.NormalQueueName;
                }
                
                this.channel.BasicConsume(queue: lstrQueueName,
                                autoAck: true,
                                consumer: this.Consumer);
            }
            catch (Exception e)
            {
                this.closeAllResources();
                return false;
            }

            return true;
        }

        protected Boolean init(int anQueueType = 2,
            string args = null, Boolean abForeReconnect = false
            )
        {
            if (this.channel != null && !abForeReconnect)
            {
                return true;
            }

            if (!String.IsNullOrEmpty(args))
            {
                try
                {
                    this.ConnctionFactory.Uri = new Uri(args);
                }
                catch (Exception e)
                {

                }

            }

            try
            {

                this.conn = this.ConnctionFactory.CreateConnection();

            }
            catch (Exception e)
            {
                this.closeAllResources();
                return false;
            }

            try
            {
                String lstrQueueName = this.AlarmQueueName;

                if (anQueueType == 1)
                {
                    lstrQueueName = this.NormalQueueName;
                }
                this.channel = conn.CreateModel();
                channel.QueueDeclare(queue: lstrQueueName,
                 durable: false,
                 exclusive: false,
                 autoDelete: false,
                 arguments: null);

            }
            catch (Exception e)
            {
                this.closeAllResources();
                return false;
            }



            return true;
        }

        public void OnStop()
        {
            //close MessageQueue on service stop
            this.Dispose();
            return;
        }
    }
}
