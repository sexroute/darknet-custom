﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EricZhao
{
    public class FixedSizedQueue<T>
    {
        private readonly object privateLockObject = new object();

        readonly Queue<T> queue = new Queue<T>();

        public int Size { get; private set; }

        public FixedSizedQueue(int size)
        {
            Size = size;
        }

        public Boolean dequeue(out T outObj)
        {
           // lock(this)
            {
                if (queue.Count > 0)
                {
                    outObj = queue.Dequeue();
                }else
                {
                    outObj = default(T);
                }

               // lock (privateLockObject)
                {
                    if (queue.Count > (Size / 2))
                    {

                        queue.Clear();
                    }
                }
            }

            return true;
           
        }

        public void Enqueue(T obj)
        {
           // lock (this)
            {
                queue.Enqueue(obj);
            }

          //  System.Threading.Thread.Sleep(10);
          
        }
    }
}
