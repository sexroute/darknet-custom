﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace EricZhao
{
    public class Packet
    {
        DateTime m_oDateTime = new DateTime();
        public System.DateTime DateTime
        {
            get { lock (this) return m_oDateTime; }
            set { lock (this) m_oDateTime = value; }
        }
        double m_fValue = 0;
        public double Value
        {
            get { lock (this) return m_fValue; }
            set { lock (this) m_fValue = value; }
        }

        int m_nStartIndex = 0;
        public int StartIndex
        {
            get { lock (this) return m_nStartIndex; }
            set { lock (this) m_nStartIndex = value; }
        }
        String m_strCompany;
        public System.String Company
        {
            get { lock (this) return m_strCompany; }
            set { lock (this) m_strCompany = value; }
        }
        String m_strFactory;
        public System.String Factory
        {
            get { lock (this) return m_strFactory; }
            set { lock (this) m_strFactory = value; }
        }
        String m_strPlant;
        public System.String Plant
        {
            get { lock (this) return m_strPlant; }
            set { lock (this) m_strPlant = value; }
        }

        private String m_strMiddleware = "127.0.0.1";
        public System.String Middleware
        {
            get { lock (this) return m_strMiddleware; }
            set { lock (this) m_strMiddleware = value; }
        }
        private int m_nPort = 17001;
        public int Port
        {
            get { lock (this) return m_nPort; }
            set { lock (this) m_nPort = value; }
        }
        String m_strKey = "";
        public System.String Key
        {
            get { return this.m_strCompany + ":" + this.m_strFactory + ":" + this.m_strPlant + ":"+this.StartIndex; }

        }

        int m_nKeepSecond = 1;
        public int KeepSecond
        {
            get { return m_nKeepSecond; }
            set { m_nKeepSecond = value; }
        }

        int m_nType = 0;
        public int Type
        {
            get { return m_nType; }
            set { m_nType = value; }
        }

        String m_strCHANNID = "";
        public System.String CHANNID
        {
            get { return m_strCHANNID; }
            set { m_strCHANNID = value; }
        }

        String m_strSource = "";
        public System.String Source
        {
            get { return m_strSource; }
            set { m_strSource = value; }
        }
        public static string GetLocalIPAddress()
        {
            try
            {
                var host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (var ip in host.AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        return ip.ToString();
                    }
                }

            }
            catch(Exception e)
            {

            }

            return "";
        }
    }
}
