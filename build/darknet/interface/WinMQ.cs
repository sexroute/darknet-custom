﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace SenderApplication
{
    public class WinMQ : IMessageQueue
    {
        public MessageQueue getWindowsMesageQueue()
        {
            if (m_oWindowsMsgQueue == null)
            {
                if (!MessageQueue.Exists(QueueName))
                {
                    MessageQueue.Create(QueueName);
                }
                m_oWindowsMsgQueue = new MessageQueue(QueueName, QueueAccessMode.ReceiveAndAdmin);
                m_oWindowsMsgQueue.Formatter = new BinaryMessageFormatter();
            }

            return m_oWindowsMsgQueue;
        }

        private void mq_ReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            //queue that have received a message
            MessageQueue cmq = (MessageQueue)sender;
            try
            {
                //a message we have received (it is already removed from queue)
                System.Messaging.Message msg = cmq.EndReceive(e.AsyncResult);
                //here you can process a message

                try
                {
                    Packet lpPacket = (Packet)msg.Body;
                    System.Diagnostics.Debug.WriteLine(lpPacket.DateTime.ToLongTimeString());
                }
                catch (Exception ex)
                {

                }
            }
            catch
            {
            }
            //refresh queue just in case any changes occurred (optional)
            cmq.Refresh();
            //tell MessageQueue to receive next message when it arrives
            cmq.BeginReceive();
        }

         public  void OnStart(string args)
        {
            //let start receive a message (when arrives)
            if (WinMsgQueue != null)
            {
                WinMsgQueue.BeginReceive();
            }
            //you can do any additional logic if mq == null
        }

        public  void OnStop()
        {
            //close MessageQueue on service stop
            if (WinMsgQueue != null)
            {
                WinMsgQueue.Close();
            }
            return;
        }

        String m_strQueueName = @".\private$\MyTransactionalQueue";
        public System.String QueueName
        {
            get { lock (this) return m_strQueueName; }
            set { lock (this) m_strQueueName = value; }
        }

        MessageQueue m_oWindowsMsgQueue = null;
        public System.Messaging.MessageQueue WinMsgQueue
        {
            get
            {
                lock (this)
                {
                    this.m_oWindowsMsgQueue = this.getWindowsMesageQueue();
                    return this.m_oWindowsMsgQueue;
                }
            }
            set { lock (this) m_oWindowsMsgQueue = value; }
        }
    }
}
