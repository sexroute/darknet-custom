﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;

namespace EricZhao
{
    [ProtoContract]
    public  class PacketTypeValues
    {
        public  const int UNKNOWN = 0,
         NORMAL_DATA =1,
         ALARM_IMAGE_DATA=2,
         HEART_BEAT=3,
         CONFIG=4,
         CONFIRM_LEAK=5,
         CONFIRM_FIRE=6,
         CANCEL_ALL_ALARM=7,
         NOTIFY_STATUS = 8;
    }
    [ProtoContract]
    public class AlarmImagePacket
    {
        [ProtoMember(1)]
        DateTime m_oDateTime = new DateTime();
      
        public System.DateTime DateTime
        {
            get { lock (this) return m_oDateTime; }
            set { lock (this) m_oDateTime = value; }
        }

        [ProtoMember(2)]
        byte[] m_oOriginalImage = new byte[0];
        public byte[] OriginalImage
        {
            get { lock (this) return m_oOriginalImage; }
            set { lock (this) m_oOriginalImage = value; }
        }

        [ProtoMember(3)]
        byte[] m_oDetectImage = new byte[0];
        public byte[] DetectImage
        {
            get { lock (this) return m_oDetectImage; }
            set { lock (this) m_oDetectImage = value; }
        }

        [ProtoMember(4, IsRequired = true)]
        int m_nPacketType = PacketTypeValues.UNKNOWN;
        public int PacketType
        {
            get { lock (this) return m_nPacketType; }
            set { lock (this) m_nPacketType = value; }
        }

        [ProtoMember(5)]
        int m_nLeakMode = 0;
        public int LeakMode
        {
            get { lock (this) return m_nLeakMode; }
            set { lock (this) m_nLeakMode = value; }
        }

        [ProtoMember(6)]
        int m_nVideoChannelIndex = 0;
        public int VideoChannelIndex
        {
            get { lock (this) return m_nVideoChannelIndex; }
            set { lock (this) m_nVideoChannelIndex = value; }
        }

        [ProtoMember(7)]
        int m_nChannels = 0;
        public int Channels
        {
            get { lock (this) return m_nChannels; }
            set { lock (this) m_nChannels = value; }
        }

        [ProtoMember(8)]
        String m_strServerID = "";
        public System.String ServerID
        {
            get { lock (this) return m_strServerID; }
            set { lock (this) m_strServerID = value; }
        }

        [ProtoMember(9)]
        String m_strPacketSource = "";
        public System.String PacketSource
        {
            get { lock (this) return m_strPacketSource; }
            set { lock (this) m_strPacketSource = value; }
        }

        [ProtoMember(10)]
        int m_nStatus = 0;
        public int Status
        {
            get { lock (this) return m_nStatus; }
            set { lock (this) m_nStatus = value; }
        }
       
        public static byte[] toArray(AlarmImagePacket apPacket)
        {
            byte[] data = new byte[0];

            if(null!=apPacket)
            {
                try
                {
                    using (var ms = new MemoryStream())
                    {
                        ProtoBuf.Serializer.Serialize(ms, apPacket);
                        data = ms.ToArray();
                    }

                }
                catch(Exception e)
                {

                }

            }

            return data;
       }

       public static AlarmImagePacket toObject(byte[] apData)
        {
            AlarmImagePacket lpRet = null;
            if (null != apData && apData.Length>0)
            {
                try
                {
                    using (MemoryStream lStream = new MemoryStream(apData))
                    {
                        
                      lpRet = ProtoBuf.Serializer.Deserialize<AlarmImagePacket>(lStream);
                        
                    }
                }
                catch(Exception e)
                {

                }
            }
            return lpRet;
        }
    }
}
