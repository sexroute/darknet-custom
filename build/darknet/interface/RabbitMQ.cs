﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Framing.Impl;
using RabbitMQ.Client.Events;
using Dse;
using static Dse.FDse_define;
using static Dse.FixDef_Daq;
using static Dse.FixDef_Field;
using System.Diagnostics;
using EricZhao.UiThread;

namespace EricZhao
{
    public class RabbitMQ : IMessageQueue, IDisposable
    {
        ConnectionFactory m_oConnctionFactory = new ConnectionFactory();
        public ConnectionFactory ConnctionFactory
        {
            get { lock (this) return m_oConnctionFactory; }
            set { lock (this) m_oConnctionFactory = value; }
        }
        IConnection conn = null;
        public IConnection Conn
        {
            get { lock (this) return conn; }
            set { lock (this) conn = value; }
        }
        IModel channel = null;
        public IModel Channel
        {
            get { lock (this) return channel; }
            set { lock (this) channel = value; }
        }

        EventingBasicConsumer m_oConsumer = null;
        public EventingBasicConsumer Consumer
        {
            get { lock (this) return m_oConsumer; }
            set { lock (this) m_oConsumer = value; }
        }

        public void OnStart(string args)
        {           
             this.init(args);          
        }
        String m_StrQueueName = "dse_packet";
        public System.String QueueName
        {
            get { return m_StrQueueName; }
            set { m_StrQueueName = value; }
        }
        String m_strRoutingKey = "m_strRoutingKey";
        public System.String RoutingKey
        {
            get { lock (this) return m_strRoutingKey; }
            set { lock (this) m_strRoutingKey = value; }
        }
        public void Send(Packet obj)
        {
            try
            {
                if (obj != null && this.init())
                {
                    String str = JsonConvert.SerializeObject(obj);
                    byte[] messageBodyBytes = System.Text.Encoding.UTF8.GetBytes(str);
                    IBasicProperties props = this.channel.CreateBasicProperties();
                    props.ContentType = "text/plain";
                    props.DeliveryMode = 2;
                    this.channel.BasicPublish("",
                       this.QueueName, null,
                       messageBodyBytes);
                }
            }
            catch(Exception e)
            {

            }

        }

        public Boolean Read()
        {
            if(this.init() && this.InitReader())
            {
                return true;
            }

            return false;
        }

        public bool doConfirm(Packet apPacket)
        {
            try
            {
                if (null == apPacket)
                {
                    return false;
                }

                using (Dse.FixComm lpFixComm = Dse.FixComm.New(apPacket.Middleware, "" + apPacket.Port))
                {
                    bool lbRet = false;

                    lbRet = lpFixComm.CreateHead(SERVICE_SERVICE_ON_ALARM_CONFIRM);
                    if (lbRet)
                    {

                        lpFixComm.Set(FIELD_SYSTEM_COMPANY, apPacket.Company);
                        lpFixComm.Set(FIELD_SYSTEM_FACTORY, apPacket.Factory);
                        lpFixComm.Set(FIELD_SERVICE_PLANTID, apPacket.Plant);
                        lpFixComm.Set(FIELD_SYSTEM_CHANN, apPacket.CHANNID);
                        lpFixComm.Set(FIELD_SERVICE_ON_SEND_STARTID, apPacket.StartIndex);
                        lpFixComm.Set(FIELD_SERVICE_OFF_TIMESTART, DateTime.Now);
                        lpFixComm.Set(FIELD_SERVICE_OFF_TIMEEND, DateTime.Now+new TimeSpan(0,1,0));
                        lpFixComm.Set(FIELD_SYSTEM_USEID, "admin");
                        lpFixComm.Set(FIELD_SYSTEM_CLIENT_IP, apPacket.Source);
                        lpFixComm.Set(FIELD_SERVICE_ALARM_CONFIRM_TYPE, apPacket.Value);
                        lpFixComm.Set(FIELD_SERVICE_TIME, apPacket.DateTime);

                        if (lpFixComm.More())
                        {
                            return true;
                        }
                    }
                }


            }
            catch (Exception e)
            {

            }

            return false;
        }
        public bool doSendData(Packet apPacket)
        {
            try
            {
                if (null == apPacket)
                {
                    return false;
                }

                using (Dse.FixComm lpFixComm = Dse.FixComm.New(apPacket.Middleware, "" + apPacket.Port))
                {
                    bool lbRet = false;

                    lbRet = lpFixComm.CreateHead(SERVICE_SERVICE_ON_RECEIVEPROC);
                    if (lbRet)
                    {

                        lpFixComm.Set(FIELD_SYSTEM_COMPANY, apPacket.Company);
                        lpFixComm.Set(FIELD_SYSTEM_FACTORY, apPacket.Factory);
                        lpFixComm.Set(FIELD_SERVICE_PLANTID, apPacket.Plant);
                        lpFixComm.Set(FIELD_SERVICE_ON_SEND_STARTID, apPacket.StartIndex);
                        lpFixComm.Set(FIELD_SERVICE_CHANNTYPE, CHANNTYPE.GE_ALLPROC);
                        lpFixComm.Set(FIELD_SERVICE_ON_SEND_PROCCHANNNUM, 1);
                        using (ValueCache loBuffer = new ValueCache())
                        {

                            loBuffer.Ensure(sizeof(float) * 1);
                            loBuffer.Clear();
                            loBuffer.Add((float)apPacket.Value);
                            var buf = loBuffer.ToArray();
                            Debug.Assert(buf.Length == (sizeof(float) * 1));
                            lpFixComm.Set(FIELD_SERVICE_ON_SEND_ALLPROCVALUE, buf);
                        }

                        lpFixComm.Set(FIELD_SERVICE_TIME, apPacket.DateTime);

                        if (lpFixComm.More())
                        {
                            return true;
                        }
                    }
                }


            }
            catch (Exception e)
            {

            }

            return false;
        }

        public bool doSendToMiddleware(Packet apPacket)
        {            
            if(apPacket!=null)
            {
                if(apPacket.Type>0)
                {
                    return this.doConfirm(apPacket);
                }else
                {
                    return this.doSendData(apPacket);
                }
            }

            return false;
        }

        public event EventHandler<BasicDeliverEventArgs> m_OnMessageReceived = null;

        public  void OnMessageReicved(object sender, BasicDeliverEventArgs e)
        {
            if(e!=null)
            {
                try
                {
                    var body = e.Body;
                    String message = Encoding.UTF8.GetString(body);
                    System.Diagnostics.Debug.WriteLine(" [x] Received {0}", message);
                   
                    Packet loPacket = (Packet)JsonConvert.DeserializeObject(message);
                    this.doSendToMiddleware(loPacket);

                }
                catch(Exception ex)
                {

                }
                
            }
        }

        public void closeAllResources()
        {
            try
            {
                if (this.Consumer != null)
                {                    
                    this.Consumer = null;
                }
            }
            catch (System.Exception ex)
            {

            }

            try
            {
                if (this.channel != null)
                {
                    this.channel.Close();
                    this.channel.Dispose();
                    this.channel = null;
                }
            }
            catch (System.Exception ex)
            {

            }

            try
            {
                if (this.Conn != null)
                {
                    this.Conn.Close();
                    this.Conn.Dispose();
                    this.Conn = null;
                }
            }
            catch (System.Exception ex)
            {

            }
        }

        public void Dispose()
        {
            this.closeAllResources();
        }

        protected Boolean InitReader(Boolean abForeReconnect = false)
        {
            if (this.Consumer != null && !abForeReconnect)
            {
                return true;
            }
            try
            {

                this.Consumer = new EventingBasicConsumer(this.channel);

                if(this.m_OnMessageReceived == null)
                {
                    this.Consumer.Received += this.OnMessageReicved;
                }
                else
                {
                    this.Consumer.Received += this.m_OnMessageReceived;
                }

            }
            catch (Exception e)
            {
                this.closeAllResources();
                return false;
            }

            try
            {
                this.channel.BasicConsume(queue: this.QueueName,
                                autoAck: true,
                                consumer: this.Consumer);
            }
            catch (Exception e)
            {
                this.closeAllResources();
                return false;
            }

            return true;
        }

        protected Boolean init(string args=null, Boolean abForeReconnect = false)
        {
            if (this.channel != null && !abForeReconnect)
            {
                return true;
            }

            if (!String.IsNullOrEmpty(args))
            {
                try
                {
                    this.ConnctionFactory.Uri = new Uri(args);
                }
                catch (Exception e)
                {

                }          

            }

            try
            {

                this.conn = this.ConnctionFactory.CreateConnection();

            }
            catch (Exception e)
            {
                this.closeAllResources();
                return false;
            }

            try
            {

                this.channel = conn.CreateModel();
                channel.QueueDeclare(queue: this.QueueName,
                 durable: false,
                 exclusive: false,
                 autoDelete: false,
                 arguments: null);

            }
            catch (Exception e)
            {
                this.closeAllResources();
                return false;
            }

          

            return true;
        }

        public void OnStop()
        {
            //close MessageQueue on service stop
            this.Dispose();
            return;
        }


    }
}
