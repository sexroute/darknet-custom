﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EricZhao;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace EricZhao
{
    public class AlarmImageRabbitExchangHandler: IDisposable
    {
        public AlarmImageRabbitExchangHandler(int anType = PacketTypeValues.ALARM_IMAGE_DATA)
        {
            this.ExChangeType = anType;            
        }

        public class Status
        {
            public const int CONNECTING = 0;
            public const int CONNECTED = 1;
            public const int DISCONNECTING = 2;
            public const int CONNECT_FAILED = 3;
            public const int DISCONNECTED = 4;

            public static String getStatus(int anStatus)
            {
                switch (anStatus)
                {
                    case Status.CONNECTED:
                        return "已连接";
                    case Status.DISCONNECTING:
                        return "断开连接中";
                    case Status.CONNECT_FAILED:
                        return "连接失败";
                    case Status.CONNECTING:
                        return "连接中";
                    case Status.DISCONNECTED:
                        return "未连接";
                    default:
                        return "未连接";
                }
            }
        }
        String m_strExchangeName_Prefix = "";
        public System.String ExchangeName_Prefix
        {
            get { lock (this) return m_strExchangeName_Prefix; }
            set { lock (this) m_strExchangeName_Prefix = value; }
        }

        String m_strExchangeNameNormal = "dse_image_normal_exchange";
        public System.String ExchangeNameNormal
        {
            get { lock (this) return m_strExchangeNameNormal; }
            set { lock (this) m_strExchangeNameNormal = value; }
        }

        String m_strExchangeNameAlarm = "dse_image_alarm_exchange";
        public System.String ExchangeNameAlarm
        {
            get { lock (this) return m_strExchangeNameAlarm; }
            set { lock (this) m_strExchangeNameAlarm = value; }
        }

        String m_strExchangeName = "dse_image_alarm_exchange";
        public System.String ExchangeName
        {
            get { lock (this) return m_strExchangeName; }
            set { lock (this) m_strExchangeName = value; }
        }




        int m_nExChangeType = 2;
        public int ExChangeType
        {
            get { lock (this) return m_nExChangeType; }
            set { lock (this) m_nExChangeType = value; }
        }
        ConnectionFactory m_oConnctionFactory = new ConnectionFactory();
        public ConnectionFactory ConnctionFactory
        {
            get { lock (this) return m_oConnctionFactory; }
            set { lock (this) m_oConnctionFactory = value; }
        }
        IConnection conn = null;
        public IConnection Conn
        {
            get { lock (this) return conn; }
            set { lock (this) conn = value; }
        }
        IModel channel = null;
        public IModel Channel
        {
            get { lock (this) return channel; }
            set { lock (this) channel = value; }
        }

        EventingBasicConsumer m_oConsumer = null;
        public EventingBasicConsumer Consumer
        {
            get { lock (this) return m_oConsumer; }
            set { lock (this) m_oConsumer = value; }
        }

        public void OnStart(string args)
        {
            this.URL = args;
            this.init();
        }
        String m_StrAlarmQueueName = "dse_alarm_image_packet";
        public System.String AlarmQueueName
        {
            get { return m_StrAlarmQueueName; }
            set { m_StrAlarmQueueName = value; }
        }

        String m_StrNormalQueueName = "dse_normal_image_packet";
        public System.String NormalQueueName
        {
            get { return m_StrNormalQueueName; }
            set { m_StrNormalQueueName = value; }
        }
        String m_strRoutingKey = "m_strRoutingKey";
        public System.String RoutingKey
        {
            get { lock (this) return m_strRoutingKey; }
            set { lock (this) m_strRoutingKey = value; }
        }

        String m_strURL = "amqp://guest:guest@127.0.0.1:5672";
        public System.String URL
        {
            get { lock (this) return m_strURL; }
            set { lock (this) m_strURL = value; }
        }

        public void Send(AlarmImagePacket obj)
        {
            try
            {
                String lstrQueueName = this.AlarmQueueName;
                if (obj.PacketType == 1)
                {
                    lstrQueueName = this.NormalQueueName;
                }

                if (obj != null && this.init())
                {
                    byte[] messageBodyBytes = AlarmImagePacket.toArray(obj);
                    this.channel.BasicPublish(this.ExchangeName,
                      "", null,
                      messageBodyBytes);
                }
            }
            catch (Exception e)
            {

            }
        }



        static Dictionary<String, Dictionary<int, AlarmImageRabbitExchangHandler>> g_oHandlers 
            = new Dictionary<string, Dictionary<int, AlarmImageRabbitExchangHandler>>();

        public static void SingleSend(AlarmImagePacket obj,
            String astrUUID= "",
            String URL= "amqp://guest:guest@127.0.0.1:5672",
            int anExchangeType= PacketTypeValues.ALARM_IMAGE_DATA)
        {
           
            if (null != obj && astrUUID !=null)
            {
                AlarmImageRabbitExchangHandler lpHandler = null;

                lock (g_oHandlers)
                {
                    Dictionary<int, AlarmImageRabbitExchangHandler> loHandlers = null;

                    String lstrKey = astrUUID;
                    if (g_oHandlers.ContainsKey(lstrKey))
                    {
                        loHandlers = g_oHandlers[lstrKey];
                    }
                    else
                    {
                        loHandlers = new Dictionary<int, AlarmImageRabbitExchangHandler>();
                        g_oHandlers.Add(lstrKey, loHandlers);
                    }

                    if(loHandlers.ContainsKey(obj.PacketType))
                    {
                        lpHandler = loHandlers[obj.PacketType];
                    }else
                    {
                        lpHandler = new AlarmImageRabbitExchangHandler(anExchangeType);
                        loHandlers.Add(obj.PacketType, lpHandler);
                    }
                }

                lpHandler.URL = URL;
                lpHandler.Send(obj);
            }
        }

        public static void releaseSender(String astrUUID)
        {
            lock (g_oHandlers)
            {
                if(null!=astrUUID)
                {
                    Dictionary<int, AlarmImageRabbitExchangHandler> loHandlers = null;

                    if(g_oHandlers.ContainsKey(astrUUID))
                    {
                        loHandlers = g_oHandlers[astrUUID];

                        if(loHandlers!=null)
                        {
                            loHandlers.Clear();
                        }

                        g_oHandlers.Remove(astrUUID);
                    }
                }
               
            }
        }

        private int m_nStatus = Status.DISCONNECTED;
        public int ConnectStatus
        {
            get { lock(this) return m_nStatus; }
            set { lock (this) m_nStatus = value; }
        }
        public void Read()
        {
            this.ConnectStatus = Status.CONNECTING;
            if (  this.init())
            {
                if(this.InitReader())
                {
                    this.ConnectStatus = Status.CONNECTED;
                    return ;
                }
            }

            this.ConnectStatus = Status.CONNECT_FAILED;
        }

        public event EventHandler<BasicDeliverEventArgs> m_OnMessageReceived = null;

        public void OnMessageReicved(object sender, BasicDeliverEventArgs e)
        {
            if (e != null)
            {
                try
                {
                    var body = e.Body;
                    AlarmImagePacket lpRet = AlarmImagePacket.toObject(body);
                    Debug.Assert(lpRet != null);

                }
                catch (Exception ex)
                {

                }

            }
        }

        public void closeAllResources()
        {
            try
            {
                if (this.Consumer != null)
                {
                    this.Consumer = null;
                }
            }
            catch (System.Exception ex)
            {

            }

            try
            {
                if (this.channel != null)
                {
                    this.channel.Close();
                    this.channel.Dispose();
                    this.channel = null;
                }
            }
            catch (System.Exception ex)
            {

            }

            try
            {
                if (this.Conn != null)
                {
                    this.Conn.Close();
                    this.Conn.Dispose();
                    this.Conn = null;
                }
            }
            catch (System.Exception ex)
            {

            }
        }

        public void Dispose()
        {
            this.closeAllResources();
        }

        protected Boolean InitReader(
            Boolean abForeReconnect = false
            )
        {
            if (this.Consumer != null && !abForeReconnect)
            {
                return true;
            }
            var queueName = "";
            try
            {
                 queueName = this.channel.QueueDeclare().QueueName;

                this.channel.QueueBind(queue: queueName,
                             exchange: this.ExchangeName,
                             routingKey: "");

                this.Consumer = new EventingBasicConsumer(this.channel);
               

                if (this.m_OnMessageReceived == null)
                {
                    this.Consumer.Received += this.OnMessageReicved;
                }
                else
                {
                    this.Consumer.Received += this.m_OnMessageReceived;
                }

            }
            catch (Exception e)
            {
                this.closeAllResources();
                return false;
            }

            try
            {
              

                this.channel.BasicConsume(queue: queueName,
                                autoAck: true,
                                consumer: this.Consumer);
            }
            catch (Exception e)
            {
                this.closeAllResources();
                return false;
            }

            return true;
        }

        protected Boolean init( Boolean abForeReconnect = false
            )
        {
            if (this.channel != null && !abForeReconnect)
            {
                return true;
            }

            if (!String.IsNullOrEmpty(this.URL))
            {
                try
                {
                    this.ConnctionFactory.Uri = new Uri(this.URL);
                }
                catch (Exception e)
                {

                }

            }

            try
            {

                this.conn = this.ConnctionFactory.CreateConnection();

            }
            catch (Exception e)
            {
                this.closeAllResources();
                return false;
            }

            try
            {
                String lstrExchangeName  = this.ExchangeName_Prefix + "_"+ this.ExchangeNameAlarm;

                if (this.ExChangeType == PacketTypeValues.NORMAL_DATA)
                {
                    lstrExchangeName = this.ExchangeName_Prefix + "_" + this.ExchangeNameNormal;
                }

                this.ExchangeName = lstrExchangeName;
                this.channel = conn.CreateModel();
                this.channel.ExchangeDeclare(exchange:this.ExchangeName, type: "fanout");

                /*
                                channel.QueueDeclare(queue: lstrQueueName,
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);*/

            }
            catch (Exception e)
            {
                this.closeAllResources();
                return false;
            }

            return true;
        }

        public void OnStop()
        {
            //close MessageQueue on service stop
            this.Dispose();
            return;
        }
    }
}
