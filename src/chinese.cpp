#include "chinese.h"
#include "CvxText.h"
#include "interface.hpp"
#ifdef _NO_DLL_
#define YOLODLL_API 
#endif // _NO_DLL_

YOLODLL_API void PutText(IplImage*  image, const char *msg, CvPoint  point, CvScalar  color, CvScalar size)
{
	CvxText text(".\\Fonts\\msyhbd.ttc");
	float p = 1;
	text.setFont(NULL, &size, NULL, &p);
	text.putText(image, msg, point, color);
}

