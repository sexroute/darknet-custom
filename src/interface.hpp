#pragma once
#include "yolo_v2_class.hpp"

extern "C" {
	YOLODLL_API void * getInstane(std::string cfg_filename, std::string weight_filename, int gpu_id = 0);
	YOLODLL_API void PutText(IplImage*  image, const char *msg, CvPoint  point, CvScalar  color, CvScalar size);
};
