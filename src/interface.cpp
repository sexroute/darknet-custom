#include "interface.hpp"

IDetector * gPDetector = NULL;

YOLODLL_API void *   getInstane(std::string cfg_filename, std::string weight_filename, int gpu_id /*= 0*/)
{
	if (NULL == gPDetector)
	{
		gPDetector = new Detector(cfg_filename, weight_filename, gpu_id);
	}

	return gPDetector;
}